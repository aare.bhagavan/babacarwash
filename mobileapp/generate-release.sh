KEYSTORE_PATH='babacarwash.keystore'
PROJECT_ROOT=`pwd`
cd $PROJECT_ROOT && npm run android:prod:build && cd ../../
echo 'deleting old bundle files'
rm $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/app.aab
rm $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/app-release.aab
rm $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/lt-release.aab
echo 'Generating bundle'
cd $PROJECT_ROOT/platforms/android/ && ./gradlew build --refresh-dependencies && ./gradlew bundleRelease && cd ../../
echo 'Signing the build file'   
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore $KEYSTORE_PATH $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/app.aab carwash < keystorepwd.secret
zipalign -v 4 $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/app.aab $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/lt-release.aab
cp $PROJECT_ROOT/platforms/android/app/build/outputs/bundle/release/lt-release.aab $PROJECT_ROOT/app-release.aab