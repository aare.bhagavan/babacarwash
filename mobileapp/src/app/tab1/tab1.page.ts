import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
  AbstractControl
} from "@angular/forms";
import { element } from 'protractor';
import { ModalController } from '@ionic/angular';
import { CleaningStatusComponent } from './cleaning_status.dialog';
import { EvAlertController } from '../alert.controller';
import Swal from 'sweetalert2';
import { MastersService } from '../api/masters.service';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { build$ } from 'protractor/built/element';
import * as _ from "underscore";
import { Storage } from '@ionic/storage';
import * as moment from 'moment';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  myControl = new FormControl();
  authForm: FormGroup;
  jobsarray: any;
  show_btn: boolean = false;
  today: any;
  building: any;
  color: any;
  location: any;
  status: any;
  buildingname: any;
  user_id;
  allJobs: any = [];
  allBuildings: any = [];
  buildingData: any;
  searchvalue: any;
  isDialogPresent: boolean = true;
  buildingsArray: any = [];
  searchValue: string = '';
  vehiclearray: any;
  todayjobs: any;
  yesterdayjobs: any
  pendingjobs: any;
  pendingsarray: any
  completedjobs: any;
  completedarray: any;
  fridayjobs: any;
  fridayarray: any;
  thursdayjobs: any;
  thursdayarray: any;
  wensdayjobs: any;
  wensdayarray: any;
  tuesdayjobs: any;
  tuesdayarray: any;
  mondayjobs: any;
  mondayarray: any;
  vehilelength: any;
  allstatus: any;
  todaystatus: any;
  yesterdaystatus: any;
  fridaysatuts: any;
  thursdaystatus: any;
  wensdaystatus: any;
  tuesdaystatus: any;
  mondaystatus: any;
  saturdayarray:any;
  saturdayjobs:any;
  saturdaystatus:any;
  sundayarray:any;
  sundayjobs:any;
  sundaystatus:any;

  pendingstatus: any;
  completedstatus: any;
  all1status: any;
  yesterday1:any;
  day:any;
  date:any;


  constructor(public fb: FormBuilder,
    public modalController: ModalController,
    public alertController: EvAlertController,
    public mastersService: MastersService,
    private router: ActivatedRoute,
    private storage: Storage

  ) {
   


  }


  // date: Date = new Date();
  // tomorrow: Date = new Date(this.date.getTime() + (1000 * 60 * 60 * 24));
  // jobs: any = [
  //   { 'day': 'TODAY', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'location': 'JublieHills', 'building': 'Building3010,Block-C', 'parking': '123', 'status': 'Pending', 'toggle': false, 'jobstatus': "true" },
  //   { 'day': 'TODAY', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "susmitha", 'carnumber': "DU-12-99", 'location': 'Secundrabad', 'building': 'Building007,A-Block', 'parking': '0923', 'status': 'Pending', 'toggle': false, 'jobstatus': "true" },
  //   { 'day': 'YESTERDAY', 'date': '02 Aug 2020', 'car': "AURD", 'customer': "Shaik Abdul", 'carnumber': "DU-55-44", 'location': 'BanjaraHills', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Completed', 'toggle': true, 'jobstatus': "true" },
  //   // { 'day': '', 'date': '01 Aug 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-43", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Completed', 'toggle': false, 'jobstatus': "true" },
  //   // { 'day': '', 'date': '01 Aug 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-43", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '09839', 'status': 'Completed', 'toggle': false, 'jobstatus': "true" },


  jobs: any = [
    {
      'building': 'Building-1',
      'location': 'JublieHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-2',
      'location': 'BanjaraHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-3',
      'location': 'Hi-Tech City',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-4',
      'location': 'Panjagutta',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    }



  ];



  count: number = 0;
  filter(value) {
    console.log(value + "is")
    this.searchvalue = value;
    this.location = this.allJobs
    console.log(this.allJobs)
    console.log(this.allJobs[0].vehicles[0].carnumber);

    this.status = true
    console.log("inside function")

    if (value != null || value != "") {
      this.buildingname = this.allJobs.filter(device => (
        (device.building_name.toLowerCase().includes(value.toLowerCase()
        ))
      )
      )

      console.log(this.location)
      this.location = this.buildingname
    }

    else {
      this.buildingname = this.allJobs
      this.location = this.buildingname
    }

  }

  ngOnInit() {
    console.log("ngOnInit");
    this.location = this.allJobs
    console.log(this.location)
    this.todaystatus = false
    this.date=moment();
  
   
    this.building = false
    this.allstatus = true
    this.jobsarray = this.jobs
    this.count = this.jobs.length;
    this.myControl.valueChanges.subscribe(value => {
      this.filter(value)
    })
    let date = new Date();
    let date1 = moment(new Date(date)).format('DD/MM/YYYY');
    var d = new Date(date);
    var dayName = d.toString().split(' ')[0];
    console.log(dayName+"day is")
    this.day=dayName
    console.log(this.day);
    var yd = new Date();
    yd.setDate(yd.getDate() - 1);
    console.log(yd)
    var dayName1 = yd.toString().split(' ')[0];
    console.log(dayName1);
    this.yesterday1=dayName1
    console.log("yd"+this.yesterday1)

  }
  ionViewWillEnter() {
    console.log("ionViewWillEnter");
    this.getAllJobs();
      // this.Today1();
    
      
  }
 
  Today() {
 
    this.building = false
    this.today = !this.today
    this.color = "red"
  
 

  }
  all1() {
    // this.getAllJobs()
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = true,this.saturdaystatus=false,this,this.sundaystatus=false;


    this.location =     JSON.parse(localStorage.getItem("jobs"));

  }

  Building() {
    this.getAllJobs()
    this.building = !this.building
    this.today = false
    this.buildingsArray = this.allJobs;
    this.buildingsArray.filter(building => building.building_id == this.searchValue);
    console.log(this.buildingsArray)

  }
  all() {
    // this.getAllJobs()
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = true, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;


    this.location =     JSON.parse(localStorage.getItem("jobs"));
  }
  Today1() {
    console.log("today111")
    this.todaystatus = true, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;

    let date = new Date();
    localStorage.getItem('jobs')

    this.todayjobs = JSON.parse(localStorage.getItem("jobs"));
    console.log(this.todayjobs[0].vehicles)
    JSON.parse(localStorage.getItem("jobs"));

    let date1 = moment(new Date(date)).format('MM/DD/YYYY');
   
 



    for (let j in this.todayjobs) {

      this.vehiclearray = this.todayjobs[j].vehicles.filter(device => device.date == date1||(device.date <= date1&&device.status == "pending") )
      console.log(this.vehiclearray+"array is")
      this.location[j].vehicles = [];

      for (let i in this.vehiclearray) {
        this.location[j].vehicles.push(this.vehiclearray[i])



      }
      
    }
   

    return this.location;









  }
  Yesterday() {
    this.todaystatus = false, this.tuesdaystatus = false, this.tuesdaystatus = false, this.yesterdaystatus = true,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.location = this.allJobs
   
    this.yesterdayjobs = JSON.parse(localStorage.getItem("jobs"));

    var d = new Date();
    d.setDate(d.getDate() - 1);
  
    let date1 = moment(new Date(d)).format('MM/DD/YYYY');
  
    for (let j in this.yesterdayjobs) {

      this.vehiclearray = this.yesterdayjobs[j].vehicles.filter(device => device.date == date1)
      this.location[j].vehicles = [];

      for (let i in this.vehiclearray) {
        this.location[j].vehicles.push(this.vehiclearray[i])


      }
    }
    console.log(this.location)
  }
  pending() {
    this.todaystatus = false, this.tuesdaystatus = false, this.tuesdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = true, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.pendingjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.pendingjobs) {

      this.pendingsarray = this.pendingjobs[j].vehicles.filter(device => device.status == "pending" || device.status == "Assigned")
      this.location[j].vehicles = [];

      for (let i in this.pendingsarray) {
        this.location[j].vehicles.push(this.pendingsarray[i])


      }
    }
    console.log(this.location)
  }
  completed() {
    this.todaystatus = false, this.tuesdaystatus = false, this.tuesdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = true, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.completedjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.completedjobs) {

      this.completedarray = this.completedjobs[j].vehicles.filter(device => device.status == "completed")
      this.location[j].vehicles = [];

      for (let i in this.completedarray) {
        this.location[j].vehicles.push(this.completedarray[i])


        this.completedarray = this.completedjobs[j].vehicles.filter(device => device.status == "completed")
        this.location[j].vehicles = [];

        for (let i in this.completedarray) {
          this.location[j].vehicles.push(this.completedarray[i])


        }
      }
      console.log(this.location)

    }
  }
  Friday() {
    this.todaystatus = false, this.tuesdaystatus = false, this.tuesdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = true,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.fridayjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.fridayjobs) {

      this.fridayarray = this.fridayjobs[j].vehicles.filter(device => device.dayname == "Fri")
      this.location[j].vehicles = [];

      for (let i in this.fridayarray) {
        this.location[j].vehicles.push(this.fridayarray[i])



      }
    }
    console.log(this.location)


  }

  Thursday() {
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = true, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.thursdayjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.thursdayjobs) {

      this.thursdayarray = this.thursdayjobs[j].vehicles.filter(device => device.dayname == "Thu")
      this.location[j].vehicles = [];

      for (let i in this.thursdayarray) {
        this.location[j].vehicles.push(this.thursdayarray[i])



      }
    }
    console.log(this.location)

  }

  Wednesday() {
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = true, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.wensdayjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.wensdayjobs) {

      this.wensdayarray = this.wensdayjobs[j].vehicles.filter(device => device.dayname == "Wed")
      this.location[j].vehicles = [];

      for (let i in this.wensdayarray) {
        this.location[j].vehicles.push(this.wensdayarray[i])


      
      }
      console.log(this.location)

    }
  }
  Tuesday() {
    this.todaystatus = false, this.tuesdaystatus = true, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.tuesdayjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.tuesdayjobs) {

      this.tuesdayarray = this.tuesdayjobs[j].vehicles.filter(device => device.dayname == "Tue")
      this.location[j].vehicles = [];

      for (let i in this.tuesdayarray) {
        this.location[j].vehicles.push(this.tuesdayarray[i])


      }
    }
    console.log(this.location)
    console.log(this.location.vehicles)


  }

  Monday() {
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
      this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = true, this.fridaysatuts = false,
      this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=false;
    this.mondayjobs = JSON.parse(localStorage.getItem("jobs"));
    for (let j in this.mondayjobs) {

      this.mondayarray = this.mondayjobs[j].vehicles.filter(device => device.dayname == "Mon")
      this.location[j].vehicles = [];

      for (let i in this.mondayarray) {
        this.location[j].vehicles.push(this.mondayarray[i])



      }
    }
    console.log(this.location)

  }
  saturday(){
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
    this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
    this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=true,this,this.sundaystatus=false;
  this.saturdayjobs = JSON.parse(localStorage.getItem("jobs"));
  for (let j in this.saturdayjobs) {

    this.saturdayarray = this.saturdayjobs[j].vehicles.filter(device => device.dayname == "Sat")
    this.location[j].vehicles = [];

    for (let i in this.saturdayarray) {
      this.location[j].vehicles.push(this.saturdayarray[i])



    }
  }
  console.log(this.location)



  }
  sunday(){
    this.todaystatus = false, this.tuesdaystatus = false, this.thursdaystatus = false, this.yesterdaystatus = false,
    this.wensdaystatus = false, this.allstatus = false, this.mondaystatus = false, this.fridaysatuts = false,
    this.pendingstatus = false, this.completedstatus = false, this.all1status = false,this.saturdaystatus=false,this,this.sundaystatus=true;
  this.sundayjobs = JSON.parse(localStorage.getItem("jobs"));
  for (let j in this.sundayjobs) {

    this.sundayarray = this.sundayjobs[j].vehicles.filter(device => device.dayname == "Sun")
    this.location[j].vehicles = [];

    for (let i in this.sundayarray) {
      this.location[j].vehicles.push(this.sundayarray[i])



    }
  }
  console.log(this.location)

  }





  loadData(event) {
    if (event.status == "Completed") {
      this.jobs.forEach(element => {

        if (element.carnumber == event.carnumber) {
          element.status = "Pending";
        }
      });
    } else {
      this.jobs.forEach(element => {
        if (element.carnumber == event.carnumber) {
          element.status = "Completed";
        }
      });
    }
  }
  show_button() {
    console.log("show button")
    this.show_btn = true;
    console.log(this.show_btn)

  }
  async presentModal(details: any) {
    console.log("inside")
    this.isDialogPresent = false;
    let schedules = [];
    schedules[0] = details.sunday;
    schedules[1] = details.monday;
    schedules[2] = details.tuesday;
    schedules[3] = details.wednesday;
    schedules[4] = details.thursday;
    schedules[5] = details.friday;
    schedules[6] = details.saturday;
    let data = {
      'schedule_id': details.schedule_id,
      'date': details.Tdate,
      schedules: schedules
      // moment(schedule_date).format('YYYY-MM-DD')

    }
    console.log(moment(details.Tdate).format('YYYY-MM-DD'))

    const res = await this.alertController.showConfirmationDialog(data);
    console.log(res);
    if (res) {
      this.isDialogPresent = true;
      this.getAllJobs();
    }

  }
  async getAllJobs() {
    await this.storage.get('user_id').then(async (user_id) => {
      this.user_id = user_id;
      console.log(this.user_id);
      (await this.mastersService.getAllJobs(this.user_id)).subscribe((response: any) => {
        console.log("all jobs", response)
        //this.allJobs=response;
        let uniqueArray = _.map(_.groupBy(response, (obj: any) => {
          return obj.building_id;
        }), (grouped) => {
          return grouped[0];
        })

        //console.log(uniqueArray);



        uniqueArray.forEach(building => {
          building['vehicles'] = []
          let vehiclesData = response.filter(eachvehicle => eachvehicle.building_id == building.building_id);

          vehiclesData.forEach((vehicle, index) => {
            // if ((new Date(vehicle.date).toLocaleDateString() == this.date.toLocaleDateString()) && (vehicle.status != 'completed')) {
            //   vehicle.status = 'Assigned';
            // }
            vehicle.date=moment(vehicle.date).format('MM/DD/YYYY');
            this.date=moment(this.date).format('MM/DD/YYYY');
            if ((moment(vehicle.date).isSameOrAfter(this.date)) && (vehicle.status != 'completed')) {
              vehicle.status = 'Assigned';
            }
            let day = vehicle.date
          if( vehicle.status=="completed"){
            console.log("inside if");
            vehicle.statusreport=1


          }
          else{
            console.log("inside else");
            vehicle.statusreport=0

          }



            var d = new Date(vehicle.date);
            var dayName = d.toString().split(' ')[0];


            let obj = {
              carnumber: vehicle.registration_no,
              parking_no: vehicle.parking_no,
              status: vehicle.status,
              date: vehicle.date,
              Tdate: vehicle.date,
              schedule_id: vehicle.schedule_id,
              report:  vehicle.statusreport,
              dayname: dayName,
              monday: vehicle.monday,
              tuesday: vehicle.tuesday,
              wednesday: vehicle.wednesday,
              thursday: vehicle.thursday,
              friday: vehicle.friday,
              saturday: vehicle.saturday,
              sunday: vehicle.sunday,
            }
            building.vehicles.push(obj)

          });

        });
        console.log(uniqueArray);

        for (let i in uniqueArray) {

      
          uniqueArray[i].vehicles = _.sortBy(uniqueArray[i].vehicles, ["report"]);
  
          console.log( uniqueArray[0].vehicles)
  
  
        }
        console.log(uniqueArray)
        localStorage.setItem('jobs', JSON.stringify(uniqueArray))

        this.allJobs = uniqueArray;
        
        
        this.location =this.allJobs;


        this.location =this.Today1();
        console.log(this.location)
        this.buildingname = this.allJobs


      });



    });

  }

}
