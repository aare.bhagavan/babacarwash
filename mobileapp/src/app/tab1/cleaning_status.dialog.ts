import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { MastersService } from '../api/masters.service';
// import { EvAlertController } from '../alert.controller';
// import { DeclareFunctionStmt } from '@angular/compiler';
// import { ROUTE } from '../../routes';

// import { StorageProvider } from '../../api/storage.provider';
import Swal from 'sweetalert2';
// import { ToastrService } from 'ngx-toastr'


@Component({
  selector: 'app-cleaning_status-dialog',
  template: `
    <app-alert-dialog class='declarationdialog' >
      <div class="alert_dialog">
        <ion-grid class="gridcss1">
          <ion-row style="justify-content:center">
            <p class="contentcolor" id="declare_head">Are you sure you want to mark it as done?</p>
          </ion-row>
    
          <ion-row justify-content-center style="justify-content:space-around">
       
           <a><button type="button" (click)="closeAlert('')" class="btn mr-3">No</button></a> 
  
            <a><button type="button" (click)="updateStatus()"  class="btn ml-4 yesBtn">Yes</button></a>


          </ion-row>
        </ion-grid>
      </div>
    </app-alert-dialog>
  `,
   styleUrls: ['../alert-dialog/alert-dialog.component.scss'],
})
export class CleaningStatusComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  @Input() data: any;
  Ischecked: boolean = false; IsUserexists: boolean = false;
  IsTermsChecked: boolean = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController,
    // public storageProvider: StorageProvider,
     private mastersService: MastersService,
    //  public evAlertController: EvAlertController,
    // public alertController: EvAlertController,
    // private toastr: ToastrService
  ) {
    this.backButtonEvent();
  }
 ngOnInit() {
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });;
          return;
        }
      } catch (error) {
      }

    });
  }
  async closeAlert(response) {
    try {
      const element = await this.modalController.getTop();
      if (element) {
      
        element.dismiss(response).then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }
  async updateStatus(){
    (await this.mastersService.updateWorkStatus(this.data)).subscribe((response: any) => {
      console.log(response);
      if(response.success==true){
        Swal.fire({
          icon: 'success',
          title: "Status Updated Successfully."
        })
        //this.toastr.success('Status Updated Successfully')
       
      }
      this.closeAlert(response);

      
     

    })
  }
}
