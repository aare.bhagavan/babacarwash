import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IonicStorageModule } from '@ionic/storage';
import { APIServiceModule } from './api/api.module';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule } from '@angular/forms';
import { PaymentCollectComponent } from './tab2/payment_collect.dialog';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { CleaningStatusComponent } from './tab1/cleaning_status.dialog';
import { NgxSpinnerModule } from "ngx-spinner";





@NgModule({
  declarations: [AppComponent,PaymentCollectComponent, AlertDialogComponent, CleaningStatusComponent],
  entryComponents: [PaymentCollectComponent],
  imports: [BrowserModule,
     IonicStorageModule.forRoot(),
     IonicModule.forRoot(),
     AppRoutingModule,
     APIServiceModule,CommonModule,ReactiveFormsModule,NgxSpinnerModule

    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
