import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public loginKey: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private spinner: NgxSpinnerService

  ) {
    this.initializeApp();
    this.verifytoken();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    },3000)
  }


  async verifytoken() {
    console.log("inside function")
    this.loginKey = localStorage.getItem('login_key')
    console.log(this.loginKey)
    if (this.loginKey == "true") {
      console.log("inside if ")
    }
    else {
      this.router.navigate(['/login'], { state: { updateInfos: true } });
    }
  }
}
