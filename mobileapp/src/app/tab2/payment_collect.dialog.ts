import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { PaymentService } from '../api/payment.service';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AnyARecord } from 'dns';
// import { AuthService } from 'src/app/api/auth.service';
// import { EvAlertController } from '../alert.controller';
// import { DeclareFunctionStmt } from '@angular/compiler';
// import { ROUTE } from '../../routes';

// import { StorageProvider } from '../../api/storage.provider';

@Component({
  selector: 'app-payment-collect-dialog',
  template: `
  <div class='declarationdialog' >
    <div class="alert_dialog">
      <ion-grid class="gridcss">
        <ion-row justify-content-center>
          <p class="contentcolor" id="declare_head">Enter the amount collected from the Customer</p>
        </ion-row>
        <ion-row justify-content-center class="input_row">
          <input type="number" class="form-control" class="inputstyle " (input)="onSearchChange($event.target.value)">
        </ion-row>
     
      
        
        <ion-row *ngIf="lessamount" class="sicon">
 
        <ion-icon name="alert" class="icon"></ion-icon><span>Amount must be greater than zero</span>
     
        </ion-row>
      
        <ion-row   class="dialog">
          <a><button type="button" (click)="closeAlert()" class="btn  ">Cancel</button></a> 
          <a><button type="submit"  (click)="collectpayment()"  class="btn yesBtn">Collect</button></a>
        </ion-row>
      </ion-grid>
    </div>
  </div>
`,
  styleUrls: ['../alert-dialog/alert-dialog.component.scss'],
})
export class PaymentCollectComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  @Input() data: any;
  amount: any;
  amountcal:any;
  amount_charged:any;
  lessamount:any;

  amount1: string = '';
  Ischecked: boolean = false; IsUserexists: boolean = false;
  IsTermsChecked: boolean = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController, private router: Router,
    // public storageProvider: StorageProvider,
    // private authservice: AuthService,
    //  public evAlertController: EvAlertController,
    // public alertController: EvAlertController
    private PaymentService: PaymentService,
  ) {

    this.backButtonEvent();
  }
  ngOnInit() {
    this.amountcal=false
  }
  handleAmount(event) {
    this.amount = event.target.value
    console.log(this.amount)

    


  }
  onSearchChange(searchValue: string): void {
    
   this.amount_charged= localStorage.getItem("charged_amount")
   var amount_charge= Number(this.amount_charged);
    console.log(localStorage.getItem("charged_amount"))
   this.amount = searchValue;
   console.log(this.amount)
    var amount1= this.amount;
    console.log(amount1)
   
    // if(amount1>amount_charge)
    // {
    //   console.log("inside if")
    //   this.amountcal=true;
    // }
    // else{
    //   console.log("inside else")
    //   this.amountcal=false;
    // }
    if(amount1<=0)
    {
      console.log("inside if")
      this.lessamount=true;
    }
    else{
      console.log("inside else")
      this.lessamount=false;
    }
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });;
          return;
        }
      } catch (error) {
      }

    });
  }
  async closeAlert() {
    try {
      const element = await this.modalController.getTop();
      if (element) {
        element.dismiss().then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }
  // onSubmit() {
  //   return this.amount;
  // }



  async collectpayment() {

    console.log(this.amount)
    console.log(this.amount1)
    console.log(this.data);

    this.data["value"] = this.amount
    this.data["note"]="From Mobile"

    console.log(this.data);
    console.log(this.data.charged_amount);
    console.log(this.amount)
    


    if(this.amount<=0||this.amount==""||this.amount==undefined){

      this.lessamount=true;
      console.log("if condition")


    }
    else{
      console.log("else condition");
      this.amountcal=true;

      (await this.PaymentService.userPaymentDetails(this.data)).subscribe((response: any) => {
        console.log(response);
  
        if (response == true) {
          Swal.fire({
            icon: 'success',
            title: `د.إ ${this.amount} is Paid`,
          })
          this.closeAlert();
  
  
  
        }
        // this.closeAlert(response);
  
  
  
      })

    }


  

  }
}
