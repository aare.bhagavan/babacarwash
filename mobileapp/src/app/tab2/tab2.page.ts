import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { EvAlertController } from '../alert.controller';
import { PaymentService } from '../api/payment.service';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import * as _ from "underscore";
import * as moment from 'moment';


import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
  AbstractControl
} from "@angular/forms";
import { SlicePipe } from '@angular/common';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  myControl1 = new FormControl();
  // myControl = new FormControl();
  // authForm: FormGroup;
  jobsarray: any;
  show_btn: boolean = false;
  building1: any;
  color: any;
  location: any;
  status: any;
  today: any;
  user_id: any;
  allJobs: any;
  due_amount: any;
  buildingarray:any;
  searchvalue:any;
  currentmonth:any
  isDialogPresent: boolean = true;
  constructor(public modalController: ModalController,public fb: FormBuilder,
    private router1: Router, public paymentService: PaymentService, private router: ActivatedRoute,
    public alertController: EvAlertController, private storage: Storage, public navCtrl: NavController,
  ) {
    this.getUser_id();
  }

  jobs: any = [
    {
      'building': 'Building-1',
      'location': 'JublieHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06/Aug/2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Pending', 'jobstatus': "true", 'address': '302', 'amount': '15' },
        { 'day': 'Today', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Collect', 'jobstatus': "true", 'address': '304', 'amount': '10' },
        { 'day': 'Yesterday', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Collect', 'jobstatus': "true", 'address': '300', 'amount': '100' }
      ]
    },
    {
      'building': 'Building-2',
      'location': 'BanjaraHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06/Aug/2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Pending', 'jobstatus': "true", 'address': 'Flat-3,1st Floor', 'amount': '35' },
        { 'day': 'Today', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Collect', 'jobstatus': "true", 'address': 'Flat-2,2nd Floor', 'amount': '95' },
        { 'day': 'Yesterday', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Collect', 'jobstatus': "true", 'address': 'Flat-1,1st Floor', 'amount': '86' }
      ]
    },
    {
      'building': 'Building-3',
      'location': 'Hi-Tech City',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true", 'address': '309', 'amount': '25' },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true", 'address': '322', 'amount': '65' },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true", 'address': '300', 'amount': '65' }
      ]
    },
    {
      'building': 'Building-4',
      'location': 'Panjagutta',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true", 'address': '212', 'amount': '15' },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true", 'address': '200', 'amount': '30' },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true", 'address': '208', 'amount': '55' }
      ]
    }



  ];



  count: number = 0;
  filter(value) {
    this.location = this.allJobs
    this.status = true
    this.searchvalue=value;
    console.log("inside function")
    console.log(value)
    // const value=ev.target.value;
    if (value != null || value != "") {
      this.buildingarray = this.allJobs.filter(device => (
        (device.building_name.toLowerCase().includes(value.toLowerCase())
      )
      ))
      this.location= this.buildingarray
    }
    else {
      this.location= this.buildingarray


    }
  }
  ngOnInit() {
    this.today = false
    this.building1 = false
    this.jobsarray = this.jobs
    this.count = this.jobs.length;
    this.myControl1.valueChanges.subscribe(value => {
      this.filter(value)
    })
    let d=moment();
    // console.log(d+"is")
     let month = 1 + moment(d, 'YYYY/MM/DD').month();
     this.currentmonth=month;
            // console.log(this.currentmonth)
  }
  ionViewWillEnter() {
    console.log("ionViewWillEnter");
    this.getUser_id();
  }

  async getUser_id() {
    await this.storage.get('user_id').then(async (user_id) => {

      console.log(user_id + "user id")
      this.user_id = user_id;
      (await this.paymentService.getuserpayment(this.user_id)).subscribe((res: any) => {
        console.log(res,"payments")
        console.log(res[0].vehicles);







        let uniqueArray = _.map(_.groupBy(res, (obj: any) => {
          return obj.building_id;
        }), (grouped) => {
          return grouped[0];
        })

        uniqueArray.forEach(building => {
          building['vehicles'] = []
          let vehiclesData = res.filter(eachvehicle => eachvehicle.building_id == building.building_id);

          vehiclesData.forEach((vehicle, index) => {
            // console.log(vehicle.ws_date.getMonth());
            console.log(vehicle.ws_date)
            console.log(new Date(vehicle.ws_date).toLocaleDateString())
            let month = 1 + moment(vehicle.ws_date,'YYYY/MM/DD').month();
            console.log(month)

            this.due_amount = vehicle.amount_charged - vehicle.amount_paid
            if (this.due_amount == 0) {

              vehicle.payment_status = "completed";
            }
            else {

              vehicle.payment_status = "pending";
            }
            if (vehicle.payment_status=='completed'&&(vehicle.payment_status=='completed'&& 1 + moment(vehicle.ws_date, 'YYYY/MM/DD').month()<this.currentmonth)) {

                vehicle.statusreport=1
            
            }
            else {

              console.log("false condition")
              vehicle.statusreport=0
            }
            let obj = {
              carnumber: vehicle.registration_no,
              parking_no: vehicle.parking_no,
              status: vehicle.payment_status,
              date: vehicle.date,
              payment_amount: vehicle.payment_amount,
              amount_paid: vehicle.amount_paid,
              parkingno:vehicle.parking_no,
         


              amount_charged: vehicle.amount_charged,
              payment_date: vehicle.payment_date,
              flat_no: vehicle.flat_no,
              vehicle_id: vehicle.vehicle_id,
              due_amount: (vehicle.amount_charged-vehicle.amount_paid)> 0 ? (vehicle.amount_charged-vehicle.amount_paid) : 0,
              sl_id: vehicle.s_id,
              user_name: vehicle.name,
              ws_date: new Date(vehicle.ws_date).toLocaleDateString(),
              startmonth:1 + moment(vehicle.ws_date,'YYYY/MM/DD').month(),
              report:  vehicle.statusreport



            }
            building.vehicles.push(obj)

          });

        });
        console.log(uniqueArray);
        console.log(uniqueArray[0].vehicles);
        
        for (let i in uniqueArray) {

      
        uniqueArray[i].vehicles = _.sortBy(uniqueArray[i].vehicles, ["report"]);

        console.log( uniqueArray[0].vehicles)


      }
      this.allJobs = uniqueArray;
      this.location = this.allJobs
      this.buildingarray= this.allJobs











        



      })

    })
    console.log(this.user_id)
  }



  Today() {
    this.building1 = false
    this.today = !this.today
    this.color = "red"

  }

  Building() {
    console.log("inside building")
    console.log( this.building1 )
   
    this.building1 = !this.building1
    this.today = false

  }
  loadData(event) {
    if (event.status == "Completed") {
      this.jobs.forEach(element => {

        if (element.carnumber == event.carnumber) {
          element.status = "Pending";
        }
      });
    } else {
      this.jobs.forEach(element => {
        if (element.carnumber == event.carnumber) {
          element.status = "Completed";
        }
      });
    }
  }
  show_button() {
    console.log("show button")
    this.show_btn = true;
    console.log(this.show_btn)

  }
  async presentModal(selected: any) {

    console.log(selected.carnumber)
    console.log(selected.sl_id + "is")
    console.log(selected.user_name + "is")
    console.log(selected.due_amount+"charged amount")
    localStorage.setItem("charged_amount",selected.due_amount);

    let data = {
      'schedule_id': selected.sl_id,

      'user_name': selected.user_name,
      'payment_registered_by': selected.user_name,
      'user_id': this.user_id,
      'added_by': selected.user_name,
      'updated_by': selected.user_name,
      'status': "Complted",
      'charged_amount':selected.due_amount,
      'note': "",
    }

    console.log(data)

    this.isDialogPresent = false;
    const res = await this.alertController.PaymentCollectDialog(data);

    console.log(res);
    if (res == true) {


      this.isDialogPresent = true;
      this.getUser_id()


    }
  }
}
