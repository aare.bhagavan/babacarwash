import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { RESPONSE } from './../api/response';
import { AuthService } from './../api/auth.service';
import { Storage } from '@ionic/storage';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  clickedtab = 1; pushes: any = []; selectedTabId = 1; unreadCount = 0;

  constructor(private platform: Platform,
    private router: Router,
    private storage: Storage,
    private route: ActivatedRoute, public authService: AuthService) {
    this.backButtonEvent();
    console.log(this.selectedTabId)
    this.platform.ready()
      .then(async () => {

      });
  }
  ngOnInit() {

  }
  ionViewWillEnter() {
    console.log("ionViewWillEnter");
    this.selectedTabId = 1;
    console.log(this.selectedTabId);

  }



  tabClicked(tabId: number) {
    console.log(tabId);

    this.selectedTabId = tabId;
  }


  logout() {
    Swal.fire({
      title: 'Do You want to Logout?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(async (result) => {
      if (result.value) {
        localStorage.clear();
        this.storage.clear();
        this.router.navigate(['/login']);
      }
    })



  }
  backButtonEvent() {
    this.platform.backButton.subscribe(() => {
      console.log('exit');
      navigator['app'].exitApp();
    })
  }
}
