// import { ROUTE } from './../routes';
import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

import {
    Router
} from '@angular/router';


@Injectable({
    providedIn: 'root',
})

export class TokenInterceptor implements HttpInterceptor {

    public test_TOKEN_ID;

    constructor(private router: Router) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
 
        const token = localStorage.getItem('token');
        const clonedReq = this.addToken(request, token);        
        return next.handle(clonedReq)
        // return next.handle(request)
    }
 
 
    // Adds the token to your headers if it exists
    private addToken(request: HttpRequest<any>, token: any) {
        if (token) {
            let clone: HttpRequest<any>;
            clone = request.clone({
                // withCredentials: true,
                setHeaders: {
                    'Content-Type': `application/json`,
                    Authorization: `${token}`,
                    'content-type': 'application/json; charset=UTF-8',
                }
            });
            return clone;
        }
        return request.clone();
    }
}
