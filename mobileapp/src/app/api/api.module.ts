import { AuthService } from './auth.service';

import { HttpClient } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// import { BusinessService } from './business.service';
import { scheduled } from 'rxjs';
import { TokenInterceptor } from './token.interceptor';

@NgModule({
    imports: [
        HttpClientModule
    ],

})
export class APIServiceModule {
    static forRoot(): ModuleWithProviders<APIServiceModule> {
        return {
            ngModule: APIServiceModule,
            providers: [
                {
                    provide: HTTP_INTERCEPTORS,
                    useClass: TokenInterceptor,
                    multi: true
                },

                // HTTP,
                HttpClient,
                // BusinessService,
                AuthService
            ]
        };
    }
}
