import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, BusinessRequest, AuthRequest } from './request';
import { NetworkService } from './network.service';
// import { Observable, Observer, Subject, from } from 'rxjs';
// import { RESPONSE } from './response';
// import { Time } from '@angular/common';
// import { tap, map, delayWhen } from 'rxjs/operators';
// import { StorageProvider } from './storage.provider';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
// import { EvAlertController } from '../components/alert.controller';
// import { LoadingController } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends NetworkService {
  // public onScheduled: Subject<any> = new Subject<any>();

  constructor(
    // protected storageProvider: StorageProvider,
    protected httpClient: HttpClient,
    public spinner: NgxSpinnerService
    // protected alertController: EvAlertController,
    // protected socketMaster: SocketMasterService,
    // protected loadingCtrl: LoadingController
  ) {
    super(httpClient,
      spinner
    );
  }

  async login(mobileno: string, password: string) {
    const data = { mobileno, password };
    console.log(password)
    const request = new REQUEST(AuthRequest.LOGIN, data, REQUEST.POST);
    return (await this.request(request));
  }

  async register(phone: string, society_id: string, name: string, password: string, email: string, role: string) {
    const data = {
      "phone": phone,
      "password": password,
      "user_name": name,
      "email": email,
      "business_id": parseInt(society_id),
    };
    const request = new REQUEST(AuthRequest.REGISTRATION, data, REQUEST.POST);
    return (await this.request(request))
  }

  async oneTimeRegistration(registerData: any, user_name: any) {
    let data = {
      registerData,
      user_name : user_name,
      token : localStorage.getItem("token"),
    }
    const request = new REQUEST(AuthRequest.ONE_TIME_REGISTRATION, data, REQUEST.POST);
    return (await this.request(request))
  }

  async userRegister(phone: string, society_id: string, name: string, password: string, email: string, role: string) {
    let data;
    if (role == 'super_admin') {
      data = {
        "phone": phone,
        "password": password,
        "user_name": name,
        "email": email,
        "super_business_id": parseInt(society_id),
      }
    } else {
      data = {
        "phone": phone,
        "password": password,
        "user_name": name,
        "email": email,
        "business_id": parseInt(society_id),
      };
    }
    const request = new REQUEST(AuthRequest.USER_REGISTRATION(role), data, REQUEST.POST);
    return (await this.request(request))
  }


}
































