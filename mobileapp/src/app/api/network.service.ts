import { REQUEST } from './request';
// import { StorageProvider } from './storage.provider';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
// import { LoadingController, } from '@ionic/angular';
import { Observable, from, throwError } from 'rxjs';
import { tap, map, catchError } from 'rxjs/operators';
import { RESPONSE } from './response';
import { AuthRequest } from './request';
import Swal from 'sweetalert2';
// import { EvAlertController } from '../components/alert.controller';
// import { SocketMasterService } from './socket.service';

import { NgxSpinnerService } from "ngx-spinner";


@Injectable({
    providedIn: 'root',
})
export class NetworkService {

    private loading;
    private STATUS_SUCCESS = 'Success';
    private STATUS_ERROR = 'Error';

    constructor(protected httpClient: HttpClient, 
        public spinner: NgxSpinnerService
        ) {
    }

    public static prepareGetRequestUrl(url: string, params: any): string {
        if (params == null || params === undefined) {
            return url;
        }
        const paramsArray = Object.keys(params);
        if (paramsArray.length === 0) {
            return url;
        }
        let queryString = paramsArray
            .map(key => key + '=' + params[key])
            .join('&');
        if (queryString != null) {
            queryString = '?' + queryString;
        }
        return url + queryString;
    }

    public async request(api: REQUEST): Promise<Observable<RESPONSE>> {
        let loading = null;
        if (api.isLoadingRequired) {
            this.spinner.show();
        }
        const httpObservable: Observable<any> = this.sendHttpRequest(api.method, api.endPoint, api.data)
            .pipe(

                tap(async (data) => {
                    if (api.isLoadingRequired) {
                        this.spinner.hide();
                    }
                }),
                catchError(async (error: HttpErrorResponse) => {
                    if (api.isLoadingRequired) {
                        this.spinner.hide();
                    }
                    if (api.isLoadingRequired && error.status != 201) {

                        if (error.status === 500) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong!',
                            });
                        }
                        else if (error.status == 401) {
                            if (error.url === AuthRequest.LOGIN) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Invalid Credentials',
                                    text: 'Please try again!',
                                });
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops..',
                                    text: error.error.message,
                                });
                            }
                        }
                        // this.alertController.showError('Something Went Wrong', 'Internal Server Error Occured, Please try again');
                        else if (error.status == 408) {
                            if (error.url === AuthRequest.REGISTRATION) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Mobile number already exists',
                                    text: 'Please try again!',
                                });
                            }
                        }
                        else if (error.status === 506) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Failed to perform request!',
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Something went wrong while contacting the server, please try again!',
                            });
                        }
                    }
                    return error;
                })
            );
        return httpObservable;
    }

    public sendHttpRequest(method: string, api: string, params: any = {}): Observable<object> {
        if (method === REQUEST.POST) {
            return this.httpClient.post(api, params);
        } else if (method === REQUEST.GET) {
            const options = {
                params
            };
            return this.httpClient.get(api, options);
        } else if (method === REQUEST.DELETE) {
            const options = {
                params
            };
            return this.httpClient.delete(api, options);
        } else {
            return null;
        }
    }

    async getLoadingBar() {
        if (this.loading) {
            return this.loading;
        }
        // this.loading = await this.loadingCtrl.create({
        //     translucent: true,
        // });
        return this.loading;
    }
}
