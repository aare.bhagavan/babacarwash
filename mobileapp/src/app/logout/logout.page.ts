import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage implements OnInit {

  constructor(private router: Router,
    private storage: Storage,
  ) { }

  ngOnInit() {
    this.logout();
  }
  ngOnViewInit() {
    this.logout();
  }
  logout() {
    localStorage.clear();
    this.storage.clear();
    this.router.navigate(['/login']);

  }

}
