import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  registrationForm: FormGroup;
  minDate = moment(new Date()).format("YYYY-MM-DD");
  user_name;

  constructor(
    public formBuilder: FormBuilder,
    public router: ActivatedRoute,
    public authService: AuthService,
  ) {

    this.user_name = localStorage.getItem('user_name');
  }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      // first_name: ['', Validators.required],
      // last_name: ['', Validators.required],
      // mobile: ['', Validators.required],
      parking_no:['',Validators.required],
      vehicle_no: ['', Validators.compose([Validators.pattern("([A-Za-z]+[0-9]|[0-9]+[A-Za-z])[A-Za-z0-9]*"),Validators.required])],     
      amount: ['',Validators.compose([Validators.pattern("[1-9][0-9]*"),Validators.required])],
    });
  }
  async register() {
    (await this.authService.oneTimeRegistration(this.registrationForm.value, this.user_name)).subscribe((res:any) => {
      console.log('response....', res)
      if (res == true) {
        Swal.fire({ icon: 'success', title: 'Registration Successful !', });
        this.registrationForm.reset();
      }
    });
    
  }
}
