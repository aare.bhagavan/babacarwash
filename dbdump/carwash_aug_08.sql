-- MySQL dump 10.13  Distrib 8.0.19, for Linux (x86_64)
--
-- Host: localhost    Database: carwash_db
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `buildings` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `location_id` int NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_location_id` (`location_id`),
  CONSTRAINT `fk_location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` VALUES (1,'new building',1,1,NULL,NULL);
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `building_id` int DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_building_id` (`building_id`),
  CONSTRAINT `fk_building_id` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (6,'mukesh',1,'9087654321','k'),(9,'jh',1,'9087654321','hjkhk'),(10,'jhjkh',1,'9087654321','kjhkj'),(11,'jhjkh',1,'9087654321','kjhkj'),(12,'jhg',1,'9087654321','gjj'),(13,'jhg',1,'9087654321','gjj'),(14,'yjgjhg',1,'9087654321','jgkj'),(15,'hjhg',1,'9087654321','kjgkj'),(16,'mukesh',1,'9087654321','kota'),(17,'mukesh',1,'9087654321','kota'),(18,'mukesh',1,'9876543212','kot'),(19,'mukesh',1,'9087654321','kota'),(20,'mukesh',1,'9087654321','kota'),(21,'mukesh',1,'8374393621','k'),(22,'jkgjh',1,'9087654321','kjg'),(23,'mukesh',1,'9087654321','ko'),(24,'mukesh',1,'9087654321','kj'),(25,'kjg',1,'9090909090','jjkhjk'),(26,'jhg',1,'9087654321','jgjhg'),(27,'kjjhkj',1,'9087654321','oiiu'),(28,'jhgjh',1,'9087654321','gjgjh'),(29,'jhgh',1,'9087654321','kjghj'),(30,'mukesh',1,'8767890987','kota'),(31,'jhhg',1,'9876545458','jkgj'),(32,'hgjhghjgj',1,'2134565678','kjgjgh'),(33,'kjhkjh',1,'9088899000','kjhkjhkj'),(34,'jhghjg',1,'9081234556','khjkhkj'),(35,'jhgjh',1,'opo0909900','khkjhk'),(36,'jhgjh',1,'opo0909901','khkjhk'),(37,'mukh',1,'1234567890','kjj'),(38,'jhg',1,'8909999999','jgj'),(39,'mukesh',1,'3452678900','ko'),(40,'jhfhg',1,'9087676734','kjhhkj'),(41,'mukesh',1,'9087656565','ll'),(42,'mukesh',1,'9807654000','ko'),(43,'mukesh',1,'9876564321','ko'),(44,'mukesh',1,'9087654777','koii'),(45,'mukesh',1,'9087654700','koii'),(46,'mukesh',1,'9087654722','koii'),(47,'jhgj',1,'9089877777','kjhj'),(48,'jhgj',1,'9087676767','jghj'),(49,'ihjkhjk',1,'97t4354357','kjhkjjk');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `location` (
  `id` int NOT NULL AUTO_INCREMENT,
  `zip_code` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'1234','jubliee hills'),(2,'1235','secunderabad'),(3,'1236','banjara hills'),(4,'1237','Hitech city');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_transaction`
--

DROP TABLE IF EXISTS `payment_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_transaction` (
  `id` int NOT NULL AUTO_INCREMENT,
  `schedule_id` int NOT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` int DEFAULT NULL,
  `payment_registered_by` varchar(45) DEFAULT NULL,
  `cleaner_id` int NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `added_by` varchar(45) DEFAULT NULL,
  `updated_by` varchar(45) DEFAULT NULL,
  `client_ip` varchar(150) DEFAULT NULL,
  `client_browser` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_scheduleid` (`schedule_id`),
  KEY `fk_cleaner_id` (`cleaner_id`),
  CONSTRAINT `fk_cleaner_id` FOREIGN KEY (`cleaner_id`) REFERENCES `workers` (`id`),
  CONSTRAINT `fk_scheduleid` FOREIGN KEY (`schedule_id`) REFERENCES `schedule_logs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_transaction`
--

LOCK TABLES `payment_transaction` WRITE;
/*!40000 ALTER TABLE `payment_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule_logs`
--

DROP TABLE IF EXISTS `schedule_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `schedule_logs` (
  `id` int NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `schedule_id` int NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `client_ip` varchar(150) DEFAULT NULL,
  `browser_details` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_schedule_id` (`schedule_id`),
  CONSTRAINT `fk_schedule_id` FOREIGN KEY (`schedule_id`) REFERENCES `work_schedule` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=425184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule_logs`
--

LOCK TABLES `schedule_logs` WRITE;
/*!40000 ALTER TABLE `schedule_logs` DISABLE KEYS */;
INSERT INTO `schedule_logs` VALUES (424972,'2020-08-09',24,'pending','2020-08-06 11:28:51','2020-08-06 11:28:51','undefined','undefined',NULL,NULL),(424973,'2020-08-16',24,'pending','2020-08-06 11:28:51','2020-08-06 11:28:51','undefined','undefined',NULL,NULL),(424974,'2020-08-23',24,'pending','2020-08-06 11:28:51','2020-08-06 11:28:51','undefined','undefined',NULL,NULL),(424975,'2020-08-30',24,'pending','2020-08-06 11:28:51','2020-08-06 11:28:51','undefined','undefined',NULL,NULL),(424976,'2020-08-10',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424977,'2020-08-17',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424978,'2020-08-24',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424979,'2020-08-31',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424980,'2020-08-11',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424981,'2020-08-18',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424982,'2020-08-25',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424983,'2020-08-12',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424984,'2020-08-19',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424985,'2020-08-26',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424986,'2020-08-06',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424987,'2020-08-13',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424988,'2020-08-20',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424989,'2020-08-27',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424990,'2020-08-07',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424991,'2020-08-14',24,'pending','2020-08-06 11:28:52','2020-08-06 11:28:52','undefined','undefined',NULL,NULL),(424992,'2020-08-21',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424993,'2020-08-28',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424994,'2020-08-08',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424995,'2020-08-15',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424996,'2020-08-22',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424997,'2020-08-29',24,'pending','2020-08-06 11:28:53','2020-08-06 11:28:53','undefined','undefined',NULL,NULL),(424998,'2020-08-09',25,'pending','2020-08-06 11:35:53','2020-08-06 11:35:53','undefined','undefined',NULL,NULL),(424999,'2020-08-09',25,'pending','2020-08-06 11:35:58','2020-08-06 11:35:58','undefined','undefined',NULL,NULL),(425000,'2020-08-16',25,'pending','2020-08-06 11:36:13','2020-08-06 11:36:13','undefined','undefined',NULL,NULL),(425001,'2020-08-23',25,'pending','2020-08-06 11:36:48','2020-08-06 11:36:48','undefined','undefined',NULL,NULL),(425002,'2020-08-30',25,'pending','2020-08-06 11:37:16','2020-08-06 11:37:16','undefined','undefined',NULL,NULL),(425003,'2020-08-09',26,'pending','2020-08-06 11:38:36','2020-08-06 11:38:36','undefined','undefined',NULL,NULL),(425004,'2020-08-16',26,'pending','2020-08-06 11:38:38','2020-08-06 11:38:38','undefined','undefined',NULL,NULL),(425005,'2020-08-23',26,'pending','2020-08-06 11:38:42','2020-08-06 11:38:42','undefined','undefined',NULL,NULL),(425006,'2020-08-30',26,'pending','2020-08-06 11:38:44','2020-08-06 11:38:44','undefined','undefined',NULL,NULL),(425007,'2020-08-09',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425008,'2020-08-16',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425009,'2020-08-23',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425010,'2020-08-30',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425011,'2020-08-10',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425012,'2020-08-17',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425013,'2020-08-24',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425014,'2020-08-31',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425015,'2020-08-11',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425016,'2020-08-18',27,'pending','2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL),(425017,'2020-08-25',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425018,'2020-08-12',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425019,'2020-08-19',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425020,'2020-08-26',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425021,'2020-08-06',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425022,'2020-08-13',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425023,'2020-08-20',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425024,'2020-08-27',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425025,'2020-08-07',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425026,'2020-08-14',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425027,'2020-08-21',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425028,'2020-08-28',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425029,'2020-08-08',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425030,'2020-08-15',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425031,'2020-08-22',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425032,'2020-08-29',27,'pending','2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL),(425033,'2020-08-09',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425034,'2020-08-16',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425035,'2020-08-23',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425036,'2020-08-30',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425037,'2020-08-10',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425038,'2020-08-17',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425039,'2020-08-24',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425040,'2020-08-31',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425041,'2020-08-11',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425042,'2020-08-18',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425043,'2020-08-25',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425044,'2020-08-12',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425045,'2020-08-19',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425046,'2020-08-26',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425047,'2020-08-06',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425048,'2020-08-13',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425049,'2020-08-20',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425050,'2020-08-27',28,'pending','2020-08-06 11:42:26','2020-08-06 11:42:26','undefined','undefined',NULL,NULL),(425051,'2020-08-07',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425052,'2020-08-14',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425053,'2020-08-21',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425054,'2020-08-28',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425055,'2020-08-08',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425056,'2020-08-15',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425057,'2020-08-22',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425058,'2020-08-29',28,'pending','2020-08-06 11:42:27','2020-08-06 11:42:27','undefined','undefined',NULL,NULL),(425059,'2020-08-08',29,'pending','2020-08-06 11:46:29','2020-08-06 11:46:29','undefined','undefined',NULL,NULL),(425060,'2020-08-15',29,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425061,'2020-08-22',29,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425062,'2020-08-29',29,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425063,'2020-08-08',30,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425064,'2020-08-15',30,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425065,'2020-08-22',30,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425066,'2020-08-29',30,'pending','2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL),(425067,'2020-08-10',31,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425068,'2020-08-17',31,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425069,'2020-08-24',31,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425070,'2020-08-31',31,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425071,'2020-08-10',32,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425072,'2020-08-17',32,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425073,'2020-08-24',32,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425074,'2020-08-31',32,'pending','2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL),(425075,'2020-08-06',33,'pending','2020-08-06 12:39:10','2020-08-06 12:39:10','undefined','undefined',NULL,NULL),(425076,'2020-08-13',33,'pending','2020-08-06 12:39:10','2020-08-06 12:39:10','undefined','undefined',NULL,NULL),(425077,'2020-08-20',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425078,'2020-08-27',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425079,'2020-08-08',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425080,'2020-08-15',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425081,'2020-08-22',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425082,'2020-08-29',33,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425083,'2020-08-06',34,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425084,'2020-08-13',34,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425085,'2020-08-20',34,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425086,'2020-08-27',34,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425087,'2020-08-08',34,'pending','2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL),(425088,'2020-08-15',34,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425089,'2020-08-22',34,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425090,'2020-08-29',34,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425091,'2020-08-09',35,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425092,'2020-08-16',35,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425093,'2020-08-23',35,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425094,'2020-08-30',35,'pending','2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL),(425095,'2020-08-09',36,'pending','2020-08-06 13:17:34','2020-08-06 13:17:34','undefined','undefined',NULL,NULL),(425096,'2020-08-16',36,'pending','2020-08-06 13:17:44','2020-08-06 13:17:44','undefined','undefined',NULL,NULL),(425097,'2020-08-23',36,'pending','2020-08-06 13:17:44','2020-08-06 13:17:44','undefined','undefined',NULL,NULL),(425098,'2020-08-30',36,'pending','2020-08-06 13:17:44','2020-08-06 13:17:44','undefined','undefined',NULL,NULL),(425099,'2020-08-10',36,'pending','2020-08-06 13:17:47','2020-08-06 13:17:47','undefined','undefined',NULL,NULL),(425100,'2020-08-17',36,'pending','2020-08-06 13:17:47','2020-08-06 13:17:47','undefined','undefined',NULL,NULL),(425101,'2020-08-24',36,'pending','2020-08-06 13:17:47','2020-08-06 13:17:47','undefined','undefined',NULL,NULL),(425102,'2020-08-31',36,'pending','2020-08-06 13:17:47','2020-08-06 13:17:47','undefined','undefined',NULL,NULL),(425103,'2020-08-11',36,'pending','2020-08-06 13:17:51','2020-08-06 13:17:51','undefined','undefined',NULL,NULL),(425104,'2020-08-18',36,'pending','2020-08-06 13:17:51','2020-08-06 13:17:51','undefined','undefined',NULL,NULL),(425105,'2020-08-25',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425106,'2020-08-12',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425107,'2020-08-19',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425108,'2020-08-26',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425109,'2020-08-06',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425110,'2020-08-13',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425111,'2020-08-20',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425112,'2020-08-27',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425113,'2020-08-07',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425114,'2020-08-14',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425115,'2020-08-21',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425116,'2020-08-28',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425117,'2020-08-08',36,'pending','2020-08-06 13:17:52','2020-08-06 13:17:52','undefined','undefined',NULL,NULL),(425118,'2020-08-15',36,'pending','2020-08-06 13:17:53','2020-08-06 13:17:53','undefined','undefined',NULL,NULL),(425119,'2020-08-22',36,'pending','2020-08-06 13:17:53','2020-08-06 13:17:53','undefined','undefined',NULL,NULL),(425120,'2020-08-29',36,'pending','2020-08-06 13:17:53','2020-08-06 13:17:53','undefined','undefined',NULL,NULL),(425121,'2020-08-09',37,'pending','2020-08-06 13:20:21','2020-08-06 13:20:21','undefined','undefined',NULL,NULL),(425122,'2020-08-16',37,'pending','2020-08-06 13:20:21','2020-08-06 13:20:21','undefined','undefined',NULL,NULL),(425123,'2020-08-23',37,'pending','2020-08-06 13:20:21','2020-08-06 13:20:21','undefined','undefined',NULL,NULL),(425124,'2020-08-30',37,'pending','2020-08-06 13:20:21','2020-08-06 13:20:21','undefined','undefined',NULL,NULL),(425125,'2020-08-10',37,'pending','2020-08-06 13:20:21','2020-08-06 13:20:21','undefined','undefined',NULL,NULL),(425126,'2020-08-17',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425127,'2020-08-24',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425128,'2020-08-31',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425129,'2020-08-11',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425130,'2020-08-18',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425131,'2020-08-25',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425132,'2020-08-12',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425133,'2020-08-19',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425134,'2020-08-26',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425135,'2020-08-06',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425136,'2020-08-13',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425137,'2020-08-20',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425138,'2020-08-27',37,'pending','2020-08-06 13:20:22','2020-08-06 13:20:22','undefined','undefined',NULL,NULL),(425139,'2020-08-07',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425140,'2020-08-14',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425141,'2020-08-21',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425142,'2020-08-28',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425143,'2020-08-08',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425144,'2020-08-15',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425145,'2020-08-22',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425146,'2020-08-29',37,'pending','2020-08-06 13:20:23','2020-08-06 13:20:23','undefined','undefined',NULL,NULL),(425147,'2020-08-09',38,'pending','2020-08-06 13:30:50','2020-08-06 13:30:50','undefined','undefined',NULL,NULL),(425148,'2020-08-16',38,'pending','2020-08-06 13:30:50','2020-08-06 13:30:50','undefined','undefined',NULL,NULL),(425149,'2020-08-23',38,'pending','2020-08-06 13:30:50','2020-08-06 13:30:50','undefined','undefined',NULL,NULL),(425150,'2020-08-30',38,'pending','2020-08-06 13:30:50','2020-08-06 13:30:50','undefined','undefined',NULL,NULL),(425151,'2020-08-09',39,'pending','2020-08-06 13:31:58','2020-08-06 13:31:58','undefined','undefined',NULL,NULL),(425152,'2020-08-16',39,'pending','2020-08-06 13:31:58','2020-08-06 13:31:58','undefined','undefined',NULL,NULL),(425153,'2020-08-23',39,'pending','2020-08-06 13:31:59','2020-08-06 13:31:59','undefined','undefined',NULL,NULL),(425154,'2020-08-30',39,'pending','2020-08-06 13:31:59','2020-08-06 13:31:59','undefined','undefined',NULL,NULL),(425155,'2020-08-09',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425156,'2020-08-16',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425157,'2020-08-23',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425158,'2020-08-30',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425159,'2020-08-10',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425160,'2020-08-17',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425161,'2020-08-24',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425162,'2020-08-31',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425163,'2020-08-11',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425164,'2020-08-18',40,'pending','2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL),(425165,'2020-08-25',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425166,'2020-08-12',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425167,'2020-08-19',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425168,'2020-08-26',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425169,'2020-08-06',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425170,'2020-08-13',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425171,'2020-08-20',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425172,'2020-08-27',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425173,'2020-08-07',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425174,'2020-08-14',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425175,'2020-08-21',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425176,'2020-08-28',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425177,'2020-08-08',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425178,'2020-08-15',40,'pending','2020-08-06 13:49:22','2020-08-06 13:49:22','undefined','undefined',NULL,NULL),(425179,'2020-08-22',40,'pending','2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL),(425180,'2020-08-29',40,'pending','2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL),(425181,'2020-08-12',41,'pending','2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL),(425182,'2020-08-19',41,'pending','2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL),(425183,'2020-08-26',41,'pending','2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL);
/*!40000 ALTER TABLE `schedule_logs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subscription` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vehicle_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_vehic_id` (`vehicle_id`),
  CONSTRAINT `fk_vehic_id` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription`
--

LOCK TABLES `subscription` WRITE;
/*!40000 ALTER TABLE `subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_token` (
  `id` int NOT NULL AUTO_INCREMENT,
  `token` varchar(500) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `is_expired` tinyint(1) NOT NULL,
  `login_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `logout_time` varchar(500) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
INSERT INTO `user_token` VALUES (1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzA5MjR9.4BMMnrAK2LW7l0Dv6vFn_ly3A0jWtUM8dHW5VCk7n5M',1,1,'2020-08-01 08:35:24',NULL,'2020-08-01 08:35:24','2020-08-01 08:35:24'),(2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzA5NTF9.3TZKwNvIwtCx8sitcmh4l6XsBF20FwI6zN8eiAJUpjQ',1,1,'2020-08-01 08:35:51',NULL,'2020-08-01 08:35:51','2020-08-01 08:35:51'),(3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzA5OTZ9.NRh2J_s5RL5erW5KaMiZVSJAV-ZC26dTCfumyQ21DuA',1,1,'2020-08-01 08:36:36',NULL,'2020-08-01 08:36:36','2020-08-01 08:36:36'),(4,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzE2MjJ9.9jedsFgnePGm1nzdDXt5wLrbRoM-5VddxA7byRCWtlY',1,1,'2020-08-01 08:47:02',NULL,'2020-08-01 08:47:02','2020-08-01 08:47:02'),(5,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzE2NDV9.o1Ubh3aalMjMclLx28bXwk5OaLD7b6RbaQYLA6SZ4R0',1,1,'2020-08-01 08:47:25',NULL,'2020-08-01 08:47:25','2020-08-01 08:47:25'),(6,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzM5NDh9.wbWyS5kKYWri3HJhX9ZhziGvYNn1FvVToMZDg9yRPP8',1,1,'2020-08-01 09:25:48',NULL,'2020-08-01 09:25:48','2020-08-01 09:25:48'),(7,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzQyNzh9.E_Urz0Z5_MSQNur5HSUk-j5aLacWlPyXsip7QxpG83k',1,1,'2020-08-01 09:31:18',NULL,'2020-08-01 09:31:18','2020-08-01 09:31:18'),(8,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzQ0NTJ9.7uskkKFpCXzBy-fQUdZa1f-ijOzHPUoTITQXdQFYWi4',1,1,'2020-08-01 09:34:12',NULL,'2020-08-01 09:34:12','2020-08-01 09:34:12'),(9,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzQ0ODR9.0rzWOV8nCYkW62YDGl2SYOaamPY8FCh-0Z2xTDFSlKg',1,1,'2020-08-01 09:34:44',NULL,'2020-08-01 09:34:44','2020-08-01 09:34:44'),(10,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTYyNzQ4MDl9.XJxm7PbVODGeKGeUHzyypfH4Fibye8eDaA3k6lzUSv4',1,1,'2020-08-01 09:40:09',NULL,'2020-08-01 09:40:09','2020-08-01 09:40:09'),(11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyZXN1bHRQaG9uZSI6IjgzNzQzOTM2MjEiLCJpYXQiOjE1OTY3MDU1NTh9.vXq6MNl8TiJgzuJo1MNQQfQ3HCIXZldnXKJwSv2KPtg',1,1,'2020-08-06 09:19:18',NULL,'2020-08-06 09:19:18','2020-08-06 09:19:18');
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'mukesh','8374393621','mukesh','admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehicles`
--

DROP TABLE IF EXISTS `vehicles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehicles` (
  `id` int NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(45) DEFAULT NULL,
  `customer_id` int NOT NULL,
  `parking_no` varchar(45) DEFAULT NULL,
  `building_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_veh_building_id` (`building_id`),
  KEY `fk_cust_id` (`customer_id`),
  CONSTRAINT `fk_cust_id` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
  CONSTRAINT `fk_veh_building_id` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehicles`
--

LOCK TABLES `vehicles` WRITE;
/*!40000 ALTER TABLE `vehicles` DISABLE KEYS */;
INSERT INTO `vehicles` VALUES (2,'AP091111',6,'1234',1),(3,'AP091112',6,'1234',1),(4,'AP091114',6,'1234',1),(11,'jhgj',10,'66',1),(12,'jhgj',11,'66',1),(13,'jiii',12,'98',1),(14,'jiii',13,'98',1),(15,'hgf',14,'76',1),(16,'1234',15,'8',1),(17,'MH01QQ1111',16,'909',1),(18,'MH01QQ1111',17,'909',1),(19,'123',18,'98',1),(20,'MH01QQ1111',19,'90',1),(21,'mh00098',20,'90889',1),(22,'MH01QQ1111',21,'90',1),(23,'98uui',22,'98',1),(24,'8999999',23,'98',1),(25,'98999',24,'9898',1),(26,'789',25,'998',1),(27,'888',26,'9898',1),(28,'9898',27,'99',1),(29,'9090',28,'98',1),(30,'212',29,'12',1),(31,'MHJK1111',30,'0',1),(32,'MKLJJJ111',31,'0',1),(33,'kkjh',32,'89798',1),(34,'uupp',34,'12',1),(35,'sda',35,'12',1),(36,'MJKLNJHH',36,'12',1),(37,'1hhj',38,'12',1),(38,'jj;op',38,'12',1),(39,'LKOPIIIII',39,'90',1),(40,'QWSD',39,'90',1),(41,'123456tyt',41,'90',1),(42,'ty6yu7',41,'90',1),(43,'TS09KLIO',42,'99',1),(44,'TSOPL7777',42,'99',1),(45,'TS098KIUJ',43,'100',1),(46,'TFDLO999',43,'100',1),(47,'MH0OPIUY',43,'123',1),(48,'hyyy',44,'90',1),(49,'90965',45,'90',1),(50,'7889',45,'12',1),(51,'0OP09',46,'90',1),(52,'9098',46,'12',1),(53,'GGG',47,'99',1),(54,'lkiii',48,'987687',1),(55,'uy7hjghg',48,'-86',1),(56,'plpl',49,'12',1),(57,'jghj777',49,'99879',1);
/*!40000 ALTER TABLE `vehicles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_schedule`
--

DROP TABLE IF EXISTS `work_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_schedule` (
  `id` int NOT NULL AUTO_INCREMENT,
  `worker_id` int NOT NULL,
  `vehicle_id` int NOT NULL,
  `monday` int DEFAULT NULL,
  `tuesday` int DEFAULT NULL,
  `wednesday` int DEFAULT NULL,
  `thursday` int DEFAULT NULL,
  `friday` int DEFAULT NULL,
  `saturday` int DEFAULT NULL,
  `sunday` int DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `client_ip` varchar(150) DEFAULT NULL,
  `browser_details` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_worker_id` (`worker_id`),
  KEY `fk_vehicle_id` (`vehicle_id`),
  CONSTRAINT `fk_vehicle_id` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `fk_worker_id` FOREIGN KEY (`worker_id`) REFERENCES `workers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_schedule`
--

LOCK TABLES `work_schedule` WRITE;
/*!40000 ALTER TABLE `work_schedule` DISABLE KEYS */;
INSERT INTO `work_schedule` VALUES (2,1,11,1,1,1,1,1,1,1,'2020-08-05 04:59:21','2020-08-05 04:59:21','undefined','undefined',NULL,NULL,NULL,NULL),(3,1,12,1,1,1,1,1,1,1,'2020-08-05 05:01:21','2020-08-05 05:01:21','undefined','undefined',NULL,NULL,NULL,NULL),(4,1,13,1,1,1,1,1,1,1,'2020-08-05 05:04:32','2020-08-05 05:04:32','undefined','undefined',NULL,NULL,NULL,NULL),(5,1,14,1,1,1,1,1,1,1,'2020-08-05 05:06:32','2020-08-05 05:06:32','undefined','undefined',NULL,NULL,NULL,NULL),(6,1,15,1,1,1,1,1,1,1,'2020-08-05 08:11:01','2020-08-05 08:11:01','undefined','undefined',NULL,NULL,NULL,NULL),(7,1,16,1,1,1,1,1,1,1,'2020-08-05 09:04:56','2020-08-05 09:04:56','undefined','undefined',NULL,NULL,NULL,NULL),(8,1,17,1,1,1,1,1,1,1,'2020-08-06 04:59:46','2020-08-06 04:59:46','undefined','undefined',NULL,NULL,NULL,NULL),(9,1,18,1,1,1,1,1,1,1,'2020-08-06 05:04:14','2020-08-06 05:04:14','undefined','undefined',NULL,NULL,NULL,NULL),(10,1,19,1,1,1,1,1,1,1,'2020-08-06 05:39:45','2020-08-06 05:39:45','undefined','undefined',NULL,NULL,NULL,NULL),(11,1,20,1,1,1,1,1,1,1,'2020-08-06 05:52:15','2020-08-06 05:52:15','undefined','undefined',NULL,NULL,NULL,NULL),(12,1,21,1,1,1,1,1,1,1,'2020-08-06 06:20:00','2020-08-06 06:20:00','undefined','undefined',NULL,NULL,NULL,NULL),(13,1,22,1,1,1,1,1,1,1,'2020-08-06 06:36:11','2020-08-06 06:36:11','undefined','undefined',NULL,NULL,NULL,NULL),(14,1,23,1,1,1,1,1,1,1,'2020-08-06 06:55:08','2020-08-06 06:55:08','undefined','undefined',NULL,NULL,NULL,NULL),(15,1,24,1,1,1,1,1,1,1,'2020-08-06 07:03:26','2020-08-06 07:03:26','undefined','undefined',NULL,NULL,NULL,NULL),(16,1,25,1,1,1,1,1,1,1,'2020-08-06 07:14:43','2020-08-06 07:14:43','undefined','undefined',NULL,NULL,NULL,NULL),(17,1,26,1,1,1,1,1,1,1,'2020-08-06 07:17:03','2020-08-06 07:17:03','undefined','undefined',NULL,NULL,NULL,NULL),(18,1,27,1,1,1,1,1,1,1,'2020-08-06 07:17:52','2020-08-06 07:17:52','undefined','undefined',NULL,NULL,NULL,NULL),(19,1,28,1,1,1,1,1,1,1,'2020-08-06 07:33:59','2020-08-06 07:33:59','undefined','undefined',NULL,NULL,NULL,NULL),(20,1,29,1,1,1,1,1,1,1,'2020-08-06 07:44:51','2020-08-06 07:44:51','undefined','undefined',NULL,NULL,NULL,NULL),(21,1,30,1,1,1,1,1,1,1,'2020-08-06 07:47:21','2020-08-06 07:47:21','undefined','undefined',NULL,NULL,NULL,NULL),(22,1,33,1,1,1,1,1,1,1,'2020-08-06 11:12:22','2020-08-06 11:12:22','undefined','undefined',NULL,NULL,'2020-08-06',NULL),(23,1,35,1,1,1,1,1,1,1,'2020-08-06 11:26:13','2020-08-06 11:26:13','undefined','undefined',NULL,NULL,'2020-08-06',NULL),(24,1,36,1,1,1,1,1,1,1,'2020-08-06 11:28:51','2020-08-06 11:28:51','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(25,1,37,0,0,0,0,0,0,1,'2020-08-06 11:34:31','2020-08-06 11:34:31','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(26,1,38,0,0,0,0,0,0,1,'2020-08-06 11:38:30','2020-08-06 11:38:30','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(27,1,39,1,1,1,1,1,1,1,'2020-08-06 11:42:24','2020-08-06 11:42:24','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(28,1,40,1,1,1,1,1,1,1,'2020-08-06 11:42:25','2020-08-06 11:42:25','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(29,1,41,0,0,0,0,0,1,0,'2020-08-06 11:46:29','2020-08-06 11:46:29','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(30,1,42,0,0,0,0,0,1,0,'2020-08-06 11:46:30','2020-08-06 11:46:30','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(31,1,43,1,0,0,0,0,0,0,'2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(32,1,44,1,0,0,0,0,0,0,'2020-08-06 12:34:20','2020-08-06 12:34:20','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(33,1,45,0,0,0,1,0,1,0,'2020-08-06 12:39:10','2020-08-06 12:39:10','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(34,1,46,0,0,0,1,0,1,0,'2020-08-06 12:39:11','2020-08-06 12:39:11','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(35,1,47,0,0,0,0,0,0,1,'2020-08-06 12:39:12','2020-08-06 12:39:12','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(36,1,49,1,1,1,1,1,1,1,'2020-08-06 13:17:21','2020-08-06 13:17:21','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(37,1,51,1,1,1,1,1,1,1,'2020-08-06 13:20:18','2020-08-06 13:20:18','undefined','undefined',NULL,NULL,'2020-06-08',NULL),(38,1,53,0,0,0,0,0,0,1,'2020-08-06 13:30:49','2020-08-06 13:30:49','undefined','undefined',NULL,NULL,'2020-08-06',NULL),(39,1,54,0,0,0,0,0,0,1,'2020-08-06 13:31:58','2020-08-06 13:31:58','undefined','undefined',NULL,NULL,'2020-08-06',NULL),(40,1,56,1,1,1,1,1,1,1,'2020-08-06 13:49:21','2020-08-06 13:49:21','undefined','undefined',NULL,NULL,'2020-08-06',NULL),(41,1,57,0,0,1,0,0,0,0,'2020-08-06 13:49:23','2020-08-06 13:49:23','undefined','undefined',NULL,NULL,'2020-08-12',NULL);
/*!40000 ALTER TABLE `work_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `worker_building`
--

DROP TABLE IF EXISTS `worker_building`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `worker_building` (
  `id` int NOT NULL AUTO_INCREMENT,
  `worker_id` int NOT NULL,
  `building_id` int NOT NULL,
  `created_By` varchar(50) DEFAULT NULL,
  `deleted_By` varchar(50) DEFAULT NULL,
  `client_ip` varchar(200) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_build_id` (`building_id`),
  KEY `fk_workr_id` (`worker_id`),
  CONSTRAINT `fk_build_id` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`),
  CONSTRAINT `fk_workr_id` FOREIGN KEY (`worker_id`) REFERENCES `workers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `worker_building`
--

LOCK TABLES `worker_building` WRITE;
/*!40000 ALTER TABLE `worker_building` DISABLE KEYS */;
INSERT INTO `worker_building` VALUES (2,1,1,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `worker_building` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workers`
--

DROP TABLE IF EXISTS `workers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deletedAt` datetime DEFAULT NULL,
  `deletedBY` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workers`
--

LOCK TABLES `workers` WRITE;
/*!40000 ALTER TABLE `workers` DISABLE KEYS */;
INSERT INTO `workers` VALUES (1,'mukesh','9087654321',1,NULL,NULL);
/*!40000 ALTER TABLE `workers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-08  9:48:29
