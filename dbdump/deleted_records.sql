update customers
set status = 0
where id in (
    select customer_id
    from vehicles v
    -- join customers c on v.customer_id = c.id 
    where v.registration_no in (
    "MH01GH1234", 
    "TS01DS1948", 
    "MH1267", 
    "TN1213", 
    "HM567665", 
    "KR1234", 
    "AI12QU135", 
    "AY12QU789", 
    "D11258", 
    "AP10U1234", 
    "AP10RG1082", 
    "tes15em234", 
    "UA09E145", 
    "UA09E145", 
    "TS12HK6395", 
    "TS15EM0766", 
    "TS06RT", 
    "TS15EM0767", 
    "rapolu", 
    "rapolu", 
    "H13689", 
    "AP10RG1082", 
    "tes15em234", 
    "rapolu", 
    "202", 
    "1787", 
    "122", 
    "122", 
    "122", 
    "122", 
    "122", 
    "122", 
    "456", 
    "123", 
    "jjhj", 
    "78789T", 
    "70001", 
    "54177D", 
    "ap20fg5968", 
    "hf85uf0909", 
    "DU09GA1212", 
    "TS5678", 
    "AP09556", 
    "GJ123", 
    "HP456", 
    "MM123", 
    "TT", 
    "Fh56vh5676", 
    "Ap10hd5874", 
    "TS07HG5234", 
    "TS07HG5234", 
    "Du12AE3456", 
    "AP10HG5868", 
    "QI12AP3490", 
    "AP10DC5421", 
    "MH09FD1234", 
    "PB12KH0987", 
    "PB12KH0987", 
    "MH01GH9999", 
    "TS01HJ9342", 
    "TS01HJ9342", 
    "AP22AY3230", 
    "AP19TR0909", 
    "DU12UH09", 
    "DU09HG9061", 
    "HR09LU", 
    "TS091234", 
    "H50672", 
    "R45678", 
    "K77068/90072", 
    "82170", 
    "PA10IS5213", 
    "TS66DC", 
    "TS44DS", 
    "MH01BN6852", 
    "RJ123123", 
    "MH07IK1290", 
    "TS98", 
    "112TS", 
    "UIKJSDFLJL", 
    "hshshhsh", 
    "Ap10hd5874", 
    "vxhcgzfxg", 
    "TS09GH12", 
    "mcxvgzcvbb57ioo9", 
    "Ap123456", 
    "BH09NK85", 
    "PB09Jk9882", 
    "KL01GT8978", 
    "JK01UK8001", 
    "DU09JH8799", 
    "DU09JH8799", 
    "PR90DY5632", 
    "HR09KJ98", 
    "UP90HG00", 
    "MP90JF87", 
    "GH09BH36", 
    "TS01Jk9841", 
    "MG1234", 
    "KS1234", 
    "HR0987", 
    "PJ1222", 
    "TS09JU3451", 
    "H50672", 
    "TM123", 
    "AP0869", 
    "767hhh", 
    "AP563", 
    "AP32", 
    "AP32", 
    "hsjchhxj274848", 
    "jjghdhdh3", 
    "KL09HG9893", 
    "MP09GT9822", 
    "UP12345", 
    "RR1234", 
    "TT09876", 
    "DU12AW9876", 
    "TN09JK9898", 
    "AP05NB68", 
    "AP09PN0702", 
    "AP01KG0967", 
    "UA45GH09"
    )
);


---------------------
+------------------+-------------+
| registration_no  | customer_id |
+------------------+-------------+
| DU09GA1212       |         136 |
| MH01GH1234       |         137 |
| TS01DS1948       |         138 |
| AP10RG1082       |         139 |
| MH07IK1290       |         140 |
| UA09E145         |         141 |
| TS15EM0766       |         142 |
| ap20fg5968       |         143 |
| hf85uf0909       |         144 |
| TS15EM0767       |         145 |
| Fh56vh5676       |         146 |
| PA10IS5213       |         147 |
| TS06RT           |         142 |
| TS07HG5234       |         148 |
| Du12AE3456       |         149 |
| rapolu           |         150 |
| rapolu           |         151 |
| AP10HG5868       |         152 |
| 202              |         153 |
| QI12AP3490       |         154 |
| 1787             |         155 |
| 122              |         156 |
| 122              |         157 |
| 122              |         158 |
| 122              |         159 |
| 122              |         160 |
| 122              |         161 |
| 456              |         170 |
| 123              |         171 |
| AI12QU135        |         172 |
| AY12QU789        |         173 |
| jjhj             |         174 |
| AP10DC5421       |         175 |
| TS66DC           |         176 |
| TS44DS           |         143 |
| MH01BN6852       |         177 |
| TS12HK6395       |         178 |
| MH09FD1234       |         179 |
| PB12KH0987       |         179 |
| PB12KH0987       |         179 |
| MH01GH9999       |         180 |
| TS01HJ9342       |         180 |
| TS01Jk9841       |         181 |
| TS091234         |         182 |
| Ap10hd5874       |         183 |
| DU12UH09         |         184 |
| tes15em234       |         185 |
| H50672           |         186 |
| R45678           |         187 |
| AP22AY3230       |         188 |
| UA45GH09         |         202 |
| TS98             |         140 |
| DU09HG9061       |         203 |
| TS5678           |         138 |
| AP09556          |         138 |
| GJ123            |         138 |
| HP456            |         138 |
| MH1267           |         137 |
| AP19TR0909       |         179 |
| AP09PN0702       |         207 |
| AP01KG0967       |         207 |
| MM123            |         138 |
| TT               |         138 |
| TN1213           |         137 |
| HM567665         |         137 |
| AP05NB68         |         208 |
| TN09JK9898       |         209 |
| KR1234           |         137 |
| KL09HG9893       |         210 |
| MP09GT9822       |         210 |
| DU12AW9876       |         211 |
| TM123            |         147 |
| RR1234           |         207 |
| TT09876          |         207 |
| UP12345          |         210 |
| MG1234           |         186 |
| KS1234           |         186 |
| HR0987           |         186 |
| PJ1222           |         186 |
| PB09Jk9882       |         212 |
| KL01GT8978       |         212 |
| JK01UK8001       |         212 |
| DU09JH8799       |         213 |
| PR90DY5632       |         214 |
| HR09KJ98         |         215 |
| GH09BH36         |         216 |
| UP90HG00         |         217 |
| MP90JF87         |         217 |
| BH09NK85         |         218 |
| hshshhsh         |         226 |
| vxhcgzfxg        |         227 |
| UIKJSDFLJL       |         228 |
| TS09GH12         |         230 |
| AP0869           |         231 |
| 767hhh           |         232 |
| mcxvgzcvbb57ioo9 |         233 |
| Ap123456         |         234 |
| TS09JU3451       |         235 |
| AP563            |         236 |
| AP32             |         237 |
| AP32             |         238 |
| hsjchhxj274848   |         239 |
| jjghdhdh3        |         240 |
| HR09LU           |         243 |
| RJ123123         |         210 |
| 112TS            |         252 |
| D11258           |         229 |
| H13689           |         139 |
| AP10U1234        |         256 |
| 78789T           |         323 |
| 70001            |         324 |
| 54177D           |         329 |
| K77068/90072     |         689 |
| 82170            |        1142 |
+------------------+-------------+
