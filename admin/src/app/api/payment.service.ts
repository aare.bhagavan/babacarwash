import { Injectable } from '@angular/core';
import { REQUEST, PaymentRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { R } from '@angular/cdk/keycodes';
import * as moment from 'moment';

@Injectable({
    providedIn: 'root'
})
export class PaymentService extends NetworkService {

    constructor(protected httpClient: HttpClient, public spinner: NgxSpinnerService) {
        super(httpClient, spinner);
    }

    async getAllPaymentDetails() {
        const request = new REQUEST(PaymentRequest.GET_ALL_PAYMENT_DETAILS);
        return this.request(request);
    }

    async userPaymentDetails(data: number, type: string) {
        const request = new REQUEST(PaymentRequest.GET_USER_PAYMENT_DETAILS, { data, type }, REQUEST.POST);
        return (await this.request(request))
    }

    async adminFeedBack(data: any, feedback: string, type: string) {
        const request = new REQUEST(PaymentRequest.ADMIN_FEED_BACK, { data, feedback, type }, REQUEST.POST);
        return (await this.request(request))
    }

    async payAmount(value, note, data) {
        let user_name = localStorage.getItem("user_name");
        let token = localStorage.getItem("token");
        const request = new REQUEST(PaymentRequest.PAY_AMOUNT, { value, note, data, user_name, token }, REQUEST.POST);
        return (await this.request(request))
    }
    async renewSchedules(details: any) {
        let data = {
            schedule_id: details.schedule_id,
            schedules: details.schedules
        }
        const request = new REQUEST(PaymentRequest.RENEW_SCHEDULES, data, REQUEST.POST);
        return (await this.request(request))
    }
    async buildingpaymentsdetails(data: any) {
        console.log(data)
        // let datajson={"id":data}
        let reqData = {
            id: data.id,
            fromdate: moment(data.fromdate).subtract(1,'day').format('YYYY-MM-DD'),
            todate: moment(data.todate).add(1,'day').format('YYYY-MM-DD')
        }

        const request = new REQUEST(PaymentRequest.GET_BUILDING_PAYMENTS, reqData, REQUEST.POST)
        return (await this.request(request))

    }
    async workerPaymentsDetails(data:any){
        console.log(data)
        // let datajson={"id":data}
        let reqData = {
            id: data.id,
            fromdate: moment(data.fromdate).subtract(1,'day').format('YYYY-MM-DD'),
            todate: moment(data.todate).add(1,'day').format('YYYY-MM-DD')
        }

        const request=new REQUEST(PaymentRequest.GET_WORKER_PAYMENTS,reqData,REQUEST.POST)
        return (await this.request(request))

    }

}