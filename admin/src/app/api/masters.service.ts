import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, MasterRequest, BusinessRequest, AuthRequest, AreaRequest, SymptomsRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
    providedIn: 'root'
})
export class MastersService extends NetworkService {

    constructor(
        protected httpClient: HttpClient,
        public spinner: NgxSpinnerService
    ) {
        super(httpClient,
            spinner
        );
    }
    async getLocationsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_LOCATIONS);
        return this.request(request);
    }
    async getAllworkerBuildings() {
        const request = new REQUEST(MasterRequest.GET_ALL_WORKER_BUILDINGS_LOCATIONS);
        return this.request(request);
    }
    async getUsersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_USERS);
        return this.request(request);
    }
    async getAllCustomer() {
        const request = new REQUEST(MasterRequest.GET_ALL_CUSTOMERS);
        return this.request(request);
    }
    async getAllEnquiredCustomer(type) {
        const request = new REQUEST(MasterRequest.GET_ALL_ENQUIRED_CUSTOMERS, {type: type}, REQUEST.POST);
        return this.request(request);
    }
    async getCustomerDetailsbyVeh(vehicle:any) {
        let data = {
            vehicle_id: vehicle
        }
        const request = new REQUEST(MasterRequest.GET_CUSTOMER_DETAILS_BY_VEH,data,REQUEST.POST);
        return this.request(request);
    }
    async getWorkDetails(vehicle:any) {
        let data = {
            vehicle_id: vehicle.vehicle_id,
            startDate:vehicle.startDate
        }
        const request = new REQUEST(MasterRequest.GET_WORK_DETAILS,data, REQUEST.POST);
        return this.request(request);
    }

    async updateWorkStatus(details:any) {
        let schedules=[];
        schedules[0]=details.sunday;
        schedules[1]=details.monday;
        schedules[2]=details.tuesday;
        schedules[3]=details.wednesday;
        schedules[4]=details.thursday;
        schedules[5]=details.friday;
        schedules[6]=details.saturday;
        let data = {
            schedule_id: details.schedule_id,
            date:details.date,
            schedules:schedules
        }
        const request = new REQUEST(MasterRequest.UPDATE_WORK_STATUS,data, REQUEST.POST);
        return this.request(request);
    }
    
    async getBuildingsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_BUILDINGS);
        return this.request(request);
    }
    async addNewBuilding(data: any) {
        const request = new REQUEST(MasterRequest.ADD_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async addCustomerDetails(custdata: any, weekDays: any, vehicles: any, month: number, year: number, scheduleType: string) {
        vehicles.forEach(vehicle => {
            vehicle.startDate = moment(vehicle.startDate).format("YYYY-MM-DD")
        });
        let user_name = localStorage.getItem("user_name");
        let data = {
            custDetails: custdata,
            weekDays: weekDays,
            vehicles: vehicles,
            month: month,
            year: year,
            scheduleType: scheduleType,
            user_name:user_name
        }
        const request = new REQUEST(MasterRequest.ADD_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }

    async editCustomerDetails(cust_id: number, custDetails: any, addedVehicles: any, editedVehicles: any, removedvehicles: any) {
        addedVehicles.forEach(vehicle => {
            vehicle.startDate = moment(vehicle.startDate).format("YYYY-MM-DD")
        });
        editedVehicles.forEach(vehicle => {
            vehicle.startDate = moment(vehicle.startDate).format("YYYY-MM-DD")
        });
        removedvehicles.forEach(vehicle => {
            vehicle.startDate = moment(vehicle.startDate).format("YYYY-MM-DD")
        });
        let data = {
            cust_id:cust_id,
            custDetails: custDetails,
            addedVehicles: addedVehicles,
            editedVehicles: editedVehicles,
            removedvehicles: removedvehicles
        }
        const request = new REQUEST(MasterRequest.EDIT_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async selfCustomerDetails(custdata: any, weekDays: any, vehicles: any, month: number, year: number, scheduleType: string) {
        vehicles.forEach(vehicle => {
            vehicle.startDate = moment(vehicle.startDate).format("YYYY-MM-DD")
        });
        let user_name = localStorage.getItem("user_name");
        let data = {
            custDetails: custdata,
            weekDays: weekDays,
            vehicles: vehicles,
            month: month,
            year: year,
            scheduleType: scheduleType,
            user_name:user_name
        }
        const request = new REQUEST(MasterRequest.SELF_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editBuilding(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async getWorkersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_WORKERS);
        return this.request(request);
    }
    // async getEditBuildings() {
    //     const request = new REQUEST(MasterRequest.GET_EDIT_BUILDINGS);
    //     return this.request(request);
    // }
    async addNewWorker(workerdata: any,user_name,workerBuildingData,date) {
        const data = {
            'workerData':workerdata,
            'user_name':user_name,
            'building_ids':workerBuildingData,
            'start_date':date,
        }
        const request = new REQUEST(MasterRequest.ADD_NEW_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editWorker(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteBuilding(id: number, user_name: any) {
        const data = {
            id: id,
            user_name: user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteWorker(id: number, user_name: any) {
        const data = {
            id: id,
            user_name: user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteCustomer(id: any, user_name: any) {
        const data = {
            id: id.cust_id,
            user_name: user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async activateCustomer(id: any, user_name: any,activateDate:any) {
        const data = {
            id: id,
            user_name: user_name,
            activateDate:activateDate
        }
        const request = new REQUEST(MasterRequest.ACTIVATE_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async activateWorker(id: any) {
        const data = {
            id: id
        }
        const request = new REQUEST(MasterRequest.ACTIVATE_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteUser(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_USER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async getCarwashRecords(fromDate:any,toDate:any){
        let data={
            fromDate:fromDate,
            toDate:toDate
        }
        const request = new REQUEST(MasterRequest.GET_CARWASH_RECORDS,data,REQUEST.POST);
        return this.request(request);
    }
    async getPCarwashRecords(fromDate:any,toDate:any){
        let data={
            fromDate:fromDate,
            toDate:toDate
        }
        const request = new REQUEST(MasterRequest.GET_PCARWASH_RECORDS,data,REQUEST.POST);
        return this.request(request);
    }
    async getCarDetailsOfScheduleDate(date){
        let data={schedule_date:date}
        const request = new REQUEST(MasterRequest.GET_CAR_DETAILS,data,REQUEST.POST);
        return this.request(request);
    }
    async getPCarDetailsOfScheduleDate(date){
        let data={schedule_date:date}
        const request = new REQUEST(MasterRequest.GET_PCAR_DETAILS,data,REQUEST.POST);
        return this.request(request);
    }
    async getScheduleById(schedule_id: number) {
        const data = {
            schedule_id: schedule_id
        }
        const request = new REQUEST(MasterRequest.GET_SCHEDULE_BY_ID, data, REQUEST.POST);
        return (await this.request(request))
    }
    async getWashedCarsReport(data:any) {
        let reqData = {
            id: data.id,
            fromDate: moment(data.fromdate).format('YYYY-MM-DD'),
            toDate: moment(data.todate).format('YYYY-MM-DD')
        }
        const request = new REQUEST(MasterRequest.GET_WASHED_CARS, reqData, REQUEST.POST);
        return (await this.request(request))
    }
    async getCustomerByData(mobile: string, email: string) {
        const request = new REQUEST(MasterRequest.GET_CUSTOMER_BY_DATA, {mobile, email}, REQUEST.POST);
        return (await this.request(request))
    }
    async setAllEnquiriesAsRead(type) {
        const request = new REQUEST(MasterRequest.SET_ALL_ENQUIRED_CUSTOMERS_AS_READ, {type}, REQUEST.POST);
        return (await this.request(request))
    }
}
