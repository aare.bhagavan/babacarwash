import { ImportMembersService } from './main/apps/importutil/members/members.service';
import { ImportMembersComponent } from './main/apps/importutil/members/members.component';
// import { BusinessComponent } from './main/apps/business/business/business.component';
// import { EcommerceProductsService } from './main/apps/e-commerce/products/products.service';
// import { EcommerceProductsComponent } from 'app/main/apps/e-commerce/products/products.component';
import { RegistrationComponent } from './main/apps/e-commerce/registration/registration.component';
import { LoginModule } from './login/login.module';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';
import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';

import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';
import { fuseConfig } from 'app/fuse-config';

import { FakeDbService } from 'app/fake-db/fake-db.service';
import { AppComponent } from 'app/app.component';
import { AppStoreModule } from 'app/store/store.module';
import { LayoutModule } from 'app/layout/layout.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { AddSymptomsDailogComponent } from '../app/main/apps/e-commerce/add-symptoms-dailog/add-symptoms-dailog.component'
import { MatSortModule } from '@angular/material/sort';
import {
    MatButtonModule, MatCheckboxModule, MatFormFieldModule, MatInputModule, MatRadioModule, MatIconModule,
    MatStepperModule, MatAutocompleteModule, MatDatepickerModule, MatSelectModule, MatNativeDateModule,
    MatTableModule, MatTabsModule, MatOptionModule, MatCardModule, MatGridListModule,
    MatDividerModule, MatPaginatorModule, MatExpansionModule, MatListModule, MatButtonToggleModule,
    MatToolbarModule, MatProgressBarModule, MatMenuModule, MatSnackBarModule, MatBadgeModule

} from '@angular/material';

// import {MatCheckboxModule} from '@angular/material/checkbox';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";
import { MatChipsModule, MatChipInputEvent } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { ChartsModule } from 'ng2-charts';

// import { EditPersonDialogComponent } from '../app/main/apps/e-commerce/edit-person-dialog/edit-person-dialog.component';
import { APIServiceModule } from '../app/api/api.module';

import { EnterOtpDialogComponent } from '../app/main/apps/e-commerce/enter-otp-dialog/enter-otp-dialog.component'
import { TokenInterceptor } from './api/token.interceptor';
// import {EcommerceProductComponent} from '../app/main/apps/e-commerce/product/product.component'

import { NgxSpinnerModule } from "ngx-spinner";


import { AgmCoreModule } from '@agm/core';
import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { CustomersComponent } from './customers/customers.component';
import { LocationComponent } from './location/location.component';
import { BuldingsComponent } from './buldings/buldings.component';
import { AddBuildingsComponent } from './add-buildings/add-buildings.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { WorkersComponent } from './workers/workers.component';
import { AddEditWorkerComponent } from './add-edit-worker/add-edit-worker.component';
import { PaymentScreenComponent } from './payment-screen/payment-screen.component';
import { PaymentTransactionComponent } from './payment-transaction/payment-transaction.component';
import { WorkListComponent } from './work-list/work-list.component';
import { UsersComponent } from './users/users.component';
import { ActivateCustomerComponent } from './activate-customer/activate-customer.component';
import { CarsDetailsDialogComponent } from './main/apps/dashboards/analytics/analytics.component';
import { PaymentReportComponent } from './Payment-Report/payment-report/payment-report.component';
import { SatNativeDateModule, SatDatepickerModule, MAT_DATE_LOCALE } from 'saturn-datepicker';
import { RegisterCustomerModule } from './register-customer/registerCustomer.module';
import { EnquiredCustomersComponent } from './enquired-customers/enquired-customers.component';

const appRoutes: Routes = [
    {
        path: '', redirectTo: 'dashboard', pathMatch: 'full'
    },
    {
        path: 'dashboard',
        loadChildren: () => import('./main/apps/dashboards/analytics/analytics.module').then(model => model.AnalyticsDashboardModule)
    },
    {
        path: 'members',
        loadChildren: () => import('./main/apps/e-commerce/e-commerce.module').then(model => model.EcommerceModule)
    },
    {
        path: 'registration',
        component: RegistrationComponent,
    },
    {
        path: 'customers',
        component: CustomersComponent,
    },
    {
        path: 'enquiries/new',
        component: EnquiredCustomersComponent,
    },
    {
        path: 'enquiries/existing',
        component: EnquiredCustomersComponent,
    },
    {
        path: 'enquiries/complaints',
        component: EnquiredCustomersComponent,
    },
    {
        path: 'locations',
        component: LocationComponent,
    },
    // {
    //     path: 'customers/registration',
    //     component: RegisterCustomerComponent,
    // },
    {
        path: 'buildings',
        component: BuldingsComponent,
    },
    {
        path: 'workers',
        component: WorkersComponent,
    },
    // {
    //     path: 'payment',
    //     component: PaymentScreenComponent,
    // },
    {
        path: 'users',
        component: UsersComponent,
    },
    {
        path: 'paymentreport',
        component: PaymentReportComponent,
    },
    {
        path:'payment/all',
        component:PaymentScreenComponent,
    },
    {
        path:'payment/pending',
        component:PaymentScreenComponent,
    },
    {
        path:'payment/renew',
        component:PaymentScreenComponent,
    },

    // {
    //     path: 'organization',
    //     component: OrganizationComponent,
    // },
    // {
    //     path: 'members',
    //     component: EcommerceProductsComponent,
    //     resolve: {
    //         data: EcommerceProductsService
    //     }
    // },
    // {
    //     path: 'business',
    //     component: BusinessComponent,
    // },
    // {
    //     path: 'super-business',
    //     component: ListSuperBusinessComponent,
    // },
    // {
    //     path: 'import',
    //     component: ImportMembersComponent,
    //     resolve: {
    //         data: ImportMembersService
    //     }
    // },
    // {
    //     path: 'areas',
    //     component: ArealistComponent,
    // },

    // {
    //     path: 'users',
    //     component: UsersComponent,
    // },

    {
        path: '**',
        redirectTo: 'business/apps/e-commerce/products'
    },

];

@NgModule({
    declarations: [
        AppComponent,
        // AddSymptomsDailogComponent,
        // EditPersonDialogComponent,
        // EditBusinessComponent,
        // AddEditSuperBusinessComponent,
        EnterOtpDialogComponent,
        RegistrationComponent,
        // EcommerceProductsComponent,
        // BusinessComponent,
        // ListSuperBusinessComponent,
        ImportMembersComponent,
        CustomersComponent,
        LocationComponent,
        BuldingsComponent,
        AddBuildingsComponent,
        AddCustomerComponent,
        WorkersComponent,
        AddEditWorkerComponent,
        PaymentScreenComponent,
        PaymentTransactionComponent,
        WorkListComponent,
        UsersComponent,
        ActivateCustomerComponent,
        PaymentReportComponent,
        // ArealistComponent,
        // EditAreaDialogComponent,
        // AddAreaDialogComponent,
        // UsersComponent,
        //  SymptonsListDialogComponent,
        // EcommerceProductComponent
        CarsDetailsDialogComponent,
        EnquiredCustomersComponent

    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        ChartsModule,
        SatDatepickerModule,
        SatNativeDateModule,

        MatDialogModule,
        RouterModule.forRoot(appRoutes),

        TranslateModule.forRoot(),
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay: 0,
            passThruUnknownUrl: true
        }),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MatButtonModule,
        MatIconModule,
        FormsModule,
        ReactiveFormsModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,
        MatInputModule,
        MatFormFieldModule,
        MatCheckboxModule,
        MatStepperModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatTableModule,
        MatTabsModule,
        MatOptionModule,
        MatCardModule,
        MatGridListModule,
        MatDividerModule,
        MatPaginatorModule,
        MatExpansionModule,
        MatListModule,
        MatButtonToggleModule,
        MatToolbarModule,
        MatProgressBarModule,
        MatMenuModule,
        MatSnackBarModule,
        MatChipsModule,
        MatCardModule,
        MatProgressSpinnerModule,
        MatBadgeModule,

        // App modules
        LayoutModule,
        AppStoreModule,
        RegisterCustomerModule,

        LoginModule,

        //services
        APIServiceModule,
        NgxSpinnerModule,
        MatSortModule,

        MatGoogleMapsAutocompleteModule.forRoot(),
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD3zC0kD5-3rNM6j4ThtxZEgRf-Xc0qlEI',
            libraries: ['places']
        }),

    ],
    entryComponents: [
        // AddSymptomsDailogComponent,
        // EditPersonDialogComponent,
        // EditBusinessComponent,
        // AddEditSuperBusinessComponent,
        EnterOtpDialogComponent,
        LocationComponent,
        BuldingsComponent,
        AddCustomerComponent,
        AddBuildingsComponent,
        WorkersComponent,
        AddEditWorkerComponent,
        PaymentTransactionComponent,
        WorkListComponent,
        ActivateCustomerComponent,
        CarsDetailsDialogComponent
        //     EditAreaDialogComponent,
        //     AddAreaDialogComponent,
        //    SymptonsListDialogComponent,

        // EcommerceProductComponent
    ],
    bootstrap: [
        AppComponent
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        // UploadService,
        // EcommerceProductsService,
        ImportMembersService,
        {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
})
export class AppModule {
}
