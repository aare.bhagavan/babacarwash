import { Component, OnDestroy, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FuseUtils } from '@fuse/utils';

import { Router, ActivatedRoute } from '@angular/router';
// import{routes} from '../../apps.module'
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { MastersService } from 'app/api/masters.service';
import Swal from 'sweetalert2';
import { MatChipInputEvent } from '@angular/material/chips';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import * as moment from 'moment';
import * as _ from "underscore";


// tslint:disable-next-line:no-duplicate-imports
// import {default as _rollupMoment} from 'moment';

// const moment = _rollupMoment || _moment;
import { worker } from 'cluster';
import { MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

export interface DialogData {
  customer: any;
  edit: boolean;
  enquiry?: boolean;
  doesExist?: boolean;

}
export interface vehicle {
  number: string;
}
export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
  providers: [

    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },

    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
  ],
})
export class AddCustomerComponent implements OnInit {
  customerForm: FormGroup;
  vehicles: vehicle[] = [];
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  weekdays: any[] = [
    { day: 'Mon', value: false },
    { day: 'Tue', value: false },
    { day: 'Wed', value: false },
    { day: 'Thu', value: false },
    { day: 'Fri', value: false },
    { day: 'Sat', value: false },
    { day: 'Sun', value: false },
  ];
  cleaner_id: number = null;
  scheduleType: string = '';
  startDate: any = moment();
  allBuildings: any;
  parking_no: string = '';
  filteredLocations: any = [];
  allLocations: any;
  filteredBuildings: any = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  building_id: any = [];
  weekDays: any;
  date: Date = new Date();
  month: number;
  year: number;
  allWorkers: any;
  newvehicles: any = [];
  vehicleDetals: any = [];
  vehicleIndex: number = -1;
  isShowPrev: boolean;
  vehIndex: number;
  isShowNxt: boolean;
  isAddNxt: boolean = true;
  filterWorkers: any = [];
  removedVehicles: any = [];
  addedVehicles: any = [];
  cust_id: number;
  AllworkerBuildings: any = [];
  subscription_amount: number = null;
  noWorkers: boolean = false;
  vehicleStart_date: any;
  startDateChanged: boolean[] = [];
  scheduleChanged: boolean[] = [];
  vehicleSchedule: any;
  maxDate;
  constructor(private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public mastersService: MastersService,
    public dialogRef: MatDialogRef<AddCustomerComponent>) { dialogRef.disableClose = true; }

  tabs = ['First', 'Second', 'Third'];
  selected = new FormControl(0);

  addTab(selectAfterAdding: boolean) {
    this.tabs.push('New');

    if (selectAfterAdding) {
      this.selected.setValue(this.tabs.length - 1);
    }
  }
  

  removeTab(index: number) {
    this.tabs.splice(index, 1);
  }

  ngOnInit() {
    this.customerForm = this._formBuilder.group({
      firstName: ['', Validators.required],
      lastName: [''],
      mobile: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.pattern("[0-9][0-9]{9}$")])],
      email: ['', Validators.compose([Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")])],
      password: ['', Validators.required],
      confirm_password: ['', Validators.required],
      location: [{}, Validators.required],
      building: [null, Validators.required],
      startDate: [moment(), Validators.required],
      flat_no: ['']
    });
    this.initialize();
  }
  async initialize() {
    this.getBuildings();
    this.getAllworkerBuildings();
    this.month = this.date.getMonth() + 1;
    this.year = this.date.getFullYear();
    console.log('weekdays1111', this.weekdays);
    this.getAllLocations();
    await this.getAllWorkers();
    this.getScheduleById();

    this.filteredLocations = this.customerForm.controls.location.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterlocation(value))
      );


    this.customerForm.controls.location.valueChanges.subscribe(value => { this.filterBuilding(value) })
    this.customerForm.controls.building.valueChanges.subscribe(value => { this.filterWorker(value) })

  }

  setCustomerValues() {
    console.log('editable cust details', this.data.customer);
    this.customerForm.controls.firstName.setValue(this.data.customer.first_name)
    this.customerForm.controls.lastName.setValue(this.data.customer.last_name)
    this.customerForm.controls.mobile.setValue(this.data.customer.mobile)
    this.customerForm.controls.email.setValue(this.data.customer.email)
    this.customerForm.controls.flat_no.setValue(this.data.customer.flat_no)
    this.customerForm.controls.location.setValue(this.data.customer.location_id)
    this.customerForm.controls.building.setValue(this.data.customer.building_id)
    this.filterWorker(this.data.customer.building_id);
    this.customerForm.controls.password.setValue(this.data.customer.password);
    this.customerForm.controls.confirm_password.setValue(this.data.customer.password);
    let obj = {}
    let vehicles = [];
    vehicles = this.data.customer.vehicles
    this.cust_id = this.data.customer.cust_id;

    if(!vehicles) {
      return;
    }
    vehicles.forEach(element => {
      // console.log('startDate', moment(element.startDate));
      // element.startDate = this.startDate  ;
      let isMonday = element.weekdays.filter(day => day.day == 'Mon').length == 1;
      if (!isMonday) {
        // element.weekdays.push({ day: 'Mon', value: false })
        element.weekdays.splice(0, 0, { day: 'Mon', value: false })
      }
      let isTuesday = element.weekdays.filter(day => day.day == 'Tue').length == 1;
      if (!isTuesday) {
        // element.weekdays.push({ day: 'Tue', value: false })
        element.weekdays.splice(1, 0, { day: 'Tue', value: false })
      }
      let isWednesday = element.weekdays.filter(day => day.day == 'Wed').length == 1;
      if (!isWednesday) {
        // element.weekdays.push({ day: 'Wed', value: false })
        element.weekdays.splice(2, 0, { day: 'Wed', value: false })
      }
      let isThursday = element.weekdays.filter(day => day.day == 'Thu').length == 1;
      if (!isThursday) {
        // element.weekdays.push({ day: 'Thu', value: false })
        element.weekdays.splice(3, 0, { day: 'Thu', value: false })
      }
      let isFriday = element.weekdays.filter(day => day.day == 'Fri').length == 1;
      if (!isFriday) {
        // element.weekdays.push({ day: 'Fri', value: false })
        element.weekdays.splice(4, 0, { day: 'Fri', value: false })
      }
      let isSatday = element.weekdays.filter(day => day.day == 'Sat').length == 1
      if (!isSatday) {
        // element.weekdays.push({ day: 'Sat', value: false })
        element.weekdays.splice(5, 0, { day: 'Sat', value: false })
      }
      let isSunday = element.weekdays.filter(day => day.day == 'Sun').length == 1;
      if (!isSunday) {
        // element.weekdays.push({ day: 'Sun', value: false })
        element.weekdays.splice(6, 0, { day: 'Sun', value: false })
      }
      element['scheduleType'] = 'weekly';
      obj['parking_no'] = element.parking_no;
      obj['cleaner_id'] = element.cleaner_id;
      obj['scheduleType'] = 'weekly';
      obj['vehicles'] = [element];
      obj['startDate'] = moment(element.startDate);
      obj['weekdays'] = element.weekdays;
      obj['subscription_amount'] = element.subscription_amount;
      obj['end_date'] = element.end_date;
      this.vehicleDetals.push(obj);
      obj = {}
    });

    console.log('vehicles date', this.vehicleDetals)

  }

  filterBuilding(value) {
    if (this.allBuildings != undefined) {
      this.filteredBuildings = this.allBuildings.filter(building => building.location_id == value)
    }
  }
  filterWorker(value) {
    if (this.allWorkers != undefined) {
      this.filterWorkers = this.allWorkers.filter(worker => (worker.building_id == value) && (worker.end_date == null))
      console.log('filtered workers', this.filterWorkers)
      if (this.filterWorkers.length == 0) {
        this.noWorkers = true;
      } else this.noWorkers = false;
    }

  }
  addchip(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.vehicles.push({ number: value.trim() });
    }

    const index = this.addedVehicles.indexOf(value, 0);  //for edit 
    if (index > -1) {                                      //for edit 
      this.addedVehicles.splice(index, 1);                //for edit 
    } else if (value != "") {
      this.addedVehicles.push({ number: value.trim() }) // for edit
    }


    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addchipSelectedVehicle(event: MatChipInputEvent, selectedVehicles: any): void {
    console.log(selectedVehicles);
    const input = event.input;
    const value = event.value;
    const index = this.removedVehicles.indexOf(value, 0);  //for edit 
    if (index > -1) {                                      //for edit 
      this.removedVehicles.splice(index, 1);                //for edit 
    }                                                       //for edit 
    // Add our fruit
    if ((value || '').trim()) {
      selectedVehicles.vehicles.push({ number: value.trim() });
    }
    const index1 = this.addedVehicles.indexOf(value, 0);  //for edit 
    if (index1 > -1) {                                      //for edit 
      this.addedVehicles.splice(index, 1);                //for edit 
    } else if (value != "") {
      this.addedVehicles.push({ number: value.trim() }) // for edit
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  onChange(scheduleType) {
    this.scheduleType = scheduleType;
    console.log(this.scheduleType);
  }


  OnDateChange(selectedDate, vehicle) {
    let vehicle_id=vehicle.vehicles[0].vehicle_id;
    this.vehicleStart_date = this.getStartDate(vehicle_id);
    let formatedStartDate=moment(this.vehicleStart_date).format('YYYY-MM-DD');
    let selected_date=moment(selectedDate._d).format('YYYY-MM-DD');
    this.startDateChanged[vehicle_id] = false;
    if (moment(selected_date).isSame(formatedStartDate)) {
     
      this.scheduleChanged[vehicle_id]=false;
    } else {
      this.scheduleChanged[vehicle_id] = true;
      }
    }
  
  getStartDate(vehicle_id){
    for (let schedule in this.vehicleSchedule) {
      let data = this.vehicleSchedule[schedule].filter(vehilce => vehilce.vehicle_id == vehicle_id);
      if (data.length > 0) {
         console.log(data);
         return data[0].start_date;  
      }
  }
}


  remove(value: vehicle): void {
    const index = this.vehicles.indexOf(value);

    if (index >= 0) {
      this.vehicles.splice(index, 1);
    }
  }
  selectedChipremove(value: vehicle, selectedVehicles: any, allVehicles: any): void {
    const index = selectedVehicles.vehicles.indexOf(value);
    this.removedVehicles.push(value);
    if (index >= 0) {
      selectedVehicles.vehicles.splice(index, 1);
      if (selectedVehicles.vehicles.length == 0) {
        const index = allVehicles.indexOf(selectedVehicles);
        allVehicles.splice(index, 1);
        // selectedVehicles=[];
      }
    }
  }

  async getBuildings() {
    (await this.mastersService.getBuildingsList()).subscribe((response: any) => {

      console.log("all Buildings", response)
      this.allBuildings = response;
      this.filteredBuildings = response;

    });
  }

  async getScheduleById() {
    let scheduleArray = [];
    const schedules = this.data.customer.vehicles.filter(vehicle => vehicle.end_date == null);
    for (let schedule of schedules) {
      (await this.mastersService.getScheduleById(schedule.work_schedule_id)).subscribe((response: any) => {

        console.log("schedule", response)
        scheduleArray.push(response);
      });
    }
    this.vehicleSchedule = scheduleArray;
    console.log(this.vehicleSchedule);

  }

  toggle(day, currentSchedule, vehicleData) {
    let previousSchedule: any;
    let vehicle_id = vehicleData.vehicles[0].vehicle_id;
    for (let schedule in this.vehicleSchedule) {
      let data = this.vehicleSchedule[schedule].filter(vehilce => vehilce.vehicle_id == vehicle_id);
      if (data.length > 0) {
        previousSchedule = data;
      }

    }

    console.log('weekdays', day);
    console.log('weekdays1111', this.weekdays);
    this.scheduleChanged[vehicle_id] = false;
    if (this.scheduleHasChanged(previousSchedule, currentSchedule)) {
      this.startDateChanged[vehicle_id] = true;
    }
    else {
      this.startDateChanged[vehicle_id] = false;
    }

  }
 
  getStartDateChanged(vehicle_id) {
    if (vehicle_id == undefined) {
      return false;
    }
    if (this.startDateChanged[vehicle_id] == undefined) {
      this.startDateChanged[vehicle_id] = false;
    }
    return this.startDateChanged[vehicle_id];
  }
  getScheduleChanged(vehicle_id) {
    if (vehicle_id == undefined) {
      return false;
    }
    if (this.scheduleChanged[vehicle_id] == undefined) {
      this.scheduleChanged[vehicle_id] = false;
    }
    return this.scheduleChanged[vehicle_id];
  }
  scheduleHasChanged(prev: any, cur: any): boolean {
    let chg: boolean = false;
    // for (let i in cur) {
    //   if (cur[i].value == true) {
    //     cur[i].value = 1;
    //   } else cur[i].value = 0;
    // }
    console.log(cur);
    if ((prev[0].monday != (cur[0].value) ? 1 : 0) ||
      (prev[0].tuesday != (cur[1].value) ? 1 : 0) ||
      (prev[0].wednesday != (cur[2].value) ? 1 : 0) ||
      (prev[0].thursday != (cur[3].value) ? 1 : 0) ||
      (prev[0].friday != (cur[4].value) ? 1 : 0) ||
      (prev[0].saturday != (cur[5].value) ? 1 : 0) ||
      (prev[0].sunday != (cur[6].value) ? 1 : 0)) {
      chg = true;
    }
    return chg;
  }

  private _filterlocation(value: string): string[] {
    if (this.allBuildings != undefined && this.allLocations != undefined) {
      const filterValue = value.toLowerCase();
      // this.allBuildings = this.allBuildings.filter(option => option.address.toLowerCase().includes(filterValue));
      this.allLocations = this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
      return this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
    }
  }
  private _filterbuilding(value: string): string[] {
    if (this.allBuildings != undefined) {
      const filterValue = value.toLowerCase();
      // this.allLocations = this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
      return this.allBuildings.filter(option => option.name.toLowerCase().includes(filterValue));
    }

  }

  async getAllLocations() {
    (await this.mastersService.getLocationsList()).subscribe((response: any) => {

      console.log("all locations", response)
      this.allLocations = response;

    });
  }

  async getAllworkerBuildings() {
    (await this.mastersService.getAllworkerBuildings()).subscribe((response: any) => {

      console.log("all worker building mapping", response)
      this.AllworkerBuildings = response;

    });
  }

  filterCleaners() {

    // this.allWorkers=this.allWorkers.filter(worker=>)

  }
  async getAllWorkers() {
    const response = await (await this.mastersService.getWorkersList()).toPromise();
    console.log("all workers", response)
    this.allWorkers = response;
    if (this.data.edit || (this.data.enquiry && !this.data.customer.doesExist)) {
      this.setCustomerValues();
    }
    if(this.data.enquiry && this.data.customer.doesExist) {
      const custResponse = await this.getCustomerById(this.data.customer.mobile, this.data.customer.email);
      this.data.customer = this.parseCustomer(custResponse);
      this.setCustomerValues();
    }
  }

  async getCustomerById(mobile, email) {
    return (await this.mastersService.getCustomerByData(mobile, email)).toPromise();
  }

  parseCustomer(response) {
    let uniqueArray = _.map(_.groupBy(response, (obj: any) => {
      return obj.cust_id;
    }), (grouped) => {
      return grouped[0];
    });

    uniqueArray.forEach(customer => {
      customer['fullName']=customer.first_name+' '+customer.last_name;
      customer['building_name']=customer.building_id == 1 ? '' : customer.name;
      customer['location']=customer.location_id == 3 ? '' : customer.address;
      customer['vehicles'] = [];
      customer['email'] = customer.email ? customer.email : this.data.customer.email;
      let customerVehicles = response.filter(custvehicle => custvehicle.cust_id == customer.cust_id);

      customerVehicles.forEach((vehicle, index) => {
        if (vehicle.veh_id != null) {
          let obj = {
            end_date: vehicle.end_date,
            number: vehicle.registration_no,
            vehicle_id: vehicle.veh_id,
            cleaner_id: vehicle.worker_id,
            worker_name: vehicle.worker_name,
            parking_no: vehicle.parking_no,
            startDate: moment(vehicle.start_date).format("LL"),
            weekdays: [],
            subscription_amount:vehicle.subscription_amount,
            work_schedule_id: vehicle.work_schedule_id,
            
          }
          customer.vehicles.push(obj)
          if (vehicle.monday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Mon', value: true })
          } if (vehicle.tuesday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Tue', value: true })
          } if (vehicle.wednesday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Wed', value: true })
          } if (vehicle.thursday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Thu', value: true })
          } if (vehicle.friday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Fri', value: true })
          } if (vehicle.saturday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Sat', value: true })
          } if (vehicle.sunday == 1) {
            customer.vehicles[index].weekdays.push({ day: 'Sun', value: true })
          }
        }
      });

    });

    return uniqueArray[0];
  }

  async addCustomer() {
    console.log(this.vehicleDetals);
    this.newvehicles = [];
    this.vehicleDetals.forEach(detail => {
      // detail.vehicles
      let obj = {}

      let schedule = []
      if (detail.scheduleType == 'daily') {
        detail.weekdays.forEach(eachDay => {
          eachDay.value = true;
        });
      }
      detail.weekdays.forEach(day => {
        schedule.push(day.value == true ? 1 : 0)
      });
      detail.vehicles.forEach(veh => {
        this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
        let worker_building = this.AllworkerBuildings.filter(w_b => w_b.building_id == this.building_id[0].id && w_b.worker_id == detail.cleaner_id);
        let vehicleObj = {};
        vehicleObj['number'] = veh.number;
        vehicleObj['schedule'] = schedule;
        vehicleObj['parking'] = detail.parking_no;
        vehicleObj['cleaner_id'] = detail.cleaner_id;
        vehicleObj['worker_building_id'] = worker_building[0].id;
        vehicleObj['startDate'] = detail.startDate;
        vehicleObj['subscription_amount'] = detail.subscription_amount;
        this.newvehicles.push(vehicleObj)
      });
    });


    if (this.scheduleType == 'daily') {
      this.weekdays.forEach(eachDay => {
        eachDay.value = true;
      });
    }
    this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
    this.customerForm.value['building_id'] = this.building_id[0].id;
    (await this.mastersService.addCustomerDetails(this.customerForm.value, this.weekdays, this.newvehicles, this.month, this.year, this.scheduleType)).subscribe((response: any) => {
      console.log('result by adding customer', response)
      if (response.status == true || response.success == true) {
        Swal.fire({
          icon: 'success',
          title: "Customer Added"
        });
        this.dialogRef.close();
      } else if (response.result && response.result.customers != undefined) {
        if (response.result.customers.length > 0) {
          Swal.fire({
            icon: 'error',
            title: "Customer already exist"
          })
          

        } else if (response.result && response.result.vehicles.length > 0) {
          Swal.fire({
            icon: 'error',
            title: "Vehicle already exist"
          })

        }
      }
      console.log("all locations", response)
      this.allLocations = response;

    });
  }

  close() {
    this.dialogRef.close();
  }

  addVehicle() {
    // this.vehicleIndex = this.vehicleIndex + 1
    let obj = {}
    let vehicleObj = {};
    let schedule = []

    obj['vehicles'] = this.vehicles;
    obj['weekdays'] = this.weekdays;
    obj['cleaner_id'] = this.cleaner_id;
    obj['parking_no'] = this.parking_no;
    obj['scheduleType'] = this.scheduleType;
    obj['startDate'] = this.startDate;
    obj['subscription_amount'] = this.subscription_amount;
    this.vehicleDetals.push(obj);
    console.log('added vehicle list', this.vehicleDetals)
    if (this.scheduleType == 'daily') {
      this.weekdays.forEach(eachDay => {
        eachDay.value = true;
      });
    }
    this.weekdays.forEach(day => {
      schedule.push(day.value == true ? 1 : 0)
    });
    this.vehicles.forEach(veh => {
      vehicleObj['number'] = veh.number;
      vehicleObj['schedule'] = schedule;
      vehicleObj['parking'] = this.parking_no;
      vehicleObj['cleaner_id'] = this.cleaner_id;
      vehicleObj['startDate'] = this.startDate;
      vehicleObj['subscription_amount'] = this.subscription_amount;
      this.newvehicles.push(vehicleObj)
    });


    console.log('vehicle details', this.newvehicles)
    this.vehicles = [];
    this.weekdays = [
      { day: 'Mon', value: false },
      { day: 'Tue', value: false },
      { day: 'Wed', value: false },
      { day: 'Thu', value: false },
      { day: 'Fry', value: false },
      { day: 'Sat', value: false },
      { day: 'Sun', value: false }
    ];
    this.scheduleType = ''
    this.cleaner_id = null;
    this.parking_no = '';
    this.startDate = moment();
    this.subscription_amount = null;
    this.isShowPrev = true;
    let legth = this.vehicleDetals.length;
    this.vehIndex = legth;
  }

  showPreVeh() {
    // let legth = this.vehicleDetals.length;
    if (this.vehIndex > 0) {
      this.vehIndex = this.vehIndex - 1;
      let values = this.vehicleDetals[this.vehIndex];
      this.vehicles = values.vehicles;
      this.scheduleType = values.scheduleType;
      this.weekdays = values.weekdays;
      this.parking_no = values.parking_no;
      this.startDate = values.startDate;
      this.cleaner_id = values.cleaner_id;
      this.isShowNxt = true;
      this.isAddNxt = false;
    }

  }
  showNxtVeh() {
    if (this.vehIndex >= 0) {
      let editedVal = this.vehicleDetals[this.vehIndex];
      editedVal.vehicles = this.vehicles;
      editedVal.scheduleType = this.scheduleType;
      editedVal.weekdays = this.weekdays;
      editedVal.parking_no = this.parking_no;
      editedVal.startDate = this.startDate;
      editedVal.cleaner_id = this.cleaner_id;
      this.vehIndex = this.vehIndex + 1;
      if (this.vehIndex < this.vehicleDetals.length) {
        let values = this.vehicleDetals[this.vehIndex];
        this.vehicles = values.vehicles;
        this.scheduleType = values.scheduleType;
        this.weekdays = values.weekdays;
        this.parking_no = values.parking_no;
        this.startDate = values.startDate;
        this.cleaner_id = values.cleaner_id;

      } else {
        this.vehicles = [];
        this.scheduleType = '';
        this.weekdays = [
          { day: 'Mon', value: false },
          { day: 'Tue', value: false },
          { day: 'Wed', value: false },
          { day: 'Thu', value: false },
          { day: 'Fry', value: false },
          { day: 'Sat', value: false },
          { day: 'Sun', value: false }];
        this.parking_no = '';
        this.startDate = moment();
        this.cleaner_id = null;
        this.isAddNxt = true;
        this.isShowNxt = false;
        if (this.vehIndex > this.vehicleDetals.length) {
          this.vehIndex = this.vehicleDetals.length;
        }
      }

    }
  }


  async editCustomer() {
    console.log('added vehicles', this.addedVehicles)
    console.log('removed vehicles', this.removedVehicles)
    this.newvehicles = [];
    this.vehicleDetals.forEach(detail => {
      // detail.vehicles
      let obj = {}

      let schedule = []
      if (detail.scheduleType == 'daily') {
        detail.weekdays.forEach(eachDay => {
          eachDay.value = true;
        });
      }
      detail.weekdays.forEach(day => {
        schedule.push(day.value == true ? 1 : 0)
      });
      detail.vehicles.forEach(veh => {
        this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
        let worker_building = this.AllworkerBuildings.filter(w_b => (w_b.building_id == this.building_id[0].id) && (w_b.worker_id == detail.cleaner_id));
        let vehicleObj = {};
        vehicleObj['number'] = veh.number;
        vehicleObj['vehicle_id'] = veh.vehicle_id;
        vehicleObj['schedule'] = schedule;
        vehicleObj['parking'] = detail.parking_no;
        vehicleObj['cleaner_id'] = detail.cleaner_id;
        if(worker_building.length==0){
          Swal.fire({
            icon: 'error',
            title: "Worker must be required"
          });
        }
        vehicleObj['worker_building_id'] = worker_building[0].id;
        vehicleObj['startDate'] = detail.startDate;
        vehicleObj['work_schedule_id'] = veh.work_schedule_id;
        vehicleObj['subscription_amount'] = detail.subscription_amount;
        this.newvehicles.push(vehicleObj)
      });
    });


    // if (this.scheduleType == 'daily') {
    //   this.weekdays.forEach(eachDay => {
    //     eachDay.value = true;
    //   });
    // }
    let vehiclesToBeAdded = []
    let editedVehicles = [];
    // this.addedVehicles.forEach(addInfo => {

    //   let addVehile = this.newvehicles.find(newVeh => newVeh.number == addInfo.number);
    //   let editVehile = this.newvehicles.filter(newVeh => newVeh.number != addInfo.number);
    //   if (addVehile != undefined) {
    //     vehiclesToBeAdded.push(addVehile);
    //     addVehile = undefined;
    //   }
    //   if (editVehile != undefined) {
    //     editedVehicles.push(editVehile);
    //     editVehile = undefined;
    //   }

    // });

    let yFilter = this.addedVehicles.map(itemY => { return itemY.number; });
    let XFilter = this.removedVehicles.map(itemX => { return itemX.number; });

    editedVehicles = this.newvehicles.filter(obj => !yFilter.includes(obj.number) && !XFilter.includes(obj));
    vehiclesToBeAdded = this.newvehicles.filter(obj => yFilter.includes(obj.number));
    // this.newvehicles.forEach(newInfo => {

    //   let addVehile = this.addedVehicles.find(addVeh => addVeh.number == newInfo.number);
    //   this.addedVehicles.forEach(ele => {
    //     let editVehile = this.newvehicles.find(newVeh => newVeh.number != newInfo.number);
    //     if (editVehile != undefined) {
    //       editedVehicles.push(editVehile);
    //       editVehile = undefined;
    //     }
    //   });

    //   if (addVehile != undefined) {
    //     vehiclesToBeAdded.push(addVehile);
    //     addVehile = undefined;
    //   }


    // });

    this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
    this.customerForm.value['building_id'] = this.building_id[0].id;
    // let worker_building = this.AllworkerBuildings.filter(w_b => w_b.building_id = this.building_id[0].id && w_b.worker_id == this.customerForm.controls.cleaner_id.value);
    // this.customerForm.value['worker_building_id'] = worker_building[0].id;
    (await this.mastersService.editCustomerDetails(this.cust_id, this.customerForm.value, vehiclesToBeAdded, editedVehicles, this.removedVehicles)).subscribe((response: any) => {
      console.log('result by adding customer', response)
      if (response.ok || response.status) {
        Swal.fire({
          icon: 'success',
          title: "Customer Edited Successfully."
        })

      } else if (response.result.isEditVehicle == false) {
        Swal.fire({
          icon: 'error',
          title: "Something went wrong while editing the schedule. Please try again. If the problem persists, please contact development team"
        });
      }
      else if (response.result.vehicles && response.result.vehicles.length > 0) {
        Swal.fire({
          icon: 'error',
          title: "Vehicle already exist"
        })

      } else if (response.result.customers && response.result.customers.length > 0) {
        Swal.fire({
          icon: 'error',
          title: "Mobile number already exist"
        })
      } else {
        Swal.fire({
          icon: 'error',
          title: response.message
        })
      }
      this.dialogRef.close();
    });
    // Swal.fire({
    //   icon: 'success',
    //   title: "Customer Edited"
    // })

    // console.log("all locations", response)
    // this.allLocations = response;
  }




}
