import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { trigger, state, transition, animate, style } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { MastersService } from 'app/api/masters.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  dataSource: any;
  displayedColumns = ['name', 'mobile', 'role','delete'];
  allUsers = [];
  user_name:any;

  constructor(
    public _router: Router,
    public mastersService: MastersService,
  ) { }

  ngOnInit() {
    this.user_name=localStorage.getItem("user_name");
    this.getAllUsers();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
      });

  }
  async getAllUsers() {
    (await this.mastersService.getUsersList()).subscribe((response: any) => {

      console.log("all users", response)
      this.allUsers = response;
      this.dataSource = new MatTableDataSource(this.allUsers);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  async deleteUser(data:any) {
    Swal.fire({
      title: 'Do You want to Delete?',
      text: "User will be Deleted.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete !'
    }).then(async (result) => {
      if (result.value) {
        (await this.mastersService.deleteUser(data.id,this.user_name)).subscribe((response: any) => {
          console.log("response", response)
          if(response.success==true){
            Swal.fire({
              icon:'success',
              title:response.message
            })
          }
          this.getAllUsers();
        });
      }
    })

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
