import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatDialog, MatDialogConfig, MatTableDataSource, ThemePalette } from '@angular/material';
import { Subject, fromEvent } from 'rxjs';
import { MastersService } from 'app/api/masters.service';
import { Router,NavigationExtras } from '@angular/router';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AddBuildingsComponent } from 'app/add-buildings/add-buildings.component';
import { AddEditWorkerComponent } from 'app/add-edit-worker/add-edit-worker.component';
import Swal from 'sweetalert2';
import { worker } from 'cluster';
import * as _ from "underscore";
import { indexOf } from 'underscore';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    fuseAnimations
  ],
  encapsulation: ViewEncapsulation.None
})
export class WorkersComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: false })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  dataSource: any;
  displayedColumns = ['id', 'name', 'mobile', 'paymentreport','carsreport','start_date', 'edit', 'delete', 'expand'];
  allWorkers: any;
  user_name: any;
  links = ['Active', 'Inactive'];
  activeLink = '';
  background: ThemePalette = undefined;
  status: any;

  constructor(
    public _router: Router,
    public mastersService: MastersService,
    public dialog: MatDialog,
  ) { }
  // ngAfterViewInit() {
  //   this.dataSource.sort = this.sort;
  // }
  ngOnInit() {
    // this.dataSource.sort = this.sort;
    this.user_name = localStorage.getItem("user_name");
    this.getAllWorkers();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)

      });

  }
  async getAllWorkers() {
    (await this.mastersService.getWorkersList()).subscribe((response: any) => {

      // console.log("allWorkers", response)
      let uniqueArray = _.map(_.groupBy(response, (obj: any) => {
        return obj.id;
      }), (grouped) => {
        return grouped[0];
      })
      console.log(uniqueArray)
      uniqueArray.forEach(worker => {
        worker.buildings = []
        let workerBuildings = response.filter(workerBuilding => workerBuilding.id == worker.id);
        let sortedWorkerBuildings = workerBuildings.sort(function (a, b) {
          return (a.building_name == b.building_name && a.location == b.location);
        });
        let previous: any = null;
        let obj: any = null;
        sortedWorkerBuildings.forEach((building, index) => {
          if (previous == null || (previous.building_name == building.building_name && previous.address == building.address)) {
            previous = building;
          } else {
            worker.buildings.push(obj);
            previous = building;
          }
          obj = {
            building_name: building.building_name,
            location: building.address,
            end_date: building.end_date
          }

        });
        worker.buildings.push(obj);
      });
      this.allWorkers = uniqueArray;

      //   let workerDetails = [];

      // // Declare an empty object 
      // let uniqueObject = {};
      // for (let i in this.allWorkers) {

      //   // Extract the title 
      //   var objTitle = this.allWorkers[i]['id'];

      //   // Use the title as the index 
      //   uniqueObject[objTitle] = this.allWorkers[i];
      // }
      // for (let i in uniqueObject) {
      //   workerDetails.push(uniqueObject[i]);
      // }
      console.log("worker details", this.allWorkers);
      this.activeLink = 'Active';
      this.status = this.allWorkers.filter(map => map.status == 1);
      this.dataSource = new MatTableDataSource(this.status);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }



  addWorker() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "500px";
    // dialogConfig.data = {
    //   data: business
    // }
    const dialogRef = this.dialog.open(AddEditWorkerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data == 'reload') {
        this.getAllWorkers();
      }
    });
  }
  editWorker(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    dialogConfig.height = "65%";
    dialogConfig.data = {
      name: data.name,
      id: data.id,
      mobile: data.mobile,
      password: data.password,
      user_id: data.user_id,


    }
    const dialogRef = this.dialog.open(AddEditWorkerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data == 'reload') {
        this.getAllWorkers();
      }
    });
  }
  async deleteWorker(id: number) {
    Swal.fire({
      title: 'Do You want to Delete?',
      text: "Worker will be Deleted.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete !'
    }).then(async (result) => {
      if (result.value) {
        (await this.mastersService.deleteWorker(id, this.user_name)).subscribe((response: any) => {
          console.log("response", response)
          if (response.message == "Worker deleted successfully.") {
            Swal.fire({
              icon: 'success',
              title: "Worker Deleted"
            })
          } else if (response.message == "worker cannot be deleted as there is a schedule assigned to him.") {
            Swal.fire({
              icon: 'warning',
              title: 'worker cannot be deleted as there is a schedule assigned to him.'
            })
          } else if (response.message == "worker cannot be deleted as there is a building assigned to him.") {
            Swal.fire({
              icon: 'warning',
              title: 'worker cannot be deleted as there is a building assigned to him.'
            })
          }
          this.getAllWorkers();
        });
      }
    })

  }
  async activateWorker(worker: any) {
    Swal.fire({
      title: 'Do You want to Activate?',
      text: "Worker will be activated.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Activate !'
    }).then(async (result) => {
      if (result.value) {
        try {
          (await this.mastersService.activateWorker(worker.id)).subscribe((response: any) => {
            console.log("response", response)
            if (response.message == "Worker activated successfully.") {
              Swal.fire({
                icon: 'success',
                title: "Worker Activated"
              })
            } else {
              Swal.fire({ icon: 'warning', title: "Error while activating worker!" });
            }
            this.getAllWorkers();

          });
        }
        catch (err) {
          console.log('error while activating worker', err);
        }
      }
    })
  }
  displayWorkersBy(value) {
    let filterValue = 1;
    this.activeLink = value;
    if (value == 'Inactive') {
      filterValue = 0;
    }
    this.status = this.allWorkers.filter(map => map.status == filterValue);
    this.dataSource = new MatTableDataSource(this.status);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }
  paymentsreports(worker: any) {
    console.log(worker);
    // localStorage.setItem("location_id",worker.location_id)
    // localStorage.setItem("Building_name",worker.name)
    const navigationExtras: NavigationExtras = {
      queryParams: {
        worker_id:worker.id,
        worker_name:worker.name,
        comingFrom:'worker'
      }
    };

    this._router.navigate(["/paymentreport"],navigationExtras);
  }
  washedCarsReport(worker: any) {
    console.log(worker);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        worker_id:worker.id,
        worker_name:worker.name,
        comingFrom:'worker_washes'
      }
    };

    this._router.navigate(["/paymentreport"],navigationExtras);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}