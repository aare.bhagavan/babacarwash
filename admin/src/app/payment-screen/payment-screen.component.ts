import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AddCustomerComponent } from 'app/add-customer/add-customer.component';
import { PaymentService } from 'app/api/payment.service';
import { PaymentTransactionComponent } from 'app/payment-transaction/payment-transaction.component';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { NavigationExtras, ActivatedRoute, Router, NavigationEnd } from '@angular/router';
@Component({
  selector: 'app-payment-screen',
  templateUrl: './payment-screen.component.html',
  styleUrls: ['./payment-screen.component.scss'],
  animations: [
    trigger(
      'detailExpand',
      [
        state(
          'collapsed',
          style({ height: '0', minHeight: '0' })),
        state('expanded', style({ height: '*' })),
        transition('expanded <=> collapsed',
          animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
        ),
      ]
    ),
    fuseAnimations
  ],
  encapsulation: ViewEncapsulation.None
})

export class PaymentScreenComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: false })
  sort: MatSort;
  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  displayedColumns = ['ID', 'Customer', 'Vehicle', 'Cleaner', 'Start Date', 'Total Amount Pending', 'Total Amount Paid', 'Total Amount Charged', 'Pay'];
  dataSource;
  paymentList: any;
  pending_payments: any;
  renew_payments: any;
  pendingUrl: string;

  constructor(public dialog: MatDialog,
    public paymentService: PaymentService,
    public router: Router
  ) {


    this.router.events.subscribe((e) => {
      if (e instanceof NavigationEnd) {
        console.log(e.url);
        this.pendingUrl = e.url;
      }
    })

  }

  async ngOnInit() {

    this.getPaymentList();

    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
      });

  }

  async getPaymentList() {
    (await this.paymentService.getAllPaymentDetails()).subscribe((response: any) => {
      this.paymentList = response;
      console.log(this.paymentList);
      this.paymentList.forEach(element => {
        element['fullName']=element.first_name+' '+element.last_name;
        element['renew'] = false;
        let schedules = [];
        schedules[0] = element.sunday;
        schedules[1] = element.monday;
        schedules[2] = element.tuesday;
        schedules[3] = element.wednesday;
        schedules[4] = element.thursday;
        schedules[5] = element.friday;
        schedules[6] = element.saturday;
        element['schedules'] = schedules;
        element.start_date = moment(element.start_date).format("DD-MM-YYYY");
        var lastDateOfTheMonth = moment(element.date).endOf('month');
        let curDay = element.current_date;
        //let dateFrom = moment(lastDateOfTheMonth).subtract(7, 'd').format('YYYY-MM-DD');
        // if (moment(curDay).isSameOrAfter(dateFrom) || moment(curDay).isSame(lastDateOfTheMonth)) {
        if (moment(curDay).isAfter(lastDateOfTheMonth)) {
          element.renew = true;
          this.displayedColumns = ['ID', 'Customer', 'Vehicle', 'Cleaner', 'Start Date', 'Total Amount Pending','Total Amount Paid', 'Total Amount Charged',  'Pay', 'renew'];
        }
      });
      if (this.pendingUrl == "/payment/pending") {
        this.paymentList = this.paymentList.filter(payment => (payment.amount_charged > payment.amount_paid));
        console.log(this.paymentList);
      }
      if (this.pendingUrl == "/payment/renew") {
        this.paymentList = this.paymentList.filter(payment => (payment.renew == true));
        console.log(this.paymentList);
      }
      this.dataSource = new MatTableDataSource(this.paymentList);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'Customer': return item.first_name + " " + item.last_name;
            break;
          case 'Vehicle': return item.vehicle;
            break;
          case 'Cleaner': return item.cleaner;
            break;
          case 'Start Date': return item.start_date;
            break;
          case 'Total Amount Pending': return item.amount_charged - item.amount_paid;
            break;
          case 'Total Amount Paid': return item.amount_paid;
            break;
          case 'Total Amount Charged': return item.amount_charged;
            break;

          default: return item[property];
        }
      };
    });
  }

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  paymentTransaction(event, data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "55%";
    dialogConfig.height = "65%";
    dialogConfig.data = { type: event, data }
    const dialogRef = this.dialog.open(PaymentTransactionComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.getPaymentList();
    });
  }
  paymentEnd(pendingAmount: Number, data: any) {
    let currentDate = moment();
    console.log(currentDate);
    let dueDate = moment().add(1, 'M').startOf('month');
    console.log(dueDate);
    let formattedDueDate = moment(dueDate).format('DD-MM-YYYY');
    let difference = dueDate.diff(currentDate, 'days')
    console.log(difference);
    if (difference <= 1) {
      Swal.fire({
        width: 600,
        titleText: `Dear ${data.first_name} ${data.last_name} ,Your car wash bill for Vehicle ${data.vehicle} is د.إ ${pendingAmount} and due on ${formattedDueDate}.\n Thank you, Baba Car Wash Customer Service Team.`
      });
    } else {
      Swal.fire({
        width: 600,
        titleText: 'Hi ' + data.first_name + ' ' + data.last_name + ' You have a pending amount of "' + pendingAmount + '" on \n' + data.vehicle,
      });
    }

  }
  async pay(data) {
    const { value: amount } = await Swal.fire({
      title: 'Amount To Pay',
      input: 'number',
      showCancelButton: true,
      inputPlaceholder: 'Enter Amount',
      inputValidator: (value) => {
        if (parseInt(value) < 0) {
          return 'Amount cannot be negative'
        }
      }
    });
    if (amount) {
      let { value: note = '' } = await Swal.fire({
        title: 'Note',
        input: 'text',
        showCancelButton: true,
        cancelButtonText: 'No Note',
        inputPlaceholder: 'Give a note'
      });
      (await this.paymentService.payAmount(amount, note, data)).subscribe((response: any) => {
        if (response == true) {
          Swal.fire({
            icon: 'success',
            title: `د.إ ${amount} is Paid`,
            showConfirmButton: false,
            timer: 1500
          });
          this.getPaymentList();
        } else {
          Swal.fire({
            title: "There is some error",
            showConfirmButton: false,
            timer: 1500
          });
        }
      })
    }
  }
  async Renew(data) {
    Swal.fire({
      title: 'Renew?',
      text: "Do you want to renew schedules!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes!'
    }).then(async (result) => {
      if (result.value) {
        (await this.paymentService.renewSchedules(data)).subscribe((response: any) => {
          if (response.status) {
            Swal.fire(
              'Renewed!',
              'Schedules has been renewed.',
              'success'
            )
            this.getPaymentList();
          } else {
            Swal.fire({
              title: "There is some error",
              showConfirmButton: false,
              timer: 1500
            });
          }
        })
      }
    })

  }


}