import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddBuildingsComponent } from './add-buildings.component';

describe('AddBuildingsComponent', () => {
  let component: AddBuildingsComponent;
  let fixture: ComponentFixture<AddBuildingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBuildingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddBuildingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
