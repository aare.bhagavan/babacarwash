import { FuseNavigation, UserRoles } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'applications',
        title: 'Applications',
        translate: 'NAV.APPLICATIONS',
        type: 'group',
        icon: 'apps',
        role: [UserRoles.ADMIN],
        children: [
            // {
            //     id: 'users',
            //     title: 'Users',
            //     type: 'item',
            //     icon: 'person',
            //     url: '/users',
            //     role: [UserRoles.ADMIN]

            // },
            {
                id: 'dashboards',
                title: 'Dashboard',
                type: 'item',
                icon: 'dashboard',
                url: '/dashboard',
                role: [UserRoles.ADMIN]

            },
            {
                id: 'location',
                title: 'Locations',
                type: 'item',
                icon: 'place',
                url: '/locations',
                role: [UserRoles.ADMIN]

            },
            {
                id: 'buildings',
                title: 'Buildings',
                type: 'item',
                icon: 'location_city',
                url: '/buildings',
                role: [UserRoles.ADMIN],
            },
            {
                id: 'workers',
                title: 'Workers',
                type: 'item',
                icon: 'people_outline',
                url: '/workers',
                role: [UserRoles.ADMIN],
            },
            {
                id: 'customers',
                title: 'Customers',
                type: 'item',
                icon: 'people_outline',
                url: '/customers',
                role: [UserRoles.ADMIN]
            },
            {
                id: 'enqcustomers',
                title: 'Enquiries',
                icon: 'forum',
                type: 'collapsable',
                // url: '/enquiries',
                role: [UserRoles.ADMIN],
                children: [
                    {
                        id: 'newcustomers',
                        title: 'New Customers',
                        type: 'item',
                        icon: 'person_add',
                        url: '/enquiries/new',
                        role: [ UserRoles.ADMIN]
                    },
                    {
                        id: 'newcars',
                        title: 'New Carwashes',
                        type: 'item',
                        icon: 'directions_car_filled',
                        url: '/enquiries/existing',
                        role: [ UserRoles.ADMIN]
                    },
                    // {
                    //     id: 'complaints',
                    //     title: 'Others',
                    //     type: 'item',
                    //     icon: 'error_outline',
                    //     url: '/enquiries/complaints',
                    //     role: [ UserRoles.ADMIN]
                    // }
                ]
            },
            {
                id: 'payment',
                title: 'Payment Details',
                type: 'collapsable',
                icon: 'attach_money',
                // url: '/payment',
                role: [UserRoles.ADMIN],
                 children: [

                    {
                        id: 'allPayments',
                        title: 'All Payments',
                        type: 'item',
                        icon: 'attach_money',
                        url: '/payment/all',
                        role: [ UserRoles.ADMIN]

                    },
                    {
                        id: 'pendingPayments',
                        title: 'Pending Payments',
                        type: 'item',
                        icon: 'attach_money',
                        url: '/payment/pending',
                        role: [ UserRoles.ADMIN]

                    },
                    {
                        id: 'renewPayments',
                        title: 'Renew Contracts',
                        type: 'item',
                        icon: 'attach_money',
                        url: '/payment/renew',
                        role: [ UserRoles.ADMIN]

                    }
                ]
            }
        ]
        //     {
        //         id: 'users',
        //         title: 'Users',
        //         type: 'item',
        //         icon: 'person',
        //         url: '/ghmc-users-list',
        //         role: [UserRoles.SUPER_ADMIN, UserRoles.ADMIN],
        //         // children: [

        //         //     {
        //         //         id: 'users',
        //         //         title: 'Add User',
        //         //         type: 'item',
        //         //         icon: 'person',
        //         //         url: '/ghmc-users',
        //         //         role: [UserRoles.SUPER_ADMIN, UserRoles.ADMIN]

        //         //     }
        //         // ]
        //     },

        //     {
        //         id: 'departments',
        //         title: 'Departments',
        //         type: 'collapsable',
        //         url: '/members/details',
        //         exactMatch: true,
        //         role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //         icon: 'person',
        //         children: [
        //             {
        //                 id: 'dzc',
        //                 title: 'DZC mapping',
        //                 type: 'item',
        //                 url: '/dzc-mapping',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'person',
        //             }
        //         ],
        //     },
        //     {
        //         id: 'designations',
        //         title: 'Designations',
        //         type: 'item',
        //         url: '/designation',
        //         exactMatch: true,
        //         role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //         icon: 'file_copy'
        //     },

        //     {
        //         id: 'circles',
        //         title: 'Circles',
        //         type: 'item',
        //         icon: 'business',
        //         url: '/business',
        //         exactMatch: true,
        //         role: [UserRoles.SUPER_ADMIN, UserRoles.ADMIN],
        //     },
        //     {
        //         id: 'zones',
        //         title: 'Zones',
        //         type: 'item',
        //         icon: 'business',
        //         url: '/super-business',
        //         exactMatch: true,
        //         role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //     },
        //     {
        //         id: 'workers',
        //         title: 'Workers',
        //         type: 'collapsable',
        //         icon: 'person',
        //         url: '/users',
        //         exactMatch: true,
        //         role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //         children: [
        //             {
        //                 id: 'designation',
        //                 title: 'Designations Upload',
        //                 type: 'item',
        //                 url: '/assign-designation',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'person',
        //             },
        //             {
        //                 id: 'importutil',
        //                 title: 'Workers Upload',
        //                 type: 'item',
        //                 url: '/import',
        //                 exactMatch: true,
        //                 icon: "backup",
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //             },
        //             {
        //                 id: 'excelImport',
        //                 title: 'Download workers',
        //                 type: 'item',
        //                 url: '/download-worker',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'person'
        //             }
        //         ],
        //     },


        //     {
        //         id: 'payroll',
        //         title: 'Payroll',
        //         type: 'collapsable',
        //         icon: 'person',
        //         // url: '/users',
        //         exactMatch: true,
        //         role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //         children: [
        //             {
        //                 id: 'excelImport',
        //                 title: 'Attendance Import',
        //                 type: 'item',
        //                 url: '/excel-import',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'address'
        //             },
        //             {
        //                 id: 'documentsProcess',
        //                 title: 'Attendance Processed',
        //                 type: 'item',
        //                 url: '/documents-process',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'file_copy'
        //             },
        //             {
        //                 id: 'runPayroll',
        //                 title: 'Run Payroll',
        //                 type: 'item',
        //                 url: '/run-payroll',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'person'
        //             },
        //             {
        //                 id: 'generateStatement',
        //                 title: 'Generate Statement',
        //                 type: 'item',
        //                 url: '/generate-statement',
        //                 exactMatch: true,
        //                 role: [UserRoles.ADMIN, UserRoles.SUPER_ADMIN],
        //                 icon: 'file_copy'
        //             },

        //         ]
        //     },



        // ],

    }
];
