import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { FuseSharedModule } from '@fuse/shared.module';

import { RegisterCustomerComponent } from 'app/register-customer/register-customer.component';
import { ReactiveFormsModule } from '@angular/forms';
import { from } from 'rxjs';
import { MatChipsModule, MatChipInputEvent } from '@angular/material/chips';
import {
    MatRadioModule,
    MatStepperModule, MatAutocompleteModule, MatDatepickerModule, MatSelectModule, MatNativeDateModule,
    MatTableModule, MatTabsModule, MatOptionModule, MatCardModule, MatGridListModule,
    MatDividerModule, MatPaginatorModule, MatExpansionModule, MatListModule, MatButtonToggleModule,
    MatToolbarModule, MatProgressBarModule, MatMenuModule, MatSnackBarModule

} from '@angular/material';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialog } from "@angular/material/dialog";
const routes = [
    {
        path     : 'registerCustomer',
        component: RegisterCustomerComponent
    }
];

@NgModule({
    declarations: [
        RegisterCustomerComponent
    ],
    imports     : [
        RouterModule.forRoot(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
        FuseSharedModule,
        MatChipsModule,
        MatSelectModule,
        MatDatepickerModule,
        MatCardModule,
        MatDialogModule
    ]
})
export class  RegisterCustomerModule
{
}
