import { Router } from '@angular/router';
import { AuthService } from './../api/auth.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { MatChipInputEvent } from '@angular/material/chips';
import { MastersService } from 'app/api/masters.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';

export interface vehicle {
  number: string;
}

@Component({
  selector: 'app-register-customer',
  templateUrl: './register-customer.component.html',
  styleUrls: ['./register-customer.component.scss']
})
export class RegisterCustomerComponent implements OnInit {
  customerForm: FormGroup;
  vehicles: vehicle[] = [];
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  weekdays: any[] = [
    { day: 'Mon', value: false },
    { day: 'Tue', value: false },
    { day: 'Wed', value: false },
    { day: 'Thu', value: false },
    { day: 'Fri', value: false },
    { day: 'Sat', value: false },
    { day: 'Sun', value: false },
  ];
  cleaner_id: number = null;
  scheduleType: string = '';
  startDate: any = moment();
  allBuildings: any;
  parking_no: string = '';
  filteredLocations: any = [];
  allLocations: any;
  filteredBuildings: any = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  building_id: any = [];
  weekDays: any;
  date: Date = new Date();
  month: number;
  year: number;
  allWorkers: any;
  newvehicles: any = [];
  vehicleDetals: any = [];
  vehicleIndex: number = -1;
  isShowPrev: boolean;
  vehIndex: number;
  isShowNxt: boolean;
  isAddNxt: boolean = true;
  filterWorkers: any = [];
  removedVehicles: any = [];
  addedVehicles: any = [];
  cust_id: number;
  AllworkerBuildings: any = [];
  subscription_amount: number = null;
  noWorkers: boolean = false;
  vehicleStart_date: any;
  startDateChanged: boolean[] = [];
  scheduleChanged: boolean[] = [];
  vehicleSchedule: any;
  maxDate;
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    public authService: AuthService,
    private _router: Router,
    public mastersService: MastersService,
    // public dialogRef: MatDialogRef<RegisterCustomerComponent>
  ) {
    // dialogRef.disableClose = true;
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit() {
    // this.getAllworkerBuildings();
    this.startDate=moment();
    this.month = this.date.getMonth() + 1;
    this.year = this.date.getFullYear();
    console.log('weekdays1111', this.weekdays);
    // this.getAllLocations();
    // this.getBuildings();
    // this.getAllWorkers();
    // this.getScheduleById();
    this.customerForm = this._formBuilder.group({
      firstName: [''],
      lastName: [''],
      mobile: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern("[0-9][0-9]{9}$")])],
      // email: ['', Validators.compose([Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")])],
      startDate: [moment(), Validators.required],
      flat_no: ['']
    });
    // this.filteredLocations = this.customerForm.controls.location.valueChanges
    //   .pipe(
    //     startWith(''),
    //     map(value => this._filterlocation(value))
    //   );


    // this.customerForm.controls.location.valueChanges.subscribe(value => { this.filterBuilding(value) })
    // this.customerForm.controls.building.valueChanges.subscribe(value => { this.filterWorker(value) })

  }

  // setCustomerValues() {
  //   console.log('editable cust details', this.data.customer);
  //   this.customerForm.controls.firstName.setValue(this.data.customer.first_name)
  //   this.customerForm.controls.lastName.setValue(this.data.customer.last_name)
  //   this.customerForm.controls.mobile.setValue(this.data.customer.mobile)
  //   this.customerForm.controls.flat_no.setValue(this.data.customer.flat_no)
  //   this.customerForm.controls.location.setValue(this.data.customer.location_id)
  //   this.customerForm.controls.building.setValue(this.data.customer.building_id)
  //   let obj = {}
  //   let vehicles = [];
  //   vehicles = this.data.customer.vehicles
  //   this.cust_id = this.data.customer.cust_id;


  //   vehicles.forEach(element => {
  //     // console.log('startDate', moment(element.startDate));
  //     // element.startDate = this.startDate  ;
  //     let isMonday = element.weekdays.filter(day => day.day == 'Mon').length == 1;
  //     if (!isMonday) {
  //       // element.weekdays.push({ day: 'Mon', value: false })
  //       element.weekdays.splice(0, 0, { day: 'Mon', value: false })
  //     }
  //     let isTuesday = element.weekdays.filter(day => day.day == 'Tue').length == 1;
  //     if (!isTuesday) {
  //       // element.weekdays.push({ day: 'Tue', value: false })
  //       element.weekdays.splice(1, 0, { day: 'Tue', value: false })
  //     }
  //     let isWednesday = element.weekdays.filter(day => day.day == 'Wed').length == 1;
  //     if (!isWednesday) {
  //       // element.weekdays.push({ day: 'Wed', value: false })
  //       element.weekdays.splice(2, 0, { day: 'Wed', value: false })
  //     }
  //     let isThursday = element.weekdays.filter(day => day.day == 'Thu').length == 1;
  //     if (!isThursday) {
  //       // element.weekdays.push({ day: 'Thu', value: false })
  //       element.weekdays.splice(3, 0, { day: 'Thu', value: false })
  //     }
  //     let isFriday = element.weekdays.filter(day => day.day == 'Fri').length == 1;
  //     if (!isFriday) {
  //       // element.weekdays.push({ day: 'Fri', value: false })
  //       element.weekdays.splice(4, 0, { day: 'Fri', value: false })
  //     }
  //     let isSatday = element.weekdays.filter(day => day.day == 'Sat').length == 1
  //     if (!isSatday) {
  //       // element.weekdays.push({ day: 'Sat', value: false })
  //       element.weekdays.splice(5, 0, { day: 'Sat', value: false })
  //     }
  //     let isSunday = element.weekdays.filter(day => day.day == 'Sun').length == 1;
  //     if (!isSunday) {
  //       // element.weekdays.push({ day: 'Sun', value: false })
  //       element.weekdays.splice(6, 0, { day: 'Sun', value: false })
  //     }
  //     element['scheduleType'] = 'weekly';
  //     obj['parking_no'] = element.parking_no;
  //     obj['cleaner_id'] = element.cleaner_id;
  //     obj['scheduleType'] = 'weekly';
  //     obj['vehicles'] = [element];
  //     obj['startDate'] = moment(element.startDate);
  //     obj['weekdays'] = element.weekdays;
  //     obj['subscription_amount'] = element.subscription_amount;
  //     obj['end_date'] = element.end_date;
  //     this.vehicleDetals.push(obj);
  //     obj = {}
  //   });

  //   console.log('vehicles date', this.vehicleDetals)

  // }

  // filterBuilding(value) {
  //   if (this.allBuildings != undefined) {
  //     this.filteredBuildings = this.allBuildings.filter(building => building.location_id == value)
  //   }
  // }
  // filterWorker(value) {
  //   if (this.allWorkers != undefined) {
  //     this.filterWorkers = this.allWorkers.filter(worker => (worker.building_id == value) && (worker.end_date == null))
  //     console.log('filtered workers', this.filterWorkers)
  //     if (this.filterWorkers.length == 0) {
  //       this.noWorkers = true;
  //     } else this.noWorkers = false;
  //   }

  // }
  addchip(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.vehicles.push({ number: value.trim() });
    }

    const index = this.addedVehicles.indexOf(value, 0);  //for edit 
    if (index > -1) {                                      //for edit 
      this.addedVehicles.splice(index, 1);                //for edit 
    } else if (value != "") {
      this.addedVehicles.push({ number: value.trim() }) // for edit
    }


    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  addchipSelectedVehicle(event: MatChipInputEvent, selectedVehicles: any): void {
    console.log(selectedVehicles);
    const input = event.input;
    const value = event.value;
    const index = this.removedVehicles.indexOf(value, 0);  //for edit 
    if (index > -1) {                                      //for edit 
      this.removedVehicles.splice(index, 1);                //for edit 
    }                                                       //for edit 
    // Add our fruit
    if ((value || '').trim()) {
      selectedVehicles.vehicles.push({ number: value.trim() });
    }
    const index1 = this.addedVehicles.indexOf(value, 0);  //for edit 
    if (index1 > -1) {                                      //for edit 
      this.addedVehicles.splice(index, 1);                //for edit 
    } else if (value != "") {
      this.addedVehicles.push({ number: value.trim() }) // for edit
    }
    // Reset the input value
    if (input) {
      input.value = '';
    }
  }
  onChange(scheduleType) {
    this.scheduleType = scheduleType;
    console.log(this.scheduleType);
  }


  OnDateChange(selectedDate, vehicle) {
    let vehicle_id = vehicle.vehicles[0].vehicle_id;
    this.vehicleStart_date = this.getStartDate(vehicle_id);
    let formatedStartDate = moment(this.vehicleStart_date).format('YYYY-MM-DD');
    let selected_date = moment(selectedDate._d).format('YYYY-MM-DD');
    this.startDateChanged[vehicle_id] = false;
    if (moment(selected_date).isSame(formatedStartDate)) {

      this.scheduleChanged[vehicle_id] = false;
    } else {
      this.scheduleChanged[vehicle_id] = true;
    }
  }

  getStartDate(vehicle_id) {
    for (let schedule in this.vehicleSchedule) {
      let data = this.vehicleSchedule[schedule].filter(vehilce => vehilce.vehicle_id == vehicle_id);
      if (data.length > 0) {
        console.log(data);
        return data[0].start_date;
      }
    }
  }


  remove(value: vehicle): void {
    const index = this.vehicles.indexOf(value);

    if (index >= 0) {
      this.vehicles.splice(index, 1);
    }
  }
  selectedChipremove(value: vehicle, selectedVehicles: any, allVehicles: any): void {
    const index = selectedVehicles.vehicles.indexOf(value);
    this.removedVehicles.push(value);
    if (index >= 0) {
      selectedVehicles.vehicles.splice(index, 1);
      if (selectedVehicles.vehicles.length == 0) {
        const index = allVehicles.indexOf(selectedVehicles);
        allVehicles.splice(index, 1);
        // selectedVehicles=[];
      }
    }
  }

  // async getBuildings() {
  //   (await this.mastersService.getBuildingsList()).subscribe((response: any) => {

  //     console.log("all Buildings", response)
  //     this.allBuildings = response;
  //     this.filteredBuildings = response;

  //   });
  // }

  // async getScheduleById() {
  //   let scheduleArray = [];
  //   const schedules = this.data.customer.vehicles.filter(vehicle => vehicle.end_date == null);
  //   for (let schedule of schedules) {
  //     (await this.mastersService.getScheduleById(schedule.work_schedule_id)).subscribe((response: any) => {

  //       console.log("schedule", response)
  //       scheduleArray.push(response);
  //     });
  //   }
  //   this.vehicleSchedule = scheduleArray;
  //   console.log(this.vehicleSchedule);

  // }

  toggle(day, currentSchedule, vehicleData) {
    let previousSchedule: any;
    let vehicle_id = vehicleData.vehicles[0].vehicle_id;
    for (let schedule in this.vehicleSchedule) {
      let data = this.vehicleSchedule[schedule].filter(vehilce => vehilce.vehicle_id == vehicle_id);
      if (data.length > 0) {
        previousSchedule = data;
      }

    }

    console.log('weekdays', day);
    console.log('weekdays1111', this.weekdays);
    this.scheduleChanged[vehicle_id] = false;
    if (this.scheduleHasChanged(previousSchedule, currentSchedule)) {
      this.startDateChanged[vehicle_id] = true;
    }
    else {
      this.startDateChanged[vehicle_id] = false;
    }

  }

  getStartDateChanged(vehicle_id) {
    if (vehicle_id == undefined) {
      return false;
    }
    if (this.startDateChanged[vehicle_id] == undefined) {
      this.startDateChanged[vehicle_id] = false;
    }
    return this.startDateChanged[vehicle_id];
  }
  getScheduleChanged(vehicle_id) {
    if (vehicle_id == undefined) {
      return false;
    }
    if (this.scheduleChanged[vehicle_id] == undefined) {
      this.scheduleChanged[vehicle_id] = false;
    }
    return this.scheduleChanged[vehicle_id];
  }
  scheduleHasChanged(prev: any, cur: any): boolean {
    let chg: boolean = false;
    console.log(cur);
    if ((prev[0].monday != (cur[0].value) ? 1 : 0) ||
      (prev[0].tuesday != (cur[1].value) ? 1 : 0) ||
      (prev[0].wednesday != (cur[2].value) ? 1 : 0) ||
      (prev[0].thursday != (cur[3].value) ? 1 : 0) ||
      (prev[0].friday != (cur[4].value) ? 1 : 0) ||
      (prev[0].saturday != (cur[5].value) ? 1 : 0) ||
      (prev[0].sunday != (cur[6].value) ? 1 : 0)) {
      chg = true;
    }
    return chg;
  }

  // private _filterlocation(value: string): string[] {
  //   if (this.allBuildings != undefined && this.allLocations != undefined) {
  //     const filterValue = value.toLowerCase();
  //     // this.allBuildings = this.allBuildings.filter(option => option.address.toLowerCase().includes(filterValue));
  //     this.allLocations = this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
  //     return this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
  //   }
  // }
  // private _filterbuilding(value: string): string[] {
  //   if (this.allBuildings != undefined) {
  //     const filterValue = value.toLowerCase();
  //     // this.allLocations = this.allLocations.filter(option => option.address.toLowerCase().includes(filterValue));
  //     return this.allBuildings.filter(option => option.name.toLowerCase().includes(filterValue));
  //   }

  // }

  // async getAllLocations() {
  //   (await this.mastersService.getLocationsList()).subscribe((response: any) => {

  //     console.log("all locations", response)
  //     this.allLocations = response;

  //   });
  // }

  // async getAllworkerBuildings() {
  //   (await this.mastersService.getAllworkerBuildings()).subscribe((response: any) => {

  //     console.log("all worker building mapping", response)
  //     this.AllworkerBuildings = response;

  //   });
  // }

  // filterCleaners() {

  //   // this.allWorkers=this.allWorkers.filter(worker=>)

  // }
  // async getAllWorkers() {
  //   (await this.mastersService.getWorkersList()).subscribe((response: any) => {

  //     console.log("all workers", response)
  //     this.allWorkers = response;
  //     if (this.data.edit) {
  //       this.setCustomerValues();
  //     }
  //   });
  // }


  async addCustomer() {
    console.log(this.vehicleDetals);
    this.newvehicles = [];
    this.vehicleDetals.forEach(detail => {
      // detail.vehicles
      let obj = {}

      let schedule = []
      if (detail.scheduleType == 'daily') {
        detail.weekdays.forEach(eachDay => {
          eachDay.value = true;
        });
      }
      detail.weekdays.forEach(day => {
        schedule.push(day.value == true ? 1 : 0)
      });
      detail.vehicles.forEach(veh => {
        //this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
        //let worker_building = this.AllworkerBuildings.filter(w_b => w_b.building_id == this.building_id[0].id && w_b.worker_id == detail.cleaner_id);
        let vehicleObj = {};
        vehicleObj['number'] = veh.number;
        vehicleObj['schedule'] = schedule;
        vehicleObj['parking'] = detail.parking_no;
        // vehicleObj['cleaner_id'] = detail.cleaner_id;
        // vehicleObj['worker_building_id'] = worker_building[0].id;
        vehicleObj['startDate'] = detail.startDate;
        vehicleObj['subscription_amount'] = detail.subscription_amount;
        this.newvehicles.push(vehicleObj)
      });
    });


    if (this.scheduleType == 'daily') {
      this.weekdays.forEach(eachDay => {
        eachDay.value = true;
      });
    }
    // this.building_id = this.allBuildings.filter(building => building.id == this.customerForm.controls.building.value)
    // this.customerForm.value['building_id'] = this.building_id[0].id;
    (await this.mastersService.selfCustomerDetails(this.customerForm.value, this.weekdays, this.newvehicles, this.month, this.year, this.scheduleType)).subscribe((response: any) => {
      console.log('result by adding customer', response)
      if (response.status) {
        let myWindow;
        Swal.fire({
          icon: 'success',
          title: "Thank you for registering with Baba Carwash. Our representative will contact you."
        }).then(async (result) => {
          if (result.value) {
            // // let currentTab=window.open('http://localhost:4200/registerCustomer','_parent');
            // // currentTab.close();
            myWindow = window.open(window.location.href, "_parent","width=500, height=500");
            // myWindow.close();
            // this.customerForm.reset();
          }
        })

      } else if (response.result.customers != undefined) {
        if (response.result.customers.length > 0) {
          Swal.fire({
            icon: 'error',
            title: "Customer already Registered with this Mobile number"
          })

        } else if (response.result.vehicles.length > 0) {
          Swal.fire({
            icon: 'error',
            title: "Customer already registered with this vehicle."
          })

        }
      }
      // this.dialogRef.close();

      console.log("all locations", response)
      this.allLocations = response;
    });
  }

  async getCustomerDetailsbyVeh(){
    console.log("vehciles",this.vehicles);
    let vehicle_id= this.vehicles[0].number;
    console.log("veh id",vehicle_id);
    (await this.mastersService.getCustomerDetailsbyVeh(vehicle_id)).subscribe((response: any) => {
      console.log(response);
      if(response.length > 0){
        this.customerForm.controls.firstName.setValue(response[0].first_name)
        this.customerForm.controls.lastName.setValue(response[0].last_name)
        this.customerForm.controls.mobile.setValue(response[0].mobile)
        this.customerForm.controls.flat_no.setValue(response[0].flat_no)
        this.customerForm.controls.location.setValue(response[0].location_id)
        this.customerForm.controls.building.setValue(response[0].building_id)
      }else{
        Swal.fire({
          icon: 'error',
          title: "No customer details"
        })
      }

    })
  }

  // close() {
  //   this.dialogRef.close();
  // }

  addVehicle() {
    // this.vehicleIndex = this.vehicleIndex + 1
    let obj = {}
    let vehicleObj = {};
    let schedule = []

    obj['vehicles'] = this.vehicles;
    obj['weekdays'] = this.weekdays;
    // obj['cleaner_id'] = this.cleaner_id;
    obj['parking_no'] = this.parking_no;
    obj['scheduleType'] = this.scheduleType;
    obj['startDate'] = this.startDate;
    obj['subscription_amount'] = this.subscription_amount;
    this.vehicleDetals.push(obj);
    console.log('added vehicle list', this.vehicleDetals)
    if (this.scheduleType == 'daily') {
      this.weekdays.forEach(eachDay => {
        eachDay.value = true;
      });
    }
    this.weekdays.forEach(day => {
      schedule.push(day.value == true ? 1 : 0)
    });
    this.vehicles.forEach(veh => {
      vehicleObj['number'] = veh.number;
      vehicleObj['schedule'] = schedule;
      vehicleObj['parking'] = this.parking_no;
      vehicleObj['cleaner_id'] = this.cleaner_id;
      vehicleObj['startDate'] = this.startDate;
      vehicleObj['subscription_amount'] = this.subscription_amount;
      this.newvehicles.push(vehicleObj)
    });


    console.log('vehicle details', this.newvehicles)
    this.vehicles = [];
    this.weekdays = [
      { day: 'Mon', value: false },
      { day: 'Tue', value: false },
      { day: 'Wed', value: false },
      { day: 'Thu', value: false },
      { day: 'Fry', value: false },
      { day: 'Sat', value: false },
      { day: 'Sun', value: false }
    ];
    this.scheduleType = ''
    this.cleaner_id = null;
    this.parking_no = '';
    this.startDate = moment();
    this.subscription_amount = null;
    this.isShowPrev = true;
    let legth = this.vehicleDetals.length;
    this.vehIndex = legth;
  }

  showPreVeh() {
    // let legth = this.vehicleDetals.length;
    if (this.vehIndex > 0) {
      this.vehIndex = this.vehIndex - 1;
      let values = this.vehicleDetals[this.vehIndex];
      this.vehicles = values.vehicles;
      this.scheduleType = values.scheduleType;
      this.weekdays = values.weekdays;
      this.parking_no = values.parking_no;
      this.startDate = values.startDate;
      this.cleaner_id = values.cleaner_id;
      this.isShowNxt = true;
      this.isAddNxt = false;
    }

  }
  showNxtVeh() {
    if (this.vehIndex >= 0) {
      let editedVal = this.vehicleDetals[this.vehIndex];
      editedVal.vehicles = this.vehicles;
      editedVal.scheduleType = this.scheduleType;
      editedVal.weekdays = this.weekdays;
      editedVal.parking_no = this.parking_no;
      editedVal.startDate = this.startDate;
      editedVal.cleaner_id = this.cleaner_id;
      this.vehIndex = this.vehIndex + 1;
      if (this.vehIndex < this.vehicleDetals.length) {
        let values = this.vehicleDetals[this.vehIndex];
        this.vehicles = values.vehicles;
        this.scheduleType = values.scheduleType;
        this.weekdays = values.weekdays;
        this.parking_no = values.parking_no;
        this.startDate = values.startDate;
        this.cleaner_id = values.cleaner_id;

      } else {
        this.vehicles = [];
        this.scheduleType = '';
        this.weekdays = [
          { day: 'Mon', value: false },
          { day: 'Tue', value: false },
          { day: 'Wed', value: false },
          { day: 'Thu', value: false },
          { day: 'Fry', value: false },
          { day: 'Sat', value: false },
          { day: 'Sun', value: false }];
        this.parking_no = '';
        this.startDate = moment();
        this.cleaner_id = null;
        this.isAddNxt = true;
        this.isShowNxt = false;
        if (this.vehIndex > this.vehicleDetals.length) {
          this.vehIndex = this.vehicleDetals.length;
        }
      }

    }
  }
}