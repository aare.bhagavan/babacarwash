import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MastersService } from 'app/api/masters.service';
import { DialogData } from 'app/main/apps/e-commerce/enter-otp-dialog/enter-otp-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import Swal from 'sweetalert2';
import * as moment from 'moment';
@Component({
  selector: 'app-activate-customer',
  templateUrl: './activate-customer.component.html',
  styleUrls: ['./activate-customer.component.scss']
})
export class ActivateCustomerComponent implements OnInit {
  startDate: any = moment();
  activateDate;
  user_name:any;
  todayDate: Date = new Date();
  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<ActivateCustomerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private router: Router,
    public mastersService: MastersService,
    public _router: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.user_name=localStorage.getItem("user_name");
  }

  cancel() {
    this.dialogRef.close({ data: 'cancel' });
  }
  submit() {
    this.activateCustomer(this.activateDate);
  }
  async activateCustomer(activate_date) {
    let activateDate = moment(activate_date).format('YYYY-MM-DD');
    try {
      (await this.mastersService.activateCustomer(this.data.id, this.user_name,activateDate)).subscribe((response: any) => {
        console.log("response", response)
        if (response.message == "Customer activated successfully.") {
          Swal.fire({
            icon: 'success',
            title: "Customer Activated"
          })
        } else {
          Swal.fire({ icon: 'warning', title: "Error while activating customer!" });
        }
        this.dialogRef.close({ data: 'reload' });

      });
    }
    catch (err) {
      console.log('error while activating customer', err);
    }
  }
}

// import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
// import { fuseAnimations } from '@fuse/animations';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { Router, ActivatedRoute } from '@angular/router';
// import { MastersService } from 'app/api/masters.service';
// import { DialogData } from 'app/main/apps/e-commerce/enter-otp-dialog/enter-otp-dialog.component';
// import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
// import Swal from 'sweetalert2';

// @Component({
//   selector: 'app-add-buildings',
//   templateUrl: './add-buildings.component.html',
//   styleUrls: ['./add-buildings.component.scss'],
//   encapsulation: ViewEncapsulation.None,
//   animations: fuseAnimations
// })
// export class AddBuildingsComponent implements OnInit {
//   buildingForm: FormGroup;
//   title = "Edit Building";
//   isNewBuilding: any;
//   allLocations: any;
//   constructor(
//     private _formBuilder: FormBuilder,
//     public dialogRef: MatDialogRef<AddBuildingsComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: DialogData,
//     private router: Router,
//     public mastersService: MastersService,
//     public _router: ActivatedRoute,
//   ) {
//     console.log("DATA", this.data)
//     if (this.data == null) {
//       this.title = "Add New Building";
//       this.isNewBuilding = false;
//     }
//   }

//   ngOnInit() {
//     this.getAllLocations();
//     this.buildingForm = this._formBuilder.group({
//       name: ['', Validators.required],
//       location_id: ['', Validators.required],
//     })
//     if (this.data !== null) {
//       console.log("THE DATA", this.data);
//       this.buildingForm.controls.name.setValue(this.data.name);
//     }
//   }
//   submit() {
//     if (this.title == "Add New Building") {
//       this.addNewBuilding();
//     } else this.editBuilding();
//   }
//   async addNewBuilding() {
//     try {
//       (await this.mastersService.addNewBuilding(this.buildingForm.value)).subscribe(res => {
//         console.log('response....', res)
//         if (res.message == "Building added successfully.") {
//           Swal.fire({ icon: 'success', title: 'New Building Added Successfully !', });          
//         } 
//         this.dialogRef.close({ data: 'reload' });

//       });
//     }
//     catch (err) {
//       console.log('error while adding building', err);
//     }
//   }
//   async editBuilding() {
//     let data = {
//       name: this.buildingForm.controls['name'].value,
//       location_id: this.buildingForm.controls['location_id'].value,
//       id: this.data.id

//     };
//     try {
//       (await this.mastersService.editBuilding(data)).subscribe(res => {
//         console.log('response....', res);
//         if (res.message == "Building updated successfully.") {
//           Swal.fire({ icon: 'success', title: 'Building Updated Successfully !', });          
//         } 
//         this.dialogRef.close({ data: 'reload' });
//       });
//     } catch (err) {
//       console.log('error while editing building', err)
//     }
//   }
//   async getAllLocations() {
//     (await this.mastersService.getLocationsList()).subscribe((response: any) => {
//       console.log("allLocationsList", response)
//       this.allLocations = response;
//       let prefillloc = this.allLocations.filter(lo => lo.id== this.data.location_id);
//       this.buildingForm.controls.location_id.setValue(prefillloc[0].id);
//     });
//   }


//   cancel() {
//     this.dialogRef.close({ data: 'cancel' });
//   }

// }
