import { Component, OnInit, ViewEncapsulation, Inject, ViewChild, ElementRef } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FormBuilder, FormGroup, Validators, FormControl, AbstractControl, ValidationErrors } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MastersService } from 'app/api/masters.service';
import { DialogData } from 'app/main/apps/e-commerce/enter-otp-dialog/enter-otp-dialog.component';
import { MAT_DIALOG_DATA, MatDialogRef, MatChipInputEvent, MatAutocompleteSelectedEvent, MatAutocomplete } from '@angular/material';
import Swal from 'sweetalert2';
import { map, startWith } from 'rxjs/operators';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { element } from 'protractor';
import * as moment from 'moment';
import { controllers } from 'chart.js';
import { select } from 'underscore';

export interface building {
  id: string;
  building_name: string;
}
@Component({
  selector: 'app-add-edit-worker',
  templateUrl: './add-edit-worker.component.html',
  styleUrls: ['./add-edit-worker.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class AddEditWorkerComponent implements OnInit {
  @ViewChild('buildingInput', { static: true }) buildingInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto', { static: true }) matAutocomplete: MatAutocomplete;

  workerForm: FormGroup;
  title = "Edit Worker";
  isNewWorker: any;
  allWorkers: any;
  allBuildings: any = [];
  filteredBuildings: any = [];
  buildingData: any = [];
  building_id: any = [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  buildingCtrl = new FormControl();
  separatorKeysCodes: number[] = [ENTER, COMMA];
  worker_building: any = [];
  user_name: any;
  editBuildings: any = [];
  minDate = new Date();
  end_date: any;
  worker_building_id: any;
  worker_building_ids: any = [];
  buildings = [];
  buildings_data: any = [];
  buildingsInchip;
  buildingsInList: any = [];
  password: any;
  confirmPassword: any;
  mobile: any;
  buildingAdded: boolean;



  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddEditWorkerComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private router: Router,
    public mastersService: MastersService,
    public _router: ActivatedRoute,
  ) {
    console.log("DATA", this.data)
    if (this.data == null) {
      this.title = "Add New Worker";
      this.isNewWorker = false;
    }
    else this.title = "Edit Worker";

  }

  ngOnInit() {
    this.user_name = localStorage.getItem("user_name");
    this.buildingsInchip = [];
    this.getAllWorkers();
    this.getBuildings();
    if (this.title == "Add New Worker") {
      this.workerForm = this._formBuilder.group({
        name: ['', Validators.required],
        mobile: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.required])],
        password: ['', Validators.required],
        confirm_password: ['', Validators.required],
        building: [''],
      })
    }
    else {
      this.workerForm = this._formBuilder.group({
        name: ['', Validators.required],
        mobile: ['', Validators.compose([Validators.maxLength(10), Validators.minLength(10), Validators.required])],
        password: [''],
        confirm_password: [''],
        building: [''],
      })
    }

    if (this.data !== null) {
      console.log("THE DATA", this.data);
      this.workerForm.controls.name.setValue(this.data.name);
      this.workerForm.controls.mobile.setValue(this.data.mobile);
      // this.workerForm.controls.password.setValue(this.data.password);
    }
    this.buildingsInchip = [];
    this.editBuildings.forEach(element => {
      this.buildingsInchip.push(element.building_name);
    });
    this.filteredBuildings = this.workerForm.controls.building.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterbuilding(value))
      );


  }

  addchip(event, selectedBuildings): void {
    if (this.title == "Edit Worker") {
      this.buildingAdded = true;
      selectedBuildings.buildings.forEach(element => {
        if (element.building_name == event.name && element.end_date == null) {
          this.buildingAdded = false;
        }
      });
      let repeatedElementFound = false;
      selectedBuildings.buildings.forEach(element => {
        if (element.building_name == event.name) {
          repeatedElementFound = true;
          if (element.end_date != null && element.chipAdded != true && this.buildingAdded) {
            let obj = {};
            obj['building_name'] = event.name;
            selectedBuildings.buildings.push(obj);
            element.chipAdded = true;
            // this.workerForm.controls.building.setValue('');
            this.worker_building.push(event.id);
            let start_date = new Date;
            if (this.title == "Edit Worker") {
              let data = {
                name: this.workerForm.controls['name'].value,
                mobile: this.workerForm.controls['mobile'].value,
                password: this.workerForm.controls['password'].value,
                building_id: event.id,
                building_name: event.name,
                id: this.data.id,
                user_name: this.user_name,
                worker_building_id: this.editBuildings[0].worker_building_id,
                building_status: true,
                start_date: moment(start_date.toLocaleDateString('en-GB')).format("YYYY-MM-DD"),
                worker_id: this.editBuildings[0].worker_id,
                user_id: this.data.user_id,
              };
              this.buildingAdded = false;
              this.editBuildings.push(data);
            }
          }
        }
      });
      this.editBuildings.forEach(element => {
        if (element.building_name == event.name) {
          repeatedElementFound = true;
        }
      });
      if (repeatedElementFound == false) {
        let obj = {};
        obj['building_name'] = event.name;
        selectedBuildings.buildings.push(obj);
        this.worker_building.push(event.id);
        let start_date = new Date;
        if (this.title == "Edit Worker") {
          let data = {
            name: this.workerForm.controls['name'].value,
            mobile: this.workerForm.controls['mobile'].value,
            password: this.workerForm.controls['password'].value,
            building_id: event.id,
            building_name: event.name,
            id: this.data.id,
            user_name: this.user_name,
            worker_building_id: this.editBuildings[0].worker_building_id,
            building_status: true,
            start_date: moment(start_date.toLocaleDateString('en-GB')).format("YYYY-MM-DD"),
            worker_id: this.editBuildings[0].worker_id,
            user_id: this.data.user_id,
          };
          this.editBuildings.push(data);
        }
      }
    } else {
      let repeatedElementFound = false;
      const input = event.input;
      const value = event.value;
      if (this.buildingData.length == 0) {
        this.buildingData.push(event);
        this.worker_building.push(event.id)
      } else {
        this.buildingData.forEach(element => {
          if (element.name == event.name) {
            repeatedElementFound = true;
          }
        });
        if (repeatedElementFound == false) {
          this.buildingData.push(event);
          this.worker_building.push(event.id);
        }
      }

      // Reset the input value
      if (input) {
        input.value = '';
      }
      this.workerForm.controls.building.setValue('');
    }



  }
  remove(value: building, selectedBuildings: any): void {
    // const index = selectedBuildings.buildings.indexOf(value);
    let index = 0;
    if (selectedBuildings.buildings != undefined) {
      if (selectedBuildings.buildings.length != 0) {
        index = selectedBuildings.buildings.indexOf(value);
      } else {
        selectedBuildings.buildings = []
        selectedBuildings.buildings.push('building_name', value.building_name);
        index = selectedBuildings.buildings.indexOf(value);

      }
    } else {
      selectedBuildings.buildings = []
      selectedBuildings.buildings.push('building_name', value.building_name);
      index = selectedBuildings.buildings.indexOf(value);

    }

    if (index >= 0) {
      selectedBuildings.buildings.forEach(element => {
        if (element.building_name == value.building_name) {
          element.chipRemoved = true;
        }
      });
      selectedBuildings.buildings.splice(index, 1);
      this.editBuildings.forEach(element => {
        if (element.building_name == value.building_name && element.building_status == true) {
          element.building_status = false;
          element.chipRemoved = true;
        }

      });
    } else if (index < 0) {
      selectedBuildings.buildings.forEach(element => {
        if (element.building_name == value.building_name) {
          element.chipRemoved = false;
        }
      });
      selectedBuildings.buildings.splice(index, 1);
      this.editBuildings.forEach(element => {

        if (element.building_name == value.building_name && element.building_status == false && element.end_date == null) {
          element.building_status = true;
          element.chipRemoved = false;
          this.buildingsInchip.push({ 'building_name': value.building_name });
          console.log("bbbb", this.buildingsInchip);
        }

      });
    }
  }
  assignEndDate(event, data) {
    this.editBuildings.forEach(element => {
      if (element.building_id == data.building_id) {

        element.end_date = event.target.value;
      }
      console.log("final", this.editBuildings);
    });
  }
  addBuilding() {
    let obj = {}
    obj['ischecked'] = false;
    obj['end_date'] = '';
    obj['buildings'] = [];
    this.buildings_data.push(obj);

  }
  addSelected(event: MatAutocompleteSelectedEvent) {
    if (this.buildingData == undefined || this.buildingData == null) {
      this.buildingData = [];
    }
    this.buildingInput.nativeElement.value = '';

    this.workerForm.controls.building.setValue(null);
  }
  removeSelected(value: building) {
    const index = this.buildingData.indexOf(value);
    if (index >= 0) {
      this.buildingData.splice(index, 1);
    }
  }
  selected(event, selectedBuildings: any): void {
    // let repeatedElementFound = false;
    // selectedBuildings.buildings.forEach(element => {
    //   if (element.building_name == event.name) {
    //     repeatedElementFound = true;
    //     if (element.end_date != null && element.chipAdded != true) {
    //       let obj = {};
    //       obj['building_name'] = event.name;
    //       selectedBuildings.buildings.push(obj);
    //       element.chipAdded = true;
    //       // this.workerForm.controls.building.setValue('');
    //       this.worker_building.push(event.id);
    //       let start_date = new Date;
    //       if (this.title == "Edit Worker") {
    //         let data = {
    //           name: this.workerForm.controls['name'].value,
    //           mobile: this.workerForm.controls['mobile'].value,
    //           password: this.workerForm.controls['password'].value,
    //           building_id: event.id,
    //           building_name: event.name,
    //           id: this.data.id,
    //           user_name: this.user_name,
    //           worker_building_id: this.editBuildings[0].worker_building_id,
    //           building_status: true,
    //           start_date: moment(start_date.toLocaleDateString('en-GB')).format("YYYY-MM-DD"),
    //           worker_id: this.editBuildings[0].worker_id,
    //           user_id: this.data.user_id,
    //         };
    //         this.editBuildings.push(data);
    //       }
    //     }
    //   }
    // });
    // this.editBuildings.forEach(element => {
    //   if (element.building_name == event.name) {
    //     repeatedElementFound = true;
    //   }
    // });
    // if (repeatedElementFound == false) {
    //   let obj = {};
    //   obj['building_name'] = event.name;
    //   selectedBuildings.buildings.push(obj);
    //   this.worker_building.push(event.id);
    //   let start_date = new Date;
    //   if (this.title == "Edit Worker") {
    //     let data = {
    //       name: this.workerForm.controls['name'].value,
    //       mobile: this.workerForm.controls['mobile'].value,
    //       password: this.workerForm.controls['password'].value,
    //       building_id: event.id,
    //       building_name: event.name,
    //       id: this.data.id,
    //       user_name: this.user_name,
    //       worker_building_id: this.editBuildings[0].worker_building_id,
    //       building_status: true,
    //       start_date: moment(start_date.toLocaleDateString('en-GB')).format("YYYY-MM-DD"),
    //       worker_id: this.editBuildings[0].worker_id,
    //       user_id: this.data.user_id,
    //     };
    //     this.editBuildings.push(data);
    //   }
    // }
  }
  toggle(checked, selectedBuildings: any) {
    let obj = {};
    obj['isChecked'] = selectedBuildings.ischecked;
    this.buildingData.push(obj);
    console.log("building Data", this.buildingData);
    if (checked.checked == true) {
      selectedBuildings.ischecked = true;
    } else {
      selectedBuildings.ischecked = false;
    }
  }
  async getBuildings() {
    (await this.mastersService.getBuildingsList()).subscribe((response: any) => {

      console.log("all Buildings", response)
      this.allBuildings = response;
    });
  }
  private _filterbuilding(value: any) {
    if (this.allBuildings != undefined) {
      const filterValue = value.toLowerCase();
      return this.allBuildings.filter(option => option.name.toLowerCase().includes(filterValue));
    }
  }

  submit() {

    this.password = this.workerForm.controls['password'].value;
    this.confirmPassword = this.workerForm.controls['confirm_password'].value;
    if (this.password != this.confirmPassword) {
      Swal.fire({ icon: 'warning', title: "Confirm Password does not match" });
    } else if (this.title == "Add New Worker") {
      this.addNewWorker();
    } else this.editWorker();
  }
  async addNewWorker() {
    let date = {
      start_date: moment(new Date()).format("YYYY-MM-DD")
    }
    try {
      (await this.mastersService.addNewWorker(this.workerForm.value, this.user_name, this.worker_building, date)).subscribe(res => {
        console.log('response....', res)
        if (res.message == "Worker Added Successfully.") {
          Swal.fire({ icon: 'success', title: 'New Worker Added Successfully !', });
        } else if (res.message == "Worker Already Exists.") {
          Swal.fire({ icon: 'warning', title: 'Worker Already Exists !', });
        }
        this.dialogRef.close({ data: 'reload' });
      });
    }
    catch (err) {
      console.log('error while adding building', err);
    }
  }
  async editWorker() {
    this.editBuildings.forEach(element => {
      if (this.workerForm.controls['password'].value == '') {
        this.workerForm.controls['password'].setValue(element.password);
      }
      element.name = this.workerForm.controls['name'].value,
        element.mobile = this.workerForm.controls['mobile'].value,
        element.password = this.workerForm.controls['password'].value

    });
    console.log('response....');
    console.log(this.editBuildings)
    console.log(this.editBuildings[0].mobile)
    console.log(this.mobile)
    if (this.editBuildings[0].mobile == this.mobile) {
      console.log("matched")
      try {


        (await this.mastersService.editWorker(this.editBuildings)).subscribe((res: any) => {
          console.log('response....', res);
          let errorCount = 0;
          res.forEach(element => {
            if (element.success == false) {
              errorCount += 1;
            }
          });
          if (errorCount == 0) {
            Swal.fire({ icon: 'success', title: 'Worker Updated Successfully !', });

          } else {
            Swal.fire({ icon: "warning", title: "Error while updating worker !" });
          }

          this.dialogRef.close({ data: 'reload' });
        });




      }
      catch (err) {
        console.log('error while editing worker', err)
      }


    }
    else {
      console.log("not matched")
      try {
        Swal.fire({
          title: 'Are Sure You want to Change Number?',

          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#008000',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, Change !'
        }).then(async (result) => {
          if (result.value) {
            (await this.mastersService.editWorker(this.editBuildings)).subscribe((res: any) => {
              console.log('response....', res);
              let errorCount = 0;
              res.forEach(element => {
                if (element.success == false) {
                  errorCount += 1;
                }
              });
              if (errorCount == 0) {
                Swal.fire({ icon: 'success', title: 'Worker Updated Successfully !', });

              } else {
                Swal.fire({ icon: "warning", title: "Error while updating worker !" });
              }

              this.dialogRef.close({ data: 'reload' });
            });

          }
          else {
            this.workerForm.controls['password'].setValue('');

          }
        })

      }
      catch (err) {
        console.log('error while editing worker', err)
      }

    }

  }
  async getAllWorkers() {
    (await this.mastersService.getWorkersList()).subscribe((response: any) => {
      console.log("allWorkersList", response)
      this.allWorkers = response;

      this.editBuildings = this.allWorkers.filter(w => w.id == this.data.id);
      console.log("edit buildngs", this.editBuildings);
      this.mobile = this.editBuildings[0].mobile;
      console.log(this.mobile + "mobile is")
      let obj = {}

      this.editBuildings.forEach(element => {
        if (element.end_date != null) {
          element.building_status = false;
        } else {
          element.building_status = true;
        }
        element.user_name = this.user_name;
        let building_name = element.building_name;
        obj['building_name'] = building_name;

        obj['end_date'] = element.end_date;
        this.buildings.push(obj);
        obj = {};
        this.worker_building.push(element.building_id)
      });


      obj['ischecked'] = false;
      obj['end_date'] = '';
      obj['buildings'] = this.buildings;
      this.buildings_data.push(obj);
      console.log("buildddd", this.buildings_data);
      this.buildingsInchip = this.buildings_data[0].buildings;
      console.log("initial buildings", this.buildingsInchip);
    });
  }
  cancel() {
    this.dialogRef.close({ data: 'cancel' });
  }

}
