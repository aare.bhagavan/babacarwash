import { Router } from '@angular/router';
import { AuthService } from './../api/auth.service';
import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import Swal from 'sweetalert2';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    password_icon = 'visibility';
    passwordType = 'password'

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        public authService: AuthService,
        private _router: Router,
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.loginForm = this._formBuilder.group({
            number: ['', [Validators.required]],
            password: ['', Validators.required]
        });
    }

    showPassword() {
        if (this.password_icon == "visibility") {
            this.password_icon = "visibility_off";
            this.passwordType = "text";
        } else {
            this.password_icon = "visibility";
            this.passwordType = "password";
        }
    }

    async login() {
        (await this.authService.login(this.loginForm.controls['number'].value, this.loginForm.controls['password'].value)).subscribe((res: any) => {
            console.log(res);
            if (res.success == false) {
                console.log('Invalid Creds')
                Swal.fire({
                    icon: 'error',
                    title: res.message
                })
            } else {
                if (res.role == 'admin') {
                    localStorage.setItem('role', res.role);
                    localStorage.setItem('token', res.token);
                    localStorage.setItem('user_name', res.name);
                    this._router.navigate(["/locations"]);
                }
                else {
                    // localStorage.setItem('role', res.role);
                    // localStorage.setItem('org_id', res.organization_id);
                    console.log('Invalid Creds')
                    Swal.fire({
                        icon: 'error',
                        title: 'Wrong Credentials'
                    })
                }
                // localStorage.setItem('user_id', res.id);

            }
        })
    }
    OnDestroy() {
        localStorage.removeItem('loggedin');
    }
}
