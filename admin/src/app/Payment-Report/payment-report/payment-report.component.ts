import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { PaymentService } from 'app/api/payment.service';
import { MastersService } from 'app/api/masters.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import * as moment from 'moment';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-payment-report',
  templateUrl: './payment-report.component.html',
  styleUrls: ['./payment-report.component.scss'],
  animations: [
    trigger(
      'detailExpand',
      [
        state(
          'collapsed',
          style({ height: '0', minHeight: '0' })),
        state('expanded', style({ height: '*' })),
        transition('expanded <=> collapsed',
          animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
        ),
      ]
    ),
    fuseAnimations
  ],
})
export class PaymentReportComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  @ViewChild('filter', { static: true })
  filter: ElementRef;


  constructor(public paymentService: PaymentService, private _formBuilder: FormBuilder,
    private router: ActivatedRoute, public mastersService: MastersService) {
    this.router.queryParams.subscribe(params => {
      this.comingFrom = params.comingFrom
      if (params.comingFrom == 'worker') {
        this.navigate = 'workers';
        this.worker_id = params.worker_id
        this.worker_name = params.worker_name
        this.titlename = params.worker_name
        console.log(this.worker_id, this.worker_name);
      }
      if (params.comingFrom == 'building') {
        this.navigate = 'buildings';
        this.building_id = params.building_id

        this.buildingname = params.building_name
        this.titlename = params.building_name
        console.log(this.building_id, this.buildingname);
      }
      if (params.comingFrom == 'worker_washes') {
        this.navigate = 'workers';
        this.worker_id = params.worker_id
        this.worker_name = params.worker_name
        this.titlename = params.worker_name
        console.log(this.worker_id, this.worker_name);
      }


    });

  }
  dataSource: any;
  displayedColumns = ['sno', 'Customer', 'Parking', 'Vehicle', 'Cleaner', 'Building', 'Paymentdate', 'Totalpaidamount', 'note'];
  allBuildings: any;
  user_name: any;
  payments_reports: any;
  building_id: any;
  current_date: any
  from_date: any;
  date: any;
  navigate: any;
  buildingname: any;
  worker_id: any;
  worker_name: string;
  comingFrom: string;
  titlename: any;
  type: any;
  dateForm: FormGroup;
  todayDate: Date = new Date();
  ngOnInit() {

    let date = new Date()
    console.log(date)
    this.date = moment(date).format("DD-MM-YYYY");
    this.dateForm = this._formBuilder.group({
      DateRange: [{ begin: new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDay()), end: new Date() }, Validators.required]
    });



    this.current_date = date

    this.from_date = new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDay());

    if (this.comingFrom == 'worker') {
      this.workerPayments();
    }
    if (this.comingFrom == 'building') {
      this.buildingpayment();
    }
    if (this.comingFrom == 'worker_washes') {
      this.displayedColumns = ['sno', 'Customer', 'Parking', 'Vehicle', 'Cleaner', 'Building', 'cleaned_date'];
      this.getCarsWashedReport();
    }

  }
  async OnDateChange(event) {
    this.from_date = event.begin;
    this.current_date = event.end;
    if (this.comingFrom == 'worker') {
      this.workerPayments();
    }
    if (this.comingFrom == 'building') {
      this.buildingpayment();
    }
    if (this.comingFrom == 'worker_washes') {
      this.getCarsWashedReport();
    }

  }
  async report(event) {
    this.type = 'reportAll';
    if (event == 'building') {
      this.titlename = 'All Buildings';
      this.buildingpayment();
    }
    if (event == 'worker') {
      this.titlename = 'All Workers';
      this.workerPayments();
    }
    if (event == 'allWashes') {
      this.titlename = 'All Workers';
      this.getCarsWashedReport();
    }

  }
  async buildingpayment() {
    if (this.type == 'reportAll') {
      this.building_id = 'all';
    }
    let date = new Date();
    console.log(date);
    console.log(moment(date).format("DD-MM-YYYY"));

    // console.log("inside building");
    // this.building_id = localStorage.getItem("building_id");
    // this.buildingname = localStorage.getItem("Building_name");
    // console.log()
    console.log(this.building_id);
    var data = {
      'id': this.building_id,
      'fromdate': this.from_date,
      'todate': this.current_date

    }
    console.log(data);

    (await this.paymentService.buildingpaymentsdetails(data)).subscribe((res: any) => {
      console.log(res)
      this.payments_reports = res;


      console.log(this.payments_reports)

      this.payments_reports.forEach(element => {
        element.fullName=element.first_name+" "+element.last_name;
        element.building_name = element.building_name.includes('onetime') ? '' : element.building_name
        if (element.building_name == '') {
          element.location = '';
        }
        element.paid_date = moment(element.paid_date).format("DD-MM-YYYY");
        element.payment_note = element.payment_note == 'undefined' ? '-' : element.payment_note
      });




      console.log(this.payments_reports)

      this.dataSource = new MatTableDataSource(this.payments_reports);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'Customer': return item.first_name + " " + item.last_name;
            break;
          case 'Parking': return item.parking_no;
            break;
          case 'Vehicle': return item.vehicle;
            break;
          case 'Cleaner': return item.cleaner;
            break;
          case 'Building': return item.building_name + " " + item.location;
            break;
          case 'Paymentdate': return item.paid_date;
            break;
          case 'Totalpaidamount': return item.amount_paid;
            break;
          case 'note': return item.payment_note;
            break;
          default: return item[property];
        }
      };

    })

  }

  async workerPayments() {
    if (this.type == 'reportAll') {
      this.worker_id = 'all';
    }
    let date = new Date();
    console.log(date);
    console.log(moment(date).format("DD-MM-YYYY"));

    var data = {
      'id': this.worker_id,
      'fromdate': this.from_date,
      'todate': this.current_date

    }
    console.log(data);

    (await this.paymentService.workerPaymentsDetails(data)).subscribe((res: any) => {
      // console.log(res)
      this.payments_reports = res;
      console.log(this.payments_reports);

      this.payments_reports.forEach(element => {
        element.fullName=element.first_name+" "+element.last_name;
        element.building_name = element.building_name.includes('onetime') ? '' : element.building_name
        if (element.building_name == '') {
          element.location = '';
        }
        element.paid_date = moment(element.paid_date).format("DD-MM-YYYY");
        element.payment_note = element.payment_note == 'undefined' ? '-' : element.payment_note
      });
      this.dataSource = new MatTableDataSource(this.payments_reports);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'Customer': return item.first_name + " " + item.last_name;
            break;
          case 'Parking': return item.parking_no;
            break;
          case 'Vehicle': return item.vehicle;
            break;
          case 'Cleaner': return item.cleaner;
            break;
          case 'Building': return item.building_name + " " + item.location;
            break;
          case 'Paymentdate': return item.paid_date;
            break;
          case 'Totalpaidamount': return item.amount_paid;
            break;
          case 'note': return item.payment_note;
            break;
          default: return item[property];
        }
      };

    })

  }
  async getCarsWashedReport() {
    if (this.type == 'reportAll') {
      this.worker_id = 'all';
    }
    let date = new Date();
    console.log(date);
    console.log(moment(date).format("DD-MM-YYYY"));

    var data = {
      'id': this.worker_id,
      'fromdate': this.from_date,
      'todate': this.current_date

    }
    console.log(data);

    (await this.mastersService.getWashedCarsReport(data)).subscribe((res: any) => {
      // console.log(res)
      this.payments_reports = res;
      console.log(this.payments_reports);

      this.payments_reports.forEach(element => {
        element.fullName=element.first_name+" "+element.last_name;
        element.building_name = element.building_name.includes('onetime') ? '' : element.building_name
        if (element.building_name == '') {
          element.location = '';
        }
        element.work_date = moment(element.work_date).format("DD-MM-YYYY");

      });
      this.dataSource = new MatTableDataSource(this.payments_reports);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
          case 'Customer': return item.first_name + " " + item.last_name;
            break;
          case 'Parking': return item.parking_no;
            break;
          case 'Vehicle': return item.vehicle;
            break;
          case 'Cleaner': return item.cleaner;
            break;
          case 'Building': return item.building_name + " " + item.location;
            break;
          case 'cleaned_date': return item.work_date;
            break;
          default: return item[property];
        }
      };

    })
  }

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  export() {
    let header;
    let exportData = [];
    if (this.comingFrom == 'worker_washes') {
      header = ['Customer Name', 'Vehicle', 'Worker', 'Building', 'Location', 'Washed Date'];
      this.payments_reports.forEach(element => {
        let columnData = {
          'Customer Name': element.first_name + ' ' + element.last_name,
          Vehicle: element.vehicle,
          'Worker': element.cleaner,
          'Building': element.building_name,
          'Location': element.location,
          'Washed Date': element.work_date,

        }
        exportData.push(columnData);
      })
    } else {
      header = ['Customer Name', 'Vehicle', 'Worker', 'Building', 'Location', 'Payment Date', 'Paid Amount', 'Note'];
      this.payments_reports.forEach(element => {
        let columnData = {
          'Customer Name': element.first_name + ' ' + element.last_name,
          Vehicle: element.vehicle,
          'Worker': element.cleaner,
          'Building': element.building_name,
          'Location': element.location,
          'Payment Date': element.paid_date,
          'Paid Amount': element.amount_paid,
          'Note': element.payment_note
        }
        exportData.push(columnData);
      })
    }



    let csvData = this.ConvertToXLS(exportData, header);
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    if (this.comingFrom == 'worker') {
      dwldLink.setAttribute("download", this.titlename + '_Pending_Payments' + ".xls");
    }
    if (this.comingFrom == 'building') {
      dwldLink.setAttribute("download", this.titlename + '_Pending_Payments' + ".xls");
    }
    if (this.comingFrom == 'worker_washes') {
      dwldLink.setAttribute("download", this.titlename + '_Washed_Cars_Report' + ".xls");
    }

    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);


  }

  ConvertToXLS(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'sl_no,';

    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];
        line += ',' + array[i][head];
      }
      str += line + '\r\n';
    }

    return str;
  }

}

