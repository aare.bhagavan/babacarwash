import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuldingsComponent } from './buldings.component';

describe('BuldingsComponent', () => {
  let component: BuldingsComponent;
  let fixture: ComponentFixture<BuldingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuldingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuldingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
