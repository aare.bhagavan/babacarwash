import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { Subject, fromEvent } from 'rxjs';
import { MastersService } from 'app/api/masters.service';
import { Router,NavigationExtras } from '@angular/router';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AddBuildingsComponent } from 'app/add-buildings/add-buildings.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-buldings',
  templateUrl: './buldings.component.html',
  styleUrls: ['./buldings.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    fuseAnimations
  ],
  encapsulation: ViewEncapsulation.None
})
export class BuldingsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  dataSource: any;
  displayedColumns = ['id', 'name', 'location','payments','edit', 'delete'];
  allBuildings:any;
  user_name:any;

  constructor(
    public _router: Router,
    public mastersService: MastersService,
    public dialog: MatDialog,
  ) { 
    
  }

  ngOnInit() {
    this.user_name=localStorage.getItem("user_name");
    this.getAllBuildings();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged  ()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
      });
      
  }
  paymentsreports(building:any){
    console.log(building);
    // localStorage.setItem("location_id",selected.location_id)
    // localStorage.setItem("Building_name",selected.name)
    const navigationExtras: NavigationExtras = {
      queryParams: {
        building_id:building.id,
        building_name:building.name,
        comingFrom:'building'
      }
    };

    this._router.navigate(["/paymentreport"],navigationExtras);
 
  }
  async getAllBuildings() {
    (await this.mastersService.getBuildingsList()).subscribe((response: any) => {

      console.log("all Buildings", response)
      this.allBuildings = response;
      this.dataSource = new MatTableDataSource(this.allBuildings);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }
  addBuilding() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "500px";
    // dialogConfig.data = {
    //   data: business
    // }
    const dialogRef = this.dialog.open(AddBuildingsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data == 'reload') {
        this.getAllBuildings();
      }
    });
  }
  editBuilding(data) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = "500px";
    dialogConfig.data = {
      name: data.name,
      id: data.id,
      location_id: data.location_id,
      

    }
    const dialogRef = this.dialog.open(AddBuildingsComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result.data == 'reload') {
        this.getAllBuildings();
      }
    });
  }
  async deleteBuilding(id: number) {
    Swal.fire({
      title: 'Do You want to Delete?',
      text: "Building will be Deleted.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete !'
    }).then(async (result) => {
      if (result.value) {
        (await this.mastersService.deleteBuilding(id,this.user_name)).subscribe((response: any) => {
          console.log("response", response)
          if(response.message=="Building deleted successfully."){
            Swal.fire({
              icon:'success',
              title:"Building Deleted"
            })
          
          }
          this.getAllBuildings();
        });
      }
    })

  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
 
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}