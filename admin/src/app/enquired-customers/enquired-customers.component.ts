import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource, ThemePalette } from '@angular/material';
import { AddCustomerComponent } from 'app/add-customer/add-customer.component';
import { WorkListComponent } from 'app/work-list/work-list.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MastersService } from 'app/api/masters.service';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as _ from "underscore";
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { ActivateCustomerComponent } from 'app/activate-customer/activate-customer.component';
import { NavigationEnd, Router } from '@angular/router';

const ENQUIRY_TYPES = {
  NEW_ACCOUNT: 'NEW_ACCOUNT',
  NEW_CAR_ENQUIRY: 'NEW_CAR_ENQUIRY',
  COMPLAINTS: 'COMPLAINTS'
};
const enquiryTypeUrlMap = {
  new: 'NEW_ACCOUNT',
  existing: 'NEW_CAR_ENQUIRY',
  complaints: 'COMPLAINTS'
}

@Component({
  selector: 'app-enquired-customers',
  templateUrl: './enquired-customers.component.html',
  styleUrls: ['./enquired-customers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    fuseAnimations
  ],
})
export class EnquiredCustomersComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  dataSource: any;
  allCustomers: any = [];
  expandedElement: allCustomers | null;
  displayedColumns = [];

  allCustomers1: any[];
  user_name:any;
  links = ['Enquired Customers'];
  activeLink = '';badgeVal = 0;
  background: ThemePalette = undefined;
  status:any;
  enquiryType: string;

  constructor(public dialog: MatDialog, 
    public mastersService: MastersService,
    public router: Router
    ) { 
      this.router.events.subscribe((e) => {
        if (e instanceof NavigationEnd) {
          console.log(e.url);
          const type = e.url.split('/')[2];
          this.enquiryType = enquiryTypeUrlMap[type];
          if(this.enquiryType == 'NEW_CAR_ENQUIRY'){
            this.displayedColumns = ['slno','name', 'phone', 'email', 'premises', 'comments','plan'];
          }else{
            this.displayedColumns = ['slno','name', 'phone', 'email', 'premises', 'comments','plan','add'];
          }
        }
      })
    }

  
  ngOnInit() {
    this.user_name=localStorage.getItem("user_name");
    this.getCustomers();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
      });
  }
  addCustomer(addcustomer) {
    console.log("customerDetails",addcustomer);
    let customer ;
    customer = {
      first_name: addcustomer.name,
      mobile: addcustomer.phone,
      email: addcustomer.enq_email,
      flat_no: addcustomer.address,
      doesExist: addcustomer.doesExist,
      enqCustomerId: addcustomer.id
    }
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.autoFocus = true;
    dialogConfig.width = "75%";
    dialogConfig.height = "100%";
    dialogConfig.data = {
      customer: customer,
      edit: false,
      enquiry: true,
      doesExist: addcustomer.doesExist
    }
    const dialogRef = this.dialog.open(AddCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.getCustomers();
    });
  }



  async getCustomers() {
    console.log("🚀 ~ file: enquired-customers.component.ts ~ line 125 ~ EnquiredCustomersComponent ~ getCustomers ~ this.enquiryType", this.enquiryType);    
    (await this.mastersService.getAllEnquiredCustomer(this.enquiryType)).subscribe((response: any) => {

      console.log('customers list ', response)

      response = response.map((customer) => {
        try {
          customer.plan = JSON.parse(customer.plan)
        }  catch(err) {
          customer.plan = {
            selectedSubscriptionType: "",
            selectedVehicleType: "",
            weeklyFrequencyText: customer.plan
          }
        }
        return customer;
      })
      this.allCustomers = response;

      console.log('customer detais', this.allCustomers)
      this.activeLink = 'Enquired Customers';
      this.status = this.allCustomers;
      this.dataSource = new MatTableDataSource(this.status);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })
  };
  displayCustomersBy(value) {
    this.activeLink = value;
    this.status = this.allCustomers;
  
    this.dataSource = new MatTableDataSource(this.status);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  ngOnDestroy() {
    console.log("🚀 ~ file: enquired-customers.component.ts ~ line 145 ~ EnquiredCustomersComponent ~ ngOnDestroy ~ ngOnDestroy");
    this.setAllEnquiriesAsRead();
  }
  async setAllEnquiriesAsRead() {
    (await this.mastersService.setAllEnquiriesAsRead(this.enquiryType)).subscribe((response: any) => {
      console.log("🚀 ~ file: enquired-customers.component.ts ~ line 150 ~ EnquiredCustomersComponent ~ response", response)
    });
  }
}

export interface allCustomers {
  name: string;
  address: string;
  phone: string;
  comments: any;
  plan: any;
}
