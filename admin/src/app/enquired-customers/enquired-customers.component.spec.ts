import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnquiredCustomersComponent } from './enquired-customers.component';

describe('EnquiredCustomersComponent', () => {
  let component: EnquiredCustomersComponent;
  let fixture: ComponentFixture<EnquiredCustomersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnquiredCustomersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnquiredCustomersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
