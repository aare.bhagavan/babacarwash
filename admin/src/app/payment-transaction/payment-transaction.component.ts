import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatSort, MatTableDataSource } from '@angular/material';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { async } from '@angular/core/testing';
import { PaymentService } from 'app/api/payment.service';
import * as moment from 'moment';
import * as XLSX from 'xlsx';


export interface DialogData {
  type: string;
  data: any;
}

export interface allCustomers {
  column1: string;
  column2: string;
  column3: number;
}

@Component({
  selector: 'app-payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.scss'],
})

export class PaymentTransactionComponent implements OnInit {

  titleVehicle: string;
  titleCustomer: string;
  column1: string;
  column2: string;
  column3: string;
  column4: string;
  column5: string;
  column6: string;
  currentDate = moment();
  weekEnd;
  displayMarkasDone: boolean = false;

  searchItem: number;
  userLogs: any = [];
  displayedColumns = [];
  dataSource;
  _unsubscribeAll;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  @ViewChild('filter', { static: true })
  filter: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<PaymentTransactionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _router: ActivatedRoute,
    public paymentService: PaymentService,
  ) {
    this.titleVehicle = data.data.vehicle;
    this.titleCustomer = data.data.first_name + ' ' + data.data.last_name;
    this.searchItem = data.data.schedule_id;
    if (data.type == "charged") {
      this.column1 = 'Date';
      this.column2 = 'Day of Week';
      this.column3 = "Cleaner";
      this.column4 = 'Mark Not Done';
      this.column5 = 'Reason';
      this.displayedColumns = ['column1', 'column2', 'column3', 'column4', 'column5'];

    } else if (data.type == "paid") {
      this.column1 = 'Paid To';
      this.column2 = 'Amount Paid';
      this.column3 = "Paid On";
      this.column4 = 'Undo Payment';
      this.column5 = 'Reason';
      this.column6 = 'Comments';
      this.displayedColumns = ['column1', 'column2', 'column3', 'column4', 'column5', 'column6'];
    }

  }

  ngOnInit() {
    this.customerData();
    // fromEvent(this.filter.nativeElement, 'keyup')
    //   .pipe(
    //     takeUntil(this._unsubscribeAll),
    //     debounceTime(150),
    //     distinctUntilChanged()
    //   )
    //   .subscribe(() => {
    //     if (!this.dataSource) {
    //       return;
    //     }
    //     this.dataSource.filter = this.filter.nativeElement.value;
    //     console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
    //   });

  }

  async customerData() {
    (await this.paymentService.userPaymentDetails(this.searchItem, this.data.type)).subscribe((response: any) => {
      let columns;
      response.forEach(element => {
        let action;
        let color;
        if (this.data.type == "charged") {
          if (element.status == 'pending') {
            action = 'Mark as Done';
            color = 'warn';
          } else {
            action = 'Mark Not Done';
            color = 'accent';
          }
          let schedules = [];
          schedules[0] = element.sunday;
          schedules[1] = element.monday;
          schedules[2] = element.tuesday;
          schedules[3] = element.wednesday;
          schedules[4] = element.thursday;
          schedules[5] = element.friday;
          schedules[6] = element.saturday;
          this.weekEnd = this.currentDate.clone().endOf('week');
          if (moment(element.date).isSameOrAfter(this.weekEnd)) {
            this.displayMarkasDone = true;
          }
          else this.displayMarkasDone = false;
          columns = {
            column1: moment(element.date).format("DD-MM-YYYY"),
            column2: moment(element.date).format("dddd"),
            column3: element.cleaner,
            column4: action,
            column5: element.reason,
            id: element.id,
            color: color,
            status: element.status,
            schedules: schedules,
            schedule_id: this.searchItem,
            date: element.date,
            buttonDisable: this.displayMarkasDone
          }
        } else if (this.data.type == "paid") {
          if (element.status == 'pending') {
            action = 'Redo Payment';
            color = 'warn';
          } else {
            action = 'Undo Payment';
            color = 'accent';
          }
          let schedules = [];
          schedules[0] = element.sunday;
          schedules[1] = element.monday;
          schedules[2] = element.tuesday;
          schedules[3] = element.wednesday;
          schedules[4] = element.thursday;
          schedules[5] = element.friday;
          schedules[6] = element.saturday;

          columns = {
            column1: element.paid_to,
            column2: element.amount_paid,
            column3: moment(element.paid_on).format("DD-MM-YYYY"),
            column4: action,
            column5: element.reason,
            id: element.id,
            color: color,
            status: element.status,
            column6: element.comments == 'undefined' ? '' : element.comments,
            schedules: schedules,
            schedule_id: this.searchItem,
            log_month_last_date: element.log_month_last_date
          }
        }

        this.userLogs.push(columns);
      })
      this.dataSource = new MatTableDataSource(this.userLogs);
      this.dataSource.sort = this.sort;
      // this.dataSource.table = this.table;
    });
  }

  export() {
    let header;
    let exportData = [];
    if (this.data.type == "charged") {
      header = ['Date', 'Week', 'Cleaner', 'Status', 'Reason'];
      this.userLogs.forEach(element => {
        let columnData = {
          Date: element.column1,
          Week: element.column2,
          Cleaner: element.column3,
          Status: element.status,
          Reason: element.column5 == null ? '' : element.column5
        }
        exportData.push(columnData);
      })
    } else if (this.data.type == "paid") {
      header = ['Paid_to', 'Amount_paid', 'Paid_on', 'Status', 'Reason', 'Comments'];
      this.userLogs.forEach(element => {
        let columnData = {
          Paid_to: element.column1,
          Amount_paid: element.column2,
          Paid_on: element.column3,
          Status: element.status,
          Reason: element.column5 == null ? '' : element.column5,
          Comments: element.column6
        }
        exportData.push(columnData);
      })
    }

    let csvData = this.ConvertToCSV(exportData, header);
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", this.titleCustomer + " Amount " + this.data.type + ".xls");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);


  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'sl_no,';

    for (let index in headerList) {
      row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
      let line = (i + 1) + '';
      for (let index in headerList) {
        let head = headerList[index];
        line += ',' + array[i][head];
      }
      str += line + '\r\n';
    }

    return str;
  }

  cancel() {
    this.dialogRef.close({ data: 'cancel' });
  }

  raiseRequest(row_value) {
    let text = '';
    let subtext = '';
    if (this.data.type == "charged") {
      if (row_value.column4 == "Mark Not Done") {
        text = 'Do you want to mark the job as Not Done';
        subtext = "Marked as Not Done";
      } else {
        text = 'Do you want to mark the job as Done';
        subtext = "Marked as Done";
      }

    } else if (this.data.type == "paid") {
      if (row_value.column4 == "Undo Payment") {
        text = 'Do you want to undo this payment';
        subtext = 'Payment is sucessfully Undo';
      } else {
        text = 'Do you want to redo this payment';
        subtext = 'Payment is sucessfully done';
      }

    }
    const swalWithBootstrapButtons = Swal.mixin({
      buttonsStyling: true
    })

    swalWithBootstrapButtons.fire({
      title: text,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'cancel',
    }).then(async (result) => {
      if (result.value) {
        const { value: feedback } = await Swal.fire({
          title: 'Enter Your reason',
          input: 'text',
          showCancelButton: true,
          inputPlaceholder: 'Enter your reason',
          inputValidator: (value) => {
            return !value && 'Reason cannot be empty'
          }
        })
        if (feedback) {
          (await this.paymentService.adminFeedBack(row_value, feedback, this.data.type)).subscribe((response: any) => {
            if (response == true) {
              Swal.fire({
                icon: 'success',
                title: subtext,
                showConfirmButton: false,
                timer: 1500
              });
              this.dialogRef.close({ data: 'cancel', status: 'do' });
            } else {
              Swal.fire({
                title: "There is some error in undating",
                showConfirmButton: false,
                timer: 1500
              })
            }
          })
        }
      }
    })
  }

}
