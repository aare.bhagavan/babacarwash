import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource, ThemePalette } from '@angular/material';
import { AddCustomerComponent } from 'app/add-customer/add-customer.component';
import { WorkListComponent } from 'app/work-list/work-list.component';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { fuseAnimations } from '@fuse/animations';
import { MastersService } from 'app/api/masters.service';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as _ from "underscore";
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { ActivateCustomerComponent } from 'app/activate-customer/activate-customer.component';


@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    fuseAnimations
  ],
})

export class CustomersComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;

  @ViewChild(MatSort, { static: true })
  sort: MatSort;

  @ViewChild('filter', { static: true })
  filter: ElementRef;
  private _unsubscribeAll: Subject<any>;
  dataSource: any;
  allCustomers: any = [];
  expandedElement: allCustomers | null;

  displayedColumns = ['slno','name', 'mobile', 'building', 'edit', 'delete', 'expand'];
  allCustomers1: any[];
  user_name:any;
  links = ['Active', 'Inactive','SelfRegisteredCustomers'];
  activeLink = '';badgeVal = 0;
  background: ThemePalette = undefined;
  status:any;


  constructor(public dialog: MatDialog, public mastersService: MastersService) { }

  ngOnInit() {
    this.user_name=localStorage.getItem("user_name");
    this.getCustomers();
    fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(150),
        distinctUntilChanged()
      )
      .subscribe(() => {
        if (!this.dataSource) {
          return;
        }

        this.dataSource.filter = this.filter.nativeElement.value;
        console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
      });
  }
  addCustomer() {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.autoFocus = true;
    dialogConfig.width = "75%";
    dialogConfig.height = "100%";
    dialogConfig.data = {
      edit: false
    }
    const dialogRef = this.dialog.open(AddCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.getCustomers();
    });
  }
  editCustomer(customer) {
    const dialogConfig = new MatDialogConfig();
    // dialogConfig.autoFocus = true;
    dialogConfig.width = "75%";
    dialogConfig.height = "100%";
    dialogConfig.data = {
      customer: customer,
      edit: true
    }
    const dialogRef = this.dialog.open(AddCustomerComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      this.getCustomers();
    });
  }

  async getCustomers() {
    (await this.mastersService.getAllCustomer()).subscribe((response: any) => {

      console.log('customers list ', response)
      let uniqueArray = _.map(_.groupBy(response, (obj: any) => {
        return obj.cust_id;
      }), (grouped) => {
        return grouped[0];
      })
      console.log(uniqueArray)

      uniqueArray.forEach(customer => {
        customer['fullName']=customer.first_name+' '+(customer.last_name? customer.last_name: '');
        customer['building_name']=customer.building_id == 1 ? '' : customer.name;
        customer['location']=customer.location_id == 3 ? '' : customer.address;
        customer['user_id']=customer.user_id;
        customer['vehicles'] = []
        let customerVehicles = response.filter(custvehicle => custvehicle.cust_id == customer.cust_id);

        customerVehicles.forEach((vehicle, index) => {
          if (vehicle.veh_id != null) {
            let obj = {
              end_date: vehicle.end_date,
              number: vehicle.registration_no,
              vehicle_id: vehicle.veh_id,
              cleaner_id: vehicle.worker_id,
              worker_name: vehicle.worker_name,
              parking_no: vehicle.parking_no,
              startDate: moment(vehicle.start_date).format("LL"),
              weekdays: [],
              subscription_amount:vehicle.subscription_amount,
              work_schedule_id: vehicle.work_schedule_id,
              
            }
            customer.vehicles.push(obj)
            if (vehicle.monday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Mon', value: true })
            } if (vehicle.tuesday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Tue', value: true })
            } if (vehicle.wednesday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Wed', value: true })
            } if (vehicle.thursday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Thu', value: true })
            } if (vehicle.friday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Fri', value: true })
            } if (vehicle.saturday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Sat', value: true })
            } if (vehicle.sunday == 1) {
              customer.vehicles[index].weekdays.push({ day: 'Sun', value: true })
            }
          }
        });

      });

      this.allCustomers = uniqueArray;

      console.log('customer detais', this.allCustomers)
      this.activeLink = 'Active';
      this.status = this.allCustomers.filter(map => map.cust_status == 1 && map.building_id != 1);
      this.dataSource = new MatTableDataSource(this.status);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.badgeVal = this.allCustomers.filter(map => map.building_id == 1).length;
    })
  };


  async getWorkDetails(vehicle: any) {
    console.log('vehicle info', vehicle);
    (await this.mastersService.getWorkDetails(vehicle)).subscribe((response: any) => {
      console.log(response);
      const dialogConfig = new MatDialogConfig();
      dialogConfig.autoFocus = true;
      dialogConfig.width = "50%";
      dialogConfig.height = "65%";
      dialogConfig.data = { data: response, worker: vehicle.worker_name, vehicle: vehicle.number }
      const dialogRef = this.dialog.open(WorkListComponent, dialogConfig);
      dialogRef.afterClosed().subscribe(result => {
        // if (result.data == 'reload') {

        // }
      });

    })
  }
  async activateCustomer(customer:any) {
    Swal.fire({
      title: 'Do You want to Activate?',
      text: "Customer will be activated.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Activate !'
    }).then(async (result) => {
      if (result.value) {
        const dialogConfig = new MatDialogConfig();
        // dialogConfig.autoFocus = true;
        dialogConfig.width = "50%";
        dialogConfig.height = "50%";
        dialogConfig.data = {
          id: customer.cust_id,
         
        }
        const dialogRef = this.dialog.open(ActivateCustomerComponent, dialogConfig);
        dialogRef.afterClosed().subscribe(result => {
          this.getCustomers();
        });
      }
    })
  }
  async deleteCustomer(id: number) {
    Swal.fire({
      title: 'Do You want to Delete?',
      text: "Customer will be Deleted.",
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#008000',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete !'
    }).then(async (result) => {
      if (result.value) {
        (await this.mastersService.deleteCustomer(id,this.user_name)).subscribe((response: any) => {
          console.log("response", response)
          if(response.message=="Customer deleted successfully."){
            Swal.fire({
              icon:'success',
              title:"Customer Deleted"
            })
          }else{
            Swal.fire({icon:'warning',title:"Error while deleting customer!"});
          }
          this.getCustomers();
        });
      }
    })

  }
  displayCustomersBy(value) {
    let filterValue = 1;
    this.activeLink = value;
    if(value == 'Inactive'){
      filterValue = 0;
    }
    this.status = this.allCustomers.filter(map => map.cust_status == filterValue);
    if(value == 'SelfRegisteredCustomers'){
      filterValue = 1;
      this.status = this.allCustomers.filter(map => map.building_id == filterValue);
    }
  
    // else if(value == 'One Time') {
    //   filterValue = 1
    //   custStatus = this.allCustomers.filter(status => status.cust_status == filterValue )
    //   this.status = custStatus.filter(map => map.one_time == filterValue);
    // }
    this.dataSource = new MatTableDataSource(this.status);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
export interface allCustomers {
  first_name: string;
  last_name: string;
  address: string;
  building: string;
  vehicles: any;
  mobile: string;
}