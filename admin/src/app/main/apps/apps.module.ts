import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';
import {FileUploadModule} from "ng2-file-upload";

// import { EditBusinessComponent } from './business/edit-business/edit-business.component';
// import { AddBusinessComponent } from './business/add-business/add-business.component';
// import { BusinessComponent } from './business/business/business.component';

const routes = [
    {
        path: 'e-commerce',
        loadChildren: './e-commerce/e-commerce.module#EcommerceModule'
    },
    {
        path: 'dashboards',
        loadChildren: './dashboards/analytics/analytics.module#AnalyticsDashboardModule'
    },
    {
        path        : 'import',
        loadChildren: './importutil/importutil.module#ImportUtilModule'
    },

];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        FuseSharedModule,
        FileUploadModule
    ],
    // declarations: [EditBusinessComponent],
    // declarations: [AddBusinessComponent],
    // declarations: [BusinessComponent]
})
export class AppsModule {
}
