import { Router } from '@angular/router';
// import { RegisterDialogBoxComponent } from './../../../../register-dialog-box/register-dialog-box.component';
// import { EditUserComponent } from './../../../../edit-user/edit-user.component';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { fromEvent, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { EcommerceProductsService } from 'app/main/apps/e-commerce/products/products.service';
import { takeUntil } from 'rxjs/internal/operators';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
// import { AddSymptomsDailogComponent } from '../add-symptoms-dailog/add-symptoms-dailog.component'
// import { EditPersonDialogComponent } from '../edit-person-dialog/edit-person-dialog.component'
// import { BusinessService } from 'app/api/business.service';
import { Sort } from '@angular/material/sort';
// import { SymtomsService } from 'app/api/symptoms.service';
import * as moment from 'moment';
import * as _ from 'underscore';
import { MatDatepicker } from '@angular/material/datepicker';
import { MomentDateModule } from '@angular/material-moment-adapter';
// import { MastersService } from 'app/api/masters.service';
import Swal from 'sweetalert2';



@Component({
    selector: 'e-commerce-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
        fuseAnimations
    ],
    encapsulation: ViewEncapsulation.None
})
export class EcommerceProductsComponent implements OnInit {

    // app admin
    appAdminUsing: boolean = false;
    appAdminColumns = [
        'business_id',
        'business_user_id',
        'business_user_name',
        'business_user_mobile',
        'business_user_email',
        'business_user_role',
        'edit',
        'business_user_created_at',
        'business_user_updated_at'
    ];
    // app admin


    expandedElement: null;
    public business_id: any;
    public role: string;
    symptoms: any[] = [];
    dataSource: any;
    user: boolean = false;
    businessmembers: any[] = [];
    //  displayedColumns = ['name', 'mobile', 'edit', 'symptoms', 'free_zone', 'expand'];
    displayedColumns = ['id', 'organization', 'name', 'edit', 'delete'];
    orgName: any;
    subBusinessList: any;
    displayBusinessTab: boolean = false;

    @ViewChild(MatPaginator, { static: true })
    paginator: MatPaginator;

    @ViewChild(MatSort, { static: true })
    sort: MatSort;

    @ViewChild('filter', { static: true })
    filter: ElementRef;
    sortedData: any;
    private _unsubscribeAll: Subject<any>;
    // @ViewChild('picker') datePicker: MatDatepicker<Date>;
    start_date;
    to_date;
    isDateSelected: boolean = true;
    isDatepickerAvailable: boolean = true;
    allDepartments: any;
    filteredDepartments: any;
    isSuper_admin: boolean;
    org_id: number;
    constructor(
        public dialog: MatDialog,
        private _ecommerceProductsService: EcommerceProductsService,
        // public businessService: BusinessService,
        // public symptomsService: SymtomsService,
        private _router: Router,
        // public mastersService: MastersService

    ) {
        this._unsubscribeAll = new Subject();
        this.sortedData = this.businessmembers.slice();
    }

    async ngOnInit() {




        // this.role = localStorage.getItem("role");
        // this.user = this.role == 'user' ? true : false;
        // if (this.role == 'super_admin') {
        //     let superBusinessId = localStorage.getItem('super_business_id');
        //     (await this.businessService.getSubBusinessDetails(superBusinessId)).subscribe((res: any) => {
        //         console.log(res);
        //         this.subBusinessList = res.business;
        //         this.business_id = res.business[0].id;
        //         this.getBusinessMembersList();
        //     })
        //     localStorage.setItem('business_id', this.business_id);
        //     this.displayBusinessTab = true;
        // }
        // else if (this.role == 'app_admin') {
        //     (await this.businessService.getAllBusinessList()).subscribe((res: any) => {
        //         console.log(res);
        //         this.subBusinessList = res.business;
        //         this.business_id = res.business[0].id;
        //         this.getBusinessMembersList();
        //     })
        //     localStorage.setItem('business_id', this.business_id);
        //     this.displayBusinessTab = true;
        // }
        // else {
        //     this.business_id = localStorage.getItem('business_id');
        //     this.displayBusinessTab = false;
        //     this.getBusinessMembersList();
        // }

        fromEvent(this.filter.nativeElement, 'keyup')
            .pipe(
                takeUntil(this._unsubscribeAll),
                debounceTime(150),
                distinctUntilChanged()
            )
            .subscribe(() => {
                if (!this.dataSource) {
                    return;
                }

                this.dataSource.filter = this.filter.nativeElement.value;
                console.log("EcommerceProductsComponent -> ngOnInit -> this.dataSource", this.dataSource)
            });

        // if (this.role == 'user') {
        //     // this.displayedColumns = ['name', 'mobile', 'symptoms', 'free_zone', 'expand'];
        //     this.isDatepickerAvailable = false;
        // }
        this.role = localStorage.getItem('role');
        console.log(this.role);
        if (this.role == 'super_admin') {
            this.isSuper_admin = true;
        }
        this.org_id = parseInt(localStorage.getItem('org_id'));
        if (this.role == 'admin') {
            this.displayedColumns = ['id', 'name', 'edit', 'delete'];

        }
        // this.getDepartmentList();
    }

    // async getDepartmentList() {
    //     (await this.mastersService.getDepartmentList(this.role, this.org_id)).subscribe((response: any) => {
    //         console.log("all department", response)
    //         this.allDepartments = response;
    //         this.filteredDepartments = this.allDepartments.filter((department) => department.is_active == 1);
    //         this.dataSource = new MatTableDataSource(this.filteredDepartments);
    //     });
    // }
    // async deleteDepartment(department) {
    //     Swal.fire({
    //         title: 'Do You want to Delete?',
    //         text: "Department will be Deleted.",
    //         icon: 'question',
    //         showCancelButton: true,
    //         confirmButtonColor: '#008000',
    //         cancelButtonColor: '#d33',
    //         confirmButtonText: 'Yes, Delete !'
    //     }).then(async (result) => {
    //         if (result.value) {
    //             (await this.mastersService.deleteDepartment(department.id)).subscribe((response: any) => {
    //                 console.log("response", response)
    //                 if (response.status) {
    //                     Swal.fire({
    //                         icon: 'success',
    //                         title: "Department Deleted"
    //                     })
    //                     this.getDepartmentList();
    //                 }
    //             });
    //         }
    //     })
    // }


    // async getBusinessMembersList() {
    //     (await this.businessService.getBusinessMemberList(this.business_id)).subscribe((response: any) => {
    //         console.log(response)
    //         this.businessmembers = response.businessmember;
    //         this.dataSource = new MatTableDataSource(this.businessmembers);
    //         this.dataSource.paginator = this.paginator;
    //         this.dataSource.sort = this.sort;
    //         this.businessmembers.forEach(member => {
    //             member.member_symptoms.forEach(member_symptom => {
    //                 member_symptom.date = moment(member_symptom.updated_at).format('YYYY-MM-DD HH:mm:ss');
    //                 const symptoms = Object.keys(member_symptom).filter(key => member_symptom[key] === true);
    //                 member_symptom.symptoms = symptoms.map(symptom => symptom.replace('_', ' '));
    //             });
    //             member.free_zone = 'unknown';
    //             if (member.containment_data.length > 0) {
    //                 member.free_zone = member.containment_data[0].free_zone.toLowerCase()
    //             }
    //         });

    //         // SORT ARRAY //
    //         this.businessmembers.forEach(member => {
    //             member.member_symptoms = _.sortBy(member.member_symptoms, 'updated_at').reverse();
    //         });
    //         console.log(this.businessmembers);
    //     });
    // }

    // addSymptoms(person) {
    //     const dialogConfig = new MatDialogConfig();
    //     dialogConfig.autoFocus = true;
    //     dialogConfig.width = "900px";
    //     dialogConfig.maxWidth = "900px";
    //     dialogConfig.minWidth = "500px";
    //     dialogConfig.data = {
    //         id: person.id,
    //         name: person.first_name,
    //     }
    //     const dialogRef = this.dialog.open(AddSymptomsDailogComponent, dialogConfig);
    //     dialogRef.afterClosed().subscribe(result => {
    //         this.getBusinessMembersList();
    //     });
    // }

    // editDepartment(department) {
    //         const dialogConfig = new MatDialogConfig();
    //         dialogConfig.autoFocus = true;
    //         dialogConfig.width = "500px";
    //         dialogConfig.maxHeight = "500px";
    //         dialogConfig.data = {
    //             name: department.name,
    //             id: department.id,
    //             org_id:department.org_id,
    //             organization_name:department.organization_name

    //         }
    //     const dialogRef = this.dialog.open(EditPersonDialogComponent, dialogConfig);
    //         dialogRef.afterClosed().subscribe(result => {
    //             if (result.data == 'reload') {
    //                 this.getDepartmentList();
    //             }
    //         });
    //     }
    addDepartment() {
            const dialogConfig = new MatDialogConfig();
            dialogConfig.autoFocus = true;
            dialogConfig.width = "500px";
            dialogConfig.maxHeight = "500px";

            // const dialogRef = this.dialog.open(EditPersonDialogComponent, dialogConfig);
            // dialogRef.afterClosed().subscribe(result => {
            //     if (result.data == 'reload') {
            //         if(result.status.success)
            //         Swal.fire({
            //             icon: 'success',
            //             title: result.status.message
            //         });
            //         else 
            //         {
            //             Swal.fire({
            //                 icon: 'error',
            //                 title: result.status.message
            //             });
            //         }
            //         this.getDepartmentList();
            //     }
            // });
        }
    // addZone() {
    //     const dialogConfig = new MatDialogConfig();
    //     dialogConfig.autoFocus = true;
    //     dialogConfig.width = "500px";
    //     // dialogConfig.data = {
    //     //   data: business
    //     // }
    //     const dialogRef = this.dialog.open(AddEditSuperBusinessComponent, dialogConfig);
    //     dialogRef.afterClosed().subscribe(result => {

    //       if (result.data == 'reload') {
    //         Swal.fire({
    //           icon: 'success',
    //           title: 'Zone Added successfully',
    //         });
    //         this.getZoneList();
    //       }
    //     });

    //   }

    applyFilter(filterValue: string) {
            this.dataSource.filter = filterValue.trim().toLowerCase();
            if(this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }


    sortData(sort: Sort) {
        const data = this.businessmembers.slice();
        if (!sort.active || sort.direction === '') {
            this.sortedData = data;
            return;
        }
        this.sortedData = data.sort((a, b) => {
            const isAsc = sort.direction === 'asc';
            switch (sort.active) {
                case 'business_id': return compare(a.business_id, b.business_id, isAsc);
                case 'business_user_id': return compare(a.business_user_id, b.business_user_id, isAsc);
                case 'business_user_name': return compare(a.business_user_name, b.business_user_name, isAsc);
                case 'business_user_mobile': return compare(a.business_user_mobile, b.business_user_mobile, isAsc);
                case 'business_user_email': return compare(a.business_user_email, b.business_user_email, isAsc);
                case 'business_user_role': return compare(a.business_user_role, b.business_user_role, isAsc);
                case 'business_user_created_at': return compare(a.business_user_created_at, b.business_user_created_at, isAsc);
                case 'business_user_updated_at': return compare(a.business_user_updated_at, b.business_user_updated_at, isAsc);
                default: return 0;
            }
        });
    }

    // selectedBusniness(data: any) {
    //     this.subBusinessList.forEach(list => {
    //         if (list.title == data.tab.textLabel) {
    //             this.business_id = list.id;
    //         }
    //     })
    //     localStorage.setItem('business_id', this.business_id);
    //     this.getBusinessMembersList();
    // }

    public OnDateChange(event) {
        console.log(event);
        this.start_date = moment(event.begin).format('YYYY-MM-DD HH:mm:ss');
        this.to_date = moment(event.end).format('YYYY-MM-DD HH:mm:ss');
        // console.log(this.start_date)
        this.isDateSelected = false;
    }
    dateFilter() {
        let filteredMembers = [];

        this.businessmembers.forEach(member => {

            member.member_symptoms.forEach(member_data => {

                let inDateRange = moment(member_data.date).isBetween(this.start_date, this.to_date);

                if (inDateRange == true) {

                    if (filteredMembers.indexOf(member) == -1) {
                        filteredMembers.push(member);
                    }

                    // console.log(this.filteredMembers)
                }

            });
        });

        this.dataSource = filteredMembers;
        console.log(this.dataSource);

    }
}



function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
