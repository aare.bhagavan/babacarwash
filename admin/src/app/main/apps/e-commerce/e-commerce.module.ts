import { EcommerceProductsComponent } from 'app/main/apps/e-commerce/products/products.component';
import { MatCardModule } from '@angular/material/card';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatRadioModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { SatNativeDateModule, SatDatepickerModule } from 'saturn-datepicker'
import { ChartsModule } from 'ng2-charts';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { EcommerceProductsService } from 'app/main/apps/e-commerce/products/products.service';
// import { EcommerceProductComponent } from 'app/main/apps/e-commerce/product/product.component';
// import { EcommerceProductService } from 'app/main/apps/e-commerce/product/product.service';
import { EcommerceOrdersComponent } from 'app/main/apps/e-commerce/orders/orders.component';
import { EcommerceOrdersService } from 'app/main/apps/e-commerce/orders/orders.service';
import { EcommerceOrderComponent } from 'app/main/apps/e-commerce/order/order.component';
import { EcommerceOrderService } from 'app/main/apps/e-commerce/order/order.service';
// import { AddBusinessComponent } from '../business/add-business/add-business.component';
import { MatTableExporterModule } from 'mat-table-exporter';


const routes: Routes = [

    {
        path: 'details',
        component: EcommerceProductsComponent,
        resolve: {
            data: EcommerceProductsService
        }
    },

    // {
    //     path: 'business/new',
    //     component: AddBusinessComponent,
    // },
    // {
    //     path: 'details/new',
    //     component: EcommerceProductComponent,
    //     resolve: {
    //         data: EcommerceProductService
    //     }
    // },
    // {
    //     path: 'products/:id/:handle',
    //     component: EcommerceProductComponent,
    //     resolve: {
    //         data: EcommerceProductService
    //     }
    // },
    {
        path: 'orders',
        component: EcommerceOrdersComponent,
        resolve: {
            data: EcommerceOrdersService
        }
    },
    {
        path: 'orders/:id',
        component: EcommerceOrderComponent,
        resolve: {
            data: EcommerceOrderService
        }
    }
];

@NgModule({
    declarations: [
        // EcommerceProductComponent,
        EcommerceOrdersComponent,
        EcommerceOrderComponent,
        EcommerceProductsComponent,
        // AddBusinessComponent
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatRadioModule,
        MatCardModule,
        MatGridListModule,

        MatCheckboxModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatMomentDateModule,
        MatNativeDateModule,

        SatDatepickerModule,
        SatNativeDateModule,
        MatTableExporterModule,
        NgxChartsModule,
        ChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD3zC0kD5-3rNM6j4ThtxZEgRf-Xc0qlEI'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers: [
        // EcommerceProductsService,
        // EcommerceProductService,
        EcommerceOrdersService,
        EcommerceOrderService,
    ]
})
export class EcommerceModule {
}
