
import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

export interface DialogData {
  id:number;
  name: string;
  mobile: number;
  age: number;
  gender: string;
  location_id:number;
  building_id:number;
  password:any;
  user_id:number;
}
@Component({
  selector: 'app-enter-otp-dialog',
  templateUrl: './enter-otp-dialog.component.html',
  styleUrls: ['./enter-otp-dialog.component.scss']
})
export class EnterOtpDialogComponent implements OnInit {
  otpForm: FormGroup;

  constructor(private fb: FormBuilder,
    public dialogRef: MatDialogRef<EnterOtpDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public _router: Router,
    public _route: ActivatedRoute) {
    this.otpForm = this.fb.group({
      otp: ['', Validators.required]

    })
  }

  ngOnInit() {
  }
  submit() {
    this.dialogRef.close(this.otpForm.controls.otp.value);
  }
  close() {
    this.dialogRef.close({ data: 'cancel' });
  }

}
