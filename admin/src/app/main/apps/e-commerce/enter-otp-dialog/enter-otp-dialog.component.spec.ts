import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterOtpDialogComponent } from './enter-otp-dialog.component';

describe('EnterOtpDialogComponent', () => {
  let component: EnterOtpDialogComponent;
  let fixture: ComponentFixture<EnterOtpDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterOtpDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterOtpDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
