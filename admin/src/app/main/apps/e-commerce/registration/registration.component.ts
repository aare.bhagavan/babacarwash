import { takeUntil, startWith, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './../../../../api/auth.service';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import Swal from 'sweetalert2';

import { fuseAnimations } from '@fuse/animations';
import * as _ from 'underscore';


@Component({
    selector: 'registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss'],
    animations: [fuseAnimations],
    encapsulation: ViewEncapsulation.None
})
export class RegistrationComponent implements OnInit {
    registrationFrom: FormGroup;
    password_icon1 = 'visibility';
    password_icon2 = 'visibility';
    password_type = 'password';
    confirmPassword_type = 'password';
    breakpoint;
    allBusinessList: any;
    filteredBusiness: Observable<any[]>;
    roles = [
        { value: 'super_admin', viewValue: 'Super Admin' },
        { value: 'admin', viewValue: 'Admin' },
        { value: 'user', viewValue: 'User' }
    ];
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _formBuilder: FormBuilder,
        public authService: AuthService,
        private _router: Router,
    ) {
        this._unsubscribeAll = new Subject();
    }
    ngOnInit(): void {
        this.registrationFrom = this._formBuilder.group({
            password: ['', Validators.required],
            name: ['', Validators.required],
            number: ['', Validators.compose([Validators.pattern('^([6-9][0-9]{9})$'), Validators.required])],
            society_id: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            role: [''],
        });
        this.registrationFrom.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.registrationFrom.get('passwordConfirm').updateValueAndValidity();
            });

        this.filteredBusiness = this.registrationFrom.controls.society_id.valueChanges
            .pipe(
                startWith(''),
                map(value => this._filter(value))
            );
    }

    displayFn(option: any): string {
        return option && option.title ? option.title : '';
    }

    private _filter(value: any): any[] {
        if (value != "") {
            const filterValue = value.toLowerCase();
            return this.allBusinessList.filter(option => option.title.toLowerCase().includes(filterValue));
        }
        // else {
        //     return this.allBusinessList.title;
        // }
    }


    showPassword() {
        this.password_type = this.password_type === 'text' ? 'password' : 'text';
        this.password_icon1 = this.password_icon1 === 'visibility_off' ? 'visibility' : 'visibility_off';
    }

    showConfirmPassword() {
        this.confirmPassword_type = this.confirmPassword_type === 'text' ? 'password' : 'text';
        this.password_icon2 = this.password_icon2 === 'visibility_off' ? 'visibility' : 'visibility_off';
    }

    

    async register() {
        if (this.registrationFrom.controls['society_id'].value.id !== undefined) {

            console.log("dsdsds", this.registrationFrom);
            if (this.registrationFrom.controls['password'].value == this.registrationFrom.controls['confirmPassword'].value) {
                (await this.authService.userRegister(
                    this.registrationFrom.controls['number'].value,
                    this.registrationFrom.controls['society_id'].value.id,
                    this.registrationFrom.controls['name'].value,
                    this.registrationFrom.controls['password'].value,
                    this.registrationFrom.controls['email'].value,
                    this.registrationFrom.controls['role'].value,
                )).subscribe(res => {
                    if (res.status == true) {
                        this.registrationFrom.reset();
                        Swal.fire({
                            icon: 'success',
                            title: 'Account is successfully Created !',
                        });
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Please try again!',
                        });
                    }
                })
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Please try again!',
                });
            }
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Please select Business from the list!',
            });
        }


    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    async  selectedrole() {
        console.log(this.registrationFrom.controls['role'].value);
        // if (this.registrationFrom.controls['role'].value == 'super_admin') {
        //     (await this.businessService.getSuperAdminList()).subscribe((response: any) => {
        //         console.log(response)
        //         this.allBusinessList = response.super_business.slice().reverse();
        //     });
        // }
        // else if (this.registrationFrom.controls['role'].value == '') {

        // }
        // else {
        //     (await this.businessService.getAdminUserList()).subscribe((response: any) => {
        //         console.log(response)
        //         this.allBusinessList = response.business.slice().reverse();
        //     });
        // }
    }


}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return { passwordsNotMatching: true };
};
