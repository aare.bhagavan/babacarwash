import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatDialogConfig } from '@angular/material/dialog';
import { Component, OnInit, ViewChild, ViewEncapsulation, Inject } from '@angular/core';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
// import { BusinessService } from '../../../../api/business.service'
import { MatSort } from '@angular/material/sort';
import { AnalyticsDashboardService } from 'app/main/apps/dashboards/analytics/analytics.service';
import { MatTableDataSource } from '@angular/material';
import { MatFormFieldControl } from '@angular/material';
import * as underscore from 'underscore';
import { Time } from '@angular/common';
import { MastersService } from 'app/api/masters.service';
import * as _ from 'underscore';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, SatDatepickerModule } from 'saturn-datepicker'
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';


export const MY_FORMATS = {
    parse: {
        dateInput: 'LL'
    },
    display: {
        dateInput: 'YYYY-MM-DD',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY'
    }
};

@Component({
    selector: 'analytics-dashboard',
    templateUrl: './analytics.component.html',
    styleUrls: ['./analytics.component.scss'],
    providers: [
        { provide: MatFormFieldControl, useExisting: AnalyticsDashboardComponent },
        // {provide: DatetimeAdapter, useClass: MomentDatetimeAdapter},
        // {provide: MAT_DATETIME_FORMATS, useValue: DateFormat},
    ],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class AnalyticsDashboardComponent implements OnInit {
    widgets: any;
    realData: any = [];
    todayDate: Date = new Date();
    realData2: any = [];
    Dates: any = [];
    saturnDate: any;
    Dates2: any = [];
    realDataPiChart: any = [];
    widget1SelectedYear = 'Apr';
    widget5SelectedDay = 'today';
    widget1SelectedWeek = 'Week1';
    dailyreportCount: any;
    carwashReports: any;
    AllcarwashReports: any;
    today: string;
    staticArr = [];
    response: any = [];
    pieObject: any = {};
    mapObject: any = {};
    business_id: any;
    values: any = [];
    variable: any = '';
    subBusinessList: any;
    dateForm: FormGroup;

    pdateForm: FormGroup;
    pAllcarwashReports: any;
    prealData: any = [];
    pDates: any = [];
    pwidgets: any;
    pwidget1SelectedYear = 'Apr';
    pwidget5SelectedDay = 'today';
    pwidget1SelectedWeek = 'Week1';
    // imageUrl = { url: "assets/img/examples/green.png", scaledSize: { height: 40, width: 25 } }

    redImgUrl = { url: "assets/img/examples/red.svg", scaledSize: { height: 55, width: 40 } };
    greenImgUrl = { url: "assets/img/examples/green.svg", scaledSize: { height: 55, width: 40 } };
    role: string;
    displayBusinessTab: boolean;
    fromDate;
    toDate;


    /**
     * Constructor
     *
     * @param {AnalyticsDashboardService} _analyticsDashboardService
     */
    constructor(
        // private BusinessService: BusinessService,
        private _analyticsDashboardService: AnalyticsDashboardService,
        public dialog: MatDialog,
        public mastersService: MastersService, private _formBuilder: FormBuilder
    ) {
        // Register the custom chart.js plugin
        this._registerCustomChartJSPlugin();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    async ngOnInit() {

        this.role = localStorage.getItem("role");
        this.widgets = this._analyticsDashboardService.widgets;
        this.toDate = moment().format('YYYY-MM-DD');
        this.fromDate = moment(this.toDate).subtract(1, 'month').format('YYYY-MM-DD');
        this.getCarWashReports(this.fromDate, this.toDate);
        this.getPCarWashReports(this.fromDate, this.toDate);
        this.dateForm = this._formBuilder.group({
            DateRange: [{ begin: new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDay()), end: new Date() }, Validators.required]
        });
        this.pdateForm = this._formBuilder.group({
            pDateRange: [{ begin: new Date(new Date().getFullYear(), new Date().getMonth() - 1, new Date().getDay()), end: new Date() }, Validators.required]
        });

    }
    getColor(current): string {
        if (current == 0) {
            return '#006400';
        } else if (current == 1) {
            return '#5c84f1';
        } else if (current == 2) {
            return '#89a9f4';
        }
        else if (current == 3) {
            return '#FFFF00';
        }
        else if (current == 4) {
            return '#FF4500';
        } else if (current == 5) {
            return '#808000';
        } else if (current == 6) {
            return '#808080';
        }
    }
    // async getBusinessMembersList() {
    //     (await this.BusinessService.getBusinessMemberList(this.business_id)).subscribe((response: any) => {
    //         this.mapObject = {
    //             widget6: {
    //                 markers: [],
    //                 styles: [
    //                     {
    //                         'featureType': 'administrative',
    //                         'elementType': 'labels.text.fill',
    //                         'stylers': [
    //                             {
    //                                 'color': '#444444'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'landscape',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'color': '#f2f2f2'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'fe
    //                     },
    //                     {
    //                         'featureType': 'poi',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'visibility': 'off'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'road',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'saturation': -100
    //                             },
    //                             {
    //                                 'lightness': 45
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'road.highway',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'visibility': 'simplified'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'road.arterial',
    //                         'elementType': 'labels.icon',
    //                         'stylers': [
    //                             {
    //                                 'visibility': 'off'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'transit',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'visibility': 'off'
    //                             }
    //                         ]
    //                     },
    //                     {
    //                         'featureType': 'water',
    //                         'elementType': 'all',
    //                         'stylers': [
    //                             {
    //                                 'color': '#039be5'
    //                             },
    //                             {
    //                                 'visibility': 'on'
    //                             }
    //                         ]
    //                     }
    //                 ]
    //             }
    //         }
    //         this.mapObject.widget6.markers = response.businessmember;
    //         console.log("DATA", this.mapObject.widget6.markers);
    //     });
    // }
    // async getdailyReports() {
    //     let date = moment().format('YYYY-MM-DDTHH:mm:ss');
    //     (await this.BusinessService.getdailysymptomreports(date, this.business_id)).subscribe((res): any => {
    //         this.pieObject = {
    //             widget7: {
    //                 scheme: {
    //                     domain: ['#006400', '#5c84f1', '#89a9f4', '#FFFF00', '#FF4500', '#808000', '#808080']
    //                 },
    //                 devices: [
    //                     {
    //                         name: 'Fever',
    //                         // value: 48,
    //                         // change: -0.6
    //                     },
    //                     {
    //                         name: 'Cough',
    //                         // value: 14,
    //                         // change: 0.7
    //                     },
    //                     {
    //                         name: 'Head Ache',
    //                         // value: 29,
    //                         // change: 0.1
    //                         symptom: 'head_ache'
    //                     },
    //                     {
    //                         name: 'Dizziness',
    //                         // value: 29,
    //                         // change: 0.1
    //                     },
    //                     {
    //                         name: 'Runny Nose',
    //                         // value: 29,
    //                         // change: 0.1
    //                     },
    //                     {
    //                         name: 'Sore Throat',
    //                         // value: 29,
    //                         // change: 0.1
    //                     },
    //                     {
    //                         name: 'Breathing',
    //                         // value: 29,
    //                         // change: 0.1
    //                     }
    //                 ]
    //             }
    //         }
    //         console.log('report counts', res);
    //         this.dailyreportCount = res;
    //         this.pieObject.widget7.devices.forEach(widget => {
    //             if (widget.name == 'Fever') {
    //                 widget['value'] = this.dailyreportCount.temperature
    //             } else if (widget.name == 'Cough') {
    //                 widget['value'] = this.dailyreportCount.cough
    //             } else if (widget.name == 'Head Ache') {
    //                 widget['value'] = this.dailyreportCount.headache;
    //             } else if (widget.name == 'Dizziness') {
    //                 widget['value'] = this.dailyreportCount.dizziness
    //             } else if (widget.name == 'Runny Nose') {
    //                 widget['value'] = this.dailyreportCount.runny_nose
    //             } else if (widget.name == 'Sore Throat') {
    //                 widget['value'] = this.dailyreportCount.sore_throat
    //             } else if (widget.name == 'Breathing') {
    //                 widget['value'] = this.dailyreportCount.breathing
    //             }
    //         });
    //     });
    // }

    async getCarWashReports(from_date, to_date) {
        (await this.mastersService.getCarwashRecords(from_date, to_date)).subscribe((res): any => {
            console.log('carwash reports', res);
            this.AllcarwashReports = res;
            let resArray: any = []
            resArray = res;
            resArray = _.sortBy(resArray, 'work_date');
            // this.AllcarwashReports = resArray.filter(record => moment(record.work_date).isAfter(moment().subtract(1, 'month').format('YYYY-MM-DD')) && moment(record.work_date).isBefore(moment().add(1,'day').format('YYYY-MM-DD')));

            // this.carwashReports = resArray.filter(record => moment(record.work_date).isAfter(moment().subtract(1, 'month').format('YYYY-MM-DD')) && moment(record.work_date).isBefore(moment().add(1,'day').format('YYYY-MM-DD')));
            resArray.forEach(report => {
                this.realData.push(report.vehicles_cleaned)
                this.Dates.push(moment(report.work_date).format('DD-MMM-YY'))
            });

            this.widgets.widget1.datasets.Week1[0].data = this.realData;
            this.widgets.widget1.labels = this.Dates;
        });
    }
    OnDateChange(event) {
        // console.log('date diff', moment(event.end).diff(moment(event.begin), 'months', true))
        // if (moment(event.end).diff(moment(event.begin), 'months', true) > 1) {
        //     event.end = new Date(event.begin.getFullYear(), event.begin.getMonth() + 1, moment(event.begin.toISOString()).date());
        // }

        this.realData = [];
        this.Dates = [];
        // let filterReports = this.AllcarwashReports.filter(report => moment(report.work_date).isSameOrAfter(moment(event.begin).format('YYYY-MM-DD')) && moment(report.work_date).isSameOrBefore(moment(event.end).add(1, 'day').format('YYYY-MM-DD')));
        // filterReports.forEach(report => {
        //     this.realData.push(report.vehicles_cleaned)
        //     this.Dates.push(moment(report.work_date).format('DD-MMM-YY'))
        // });


        // this.widgets.widget1.datasets.Week1[0].data = this.realData;
        // this.widgets.widget1.labels = this.Dates;

        this.fromDate = moment(event.begin).format('YYYY-MM-DD');
        this.toDate = moment(event.end).format('YYYY-MM-DD');
        this.getCarWashReports(this.fromDate, this.toDate);
        this.getPCarWashReports(this.fromDate, this.toDate);
        this.dateForm.controls.DateRange.setValue({ begin: event.begin, end: event.end })
    }

    async getPCarWashReports(from_date, to_date) {
        (await this.mastersService.getPCarwashRecords(from_date, to_date)).subscribe((res): any => {
            console.log('pcarwash reports', res);
            this.pAllcarwashReports = res;
            let resArray: any = []
            resArray = res;
            resArray = _.sortBy(resArray, 'work_date');
            // this.AllcarwashReports = resArray.filter(record => moment(record.work_date).isAfter(moment().subtract(1, 'month').format('YYYY-MM-DD')) && moment(record.work_date).isBefore(moment().add(1,'day').format('YYYY-MM-DD')));

            // this.carwashReports = resArray.filter(record => moment(record.work_date).isAfter(moment().subtract(1, 'month').format('YYYY-MM-DD')) && moment(record.work_date).isBefore(moment().add(1,'day').format('YYYY-MM-DD')));
            resArray.forEach(report => {
                this.prealData.push(report.vehicles_cleaned)
                this.pDates.push(moment(report.work_date).format('DD-MMM-YY'))
            });

            this.widgets.widgetPending.datasets.Week1[0].data = this.prealData;
            this.widgets.widgetPending.labels = this.pDates;
        });
    }

    // async getTemperatureComboReports() {
    //     let date = moment().format('YYYY-MM-DDTHH:mm:ss');
    //     (await this.BusinessService.getTemperatureComboReports(date, this.business_id)).subscribe((res): any => {
    //         console.log('weekly temperature combination reports', res);
    //         this.response = res;
    //         this.response.forEach(report => {
    //             report.updated_at = moment(report.updated_at).format('DD-MMM-YYYY');
    //         });
    //         let dateArr = [];

    //         this.response.forEach(report => {
    //             let presentCount = 0;
    //             this.staticArr.forEach(element => {
    //                 if (element.date == report.updated_at) {
    //                     presentCount = presentCount + 1;
    //                 }
    //             })
    //             if (presentCount == 0) {
    //                 let count = 0;
    //                 dateArr = this.response.filter(map => map.updated_at == report.updated_at)
    //                 dateArr.forEach(ele => {
    //                     if (ele.temperature && (ele.breathing || ele.cough || ele.dizziness || ele.sore_throat || ele.runny_nose || ele.runny_nose || ele.headache)) {
    //                         count = count + 1
    //                     }
    //                 })
    //                 const data = {};
    //                 data['date'] = report.updated_at;
    //                 data['count'] = count;
    //                 this.staticArr.push(data)
    //             }
    //         });
    //         console.log('temperature combo', this.staticArr)
    //         this.staticArr.forEach(report => {
    //             this.realData2.push(report.count)
    //             this.Dates2.push(report.date)
    //         });

    //         this.widgets.widget10.datasets.Week1[0].data = this.realData2;
    //         this.widgets.widget10.labels = this.Dates2;
    //     });

    // }
    // // -----------------------------------------------------------------------------------------------------
    // // @ Private methods
    // // -----------------------------------------------------------------------------------------------------

    // /**
    //  * Register a custom plugin
    //  */
    private _registerCustomChartJSPlugin(): void {
        (window as any).Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing): any {
                // Only activate the plugin if it's made available
                // in the options
                if (
                    !chart.options.plugins.xLabelsOnTop ||
                    (chart.options.plugins.xLabelsOnTop && chart.options.plugins.xLabelsOnTop.active === false)
                ) {
                    return;
                }

                // To only draw at the end of animation, check for easing === 1
                const ctx = chart.ctx;

                chart.data.datasets.forEach(function (dataset, i): any {
                    const meta = chart.getDatasetMeta(i);
                    if (!meta.hidden) {
                        meta.data.forEach(function (element, index): any {

                            // Draw the text in black, with the specified font
                            ctx.fillStyle = 'rgba(255, 255, 255, 0.7)';
                            const fontSize = 13;
                            const fontStyle = 'normal';
                            const fontFamily = 'Roboto, Helvetica Neue, Arial';
                            ctx.font = (window as any).Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                            // Just naively convert to string for now
                            const dataString = dataset.data[index].toString() + '';

                            // Make sure alignment settings are correct
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'middle';
                            const padding = 15;
                            const startY = 24;
                            const position = element.tooltipPosition();
                            ctx.fillText(dataString, position.x, startY);

                            ctx.save();

                            ctx.beginPath();
                            ctx.setLineDash([5, 3]);
                            ctx.moveTo(position.x, startY + padding);
                            ctx.lineTo(position.x, position.y - padding);
                            ctx.strokeStyle = 'rgba(255,255,255,0.12)';
                            ctx.stroke();

                            ctx.restore();
                        });
                    }
                });
            }
        });
    }


    selectedDate(event) {
        console.log("AnalyticsDashboardComponent -> selectedDate -> event", event)

    }
    async chartClick(event, labels) {
        if (event.active[0]._index == undefined || event.active[0]._index == null) {
            return;
        } else {

            var date = moment(labels[event.active[0]._index], 'DD-MMM-YY').format('YYYY-MM-DD')
            console.log('event', event);
            console.log('event', labels);
            console.log('event', date);
        }
        (await this.mastersService.getCarDetailsOfScheduleDate(date)).subscribe((res: any) => {
            // res = underscore.sortBy(res, 'updated_at').reverse();
            // res.forEach((element: any) => {
            //     element.updated_at = moment(element.updated_at).format('h:mm a')
            // });

            console.log('cars Data', res);
            this.openDialogBox(res, '3', '', labels[event.active[0]._index]);
        })

    };
    async pchartClick(event, labels) {
        if (event.active[0]._index == undefined || event.active[0]._index == null) {
            return;
        } else {

            var date = moment(labels[event.active[0]._index], 'DD-MMM-YY').format('YYYY-MM-DD')
            console.log('event', event);
            console.log('event', labels);
            console.log('event', date);
        }
        (await this.mastersService.getPCarDetailsOfScheduleDate(date)).subscribe((res: any) => {
            // res = underscore.sortBy(res, 'updated_at').reverse();
            // res.forEach((element: any) => {
            //     element.updated_at = moment(element.updated_at).format('h:mm a')
            // });

            console.log('Pcars Data', res);
            this.openDialogBox(res, '3', '', labels[event.active[0]._index]);
        })

    };

    openDialogBox(data: any, column: any, symptom: string, date: string) {
        const dialogConfig = new MatDialogConfig();
        dialogConfig.autoFocus = true;
        dialogConfig.width = "95%";
        dialogConfig.height = "90%";
        // dialogConfig.maxWidth = "70%";
        // dialogConfig.minWidth = "500px";
        dialogConfig.data = {
            values: data,
            cloumns: column,
            date: date,
            // symptom:symptom
        };
        this.dialog.open(CarsDetailsDialogComponent, dialogConfig);

    }

}


@Component({
    selector: 'cars-details-dialog',
    styleUrls: ['./analytics.component.scss'],
    template: `
    
    <div #container class="content-card" align="center" style="overflow: auto;max-height: 400px;padding-left: 25px;padding-right:18px;height: 450px;">
        <div *ngIf="table=='3'">
            <div align="center" style="width: 100%">
                <!-- <mat-toolbar-row style="align-content: center"> -->
                <h3>
                    <b>
                     {{DATE}}
                    </b>
                </h3>
                <!-- </mat-toolbar-row> -->
            </div>
            <table  style="width:100%" mat-table [dataSource]="dataSource" matSort class="mat-elevation-z8">

                <!-- <ng-container matColumnDef="item">
                    <mat-header-cell *matHeaderCellDef mat-sort-header>{{item}}</mat-header-cell>
                    <mat-cell *matCellDef="let carDetails">
                        <p class="text-truncate">{{carDetails.name}}</p>
                    </mat-cell>
                </ng-container> -->
                <!-- Name -->
                <ng-container matColumnDef="customer_name">
                    <mat-header-cell *matHeaderCellDef mat-sort-header>Customer Name</mat-header-cell>
                    <mat-cell *matCellDef="let carDetails">
                        <p class="text-truncate">{{carDetails.first_name}} <br/> {{carDetails.last_name}}</p>
                    </mat-cell>
                </ng-container>
                <!-- building -->
                <ng-container matColumnDef="building">
                    <mat-header-cell *matHeaderCellDef mat-sort-header>Building</mat-header-cell>
                    <mat-cell *matCellDef="let carDetails">
                        <p class="text-truncate">{{carDetails.building_name}}</p>
                    </mat-cell>
                </ng-container>
                <!-- vehicle -->
                <ng-container matColumnDef="vehicle">
                    <mat-header-cell *matHeaderCellDef mat-sort-header>Vehicle</mat-header-cell>
                    <mat-cell *matCellDef="let carDetails">
                        <p class="text-truncate">{{carDetails.vehicle}}</p>
                    </mat-cell>
                </ng-container>
                <!--worker-->
                <ng-container matColumnDef="worker">
                    <mat-header-cell *matHeaderCellDef mat-sort-header>Worker</mat-header-cell>
                    <mat-cell *matCellDef="let carDetails">
                        <p class="text-truncate">{{carDetails.worker_name}}</p>
                    </mat-cell>
                </ng-container>

                <mat-header-row *matHeaderRowDef=" displayedColumns"> </mat-header-row>
                <mat-row *matRowDef="let carDetails; columns: displayedColumns;">
                </mat-row>
            </table>
        </div>
    </div>
    <mat-dialog-actions style="justify-content: space-evenly;">
        <div align="center">
            <button mat-raised-button color="accent" (click)="export()">
                Export
            </button>
            &nbsp;
            <button style="margin-top:20px;" mat-raised-button color="accent"
                    (click)="closedialog()">Close</button>
        </div>
    </mat-dialog-actions>
    `
})
export class CarsDetailsDialogComponent implements OnInit {
    carDetails: any[] = [];
    dataSource: any;
    table: string;
    displayedColumns = ['customer_name', 'vehicle', 'worker', 'building'];
    // twocolumndisplay = ['name', 'time'];
    // fourcolumndisplay = ['name', 'phone', 'time', 'symptom'];
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    DATE: string;
    // symptom: string;

    constructor(
        public dialogRef: MatDialogRef<CarsDetailsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) { }
    ngOnInit() {
        this.table = this.data.cloumns;
        this.carDetails = this.data.values;
        this.DATE = this.data.date;
        // this.symptom = this.data.symptom;
        // const ELEMENT_DATA: PeriodicElement[]=this.data.values;
        this.dataSource = new MatTableDataSource(this.carDetails);
        this.dataSource.sort = this.sort;
        this.dataSource.sortingDataAccessor = (item, property) => {
            switch (property) {
                case 'Customer': return item.first_name + " " + item.last_name;
                    break;
                case 'Vehicle': return item.vehicle;
                    break;
                case 'Cleaner': return item.cleaner;
                    break;
                case 'Start Date': return item.start_date;
                    break;
                default: return item[property];
            }
        };
    }
    closedialog() {
        this.dialogRef.close();
    }


    export() {
        let header;
        let exportData = [];

        header = ['Customer Name', 'Vehicle', 'Worker', 'Building'];
        this.carDetails.forEach(element => {
            let columnData = {
                'Customer Name': element.first_name + ' ' + element.last_name,
                Vehicle: element.vehicle,
                Worker: element.worker_name,
                Building: element.building_name
            }
            exportData.push(columnData);
        })


        let csvData = this.ConvertToXLS(exportData, header);
        let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
        let dwldLink = document.createElement("a");
        let url = URL.createObjectURL(blob);
        let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
        if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
            dwldLink.setAttribute("target", "_blank");
        }
        dwldLink.setAttribute("href", url);
        dwldLink.setAttribute("download", "Wash Records " + this.DATE + ".xls");
        dwldLink.style.visibility = "hidden";
        document.body.appendChild(dwldLink);
        dwldLink.click();
        document.body.removeChild(dwldLink);


    }

    ConvertToXLS(objArray, headerList) {
        let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        let str = '';
        let row = 'sl_no,';

        for (let index in headerList) {
            row += headerList[index] + ',';
        }
        row = row.slice(0, -1);
        str += row + '\r\n';
        for (let i = 0; i < array.length; i++) {
            let line = (i + 1) + '';
            for (let index in headerList) {
                let head = headerList[index];
                line += ',' + array[i][head];
            }
            str += line + '\r\n';
        }

        return str;
    }
}
export interface DialogData {
    values: any;
    cloumns: any;
    date: string;
    symptom: string;
}

export interface PeriodicElement {
    name: string;
    phone: number;
    time: Time;
    symptoms: string;
}
