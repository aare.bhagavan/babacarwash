import { MatCardModule } from '@angular/material/card';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatRadioModule, MatProgressBar, MatProgressBarModule } from '@angular/material';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatAutocompleteModule } from '@angular/material/autocomplete';


import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';
import { MatGridListModule } from '@angular/material/grid-list';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { ImportMembersService } from './members/members.service';
// import { ImportMembersComponent } from './members/members.component';
import {FileUploadModule} from "ng2-file-upload";


const routes: Routes = [
    
];

@NgModule({
    declarations: [
    ],
    imports: [
        RouterModule.forChild(routes),
        FileUploadModule,
        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,
        MatRadioModule,
        MatCardModule,
        MatGridListModule,
        MatProgressBarModule,

        MatCheckboxModule,
        MatAutocompleteModule,
        
        NgxChartsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD3zC0kD5-3rNM6j4ThtxZEgRf-Xc0qlEI'
        }),

        FuseSharedModule,
        FuseWidgetModule
    ],
    providers: [
        ImportMembersService,
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})
export class ImportUtilModule {
}
