import { trigger, style, state, transition, animate, group } from '@angular/animations';
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation, HostListener } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, fromEvent, merge, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { takeUntil } from 'rxjs/internal/operators';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { Sort } from '@angular/material/sort';
import * as moment from 'moment';
import * as _ from 'underscore';
import { ImportMembersService } from './members.service';
import * as Papa from 'papaparse';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';


@Component({
    selector: 'import-members',
    templateUrl: './members.component.html',
    styleUrls: ['./members.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed', style({ height: '0', minHeight: '0' })),
            state('expanded', style({ height: '*' })),
            transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
        fuseAnimations
    ],
    encapsulation: ViewEncapsulation.None
})
export class ImportMembersComponent implements OnInit {

    @ViewChild("fileInput", { static: false }) fileInput: ElementRef;
    file = null;
    // public filesToUpload: Array<File> = [];

    files: Map<File, string> = new Map();
    fileName: string;

    // public file: any;
    public currentProcessing = "";
    public logs: any[] = [];
    public totalCount = 1;
    public processedCount = 0;
    public progress = 0;
    public processStarted = false;

    public headers: any;

    public role: string;
    allDesignations: any;
    insertedRecords: any[] = [];
    rejectedRecords: any[] = [];
    updatedRecords: any[] = [];
    filteredZones: any;
    filteredCircles: any;
    dzc_id: number;
    DepZoneCircle: any;
    filteredDZC: any;
    header: string[];
    workr_group_code: string;
    sortedData: any;
    org_id: any;
    departmentId: number;
    zoneId: number;
    circleId: number;
    isDZCmapped: boolean = false;
    designationId: number;

    isDZCnotMapped: boolean = false;

    //declaring 4 flags
    isDepartmentSelected: boolean = false;
    isZoneSelected: boolean = false;
    isCircleSelected: boolean = false;
    isDesignationSelected: boolean = false;
    allOrganizations: any;
    allGroups: any;
    orgId: number;
    public fileChanged($event) {
        // console.log("file changed");
        // this.file = $event.target.files[0];
        // let fileReader = new FileReader();
        // this.processStarted = true;
        // this.currentProcessing = 'Reading file..';
        // fileReader.onload = (e) => {
        //     console.log(fileReader.result);
        //     this.currentProcessing = 'Parsing file...';
        //     Papa.parse(this.file, {
        //         header: true,
        //         skipEmptyLines: true,
        //         transformHeader: ((header: string) => { return header.toLowerCase() }) ,
        //         complete: (result, file) => {
        //             console.log(result);
        //             if (this.validateHeaders(result)) {
        //                 return;
        //             }
        //             this.processWorkers(result.data);
        //             // this.processMembers(result.data);
        //             //   this.dataList = result.data;

        //         }
        //     });
        //  }


        // fileReader.readAsText(this.file);
    }

    validateHeaders(data) {
        if (this.departmentId == 1) {
            this.headers = ['sl_no', 'groupcode', 'uniquecode', 'emp_code', 'name', 'desig', 'join_date', 'esi_no', 'pf_no', 'pf_nofull', 'pfuanno', 'gender', 'age', 'father', 'spose', 'left_date', 'comp_code', 'comp_name'];
        }
        if (this.departmentId == 2) {
            this.headers = ['sl_no', 'name', 'gender', 'fathr_spoc', 'esi_no', 'pfuanno', 'age', 'nominee', 'adharno', 'add1', 'add2', 'place', 'city', 'district', 'pincode'];
        }
        let invalid = false;
        this.headers.forEach((header) => {
            var key, keys = Object.keys(data[0]);
            var n = keys.length;
            var headerArray = {}
            while (n--) {
                key = keys[n];
                headerArray[key.toLowerCase()] = data[0][key];
            }
            if (!Object.keys(headerArray).includes(header)) {
                this.currentProcessing = 'Format is incorrect. Please upload it in correct format';
                console.log("ImportMembersComponent -> validateHeaders -> this.currentProcessing", this.currentProcessing);
                invalid = true;
            }
        });
        return invalid;
    }





    public samplerows = 'sl_no|group_code|unique_code|emp_code|name|designation|date_of_joining|esi_no|pf_no|pf_nofull|pf_uan_no|gender|date_of_birth|father|spouse|date_of_leaving|comp_code|comp_name\n'
        + '1|EEE|ASDFS|ASZSX|KUMAR|WORKER|02/02/20|90i9886|9098768998|TS/00/8768G|908656787|MALE|01/02/1989|NARAYAN| | / / | |MUJEEB|\n'
        + '2|EEES|ASDFS|ASZSX|VIJAY|WORKER|02/02/20|90i98887|90000000|TS/00/8768G|908656787|MALE|01/02/1985|SAGAR| | / / | |SARAAD|';
    downloadSample() {
        let link = document.createElement('a');
        link.setAttribute('type', 'hidden');
        link.setAttribute('href', 'data:text/plain;charset=utf-8,'
            + encodeURIComponent(this.samplerows));
        link.setAttribute('download', 'sample.csv');
        document.body.appendChild(link);
        link.click();
        link.remove();
    }
    expandedElement: null;
    public business_id: number = 0;
    public department_id: number;
    public zone_id: number = null;
    public circle_id: number = null;
    public designation_id: number;
    allDepartments: any;
    allZones: any;
    allCircles: any;
    allWorkers: any;
    workerForm: any;
    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _formBuilder: FormBuilder,
       
        public dialog: MatDialog,
        private _ecommerceProductsService: ImportMembersService,
      
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.role = localStorage.getItem('role');
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.role = localStorage.getItem('role');
        this.org_id = localStorage.getItem('org_id');
        // this.getOrganizationList();
        // this.getCircleList();
        // this.getZoneList();
        // this.getWorkersList();
        // // this.getDesignations();
        // this.getDepZoneCircle();
        // this.getDepartmentList();
        // this.getSanitatioGroupList();
        // if(this.role=='admin'){
        //     this.getDepartmentList();
        // }
        
    }
    onDepChange(data) {
        this.filteredDZC = this.DepZoneCircle.filter(element => (element.zone_id == this.zone_id && element.dept_id == data.value && element.circle_id == this.circle_id));
        this.dzc_id = this.filteredDZC[0] != undefined ? this.filteredDZC[0].id : 0;
        console.log('filtered DZC:', this.filteredDZC);
        this.departmentId = data.value;
        this.isDepartmentSelected = true;
        // this.getDesignationByDeptid(this.departmentId);

    }
    // async getSanitatioGroupList() {
    //     (await this.mastersService.getGroupList(1)).subscribe((response: any) => {
    //         console.log("Group list for sanitation department", response)
    //         this.allGroups = response;
    //     });
    // }
    onZoneChange(data) {
        // if (data.value != undefined) {
        //     this.filteredZones = this.allZones.filter(zone => zone.department_id == data.value)
        //     console.log('filtered zones:', this.filteredZones)
        // }
        this.filteredDZC = this.DepZoneCircle.filter(element => (element.zone_id == data.value && element.dept_id == this.department_id && element.circle_id == this.circle_id));
        this.dzc_id = this.filteredDZC[0] != undefined ? this.filteredDZC[0].id : 0;
        console.log('filtered DZC:', this.filteredDZC);
        this.zoneId = data.value;
        this.isZoneSelected = true;
    }
    onCircleChange(data) {
        this.filteredDZC = this.DepZoneCircle.filter(element => (element.zone_id == this.zone_id && element.dept_id == this.department_id && element.circle_id == data.value));
        this.dzc_id = this.filteredDZC[0] != undefined ? this.filteredDZC[0].id : 0;
        console.log('filtered DZC:', this.filteredDZC);
        // this.getDepZoneCircle();
        this.circleId = data.value;
        this.isCircleSelected = true;
    }

    onDesinationChange(data) {
        this.designationId = data.value;
        this.isDesignationSelected = true;
        console.log(this.isDesignationSelected);
    }
    // onOrganizationChange(data) {
    //     this.org_id = data.value;
    //     // this.isOrSelected = true;
    //     this.getDepartmentList();
    // }
    processChoices() {
        this.isDZCnotMapped = true;
        if (this.isDepartmentSelected) {
            this.isDZCmapped = false;

            this.DepZoneCircle.forEach(dzc => {

                if ((dzc.dept_id == this.departmentId) && (dzc.zone_id == this.zoneId) && (dzc.circle_id == this.circleId)) {

                    this.isDZCmapped = true;
                }

            });
        }

    }


    // async insertWorker(data, processedCount) {
    //     data.department_id = this.departmentId;
    //     if (data.place != undefined && data.district != undefined && data.add1 != undefined && data.add2 != undefined) {
    //         data.place = data.place.replace(/,|'/g, ' ');
    //         data.district = data.district.replace(/,|'/g, ' ');
    //         data.add1 = data.add1.replace(/,|'/g, ' ');
    //         data.add2 = data.add2.replace(/,|'/g, ' ');
    //         data.city = data.city.replace(/,|'/g, ' ');
    //       }
    //     (await this.workerService.insertWorker(data)).subscribe((response: any) => {
    //         if (response.success === true && response.status === 'inserted') {
    //             this.insertedRecords.push(response.worker);
    //             console.log("Successfully inserted worker", response.message);
    //         } else if (response.success === true && response.status === 'updated') {
    //             this.updatedRecords.push(response.worker)
    //             // this.logs.push({
    //             //     processedCount,
    //             //     message: "worker updated",
    //             //     data
    //             // });
    //         }
    //         else if (response.success === false && response.status === 'insert failed') {
    //             this.rejectedRecords.push(response.worker);
    //             let message = response.message;
    //             // this.logs.push({
    //             //     processedCount,
    //             //     message,
    //             //     data
    //             // });
    //             console.log("Worker Already Exists", response.message);
    //         } else {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Not inserted",
    //                 data
    //             });
    //         }
    //     });
    // }


    // async getDepartmentList() {
    //     (await this.mastersService.getDepartmentList(this.role, this.org_id)).subscribe((response: any) => {
    //         console.log("all department", response)
    //         this.allDepartments = response.filter((department) => department.org_id == this.org_id);
    //         // if (this.allDepartments.length == 0) {
    //         //     Swal.fire({
    //         //       icon: 'error',
    //         //       title: 'No Departments for this Organization!'
    //         //     });
    //         //   }
    //     });
    // }

    // async getOrganizationList() {
    //     (await this.mastersService.getOrganizationsList()).subscribe((response: any) => {
    //         console.log("all department", response)
    //         this.allOrganizations = response;
    //     });
    // }

    // async getZoneList() {
    //     (await this.mastersService.getZoneList()).subscribe((response: any) => {
    //         console.log("all zones", response)
    //         this.allZones = response;
    //         this.filteredZones = response;
    //     });
    // }

    // async getCircleList() {
    //     (await this.mastersService.getCircleList()).subscribe((response: any) => {
    //         console.log("all circles", response)
    //         this.allCircles = response;
    //         this.filteredCircles = response;
    //     });
    // }
    // async getWorkersList() {
    //     (await this.workerService.getWorkersList()).subscribe((response: any) => {
    //         console.log("all workers", response)
    //         this.allWorkers = response;
    //     });
    // }
    // async getDepZoneCircle() {
    //     (await this.workerService.getDepZoneCircle()).subscribe((response: any) => {
    //         console.log("all department and zone and circle", response)
    //         this.DepZoneCircle = response;
    //         this.filteredDZC = this.DepZoneCircle.filter(dzc => dzc.zone_id == this.zone_id && dzc.circle_id == this.circle_id && dzc.dept_id == this.department_id);
    //         this.dzc_id = this.filteredDZC[0] != undefined ? this.filteredDZC[0].id : 0;
    //         // this.DepZoneCircle.forEach(dzc => {
    //         //     if((dzc.dept_id==this.departmentId)&&(dzc.zone_id==this.zoneId)&&(dzc.circle_id==this.circleId)){
    //         //         this.isDZCmapped=true;
    //         //         console.log(this.isDZCmapped);
    //         //     }
    //         //  else {
    //         //      this.isDZCmapped=false;
    //         //  }
    //         // });
    //     });

    // }
    // // async getDesignations() {
    // //     (await this.workerService.getDesignations()).subscribe((response: any) => {
    // //         console.log("all designations", response)
    // //         this.allDesignations = response;
    // //     });
    // // }

    // async clickToMap() {
    //     try {
    //         (await this.mastersService.addNewDZC({ department_id: this.department_id, zone_id: this.zone_id, circle_id: this.circle_id })).subscribe(res => {
    //             this.getDepZoneCircle()
    //             console.log('response....', res);
    //             Swal.fire({ icon: 'success', title: 'Mapped successfully!', });
    //             this.ngOnInit()
    //         });
    //     } catch (err) {
    //         console.log('error while adding circle', err)
    //     }

    // }

    // async selectedrole() {
    //     console.log(this.business_id);
    // }
    // ConvertToCSV(objArray, headerList) {
    //     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;  
          
    //     array.forEach(data => {
    //         if (data.place != undefined && data.district != undefined && data.add1 != undefined && data.add2 != undefined) {
    //           data.place = data.place.replace(/,|'/g, ' ');
    //           data.district = data.district.replace(/,|'/g, ' ');
    //           data.add1 = data.add1.replace(/,|'/g, ' ');
    //           data.add2 = data.add2.replace(/,|'/g, ' ');
    //           data.city = data.city.replace(/,|'/g, ' ');
    //         }
    //       });  
    //     let str = '';
    //     let row = 'sl_no,';
    
    //     for (let index in headerList) {
    //       row += headerList[index] + ',';
    //     }
    //     row = row.slice(0, -1);
    //     str += row + '\r\n';
    //     for (let i = 0; i < array.length; i++) {
    //       let line = (i + 1) + '';
    //       for (let index in headerList) {
    //         let head = headerList[index];
    //         line += ',' + array[i][head];
    //       }
    //       str += line + '\r\n';
    //     }
    
    //     return str;
    //   }

    // ConvertToCSV(objArray, headerList) {
    //     let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    //     // array.forEach(data => {
    //     //     data.location = data.location.replace(/\,/g, ' ');
    //     // });
    //     let str = '';
    //     let row = 'sl_no,';

    //     for (let index in headerList) {
    //         row += headerList[index] + ',';
    //     }
    //     row = row.slice(0, -1);
    //     str += row + '\r\n';
    //     for (let i = 0; i < array.length; i++) {
    //         let line = (i + 1) + '';
    //         for (let index in headerList) {
    //             let head = headerList[index];

    //             line += ',' + array[i][head];
    //         }
    //         str += line + '\r\n';
    //     }

    //     return str;
    // }
    // downloadRecords(data, filename) {
    //     let symptom_array = [];
    //     let details;
    //     // if (this.zoneId != 0 && this.circleId != 0) {
    //     //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId && worker.zone_id == this.zoneId && worker.circle_id == this.circleId));
    //     // } else if (this.zoneId != 0 && this.circleId == 0) {
    //     //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId && worker.zone_id == this.zoneId));
    //     // } else if (this.zoneId == 0 && this.circleId != 0) {
    //     //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId && worker.circle_id == this.circleId));
    //     // } else if (this.zoneId == 0 && this.circleId == 0) {
    //     //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId));
    //     // }

    //     data.forEach(data => {
    //         data['absent_days'] = '';
    //     });
    //     let header = [];
    //     // let header = ['supervisor_name', 'group_code', 'unique_code', 'emp_code', 'designation_id', 'name', 'date_of_joining', 'esi_no', 'pf_no', 'pf_nofull', 'pf_uan_no', 'gender', 'date_of_birth', 'father', 'spouse', 'date_of_leaving', 'comp_code', 'comp_name', 'department_name', 'zone_name', 'absent_days']
    //     if (this.departmentId == 1) {
    //         header = ['groupcode', 'uniquecode', 'emp_code', 'name', 'desig', 'join_date', 'esi_no', 'pf_no', 'pf_nofull', 'pfuanno', 'gender', 'age', 'father', 'spose', 'left_date', 'comp_code', 'comp_name'];
    //     }
    //     if (this.departmentId == 2) {
    //         header = ['name', 'gender', 'fathr_spoc', 'esi_no', 'pfuanno', 'age', 'nominee', 'adharno', 'add1', 'add2', 'place', 'city', 'district', 'pincode'];
    //     }
    //     // data = _.sortBy(data, 'supervisor_id');
    //     let csvData = this.ConvertToCSV(data, header);
    //     console.log(csvData)
    //     let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    //     let dwldLink = document.createElement("a");
    //     let url = URL.createObjectURL(blob);
    //     let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    //     if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
    //         dwldLink.setAttribute("target", "_blank");
    //     }
    //     dwldLink.setAttribute("href", url);
    //     dwldLink.setAttribute("download", filename + ".csv");
    //     dwldLink.style.visibility = "hidden";
    //     document.body.appendChild(dwldLink);
    //     dwldLink.click();
    //     document.body.removeChild(dwldLink);
    // }
    // downloadRecords(data,filename) {
    //     let symptom_array = [];
    //     let details;
        // var data = []
        // if (this.dzc_id != 0) {
        //   data = this.allWorkers.filter(worker => (worker.dzc_id == this.dzc_id));
        // }
        //  else if (this.zoneId != 0 && this.circleId == 0) {
        //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId && worker.zone_id == this.zoneId));
        // } else if (this.zoneId == 0 && this.circleId != 0) {
        //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId && worker.circle_id == this.circleId));
        // } else if (this.zoneId == 0 && this.circleId == 0) {
        //   data = this.allWorkers.filter(worker => (worker.department_id == this.depId));
        // }
    
    //     data.forEach(data => {
    //       data['absent_days'] = '';
    //     });
    //     if (this.departmentId == 1) {
    //       this.header = ['group_code', 'desig', 'name', 'esi_no', 'pfuanno', 'gender', 'age', 'fathr_spoc', 'nominee', 'adharno', 'add1', 'add2', 'place', 'city', 'district', 'pincode','pf_no','unique_code','emp_code','pf_nofull','comp_code','comp_name','date_of_joining','date_of_leaving','spouse','absent_days']
    //     }
    //     else if (this.departmentId == 2) {
    //       this.header = ['name', 'gender', 'fathr_spoc', 'esi_no', 'pfuanno', 'age', 'nominee', 'adharno', 'add1', 'add2', 'place', 'city', 'district', 'pincode','pf_no','emp_code','pf_nofull','comp_code','comp_name','date_of_joining','date_of_leaving','spouse', 'absent_days'];
    //     }
    //     if (this.departmentId == 1) {
    //       data = _.sortBy(data, 'group_code');
    //       this.sortedData = _.sortBy(data, 'group_code');
    //       data.forEach((worker, index, data) => {
    
    //         if (this.workr_group_code != worker.group_code) {
    //           let filtergroup = this.allGroups.filter(group => group.group_code == worker.group_code);
    //           let s_index = this.sortedData.indexOf(worker)
    //           this.sortedData.splice(s_index, 0, filtergroup[0])
    //         }
    //         this.workr_group_code = worker.group_code;
    
    //       });
    //     } else { this.sortedData = data; }
    //     this.sortedData.forEach(data => {
    //       if (data.place != undefined && data.district != undefined && data.add1 != undefined && data.add2 != undefined && data.name != undefined) {
    //         data.place = data.place.replace(/,|'/g, ' ');
    //         data.district = data.district.replace(/,|'/g, ' ');
    //         data.add1 = data.add1.replace(/,|'/g, ' ');
    //         data.add2 = data.add2.replace(/,|'/g, ' ');
    //         data.city = data.city.replace(/,|'/g, ' ');
    //         data.name = data.name.replace(/,|'/g, ' ');
    //       }
    //     });
    
    
    //     let csvData = this.ConvertToCSV(this.sortedData, this.header);
    //     console.log(csvData)
    //     let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    //     let dwldLink = document.createElement("a");
    //     let url = URL.createObjectURL(blob);
    //     let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    //     if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
    //       dwldLink.setAttribute("target", "_blank");
    //     }
    //     dwldLink.setAttribute("href", url);
    //     dwldLink.setAttribute("download", filename + ".xls");
    //     dwldLink.style.visibility = "hidden";
    //     document.body.appendChild(dwldLink);
    //     dwldLink.click();
    //     document.body.removeChild(dwldLink);
    
    
    //   }

    // upload() {
    //     this.processStarted = true;
    //     let workBook = null;
    //     let jsonData = null;
    //     const reader = new FileReader();

    //     const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    //     this.files.clear();
    //     for (let index = 0; index < fileUpload.files.length; index++) {
    //         const file = fileUpload.files[index];
    //         this.file = file;
    //         this.fileName = file.name;// new Util().getFileNameWithDate('RCBook', file.name)
    //         this.files.set(file, this.fileName);
    //     }

    //     if (this.files.size > 0) {
    //         console.log('after changed the fiename ', this.files)
    //         reader.onload = async (event) => {
    //             const data = reader.result;
    //             workBook = XLSX.read(data, { type: 'binary' });
    //             jsonData = workBook.SheetNames.reduce((initial, name) => {
    //                 let sheetName = workBook.SheetNames[0]
    //                 const sheet = workBook.Sheets[sheetName];
    //                 initial['sheet'] = XLSX.utils.sheet_to_json(sheet, { defval: "" });
    //                 return initial;
    //             }, {});
    //             let sortedArray = jsonData.sheet.filter(obj => obj.PFUANNO != '');
    //             this.processedCount = 0;
    //             let processedCount = this.processedCount;
    //             this.totalCount = sortedArray.length;
    //             this.processWorkers1(sortedArray);
    //         }
    //         reader.readAsBinaryString(this.file);
    //     }
    // }

    // async processWorkers(workers) {
    //     this.processedCount = 0;
    //     let processedCount = this.processedCount;
    //     this.totalCount = workers.length;

    //     for (let worker of workers) {
    //         for (var i in worker) {
    //             worker[i.toLowerCase()] = worker[i];
    //             delete worker[i];
    //         }
    //         console.log(worker);
    //         // var key, keys = Object.keys(worker);
    //         // var n = keys.length;
    //         // var newobj = {}
    //         // while (n--) {
    //         //     key = keys[n];
    //         //     newobj[key.toLowerCase()] =worker[key];
    //         // }
    //         // console.log(newobj);
    //         this.processedCount++;
    //         processedCount = this.processedCount;
    //         this.progress = (processedCount / this.totalCount) * 100;

    //         this.currentProcessing = `processing members (${processedCount}/${this.totalCount})`;
    //         worker['dzc_id'] = this.dzc_id;
    //         if (this.departmentId == 1) {
    //             worker['designation_id'] = worker.desig.toLowerCase() == 'worker' ? 1 : 2;
    //         } else if (this.departmentId == 2) {
    //             worker['designation_id'] = 1;
    //         }
    //         worker['org_id'] = this.org_id;
    //         console.log("ImportMembersComponent -> processWorkers -> data", worker);
    //         if (!worker.name) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker Name is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         if (!worker.esi_no) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker ESI NO is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         if (!(worker.pf_uan_no || worker.pfuanno)) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker PFUAN NO is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         await this.insertWorker(worker, processedCount);
    //     }
    // }

    // async getDesignationByDeptid(departmentId) {
    //     (await this.mastersService.getDesignationBasedOnDepartmentID(departmentId)).subscribe((response: any) => {
    //       console.log("designation", response)
    //       this.allDesignations = response;
    //     });
    //   }

    // upload() {
    //     this.processStarted = true;
    //     let workBook = null;
    //     let jsonData = null;
    //     const reader = new FileReader();

    //     const fileUpload = document.getElementById('fileUpload') as HTMLInputElement;
    //     this.files.clear();
    //     for (let index = 0; index < fileUpload.files.length; index++) {
    //         const file = fileUpload.files[index];
    //         this.file = file;
    //         this.fileName = file.name;// new Util().getFileNameWithDate('RCBook', file.name)
    //         this.files.set(file, this.fileName);
    //     }

    //     if (this.files.size > 0) {
    //         console.log('after changed the fiename ', this.files)
    //         reader.onload = async (event) => {
    //             const data = reader.result;
    //             workBook = XLSX.read(data, { type: 'binary' });
    //             jsonData = workBook.SheetNames.reduce((initial, name) => {
    //                 let sheetName = workBook.SheetNames[0]
    //                 const sheet = workBook.Sheets[sheetName];
    //                 initial['sheet'] = XLSX.utils.sheet_to_json(sheet, { defval: "" });
    //                 return initial;
    //             }, {});
    //             for (let worker of jsonData.sheet) {
    //                 // this.allGroups.forEach(group => {
    //                 //     worker['group_code']=group.group_code;
    //                 // });
    //                 let group_code = this.allGroups.filter(group => group.name == worker.NAME);
    //                 if (group_code.length != 0) {
    //                     var group_name_rendom = '';
    //                     var group_code_rand=''
    //                     group_name_rendom = group_code[0].name;
    //                     group_code_rand = group_code[0].group_code;
    //                 }

    //                 worker['GROUP_CODE'] = group_code_rand!=undefined?group_code_rand:'';
    //                 if (group_name_rendom != undefined) {
    //                     if (group_name_rendom.toLowerCase().includes('supervisors')) {
    //                         worker['DESIG'] = 'SUPERVISOR';
    //                     } else {
    //                         worker['DESIG'] = 'WORKER';
    //                     }
    //                 }else{
    //                     worker['DESIG'] = 'WORKER';   
    //                 }


    //             }
    //             let sortedArray = jsonData.sheet.filter(obj => obj.PFUANNO != '');
    //             this.processedCount = 0;
    //             let processedCount = this.processedCount;
    //             this.totalCount = sortedArray.length;
    //             this.processWorkers(sortedArray);
    //         }
    //         reader.readAsBinaryString(this.file);
    //     }
    // }

    // async processWorkers(workers) {
    //     this.processedCount = 0;
    //     let processedCount = this.processedCount;
    //     this.totalCount = workers.length;

    //     for (let worker of workers) {
    //         for (var i in worker) {
    //             worker[i.toLowerCase()] = worker[i];
    //             delete worker[i];
    //         }
    //         console.log(worker);
    //         // var key, keys = Object.keys(worker);
    //         // var n = keys.length;
    //         // var newobj = {}
    //         // while (n--) {
    //         //     key = keys[n];
    //         //     newobj[key.toLowerCase()] =worker[key];
    //         // }
    //         // console.log(newobj);
    //         this.processedCount++;
    //         processedCount = this.processedCount;
    //         this.progress = (processedCount / this.totalCount) * 100;

    //         this.currentProcessing = `processing members (${processedCount}/${this.totalCount})`;
    //         worker['dzc_id'] = this.dzc_id;
    //         // if (this.departmentId == 1) {
    //         //     worker['designation_id'] = worker.desig.toLowerCase() == 'worker' ? 1 : 2;
    //         // } else if (this.departmentId == 2) {
    //         //     worker['designation_id'] = 1;
    //         // }
    //         this.allDesignations.forEach(desig => {
    //             if(desig.name==worker.desig.toLowerCase()){
    //                 worker['designation_id']=desig.id;
    //             }
    //         });

    //         worker['org_id'] = this.org_id;
    //         console.log("ImportMembersComponent -> processWorkers -> data", worker);
    //         if (!worker.name) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker Name is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         if (!worker.esi_no) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker ESI NO is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         if (!(worker.pf_uan_no || worker.pfuanno)) {
    //             this.logs.push({
    //                 processedCount,
    //                 message: "Worker PFUAN NO is not specified in the imported CSV",
    //                 worker
    //             });
    //             continue;
    //         }
    //         await this.insertWorker(worker, processedCount);
    //     }
    // }
}


function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}