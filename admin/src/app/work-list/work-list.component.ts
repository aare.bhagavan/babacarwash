import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatPaginator, MatSort, MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';
import { MastersService } from 'app/api/masters.service';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import Swal from 'sweetalert2';


export interface DialogData {

  data: any;
  worker:string;
  vehicle:string;
}
@Component({
  selector: 'app-work-list',
  templateUrl: './work-list.component.html',
  styleUrls: ['./work-list.component.scss']
})
export class WorkListComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true })
  paginator: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort: MatSort;
  dataSource: any;
  displayedColumns = ['Date', 'Status', 'Action'];
  workDetails: any;
  DATE: string;
  // carDetails: any[] = [];
  constructor(public dialogRef: MatDialogRef<WorkListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,public mastersService: MastersService) { }

  ngOnInit() {
    // let dataSour=[];
    this.workDetails=this.data.data;
    console.log("workdetails",this.workDetails);
    this.dataSource = new MatTableDataSource(this.data.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  cancel(){
    this.dialogRef.close();
  }

  export() {
    let header;
    let exportData = [];

    // header = ['Customer Name', 'Vehicle', 'Worker', 'Building'];
    header = ['Date', 'Status'];
    this.workDetails.forEach(element => {
        let columnData = {
            // 'Customer Name': element.first_name + ' ' + element.last_name,
            // Vehicle: element.vehicle,
            // Worker: element.worker_name,
            // Building: element.building_name
            Date: element.date,
            Status: element.status,
            // Action: element.action
        }
        exportData.push(columnData);
    })


    let csvData = this.ConvertToXLS(exportData, header);
    let blob = new Blob(['\ufeff' + csvData], { type: 'text/csv;charset=utf-8;' });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser = navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {  //if Safari open in new window to save file with random filename.
        dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    // dwldLink.setAttribute("download", "Work Details" + this.DATE + ".xls");
    dwldLink.setAttribute("download", "Work Details" + ".xls");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);


}

ConvertToXLS(objArray, headerList) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = 'sl_no,';

    for (let index in headerList) {
        row += headerList[index] + ',';
    }
    row = row.slice(0, -1);
    str += row + '\r\n';
    for (let i = 0; i < array.length; i++) {
        let line = (i + 1) + '';
        for (let index in headerList) {
            let head = headerList[index];
            line += ',' + array[i][head];
        }
        str += line + '\r\n';
    }

    return str;
}

  async updateStatus(detais){
    (await this.mastersService.updateWorkStatus(detais)).subscribe((response: any) => {
      console.log(response);
      if(response.success==true){
        Swal.fire({
          icon: 'success',
          title: "Status Updated Successfully."
        })
        this.dialogRef.close()
      }
    })
  }

}
