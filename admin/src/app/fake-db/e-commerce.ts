export class ECommerceFakeDb {
    public static products = [
        {
            'id': '1',
            'name': 'Paramesh',
            'gender': 'Male',
            'mobile': '9573552427',
            'age': '43',

        },
        {
            'id': '2',
            'name': 'Ramesh',
            'gender': 'Male',
            'mobile': '9573552427',
            'age': '43',

        },
        {
            'id': '3',
            'name': 'Priyanka',
            'gender': 'female',
            'mobile': '8989878787',
            'age': '18',

        },
        {
            'id': '4',
            'name': 'Madhuri',
            'gender': 'Male',
            'mobile': '9573552427',
            'age': '43',

        },
        {
            'id': '5',
            'name': 'Pranay',
            'gender': 'Male',
            'mobile': '9573552427',
            'age': '43',

        },
        {
            'id': '6',
            'name': 'Hrithik',
            'gender': 'Male',
            'mobile': '9573552427',
            'age': '43',

        }
    ];
}
