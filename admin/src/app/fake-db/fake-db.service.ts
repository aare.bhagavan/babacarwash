import { InMemoryDbService } from 'angular-in-memory-web-api';

import { ECommerceFakeDb } from 'app/fake-db/e-commerce';
import { FaqFakeDb } from 'app/fake-db/faq';
import { IconsFakeDb } from 'app/fake-db/icons';
import { QuickPanelFakeDb } from 'app/fake-db/quick-panel';
import { ProjectDashboardDb } from './dashboard-project';
import { AnalyticsDashboardDb } from './dashboard-analytics';

export class FakeDbService implements InMemoryDbService
{
    createDb(): any
    {
        return {
            // E-Commerce
            'e-commerce-products' : ECommerceFakeDb.products,
            // 'e-commerce-orders'   : ECommerceFakeDb.orders,

            // Dashboards
            'project-dashboard-projects' : ProjectDashboardDb.projects,
            'project-dashboard-widgets'  : ProjectDashboardDb.widgets,
            'analytics-dashboard-widgets': AnalyticsDashboardDb.widgets,
            
            // Quick Panel
            'quick-panel-notes' : QuickPanelFakeDb.notes,
            'quick-panel-events': QuickPanelFakeDb.events
        };
    }
}
