export class AnalyticsDashboardDb {
    public static widgets = {
        widget1: {
            chartType: 'line',
            datasets: {
                'Week1': [
                    {
                        label: 'Number Of Cars',
                        data: [85, 44, 108, 159, 72, 80, 43],
                        fill: 'start'
                    }
                ],
                'Week2': [
                    {
                        label: 'Number Of Cars',
                        data: [56, 75, 92, 55, 126, 49, 99],
                        fill: 'start'
                    }
                ],
                'Week3': [
                    {
                        label: 'Number Of Cars',
                        data: [139, 99, 136, 148, 29, 109, 29],
                        fill: 'start'
                    }
                ],
                'Week4': [
                    {
                        label: 'Number Of Cars',
                        data: [45, 78, 143, 165, 32, 87, 110],
                        fill: 'start'
                    }
                ]

            },
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            // ticks: {
                            //     min: 1.5,
                            //     max: 5,
                            //     stepSize: 0.5
                            // }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widgetPending: {
            chartType: 'line',
            datasets: {
                'Week1': [
                    {
                        label: 'Number Of Cars',
                        data: [85, 44, 108, 159, 72, 80, 43],
                        fill: 'start'
                    }
                ],
                'Week2': [
                    {
                        label: 'Number Of Cars',
                        data: [56, 75, 92, 55, 126, 49, 99],
                        fill: 'start'
                    }
                ],
                'Week3': [
                    {
                        label: 'Number Of Cars',
                        data: [139, 99, 136, 148, 29, 109, 29],
                        fill: 'start'
                    }
                ],
                'Week4': [
                    {
                        label: 'Number Of Cars',
                        data: [45, 78, 143, 165, 32, 87, 110],
                        fill: 'start'
                    }
                ]

            },
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            // ticks: {
                            //     min: 1.5,
                            //     max: 5,
                            //     stepSize: 0.5
                            // }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        },
        widget2: {
            conversion: {
                value: '30%',
                ofTarget: 'increase'
            },
            chartType: 'bar',
            datasets: [
                {
                    label: 'Fever',
                    data: [ 221, 294, 344, 413, 428, 471, 492 ]
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 100,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget3: {
            impressions: {
                value: '18%',
                ofTarget: 'increase'
            },
            chartType: 'line',
            datasets: [
                {
                    label: 'Cough & Cold',
                    data: [45, 65, 78, 86, 92, 108, 125],
                    fill: false
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#5c84f1'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                elements: {
                    point: {
                        radius: 2,
                        borderWidth: 1,
                        hoverRadius: 2,
                        hoverBorderWidth: 1
                    },
                    line: {
                        tension: 0
                    }
                },
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                // min: 100,
                                // max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget4: {
            visits: {
                value: '41%',
                ofTarget: 'increase'
            },
            chartType: 'bar',
            datasets: [
                {
                    label: 'Sneezing',
                    data: [ 231, 267, 327, 363, 428, 436, 456 ]
                }
            ],
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#f44336',
                    backgroundColor: '#f44336'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 24,
                        left: 16,
                        right: 16,
                        bottom: 16
                    }
                },
                scales: {
                    xAxes: [
                        {
                            display: false
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            ticks: {
                                min: 150,
                                max: 500
                            }
                        }
                    ]
                }
            }
        },
        widget5: {
            chartType: 'line',
            datasets: {
                'yesterday': [
                    {
                        label: 'Healthy',
                        data: [190, 300, 340, 220, 290, 390, 250, 380, 410, 380, 320, 290],
                        fill: 'start'

                    },
                    {
                        label: 'At Risk',
                        data: [2200, 2900, 3900, 2500, 3800, 3200, 2900, 1900, 3000, 3400, 4100, 3800],
                        fill: 'start'
                    }
                ],
                'today': [
                    {
                        label: 'Healthy',
                        data: [410, 380, 320, 290, 190, 390, 250, 380, 300, 340, 220, 290],
                        fill: 'start'
                    },
                    {
                        label: 'At Risk',
                        data: [3000, 3400, 4100, 3800, 2200, 3200, 2900, 1900, 2900, 3900, 2500, 3800],
                        fill: 'start'

                    }
                ]
            },
            labels: ['12am', '2am', '4am', '6am', '8am', '10am', '12pm', '2pm', '4pm', '6pm', '8pm', '10pm'],
            colors: [
                {
                    borderColor: '#3949ab',
                    backgroundColor: '#3949ab',
                    pointBackgroundColor: '#3949ab',
                    pointHoverBackgroundColor: '#3949ab',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                },
                {
                    borderColor: 'rgba(30, 136, 229, 0.87)',
                    backgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointHoverBackgroundColor: 'rgba(30, 136, 229, 0.87)',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                tooltips: {
                    position: 'nearest',
                    mode: 'index',
                    intersect: false
                },
                layout: {
                    padding: {
                        left: 24,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false
                            },
                            ticks: {
                                fontColor: 'rgba(0,0,0,0.54)'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            gridLines: {
                                tickMarkLength: 16
                            },
                            ticks: {
                                stepSize: 1000
                            }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                }
            }
        },
        widget6: {
            markers: [
                {
                    lat: 19.228825,
                    lng: 72.854118,
                    label: '120',
                    containment:false
                },
                {
                    lat: 18.990713,
                    lng: 73.116844,
                    label: '498',
                    containment:false
                },
                {
                    lat: 19.059984,
                    lng: 72.889999,
                    label: '443',
                    containment:true
                },
                {
                    lat: 19.044218,
                    lng: 72.855980,
                    label: '332',
                    containment:false
                },
                {
                    lat: 19.097403,
                    lng: 72.874245,
                    label: '50',
                    containment:false
                },
                {
                    lat: 19.077065,
                    lng: 72.998993,
                    label: '221',
                    containment:true
                },
                {
                    lat: 18.910000,
                    lng: 72.809998,
                    label: '455',
                    containment:false
                }
            ],
            styles: [
                {
                    'featureType': 'administrative',
                    'elementType': 'labels.text.fill',
                    'stylers': [
                        {
                            'color': '#444444'
                        }
                    ]
                },
                {
                    'featureType': 'landscape',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#f2f2f2'
                        }
                    ]
                },
                {
                    'featureType': 'poi',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'road',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'saturation': -100
                        },
                        {
                            'lightness': 45
                        }
                    ]
                },
                {
                    'featureType': 'road.highway',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'simplified'
                        }
                    ]
                },
                {
                    'featureType': 'road.arterial',
                    'elementType': 'labels.icon',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'transit',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'visibility': 'off'
                        }
                    ]
                },
                {
                    'featureType': 'water',
                    'elementType': 'all',
                    'stylers': [
                        {
                            'color': '#039be5'
                        },
                        {
                            'visibility': 'on'
                        }
                    ]
                }
            ]
        },
        widget7: {
            scheme: {
                domain: ['#006400', '#5c84f1', '#89a9f4','#FFFF00','#FF4500','#808000','#808080']
            },
            devices: [
                {
                    name: 'Fever',
                    value: 48,
                    // change: -0.6
                },
                {
                    name: 'Cough',
                    value: 14,
                    // change: 0.7
                },
                {
                    name: 'Head Ache',
                    value: 29,
                    // change: 0.1
                },
                {
                    name: 'Dizziness',
                    value: 29,
                    // change: 0.1
                },
                {
                    name: 'Runny Nose',
                    value: 29,
                    // change: 0.1
                },
                {
                    name: 'Sore Throat',
                    value: 29,
                    // change: 0.1
                },
                {
                    name: 'Breathing',
                    value: 29,
                    // change: 0.1
                }
            ]
        },
        widget8: {
            scheme: {
                domain: ['#5c84f1']
            },
            today: '12,540',
            change: {
                value: 321,
                percentage: 2.05
            },
            data: [
                {
                    name: 'Sales',
                    series: [
                        {
                            name: 'Jan 1',
                            value: 540
                        },
                        {
                            name: 'Jan 2',
                            value: 539
                        },
                        {
                            name: 'Jan 3',
                            value: 538
                        },
                        {
                            name: 'Jan 4',
                            value: 539
                        },
                        {
                            name: 'Jan 5',
                            value: 540
                        },
                        {
                            name: 'Jan 6',
                            value: 539
                        },
                        {
                            name: 'Jan 7',
                            value: 540
                        }
                    ]
                }
            ],
            dataMin: 538,
            dataMax: 541
        },
        widget9: {
            rows: [
                {
                    title: 'Vashi',
                    clicks: 3621,
                    conversion: 90
                },
                {
                    title: 'Panvel',
                    clicks: 703,
                    conversion: 7
                },
                {
                    title: 'Borivile',
                    clicks: 532,
                    conversion: 0
                },
                {
                    title: 'Parel',
                    clicks: 201,
                    conversion: 8
                },
                {
                    title: 'Bandra',
                    clicks: 94,
                    conversion: 4
                }
            ]
        },
        widget10: {
            chartType: 'line',
            datasets: {
                'Week1': [
                    {
                        label: 'Captured Records',
                        data: [85, 44, 108, 159, 72, 80, 43],
                        fill: 'start'
                    }
                ],
                'Week2': [
                    {
                        label: 'Captured Records',
                        data: [56, 75, 92, 55, 126, 49, 99],
                        fill: 'start'
                    }
                ],
                'Week3': [
                    {
                        label: 'Captured Records',
                        data: [139, 99, 136, 148, 29, 109, 29],
                        fill: 'start'
                    }
                ],
                'Week4': [
                    {
                        label: 'Captured Records',
                        data: [45, 78, 143, 165, 32, 87, 110],
                        fill: 'start'
                    }
                ]

            },
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            colors: [
                {
                    borderColor: '#42a5f5',
                    backgroundColor: '#42a5f5',
                    pointBackgroundColor: '#1e88e5',
                    pointHoverBackgroundColor: '#1e88e5',
                    pointBorderColor: '#ffffff',
                    pointHoverBorderColor: '#ffffff'
                }
            ],
            options: {
                spanGaps: false,
                legend: {
                    display: false
                },
                maintainAspectRatio: false,
                layout: {
                    padding: {
                        top: 32,
                        left: 32,
                        right: 32
                    }
                },
                elements: {
                    point: {
                        radius: 4,
                        borderWidth: 2,
                        hoverRadius: 4,
                        hoverBorderWidth: 2
                    },
                    line: {
                        tension: 0
                    }
                },
                scales: {
                    xAxes: [
                        {
                            gridLines: {
                                display: false,
                                drawBorder: false,
                                tickMarkLength: 18
                            },
                            ticks: {
                                fontColor: '#ffffff'
                            }
                        }
                    ],
                    yAxes: [
                        {
                            display: false,
                            // ticks: {
                            //     min: 1.5,
                            //     max: 5,
                            //     stepSize: 0.5
                            // }
                        }
                    ]
                },
                plugins: {
                    filler: {
                        propagate: false
                    },
                    xLabelsOnTop: {
                        active: true
                    }
                }
            }
        }
    };
}
