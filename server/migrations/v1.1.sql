DROP TABLE IF EXISTS `customer_enquiry`;
CREATE TABLE IF NOT EXISTS `customer_enquiry` (
  `mobapp_cust_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `phone` char(10) DEFAULT NULL,
  `status` varchar(10) DEFAULT 'open',
  `status_comment` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `plan` varchar(255) DEFAULT NULL,
  `type` VARCHAR(20),
  `user_id` int DEFAULT NULL,
  `email` varchar(100),
  `premises` VARCHAR(50),
  `is_read` BOOLEAN DEFAULT FALSE,
  `read_at` TIMESTAMP,
  PRIMARY KEY (`mobapp_cust_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

ALTER TABLE users
    ADD COLUMN email varchar(100);

ALTER TABLE customers
    ADD COLUMN user_id INT,
    ADD COLUMN email varchar(100),
    ADD FOREIGN KEY (user_id) REFERENCES users(id);

create table otp (
  `user_id` int, 
  `mobile` VARCHAR(30),
  `otp` VARCHAR(20),
  `reqform` VARCHAR(15),
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);