CREATE DATABASE carwash_db;
CREATE USER 'carwash_user'@'localhost' IDENTIFIED BY 'carwash123#' ;
CREATE USER 'carwash_user'@'%' IDENTIFIED BY 'carwash123#' ;
GRANT ALL ON carwash_db.* TO 'carwash_user'@'localhost';
GRANT ALL ON carwash_db.* TO 'carwash_user'@'%';

