import { Schema, model } from "mongoose";
import { Decimal128 } from "mongodb";

const curriculumTopicSchema = new Schema({
    title: String,
    description: String,
    video: {
      link: String,
      length: Schema.Types.Decimal128,
      size: Schema.Types.Decimal128,
    }
});

export const CurriculumTopicModel = model('curriculumTopic', curriculumTopicSchema);

const curriculumSchema = new Schema({
  title: String,
  description: String,
  topics: {
    type: [CurriculumTopicModel.schema]
  }
})

export const CurriculumModel = model('curriculum', curriculumSchema);


const courseSchema = new Schema({
  title: {
    type: Schema.Types.String,
    required: 'Course Title is required'
  },
  mainDescription: {
    type: Schema.Types.String,
    required: 'Course Description is required'
  },
  imageURL: {
    type: Schema.Types.String,
    required: 'imageUrl is required'
  },
  projects: {
    type: [ Schema.Types.ObjectId ]
  },
  instructors: {
    type: [{
      instructorId: Schema.Types.ObjectId,
      name: String,
      description: String
     }]
  },
  estimatedTime: {
    type: {
      days: Schema.Types.Number,
      hpd: Schema.Types.Number
    },
  },
  prerequisites: {
    type: Schema.Types.String,
  },
  description1: {
    type: [ Schema.Types.String ]
  },
  description2: {
    type: [ Schema.Types.String ]
  },
  numberOfVideos: Number,
  curriculum: {
    type: [CurriculumModel.schema],
    required: 'Course Curriculum is required'
  },
  pricing: {
    type: {
      selfTaughtPrice: Schema.Types.Number,
      instructorLedPrice: Schema.Types.Number
    }
  },
  category: {
    type: {
      categoryId: Schema.Types.ObjectId,
      name: String
    },
    required: true
  },
  rating: {
    type: Schema.Types.Number
  },
  resources: {
    type: Schema.Types.Array,
    default: []
  },
  registeredStudents: [Schema.Types.ObjectId],
});


export const CourseModel = model('course', courseSchema)
  

