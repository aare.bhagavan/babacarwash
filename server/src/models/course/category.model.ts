import { Schema, model } from "mongoose";

const categorySchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: true
  },
  parent :{
    type: Schema.Types.ObjectId,
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  courses: [Schema.Types.ObjectId]
});

export default model('category', categorySchema);