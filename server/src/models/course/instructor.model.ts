import { Schema, model } from "mongoose";

const courseSchema = new Schema({
  name: {
    type: Schema.Types.String,
    required: true
  },
  technologies :{
    type: [Schema.Types.ObjectId],
  },
  experience: {
    type: Schema.Types.Number,
    required: true
  },
  description: {
    type: Schema.Types.String,
    required: true
  }
});

export default model('instructor', courseSchema);