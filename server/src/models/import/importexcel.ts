import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';
// import { try } from 'bluebird';


export class importexcel {
    constructor() { }
    public static async insertUpdateTable(name: any, numOfRecords: any, filepath: any, uploadedBy: any, At: any, dzc_id: number, month: number, year: number) {
        // let date = new Date;
        // let month = date.getMonth()
        // month = month + 1;
        // let year = date.getFullYear()
        const query = `select * from DocumentsProcess where dzc_id = ${dzc_id} and pay_month = ${month} and pay_year=${year}`;
        const [rows, fields] = await mysql.query(query);
        const result = JSON.parse(JSON.stringify(rows));
        console.log("Login -> login -> result", result);
        let DocumentDataTable = "";

        if (result.length == 0) {
            DocumentDataTable = `insert into DocumentsProcess(name,numOfRecords,filepath,uploadedBy,createdAt,pay_month,pay_year,dzc_id,date_created) values('${name}', '${numOfRecords}','${filepath}','${uploadedBy}',CURDATE(),${month},${year},${dzc_id},CURDATE())`;
            const [rows2, fields2] = await mysql.query(DocumentDataTable);
            const result2 = JSON.parse(JSON.stringify(rows2));
            console.log("Login -> login -> result", result2);
            const status = { success: true, message: 'Data is in Table', id: result2.insertId };
            return status;
        }
        else {
            DocumentDataTable = `update DocumentsProcess SET filepath='${filepath}',name='${name}',numOfRecords=${numOfRecords},date_deleted=NULL where dzc_id = ${dzc_id} and pay_month = ${month} and pay_year=${year}`;
            const [rows2, fields2] = await mysql.query(DocumentDataTable);
            const result2 = JSON.parse(JSON.stringify(rows2));
            console.log("Login -> login -> result", result2);
            const status = { success: true, message: 'Data is in Table', id: result[0].id };
            return status;
        }

    }



    public static async setWagesTable(sheet: any, month: number, year: number, docProc_id: number,dzc_id:number) {
        let rejectedWorkers=[];
        try {
            // sheet.forEach(async (worker: any) => {
            var inserCount=0;
            for (let i in sheet) {
                let worker = sheet[i];
                console.log('processing worker ..'+JSON.stringify(worker));
                const workerQuery = `select * from workers where esi_no =${worker.esi_no}`;
                const [workerQueryRows, workerQueryFields] = await mysql.query(workerQuery);
                const workerResult = JSON.parse(JSON.stringify(workerQueryRows));
                if (workerResult[0] != undefined) {
                    const salaryQuery = `select salary from designation where id=${workerResult[0].designation_id}`
                    const [salaryQueryRows, salaryQueryFields] = await mysql.query(salaryQuery);
                    const salaryResult = JSON.parse(JSON.stringify(salaryQueryRows));


                    // let date = new Date;
                    // let month = date.getMonth();
                    // month = month + 1;
                    // let year = date.getFullYear();
                    var getDaysInMonth = function (month: number, year: number) {
                        return new Date(year, month, 0).getDate();
                    };
                    const numOfDays = getDaysInMonth(month, year);
                    if(isNaN(worker.absent_days)||(worker.absent_days<0)){
                        console.log('Worker Record is not valid ' + worker.pf_uan_no);
                        rejectedWorkers.push(worker);
                    }else{
                        const presentDays = numOfDays - worker.absent_days;
                        const wagesQuery = `insert into wages (worker_id,pf_uan_no,salary_as_on_date,days_worked,days_leave,pay_month,pay_year,docProc_id,dzc_id) values (${workerResult[0].id},${workerResult[0].pf_uan_no},${salaryResult[0].salary},${presentDays},${worker.absent_days},${month},${year},${docProc_id},${dzc_id})`;
                        const [wagesQueryRows, wagesQueryFields] = await mysql.query(wagesQuery);
                        const wagesResult = JSON.parse(JSON.stringify(wagesQueryRows));
                        // let status;
                        // if (wagesResult != null || wagesResult != undefined) {
                        //     status = { success: true, message: 'Data is in Table' };
                        // }
                        // else {
                        //     status = { success: false, message: 'Failed at query contruction' };
                        // }
                        // return status;
    
    
                        // let thisMonthWage = 0;
                        // let date = new Date;
                        // var getDaysInMonth = function (month: number, year: number) {
                        //     return new Date(year, month, 0).getDate();
                        // };
                        // const numOfDays = getDaysInMonth(date.getDate(), date.getFullYear());
                        // if (salaryQueryRows != null || salaryQueryRows != undefined) {
                        //     thisMonthWage = salaryResult - (salaryResult * (worker.absent_days / numOfDays))
                        // }
                        // return wagesResult;
                        inserCount=inserCount+1;
                        //console.log('processed worker successfully'+ JSON.stringify(worker));
                    }
               
                }
            }
            // });
            return { status: true, message: 'records inserted', numOfRecords: inserCount}
        } catch (err) {
            return { status: false, message: err };
        }

    }

    public static async deleteAndInsertWages(sheet: any, month: number, year: number) {

        try {
            var existWorker: Array<any> = []
            var pf_uan_numbers: any = '0'

            for (let i in sheet) {
                let workerData = sheet[i];
                pf_uan_numbers = pf_uan_numbers + ',' + workerData.pf_uan_no;
            }
            // let date = new Date;
            // let month = date.getMonth();
            // month = month + 1;
            // let year = date.getFullYear();
            const deleteQuery = `delete from wages where pf_uan_no in (${pf_uan_numbers}) and pay_month in (${month}) and pay_year in (${year})`;
            const [deleteQueryRows, workerQueryFields] = await mysql.query(deleteQuery);
            var workerResult = JSON.parse(JSON.stringify(deleteQueryRows));
            // for (let i in sheet) {
            //     const worker = sheet[i];
            //     let date = new Date;
            //     let month = date.getMonth();
            //     month = month + 1;
            //     let year = date.getFullYear();
            //     const deleteQuery = `delete from wages where pf_uan_no =${worker.pf_uan_no} and month=${month} and year=${year}`;
            //     const [deleteQueryRows, workerQueryFields] = await mysql.query(deleteQuery);
            //     var workerResult = JSON.parse(JSON.stringify(deleteQueryRows));
            //     if (workerResult.length > 0) {
            //         existWorker.push(workerResult[0]);
            //     }
            // }
            return { status: true, message: 'records deleted' };
        } catch (err) {
            return { status: false, message: err, existWorker: [] };
        }

    }

    public static async verifyworker(sheet: any, month: number, year: number) {

        try {
            var existWorker: Array<any> = []
            for (let i in sheet) {
                const worker = sheet[i];
                // let date = new Date;
                // let month = date.getMonth();
                // month = month + 1;
                // let year = date.getFullYear();
                console.log(worker);
                const workerQuery = `select * from wages where pf_uan_no =${worker.pf_uan_no} and pay_month=${month} and pay_year=${year}`;
                const [workerQueryRows, workerQueryFields] = await mysql.query(workerQuery);
                var workerResult = JSON.parse(JSON.stringify(workerQueryRows));
                if (workerResult.length > 0) {
                    existWorker.push(workerResult[0]);
                }
            }
            // sheet.forEach(async (worker: any) => {



            // });
            return { status: true, message: 'records already exist', existWorker: existWorker };
        } catch (err) {
            return { status: false, message: err, existWorker: [] };
        }

    }

    public static async workersWagesEntry(id: number, numOfLeaves: number, Wages: number) {
        const date = new Date;
        var getDaysInMonth = function (month: number, year: number) {
            return new Date(year, month, 0).getDate();
        };
        const numOfDays = getDaysInMonth(date.getDate(), date.getFullYear());
        const presentdays = numOfDays - numOfLeaves;
        const workersWagesEntryQuery = `insert into workers (month,year,worker_id,days_worked,days_leave,gross_wages)
         values ('${date.getMonth}','${date.getFullYear}',${id},'${presentdays}','${numOfLeaves}','${Wages}');`
        const [WagesTableUpdateRows, WagesTableUpdateFields] = await mysql.query(workersWagesEntryQuery);
        const WagesResult = JSON.parse(JSON.stringify(WagesTableUpdateRows));
        let status;
        if (WagesResult != null || WagesResult != undefined) {
            status = { success: true, message: 'Data is in Table' };
        }
        else {
            status = { success: false, message: 'Failed at query contruction' };
        }
        return status;
    }
}