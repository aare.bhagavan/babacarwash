import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';
import { query } from 'express';


export class Login {


    public mobileno: any;
    public password: any;
    constructor() {
        this.mobileno = '';
        this.password = '';
    }

    // public static async getEmployeesByName(name: string): Promise<Login[]> {
    //     log.info("employee.model -> getEmployeeByName: before query");
    //     const query = `select * from employee where name = "${name}"`;
    //     log.info("employee.model -> getEmployeeByName: ", { query });
    //     const [rows, fields] = await mysql.query(query);
    //     log.info("employee.model -> getEmployeeByName: ", { rows });
    //     const employees: Login[] = [];
    //     const result = JSON.parse(JSON.stringify(rows));
    //     result.forEach((row: any) => {
    //         const e = new Login();
    //         log.info("employee.model -> getEmployeeByName: ", { row });
    //         e.name = row['name'];
    //         employees.push(e);
    //     });
    //     return employees;
    // }

    public static async isTokenExpired(userId: number, token: string) {
        const logoutQuery = `
            SELECT * FROM user_token 
            WHERE user_id = ${userId}
            AND token = '${token}'
            AND is_expired = 1
        `;
        const [rows, fields] = await mysql.query(logoutQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result.count > 0;
    }

    public static async logout(userId: number, token: string) {
        const logoutQuery = `
            UPDATE user_token 
            SET is_expired = 1
            WHERE user_id = ${userId}
            AND token = '${token}'
        `;
        const [rows, fields] = await mysql.query(logoutQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async login(mobileno: string, password: string , logintype : string) {

        let mobile: string = mobileno;
        console.log("Login -> login -> phone", mobile)
        let pass: string = password;
        console.log("Login -> login -> pass", pass)
        let logintype1: string = logintype;
        console.log("Logintype -> login type-> logintypw", logintype1);
        let query;
        if(logintype1 == "customer"){
             query = `SELECT u.*, c.id as customer_id from users u
                        JOIN customers c ON c.user_id = u.id 
                        WHERE u.mobile = '${mobile}' 
                        AND password = '${pass}'  `;
        }else{
             query = `select * from users where mobile = '${mobile}' and password = '${pass}'`;
        }
        
        console.log("Login -> login -> query", query)
        const [rows, fields] = await mysql.query(query);
        const result = JSON.parse(JSON.stringify(rows));
        let status;
        if (result.length != 0) {
            const userName = result[0].name;
            const userId=result[0].id;
            const role=result[0].role
            // const organization_id: number = result[0].org_id;
            console.log("Login -> login -> result", result);

            // JSON.stringify(result.phone) 
            console.log("Login -> login -> JSON.stringify(result.phone)", result[0].mobile)
            let resultPhone = result[0].mobile;
            if (resultPhone === mobile) {
                let token = jwt.sign({
                    resultPhone: mobile,
                    username: userName,
                    userId: userId,
                    role: role,
                }, 'carwash', {
                    // expiresIn: 144000 // expires in 24 hours
                });
                let currentDateTime: Date = new Date();
                let isExpired = 0//false;

                //insert into user_token
                const insertQueryUserToken = `insert into user_token(token,user_id,is_expired) values('${token}', ${result[0].id},${isExpired})`;
                const [rows1, fields1] = await mysql.query(insertQueryUserToken);

                //getting role name
                // let role_id = result[0].role_id;
                // const roleQuery = `select * from role where id=${role_id}`;
                // const [rows2, fields2] = await mysql.query(roleQuery);
                // const roleNameResult = JSON.parse(JSON.stringify(rows2));
                // let roleName = roleNameResult[0].name;
                // console.log("Login -> login -> roleName", roleName)

                //sending status
                status = {
                    success: true,
                    name: userName,
                    user_id:userId,
                    token: token,
                    currentDateTime: currentDateTime,
                    isExpired: isExpired,
                    role: role,
                    customer_id: result[0].customer_id,
                    message: 'Login Successful'
                };
            }
        }
        else {
            let msg;
            if(logintype1 == "customer"){
                msg='Not registered as customer'
            }else{
                msg='Invalid Credentials'
            }
            status = {
                success: false,
                message: msg
            };
        }
        return status;
    }

}