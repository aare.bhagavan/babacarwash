import { Schema, model } from "mongoose";
import { CurriculumModel, CurriculumTopicModel } from "../course/course.model";
import orderModel from "./order.model";
import { Timestamp } from "mongodb";

const curriculumTopicProgressSchema = new Schema({
  content: CurriculumTopicModel,
  completed: Boolean,
  watchLegth: Number
});

export const CurriculumTopicProgressModel = model('curriculumTopicProgress', curriculumTopicProgressSchema);

const curriculamProgressSchema = new Schema({
  title: String,
  description: String,
  topics: [CurriculumTopicProgressModel]
});

export const CurriculamProgressModel = model('curriculamProgress', curriculamProgressSchema);


const studentSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId,
    required: true,
    // should be same as userSchema._id
  },
  name: {
    type: Schema.Types.String,
    required: 'Username is required',
  },
  email: {
    type: Schema.Types.String,
    required: 'Email is required',
    index: true
  },
  phone: {
    type: Schema.Types.Number,
    // required: 'Phone number is required'
  },
  interestedCourses: {
    type: [{
      category: Schema.Types.ObjectId,
      courseId: Schema.Types.ObjectId,
    }],
  },
  orders: {
    type: [Schema.Types.ObjectId]
    // order list from orders
  }
});

export default model('student', studentSchema);