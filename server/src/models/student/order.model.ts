import { Schema, model } from "mongoose";

const orderSchema = new Schema({
  course: {
    type: {
      id: Schema.Types.ObjectId,
      name: String,
      price: Schema.Types.Decimal128,
    },
    required: 'Course Id is required'
  },
  type:{
    type: Schema.Types.String
  },
  studentId: {
    type: Schema.Types.ObjectId,
    required: 'Student ID is required'
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  discount: {
    type: {
      couponCode: String,
      percentage: Number,
      amount: Schema.Types.Decimal128
    },
    required: false
  },
  totalAmount: {
    type: Schema.Types.Decimal128,
    required: true
  },
  paidAmount: Schema.Types.Decimal128,
  paymentId: {
    type: Schema.Types.String,
  },
  status: {
    type: String,
    enum: ['initiated','success','failed'],
    default: 'initiated'
  },
  
  request: Schema.Types.Mixed,
  response: Schema.Types.Mixed
});

export default model('order',orderSchema);