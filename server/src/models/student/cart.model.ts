import { Schema, model } from "mongoose";

const cartSchema = new Schema({
  courses: {
    type: Array,
  },
  studentId: {
    type: Schema.Types.ObjectId,
    required: 'Student ID is required'
  }
});

export default model('cart',cartSchema);