import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';



export class DocumentsProcess {

    public static async getAllDocumentProcess() {

        const getDocumentsProcessQuery = `select * from DocumentsProcess where date_deleted is null `;

        const [rows, fields] = await mysql.query(getDocumentsProcessQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }


    public static async deleteDocument(id: number) {
        let status;
        try {
            const deleteDocumentQuery = `DELETE w FROM  wages w
            LEFT JOIN DocumentsProcess dp ON dp.id = w.docProc_id 
            WHERE dp.id=${id} and w.pay_roll_run_id is null`;
            console.log(deleteDocumentQuery);
            const [rows, fields] = await mysql.query(deleteDocumentQuery);
            const result = JSON.parse(JSON.stringify(rows));
            if (result.affectedRows > 0) {
                const updateDocumentQuery = `UPDATE DocumentsProcess dp SET dp.date_deleted = now() WHERE dp.id=${id} `;
                console.log(deleteDocumentQuery);
                const [rows, fields] = await mysql.query(updateDocumentQuery);
                const result = JSON.parse(JSON.stringify(rows));

                status = {
                    success: true,
                    status: 'deleted',
                    message: "Document Deleted Successfully"
                }
            } else {
                status = {
                    success: false,
                    status: 'delete failed',
                    message: 'Document cannot be deleted'
                }
            }

        } catch (err) {
            status = {
                success: false,
                status: 'delete failed',
                message: 'delete failed'
            }
        }
        return status;
    }

}





