export class STATUS {
    static readonly OPEN = "open";
    static readonly CLOSED = "closed";
    static readonly INVALID = "invalid";
}