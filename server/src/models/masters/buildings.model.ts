import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';

export class Buildings {
    public static async getAllBuildings() {
        const getBuildinsQuery = `select b.*,l.address from buildings b 
        left join location l on b.location_id=l.id
        where b.status=1 and l.status=1`;
        const [rows, fields] = await mysql.query(getBuildinsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async getAllworkerBuildings() {
        const getWork_BuilQuery = `select * from worker_building;`;
        const [rows, fields] = await mysql.query(getWork_BuilQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    } 
    public static async addNewBuilding(data:any){
        const checkbuilding=`select * from buildings where name='${data.name}' AND location_id=${data.location_id}`

        const [rows, fields] = await mysql.query(checkbuilding);
        const result = JSON.parse(JSON.stringify(rows));
        let status

        if (result.length > 0) {
            return status = {
                success: false,
                message: 'Location Already Exists.'
            };
        }
        else{
            const addBuildingQuery = ` insert into buildings(name,location_id,status) values("${data.name}",${data.location_id},1)`;
            const [rows, fields] = await mysql.query(addBuildingQuery);
            const result = JSON.parse(JSON.stringify(rows));
            let status;
            return status = {
                success: true,
                message: 'Building added successfully.'
            };  

        }

        
    }
    public static async editBuilding(data:any,id:number){
        const editBuildingQuery = ` update buildings set name="${data.name}",location_id=${data.location_id} where id=${id}`;
        const [rows, fields] = await mysql.query(editBuildingQuery);
        const result = JSON.parse(JSON.stringify(rows));
        let status;
        return status = {
            success: true,
            message: 'Building updated successfully.'
        };
    }
    public static async deleteBuilding(id:any,user_name:any){
        const deleteBuildingQuery = ` update buildings set status=0,deletedAt=CURDATE(),deletedBY='${user_name}' where id=${id.id}`;
        const [rows, fields] = await mysql.query(deleteBuildingQuery);
        const result = JSON.parse(JSON.stringify(rows));
        let status;
        return status = {
            success: true,
            message: 'Building deleted successfully.'
        };
    }
}
