import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';
import { WashFilter } from '@common/wash.filter';
import { Paginate } from '@common/paginate.interface';

export class Payments {
    // public static async getALLPaymentDetails() {
    //     const getWorkersQuery =
    //         `select distinct
    //         c.first_name,
    //         c.last_name,
    //         c.id as customer_id,
    //         v.registration_no as vehicle,
    //         v.id as vehicle_id,
    //         w.name as cleaner,
    //         w.id as worker_id,
    //         ws.start_date,
    //         ws.id as schedule_id,
    //         (SELECT COALESCE(sum(amount), 0) from schedule_logs sl where sl.schedule_id= ws.id and sl.status="completed" and sl.status='completed') as amount_charged,
    //         (SELECT COALESCE(sum(payment_amount), 0) from payment_transaction pt where pt.schedule_id= ws.id and pt.status='completed') as amount_paid
    //         from customers c
    //         INNER JOIN vehicles v on c.id=v.customer_id
    //         INNER JOIN work_schedule ws on v.id=ws.vehicle_id and ws.end_date is null
    //         INNER JOIN worker_building wb on ws.worker_building_id = wb.id and wb.end_date is null
    //         INNER JOIN workers w on wb.worker_id=w.id`;
    //     const [rows, fields] = await mysql.query(getWorkersQuery);
    //     const result = JSON.parse(JSON.stringify(rows));
    //     return result;
    // }
    public static async getALLPaymentDetails() {
        const getPaymentListQuery =
            `select distinct
            c.first_name,
            c.last_name,
            c.id as customer_id,
            v.registration_no as vehicle,
            v.id as vehicle_id,
            w.name as cleaner,
            w.id as worker_id,
            ws.start_date,
            ws.id as schedule_id,ws.monday,ws.tuesday,ws.wednesday,ws.thursday,ws.friday,ws.saturday,ws.sunday,MAX(sl.date) as date,
            (SELECT COALESCE(sum(payment_amount), 0) from payment_transaction pt where pt.schedule_id= ws.id and pt.status='completed') as amount_paid
            from customers c
            INNER JOIN vehicles v on c.id=v.customer_id
            INNER JOIN work_schedule ws on v.id=ws.vehicle_id 
            INNER JOIN schedule_logs sl on sl.schedule_id=ws.id
            INNER JOIN worker_building wb on ws.worker_building_id = wb.id 
            INNER JOIN workers w on wb.worker_id=w.id 
            where c.status != 0
            group by ws.id
            `;
        const [rows, fields] = await mysql.query(getPaymentListQuery);
        const result1 = JSON.parse(JSON.stringify(rows));
        if (result1) {
            const currentDateQuery = 'select CURDATE()';
            const [rows1, fields1] = await mysql.query(currentDateQuery);
            const result2 = JSON.parse(JSON.stringify(rows1));
            var current_date: any = result2[0]["CURDATE()"];
        }
        let paymentData: any;
        const allPayments: any[] = [];

        for (const schedule in result1) {
            result1[schedule]['current_date'] = current_date;
            const payment_amount = await Payments.calculatePaymentAmounts(result1[schedule]);
            //    let schedule_id=result1[schedule].schedule_id;
            //    const calculateMonthsQuery = `select * from work_schedule where id=${schedule_id}`;
            //    const [rows, fields] = await mysql.query(calculateMonthsQuery);
            //    const result = JSON.parse(JSON.stringify(rows));
            //    const start_date = moment(result[0].start_date);
            //    console.log(start_date);

            //    const presentDate = moment();
            //    const diffInMonths = Math.abs(start_date.diff(presentDate, 'months'));
            //    console.log(diffInMonths);
            //    const payment_amount=(diffInMonths+1)*(result[0].subscription_amount);
            //    console.log(payment_amount);
            // paymentData={
            //     'data':result1[schedule],
            //     'amount':payment_amount
            // };
            let pay_amount = {
                'amount_charged': payment_amount
            }
            paymentData = { ...result1[schedule], ...pay_amount };

            allPayments.push(paymentData);
        }

        console.log(allPayments);
        return allPayments;

    }

    public static async calculatePaymentAmounts(data: any) {
        console.log(data);

        const calculateMonthsQuery = `select * from work_schedule where id=${data.schedule_id}`;
        const [rows, fields] = await mysql.query(calculateMonthsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        const start_date = moment(result[0].start_date);
        console.log(start_date);
        const presentDate = moment();
        let payment_amount = 0;
        if (moment(start_date).isSameOrBefore(presentDate)) {
            const diffInMonths = Math.abs(start_date.diff(presentDate, 'months'));
            console.log(diffInMonths);
            if (diffInMonths > 0) {
                payment_amount = (diffInMonths) * (result[0].subscription_amount);
            }
            console.log(payment_amount);
        }
        return payment_amount;

    }


    public static async getUserPaymentDetails(inputData: any) {
        let query = ''
        if (inputData.type == "charged") {
            query = `select distinct
            sl.id as id,
            sl.date as date,
            sl.reason as reason,
            w.name as cleaner,
            sl.status as status,ws.monday,ws.tuesday,ws.wednesday,ws.thursday,ws.friday,ws.saturday,ws.sunday 
            from schedule_logs sl
            INNER join work_schedule ws on ws.id=sl.schedule_id
            LEFT join payment_transaction pt on ws.id=pt.schedule_id
            INNER join worker_building wb on wb.id =ws.worker_building_id
            INNER join workers w on w.id =wb.worker_id
            where sl.schedule_id=${inputData.data} order by sl.date `;
        } else if (inputData.type == "paid") {
            query = `select 
            pt.id as id,
            pt.payment_registered_by as paid_to,
            pt.payment_amount as amount_paid,
            pt.payment_date as paid_on,
            pt.status as status,
            pt.reason as reason,
            pt.note as comments,
            MAX(s_l.date) as log_month_last_date,
            ws.monday,ws.tuesday,ws.wednesday,ws.thursday,ws.friday,ws.saturday,ws.sunday
            from payment_transaction pt 
            left join work_schedule ws on ws.id= pt.schedule_id
            left join schedule_logs s_l on s_l.schedule_id=pt.schedule_id  
            where pt.schedule_id=${inputData.data} group by pt.id`;
        }
        const [rows, fields] = await mysql.query(query);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }

    public static async adminFeedBack(inputData: any) {
        let table = '';
        let status = '';
        if (inputData.type == "charged") {
            table = 'schedule_logs'
            if (inputData.data.column4 == 'Mark Not Done') {
                status = 'pending';
            } else status = 'completed';
        } else if (inputData.type == "paid") {
            table = 'payment_transaction'
            if (inputData.data.column4 == 'Undo Payment') {
                status = 'pending';
            } else status = 'completed';
        }
        let query = `update ${table} set status='${status}', reason=CONCAT(ifnull(reason, ''), ' ${inputData.feedback}') where id=${inputData.data.id}`;
        const [rows, fields] = await mysql.query(query);
        const result = JSON.parse(JSON.stringify(rows));
        if (result) {
            return true;
        }
    }

    public static async payAmount(inputData: any) {
        console.log(inputData);
        console.log(inputData.token)
        console.log(inputData.user_name)
        console.log(inputData.value);
        console.log(inputData.note)

        let userquery = `select user_id from user_token where token="${inputData.token}"`;
        const [userRows, usersFields] = await mysql.query(userquery);
        const userResult = JSON.parse(JSON.stringify(userRows));
        if (userResult.length > 0) {
            let query =
                `INSERT INTO payment_transaction (
            schedule_id,
            payment_amount,
            payment_registered_by,
            user_id,
            added_by,
            updated_by,
            status,
            note
          )
        VALUES (
            ${inputData.data.schedule_id},
            ${inputData.value},
            "${inputData.user_name}",
            ${userResult[0].user_id},
            "${inputData.user_name}",
            "${inputData.user_name}",
            "completed",
            "${inputData.note}"
          );`;
            console.log(query)
            const [rows, fields] = await mysql.query(query);
            const result = JSON.parse(JSON.stringify(rows));
            if (result) {
                return true;
            }

        }


    }

    public static async getuserpaymentdetails(id: any) {
        console.log(id)
        var today = new Date();
        var dd = today.getDate();

        var mm = today.getMonth() + 1;
        var yyyy = today.getFullYear();
        console.log(dd, mm, yyyy + "date month")
        var date = yyyy + "-" + "0" + mm + "-" + dd
        console.log(date)

        const paymentdetailsQuery = ` 
        select distinct b.name as building_name,b.id as building_id,c.flat_no,ws.id as s_id,
        ws.id as schedule_id,ws.vehicle_id ,ws.monday,ws.tuesday,ws.wednesday,ws.thursday,ws.friday,ws.saturday,ws.sunday,
         l.address as location,ws.subscription_amount,
         (SELECT COALESCE(sum(payment_amount), 0) from payment_transaction pt where pt.schedule_id= ws.id and pt.status='completed') as amount_paid
         ,ws.start_date as ws_date,v.parking_no,
         ws.date_created,v.registration_no,v.parking_no,u.name,u.id from users u
               inner join workers w on u.id=w.user_id
               inner join worker_building wb on wb.worker_id=w.id 
               inner join work_schedule ws on ws.worker_building_id=wb.id 
               inner join vehicles v on v.id=ws.vehicle_id 
               inner join buildings b on wb.building_id=b.id
               inner join location l on b.location_id=l.id
               left join payment_transaction pt on ws.id=pt.schedule_id 
               inner join customers c on c.id=v.customer_id and c.status in (1,0)
               where u.id=${id}
               ORDER by building_name,ws.start_date DESC;`

        const [rows, fields] = await mysql.query(paymentdetailsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        console.log(result)
        let paymentData: any;
        const allPayments: any[] = [];


        for (const schedule in result) {
            console.log(schedule + "schedule is")
            const payment_amount = await Payments.calculatePaymentAmounts(result[schedule]);

            let pay_amount = {
                'amount_charged': payment_amount
            }
            paymentData = { ...result[schedule], ...pay_amount };

            allPayments.push(paymentData);
        }

        console.log(allPayments);
        return allPayments;










    }

    public static async getbuildingpaymentdetails(data: any) {
        console.log(data);
        var id = data.id;
        var fromdate = data.fromdate
        var todate = data.todate
        console.log(fromdate, todate)

        let buildingpayments = `select distinct
        c.first_name,
        c.last_name,
        c.id as customer_id,
        v.registration_no as vehicle,
        v.parking_no,
        b.name as building_name,
        l.address as location,
        v.id as vehicle_id,
        w.name as cleaner,
        w.id as worker_id,
        ws.start_date,
        pmt.payment_amount as amount_paid,
        pmt.payment_date as paid_date,
        pmt.note as payment_note
        from customers c
        INNER JOIN vehicles v on c.id=v.customer_id
        INNER JOIN work_schedule ws on v.id=ws.vehicle_id 
        INNER JOIN schedule_logs sl on sl.schedule_id=ws.id
        INNER JOIN worker_building wb on ws.worker_building_id = wb.id  
        INNER JOIN buildings b on b.id=wb.building_id
        INNER JOIN location l on l.id=b.location_id
        INNER JOIN payment_transaction pmt on ws.id = pmt.schedule_id 
        and pmt.payment_date between '${fromdate}' AND '${todate}'
        INNER JOIN workers w on wb.worker_id=w.id`;
        if (id != 'all') {
            buildingpayments = buildingpayments + ` and b.id=${id}`;
        }

        const [rows, fields] = await mysql.query(buildingpayments);
        const result = JSON.parse(JSON.stringify(rows));
        console.log(result)
        return result
        // let paymentData: any;
        // const allPayments: any[] = [];


        // for (const schedule in result) {
        //     console.log(schedule+"schedule is")
        //     const payment_amount = await Payments.calculatePaymentAmounts(result[schedule]);

        //     let pay_amount = {
        //         'amount_charged': payment_amount
        //     }
        //     paymentData = { ...result[schedule], ...pay_amount };

        //     allPayments.push(paymentData);
        // }

        // console.log(allPayments);
        // return allPayments;


    }

    public static async getWorkerPaymentReports(data: any) {
        console.log(data);
        var id = data.id;
        var fromdate = data.fromdate
        var todate = data.todate
        console.log(fromdate, todate)
        let cleanerPayments = `select distinct
        c.first_name,
        c.last_name,
        c.id as customer_id,
        v.registration_no as vehicle,
        v.parking_no,
        b.name as building_name,
        l.address as location,
        v.id as vehicle_id,
        w.name as cleaner,
        w.id as worker_id,
        ws.start_date,
        pmt.payment_amount as amount_paid,
        pmt.payment_date as paid_date,
        pmt.note as payment_note
        from customers c
        INNER JOIN vehicles v on c.id=v.customer_id
        INNER JOIN work_schedule ws on v.id=ws.vehicle_id 
        INNER JOIN schedule_logs sl on sl.schedule_id=ws.id
        INNER JOIN worker_building wb on ws.worker_building_id = wb.id  
        INNER JOIN buildings b on b.id=wb.building_id
        INNER JOIN location l on l.id=b.location_id
        INNER JOIN payment_transaction pmt on ws.id = pmt.schedule_id 
        and pmt.payment_date between '${fromdate} 00:00:00' AND '${todate} 23:59:59'
        INNER JOIN workers w on wb.worker_id=w.id`;
        if (id != 'all') {
            cleanerPayments = cleanerPayments + ` and w.id=${id}`;
        }
        console.log(cleanerPayments);
        const [rows, fields] = await mysql.query(cleanerPayments);
        const result = JSON.parse(JSON.stringify(rows));
        console.log(result)
        return result
    }

    public static async getCustomerPaymentsHistory(customerId: number, filter: WashFilter, paginate: Paginate) {
        const originalQuery = `
            FROM payment_transaction payment
            INNER JOIN work_schedule ws ON ws.id = payment.schedule_id 
            INNER JOIN vehicles veh ON veh.id = ws.vehicle_id 
            INNER JOIN customers cust ON cust.id = veh.customer_id
            WHERE cust.id = ${customerId}
            ${filter.car? `AND veh.id >= '${filter.car}'`: ''}
            ${filter.startDate? `AND DATE(payment.payment_date) >= DATE('${filter.startDate}')`: ''}
            ${filter.endDate? `AND DATE(payment.payment_date) <= DATE('${filter.endDate}')`: ''}
            ORDER BY payment.payment_date DESC, payment.status DESC
        `;        
        const pageQuery = `
            SELECT * 
            ${originalQuery}
            LIMIT ${paginate.endIndex}
            OFFSET ${paginate.startIndex};
        `;
        const countQuery = `
            select count(*) as total
            ${originalQuery};
        `;

        const [rows, fields] = await mysql.query(pageQuery);
        const result = JSON.parse(JSON.stringify(rows));
        const [countRows, countFields] = await mysql.query(countQuery);
        const countResult = JSON.parse(JSON.stringify(countRows));
        return {
            ...countResult[0],
            ...paginate,
            data: result
        };    
    }

}