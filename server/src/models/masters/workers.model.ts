import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';

export class Workers {
    public static async getAllWorkers() {
        const getWorkersQuery = `select u.password,b.status as building_status,b.name as building_name,w.*,wb.start_date,wb.building_id,wb.worker_id,wb.end_date,wb.created_By,l.address,wb.id as worker_building_id from workers w 
        inner join worker_building wb on wb.worker_id=w.id
        inner join buildings b on wb.building_id=b.id and b.name not like '%onetimeBuilding%' 
        inner join location l on b.location_id=l.id
        inner join users u on w.user_id=u.id;`;
        // where w.status=1 and b.status=1`;
        const [rows, fields] = await mysql.query(getWorkersQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    // public static async getEditBuildings(){
    //     const getEditBuildingsQuery = `select b.name as building_name,w.*,wb.building_id,wb.worker_id,wb.end_date,wb.id as worker_building_id from workers w
    //     left join worker_building wb on wb.worker_id=w.id
    //     left join buildings b on wb.building_id=b.id
    //     where w.status=1 and wb.end_date is NULL;`;
    //     const [rows, fields] = await mysql.query(getEditBuildingsQuery);
    //     const result = JSON.parse(JSON.stringify(rows));
    //     return result;
    // }
    public static async addNewWorker(data: any) {
        const checkQuery = `select mobile from users where mobile = '${data.workerData.mobile}'`;
        const [rows, fields] = await mysql.query(checkQuery);
        const result = JSON.parse(JSON.stringify(rows));
        let status;
        if (result.length > 0) {
            return status = {
                success: false,
                message: 'Worker Already Exists.'
            };
        } else {
            const addUserQuery = `insert into users(name,mobile,password,role,date_added,added_by) values('${data.workerData.name}','${data.workerData.mobile}','${data.workerData.password}','cleaner','${data.start_date.start_date}','${data.user_name}') `
            const [rows, fields] = await mysql.query(addUserQuery);
            const result = JSON.parse(JSON.stringify(rows));
            if (result) {
                const user_id = result.insertId;
                const addWorkerQuery = ` insert into workers(user_id,name,mobile,status) values(${user_id},'${data.workerData.name}','${data.workerData.mobile}',1)`;
                const [rows1, fields1] = await mysql.query(addWorkerQuery);
                const result1 = JSON.parse(JSON.stringify(rows1));
                if (result1) {
                    const workerId = result1.insertId;
                    data.building_ids.forEach(async (building_id: any) => {
                        const workerBuildingQuery = ` insert into worker_building(worker_id,building_id,start_date,created_By) values('${workerId}','${building_id}',CURDATE(),'${data.user_name}')`;
                        const [rows, fields] = await mysql.query(workerBuildingQuery);
                        const result2 = JSON.parse(JSON.stringify(rows));
                    });
                }
            }
            let status;
            return status = {
                success: true,
                message: 'Worker Added Successfully.'
            };
        }
    }
    public static async editWorker(data: any, id: number) {
        const editUserQuery = ` update users set name='${data.name}',mobile='${data.mobile}',password='${data.password}' where id=${data.user_id}`
        const [rows, fields] = await mysql.query(editUserQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result) {
            const editWorkerQuery = ` update workers set name='${data.name}',mobile='${data.mobile}' where id=${id}`;
            const [rows, fields] = await mysql.query(editWorkerQuery);
            const result = JSON.parse(JSON.stringify(rows));
            const dataPresentQuery = `select * from worker_building where id=${data.worker_building_id} and worker_id=${data.worker_id} and building_id=${data.building_id}`;
            const [presentRows, presentFields] = await mysql.query(dataPresentQuery);
            const presentResult = JSON.parse(JSON.stringify(presentRows));
            console.log(presentResult);
            if (presentResult.length == 1 && data.building_status == false && presentResult[0].end_date == null) {
                const editWorkerBuildingQuery1 = ` update worker_building set end_date='${data.end_date}',deleted_By='${data.user_name}' where id=${data.worker_building_id} `;
                const [editedRows, fields] = await mysql.query(editWorkerBuildingQuery1);
                const editedResult = JSON.parse(JSON.stringify(editedRows));
                if (editedResult != undefined) {
                    let status;
                    return status = { success: true, message: 'Worker Updated Successfully.' };
                }

            }
            // else if (presentResult.length == 1 && data.building_status == true && presentResult[0].end_date != null) {

            //     const editWorkerBuildingQuery1 = ` update worker_building set start_date='${data.start_date}',created_By='${data.user_name}', end_date = NULL ,deleted_By=NULL where id=${data.worker_building_id} `;
            //     const [editedRows, fields] = await mysql.query(editWorkerBuildingQuery1);
            //     const editedResult = JSON.parse(JSON.stringify(editedRows));
            //     if (editedResult != undefined) {
            //         let status;
            //         return status = { success: true, message: 'Worker is Re-Updated Successfully.' };
            //     }

            // } 
            else if ((presentResult.length == 1 && data.building_status == true && presentResult[0].end_date != null) || (presentResult.length == 0 && data.building_status == true)) {
                const insertWorkerBuildingQuery = ` insert into worker_building(worker_id,building_id,start_date,created_By) values('${data.worker_id}','${data.building_id}',CURDATE(),'${data.user_name}')`;
                const [insertRows, fields] = await mysql.query(insertWorkerBuildingQuery);
                const insertedResult = JSON.parse(JSON.stringify(insertRows));
                if (insertedResult != undefined) {
                    let status;
                    return status = { success: true, message: 'Successfully inserted in Worker.' };
                }

            } else {
                let status;
                return status = { success: true, message: 'No update.' };
            }
        }

        // if (data.building_status == true) {
        // 
        // }
        // for (let i = 0; i < data.building_ids.length; i++) {
        //     const building_id = data.building_ids[i];
        //     const editWorkerBuildingQuery1 = ` update worker_building set building_id='${building_id}',end_date=STR_TO_DATE('${data.end_date}', '%d/%m/%Y'),deleted_By='${data.user_name}' where id=${data.worker_building_id} `;
        //     const [rows, fields] = await mysql.query(editWorkerBuildingQuery1);
        //     const result1 = JSON.parse(JSON.stringify(rows));
        // }
        // 
        // (async (building_id: any) => {
        //     const editWorkerBuildingQuery = ` update worker_building set building_id='${building_id}',end_date='${data.end_date}',deleted_By='${data.user_name}' where worker_id=${id} `;
        //     const [rows, fields] = await mysql.query(editWorkerBuildingQuery);
        //     const result1 = JSON.parse(JSON.stringify(rows));
        // });
        // 
        // let status;
        // return status = {
        //     success: true,
        //     message: 'Worker Updated Successfully.'
        // };
    }


    public static async deleteWorker(id: any, user_name: any) {
        const checkBuildingQuery = `select * from worker_building wb
        inner join workers w on w.id=wb.worker_id
        inner join buildings b on b.id=wb.building_id and b.name not like '%onetimeBuilding%' 
        where wb.end_date is null and w.id=${id.id}`;
        const [rows, fields] = await mysql.query(checkBuildingQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result.length == 0) {
            const checkScheduleQuery = ` select * from work_schedule ws
            inner join worker_building wb on wb.id= ws.worker_building_id
            inner join vehicles v on v.id=ws.vehicle_id
            inner join customers c on c.id=v.customer_id and c.status=1
            where ws.end_date is null and wb.worker_id=${id.id}`;
            const [rows, fields] = await mysql.query(checkScheduleQuery);
            const result1 = JSON.parse(JSON.stringify(rows));
            if (result1.length == 0) {
                const deleteWorkerQuery = ` update workers set status=0,deletedAt=CURDATE(),deletedBY='${user_name}' where id=${id.id}`;
                const [rows, fields] = await mysql.query(deleteWorkerQuery);
                const result = JSON.parse(JSON.stringify(rows));
                let status;
                return status = {
                    success: true,
                    message: 'Worker deleted successfully.'
                };
            } else {
                let status;
                return status = {
                    warning: false,
                    message: 'worker cannot be deleted as there is a schedule assigned to him.'
                }
            }
        } else {
            let status;
            return status = {
                warning: false,
                message: 'worker cannot be deleted as there is a building assigned to him.'
            }
        }

    }
    public static async activateWorker(id: any) {


        const activateWorkerQuery = `update workers set status=1 where id=${id}`;
        console.log(activateWorkerQuery);
        const [rows, fields] = await mysql.query(activateWorkerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result) {
            const activateWorkerDatesQuery = `update worker_building set start_date=CURDATE(),end_date=null where worker_id=${id}`;
            console.log(activateWorkerQuery);
            const [rows, fields] = await mysql.query(activateWorkerDatesQuery);
            const result = JSON.parse(JSON.stringify(rows));
        }

        let status;
        return status = {
            success: true,
            message: 'Worker activated successfully.'
        }
    }




    public static async updateWorkStatus(data: any) {
        console.log(data)
        let date = moment(data.date).format('YYYY-MM-DD')
        const updateStatusQuery = `update schedule_logs set status='completed' where date='${date}' and schedule_id=${data.schedule_id}`;
        console.log(updateStatusQuery);
        const [rows, fields] = await mysql.query(updateStatusQuery);

        const result = JSON.parse(JSON.stringify(rows));
        let status;
        return status = {
            success: true,
            message: 'Status Updated successfully.'
        };
    }

    public static async ExtendScheduleDates(data: any, startDay: any) {
        let month = moment(startDay).get('month') + 1;
        let year = moment(startDay).get('year');
        const checkScheduleLogsQuery = `select * from schedule_logs where MONTH(date)=${month} and YEAR(date)=${year} and schedule_id=${data.schedule_id};`;
        const [rows, fields] = await mysql.query(checkScheduleLogsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result.length == 0) {
            const schedule_id = data.schedule_id;
            let newDate = new Date(moment(startDay).get('year'), moment(startDay).get('month'), moment(startDay).get('date'));
            let startDate = new Date(moment(startDay).get('year'), moment(startDay).get('month'), moment(startDay).get('date'));
            let presentDateFormated = moment(startDate.toISOString()).format("YYYY-MM-DD")
            let lastDay = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            // let lastDay = new Date(startDate.setFullYear(startDate.getFullYear() + 1));
            let lastDateFormated = moment(lastDay.toISOString()).format("YYYY-MM-DD")
            let i: number;
            let curDay = newDate.getDay();
            for (i = 0; i < data.schedules.length; i++) {
                if (data.schedules[i] == 1) {
                    newDate = new Date(moment(startDay).get('year'), moment(startDay).get('month'), moment(startDay).get('date'));
                    newDate.setDate(newDate.getDate() + i - curDay);
                    let newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")

                    while (new Date(newDateFormat) <= new Date(lastDateFormated)) {

                        if (new Date(newDateFormat) >= new Date(presentDateFormated)) {
                            let curDate = newDate.toLocaleDateString();
                            try {
                                const addScheduleLogsQuery = `insert into schedule_logs (date,schedule_id,status) value (STR_TO_DATE('${curDate}', '%m/%d/%Y'),${schedule_id},'pending');`;
                                const [rows, fields] = await mysql.query(addScheduleLogsQuery);
                                const result = JSON.parse(JSON.stringify(rows));
                            } catch (err) {
                                return err;
                            }
                        }
                        newDate.setDate(newDate.getDate() + 7);
                        newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")
                    }
                }
            }
            console.log('Schedule dates are extended.')
            return { status: true, message: 'Schedule dates are extended.' }
        } else {
            console.log('Schedule dates extended already.')
            return { status: true, message: 'Schedule dates extended already.' }
        }


    }
    public static async getWashedCarsReport(data: any) {
    
        let washRecordsQuery = `select sl.date as work_date,c.first_name,
        c.last_name,
        c.id as customer_id,
        v.registration_no as vehicle,
        v.parking_no,
        b.name as building_name,
        l.address as location,
        v.id as vehicle_id,
        w.name as cleaner,
        w.id as worker_id,
        ws.start_date from schedule_logs sl
        inner join work_schedule ws on ws.id = sl.schedule_id
        inner join vehicles v on v.id = ws.vehicle_id
        inner join customers c on c.id = v.customer_id
        inner join worker_building wb on wb.id=ws.worker_building_id
        inner join workers w on w.id=wb.worker_id
        inner join buildings b on b.id=wb.building_id
        inner join location l on l.id=b.location_id
        where sl.status='completed' and sl.date between '${data.fromDate}' AND '${data.toDate}'`;
        if (data.id != 'all') {
            washRecordsQuery = washRecordsQuery + ` and w.id=${data.id}`;
        }
        const [rows, fields] = await mysql.query(washRecordsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }
}