
import mysql from '@app/config/db';
import moment from 'moment';
import { WashFilter } from '@common/wash.filter';
import { Paginate } from '@common/paginate.interface';
import { STATUS } from './status.model';
import { Users } from './user.model';
import { EmailService } from '@common/email.service';
export class Customers {
    static readonly ROLE_CUSTOMER = "customer";
    static readonly OTP_EXPIRY_DURATION = 5; // 5 min
    public static async addCustomer(data: any) {
        const mobile = data.custDetails.mobile;
        const firstName = !data.custDetails.firstName || data.custDetails.firstName == "" ? "Customer-"+mobile.slice(mobile.length-4) : data.custDetails.firstName ;
        const lastName = data.custDetails.lastName? data.custDetails.lastName: "";
        const addCustQuery = 
            `insert into customers (first_name,last_name,building_id,mobile, email, flat_no,status,created_at,created_by) 
            values ('${firstName}','${lastName}',${data.custDetails.building_id},'${mobile}','${data.custDetails.email}','${data.custDetails.flat_no}',1,CURDATE(),'${data.user_name}');`;
        const addUserQuery = 
            `insert into users (name, mobile, email, password, role, added_by)
            values('${firstName}', '${mobile}','${data.custDetails.email}', '${data.custDetails.password}', '${Customers.ROLE_CUSTOMER}', '${data.user_name}')`;
        const getUpdateUserQuery = (customerId: any, userId: any) => {
            return `
                UPDATE customers 
                SET user_id = ${userId}
                WHERE id = ${customerId} 
            `;
        }
        const transaction = await mysql.getConnection();
        try{
            await transaction.beginTransaction();
            const [rows, fields] = await transaction.query(addCustQuery);
            const result = JSON.parse(JSON.stringify(rows));
            const [userrows, userfields] = await transaction.query(addUserQuery);
            const userResult = JSON.parse(JSON.stringify(userrows));
            await transaction.query(getUpdateUserQuery(result.insertId, userResult.insertId));
            await transaction.commit();
            return result;
        } catch(err) {
            await transaction.rollback();
            throw err;
        } finally {
            await transaction.release();
        }
    }

    public static async customerAppUserRegistration(data: any) {
        console.log("data",data);
        const phone = data.phone;
        const name = !data.name || data.name == "" ? "Customer-"+phone.slice(phone.length-4) : data.name ;
        const addMobAppCustQuery = `insert into customer_enquiry (plan,name,address,email,premises,comments,phone,status,type,created_at,created_by,user_id) 
        values ('${data.plan}','${data.name}','${data.address}','${data.email}','${data.premises}','${data.comments}','${data.phone}','${STATUS.OPEN}',${data.enquiryType? `'${data.enquiryType}'`: "NULL"},CURDATE(),'${data.name}', ${data.user_id? `'${data.user_id}'`: "NULL"});`;
        console.log("query",addMobAppCustQuery);
        const [rows, fields] = await mysql.query(addMobAppCustQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }

    public static async generateOtp(data: any) {
        let status;
        if (data.user) {
            const otp = Math.floor(100000 + Math.random() * 900000).toString();
            console.log("otp", otp);
            const expiryTime = moment().subtract(Customers.OTP_EXPIRY_DURATION,'minute').format('YYYY-MM-DD HH:mm:ss');
            const checkOtpQuery = `
                SELECT otp FROM otp
                WHERE user_id=${data.user.id}
                AND created_at >= '${expiryTime}'`;
            console.log("query", checkOtpQuery);
            const [rows, fields] = await mysql.query(checkOtpQuery);
            const result = JSON.parse(JSON.stringify(rows));
            if (result.length > 0) {
                const updateOtp = `
                    UPDATE otp 
                    SET otp= '${otp}', 
                        reqform = '${data.reqform}'
                    WHERE  user_id='${data.user.id}'`;
                const [rows, fields] = await mysql.query(updateOtp);
                const result = JSON.parse(JSON.stringify(rows));
                if (result) {
                    console.log("🚀 ~ file: customers.model.ts ~ line 74 ~ Customers ~ generateOtp ~ data.user.emai", data.user.emai)
                    EmailService.sendOTPMail(data.user.email, data.user.first_name, otp);
                    status = {
                        status: true,
                        email: data.user.email,
                        otp: otp,
                        message: 'updated - OTP sent to Registered Email.'
                    };
                } else {
                    status = {
                        status: false,
                        message: 'Something went wrong.'
                    };
                }
            } else {

                const addOtp = `INSERT into otp (user_id, mobile,otp,reqform) 
                                VALUES ('${data.user.id}','${data.phone}','${otp}','${data.reqform}')`;
                const [rows, fields] = await mysql.query(addOtp);
                const result = JSON.parse(JSON.stringify(rows));
                if (result) {
                    status = {
                        status: true,
                        otp: otp,
                        message: 'OTP sent to Registered Mobile Number.'
                    };
                } else {
                    status = {
                        status: false,
                        message: 'Something went wrong.'
                    };
                }
            }

        } else {
            status = {
                status: false,
                message: 'Mobile Number not Registered.'
            };
        }

        return status;
        // return result;
    }

    public static async verifyOtp(data: any) {
        let status;
        console.log("data",data);
        const fiveMinutesPast = moment().subtract(5, "minutes").format("YYYY-MM-DD HH:mm:ss");
        const checkOtpQuery = `
            SELECT otp FROM otp 
            WHERE mobile="${data.phone}" 
            AND otp="${data.otp}"  
            AND reqform="${data.reqform}" 
            AND created_at >= '${fiveMinutesPast}'`;
        console.log("query",checkOtpQuery);
        const [rows, fields] = await mysql.query(checkOtpQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if(result.length>0){
            const delotp = `delete from otp where mobile="${data.phone}" and otp="${data.otp}"  and reqform="${data.reqform}" `;
            const [rows, fields] = await mysql.query(delotp);
            const result = JSON.parse(JSON.stringify(rows));
            if(result){
                status = {
                    status: true,
                    message: 'Correct OTP.'
                };
            }else{
                status = {
                    status: false,
                    message: 'Something went wrong.'
                };
            }
        }else{
            status = {
                status: false,
                message: 'Please enter correct OTP.'
            };
        }
        
        return status ;
        // return result;
    }

    public static async updateOtp(data: any) {
        let status;
        console.log("data",data);
            const updatepwd = `update users set password = "${data.password}"  where mobile="${data.phone}" `;
            const [rows, fields] = await mysql.query(updatepwd);
            const result = JSON.parse(JSON.stringify(rows));
            if(result){
                status = {
                    status: true,
                    message: 'Password updated successfully'
                };
            }else{
                status = {
                    status: false,
                    message: 'Something went wrong.'
                };
            }
        
        
        return status ;
        // return result;
    }

    public static async oneTimeWash(data: any) {
        let userquery = `select user_id from user_token where token="${data.token}"`;
        const [userRows, usersFields] = await mysql.query(userquery);
        const userResult = JSON.parse(JSON.stringify(userRows));
        if (userResult.length > 0) {
            const addOneTimeWashCustomer = `insert into customers (first_name,last_name,building_id,mobile,status,created_at,created_by) values ('One','Time','2','9999999999',2,CURDATE(),'${data.user_name}')`;
            const [rows, fields] = await mysql.query(addOneTimeWashCustomer);
            const result = JSON.parse(JSON.stringify(rows));
            if (result) {
                const cust_id = result.insertId;
                const addOneTimeWashVehicle = `insert into vehicles (registration_no,customer_id,parking_no,building_id) values ('${data.vehicle_no}','${cust_id}','${data.parking_no}','2')`;
                const [rows, fields] = await mysql.query(addOneTimeWashVehicle);
                const result1 = JSON.parse(JSON.stringify(rows));
                if (result1) {
                    const getWorkerIdQuery = `select id from workers where user_id=${userResult[0].user_id}`;
                    const [workerRows, workerFields] = await mysql.query(getWorkerIdQuery);
                    const workerResult = JSON.parse(JSON.stringify(workerRows));
                    if (workerResult.length > 0) {
                        const insertWorkerBuildingQuery = `insert into worker_building (worker_id,building_id,created_By,start_date) values ('${workerResult[0].id}',2,'${data.user_name}',CURDATE())`;
                        const [rows1, fields1] = await mysql.query(insertWorkerBuildingQuery);
                        const result2 = JSON.parse(JSON.stringify(rows1));
                        const vehicle_id = result1.insertId;
                        const worker_building_id = result2.insertId;
                        let curDate = new Date()
                        let washDay = curDate.getDay();
                        let sunday = 0;
                        let monday = 0;
                        let tuesday = 0;
                        let wednesday = 0;
                        let thursday = 0;
                        let friday = 0;
                        let saturday = 0;
                        if (washDay == 0) {
                            sunday = 1;
                        } else if (washDay == 1) {
                            monday = 1;
                        } else if (washDay == 2) {
                            tuesday = 1;
                        } else if (washDay == 3) {
                            wednesday = 1;
                        } else if (washDay == 4) {
                            thursday = 1;
                        } else if (washDay == 5) {
                            friday = 1;
                        } else if (washDay == 6) {
                            saturday = 1;
                        }
                        const insertWorkScheduleQuery = `insert into work_schedule (worker_building_id,vehicle_id,sunday,monday,tuesday,wednesday,thursday,friday,saturday,date_created,created_by,start_date,subscription_amount) values (${worker_building_id},${vehicle_id},${sunday},${monday},${tuesday},${wednesday},${thursday},${friday},${saturday},CURDATE(),'${data.user_name}',CURDATE(),${data.amount})`;
                        const [rows2, fields2] = await mysql.query(insertWorkScheduleQuery);
                        const result3 = JSON.parse(JSON.stringify(rows2));
                        const schedule_id = result3.insertId;
                        if (result3) {
                            const insertSchedulLogQuery = `insert into schedule_logs(date,schedule_id,date_created,created_by) values(CURDATE(),${schedule_id},CURDATE(),'${data.user_name}')`;
                            const [rows3, fields3] = await mysql.query(insertSchedulLogQuery);
                            const result4 = JSON.parse(JSON.stringify(rows3));
                            if (result4) {
                                const insertPaymentTransactionQuery = `insert into payment_transaction (schedule_id,payment_amount,payment_registered_by,user_id,added_by,status,note) values (${schedule_id},${data.amount},"${data.user_name}",${userResult[0].user_id},"${data.user_name}","completed","")`;
                                const [rows4, fields4] = await mysql.query(insertPaymentTransactionQuery);
                                const result5 = JSON.parse(JSON.stringify(rows4));
                                return data.status = true;
                            }
                        }
                    }
                }
            }

        }
    }


    public static async editCustomer(id: number, data: any) {
        const mobile = data.mobile;
        const firstName = !data.firstName || data.firstName == "" ? "Customer-"+mobile.slice(mobile.length-4) : data.firstName ;
        const lastName = data.lastName? data.lastName: "";
        const user = await Users.getUser(data.mobile);
        const customer = await Customers.checkCustomer(data);
        let userQuery = '';
        let isUserExists = false;
        let userIdToLink = 0;
        if(customer.length > 0 && customer[0].user_id) {
            isUserExists = true;
            userQuery = `
                UPDATE  users 
                SET name = '${firstName}', 
                    mobile = '${mobile}', 
                    email = '${data.email}',
                    password = '${data.password}'
                WHERE id = ${customer[0].user_id}`;
            userIdToLink = customer[0].user_id;
        } else if(user.length > 0){
            isUserExists = true;
            userQuery = `
                UPDATE  users 
                SET name = '${firstName}', 
                    mobile = '${mobile}', 
                    password = '${data.password}'
                WHERE id = ${user[0].id}`;
                userIdToLink = user[0].id;
        } else {
            userQuery = `insert into users (name, mobile,email, password, role, added_by)
            values('${firstName}', '${mobile}', '${data.email}', '${data.password}', '${Customers.ROLE_CUSTOMER}', 'admin')`;
        }

        const editCustQuery = `
            UPDATE customers 
            SET first_name='${firstName}', 
                last_name='${lastName}', 
                building_id=${data.building_id}, 
                mobile='${data.mobile}', 
                flat_no='${data.flat_no}',
                email='${data.email}'
            WHERE id=${id};`;
        const getUpdateUserQuery = (customerId: any, userId: any) => {
            return `
                UPDATE customers 
                SET user_id = ${userId}
                WHERE id = ${customerId} 
            `;
        }
        const transaction = await mysql.getConnection();
        try{
            await transaction.beginTransaction();
            const [rows, fields] = await transaction.query(userQuery);
            const result = JSON.parse(JSON.stringify(rows));
            await transaction.query(editCustQuery);
            if(!isUserExists) {
                await transaction.query(getUpdateUserQuery(id, result.insertId));
            } else {
                await transaction.query(getUpdateUserQuery(id, userIdToLink));
            }
            await transaction.commit();
            return result;
        } catch(err) {
            await transaction.rollback();
            throw err;
        } finally {
            await transaction.release();
        }
    }

    public static async checkCustomerWithId(customerId: number) {
        const ValidateCustQuery = `
            select * from customers 
            where id='${customerId}';
        `;
        console.log("query",ValidateCustQuery);
        const [rows, fields] = await mysql.query(ValidateCustQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async checkCustomer(data: any) {
        const ValidateCustQuery = `
            select * from customers 
            where mobile='${data.mobile}' 
            OR email='${data.email}';`;
        const [rows, fields] = await mysql.query(ValidateCustQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async checkVehicle(data: any) {
        const ValidateCustQuery = `select * from from vehicles where mobile='${data.custDetails.mobile}';`;
        const [rows, fields] = await mysql.query(ValidateCustQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }


    public static async getCustomers(customerId = null) {
        const getCustomerQuery = `
        SELECT l.*,c.*,c.id as cust_id,c.status as cust_status,b.*,v.*,v.id as veh_id,w_s.*,w.id as worker_id, w.name as worker_name,w_s.id as work_schedule_id, 
            u.id as user_id, u.email as user_email, u.mobile as user_mobile, u.password as password 
        from customers c 
        left join buildings b on b.id = c.building_id 
        left join location l on l.id = b.location_id
        left join vehicles v on v.customer_id = c.id
        left join work_schedule w_s on w_s.vehicle_id = v.id
        left join worker_building w_b on w_s.worker_building_id = w_b.id
        left join workers w on w_b.worker_id = w.id 
        left join users u on c.user_id = u.id
        ${customerId? `WHERE c.id=${customerId}`: ''} order by c.created_at
        `;
        // where c.status=1 order by c.status desc;`;
        const [rows, fields] = await mysql.query(getCustomerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }

    public static async getCustomerDetails(customerId = null) {
        // first_name,last_name,email,mobile,v.registration_no,s.vehicle_id,
        const getCustomerQuery = `
        SELECT *
        FROM customers c
        LEFT JOIN vehicles v ON c.id = v.customer_id
        LEFT OUTER JOIN users u ON c.user_id = u.id 
        LEFT OUTER JOIN work_schedule sch ON sch.vehicle_id = v.id 
        ${customerId? `WHERE c.id=${customerId}`: ''}
        `;
        const [rows, fields] = await mysql.query(getCustomerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }

    public static async getCustomerCars(customerId = null) {
        // first_name,last_name,email,mobile,v.registration_no,s.vehicle_id,
        const getCustomerQuery = `
        SELECT distinct v.*
        FROM customers c 
        LEFT JOIN vehicles v ON c.id = v.customer_id
        LEFT OUTER JOIN work_schedule sch ON sch.vehicle_id = v.id 
        ${customerId? `WHERE c.id=${customerId}`: ''}
        `;
        const [rows, fields] = await mysql.query(getCustomerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }


    public static async getEnqCustomers(type: string) {
        const whereCondition = type ? `type = '${type}'`: "";
        let newAccountEnq = ``;
        if(type && type =="NEW_ACCOUNT") {
            newAccountEnq = `
                AND cust.user_id IS null
                OR (cust.user_id IS NOT null AND enq.is_read = false)
            `;
        }
        const getEnqCustomerQuery = `
            SELECT *, cust.id as doesExist, enq.email as enq_email 
            FROM  customer_enquiry enq
            LEFT OUTER JOIN customers cust ON (enq.phone = cust.mobile)
            where ${whereCondition}
            ${newAccountEnq} 
            order by enq.created_at
        `;
        console.log("🚀 ~ file: customers.model.ts ~ line 387 ~ Customers ~ getEnqCustomers ~ getEnqCustomerQuery", getEnqCustomerQuery);
        const [rows, fields] = await mysql.query(getEnqCustomerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    
    public static async setAllEnquiriesAsRead(type: string) {
        const whereCondition = type ? `WHERE type = '${type}'`: "";
        const setAllEnquiriesAsReadQuery = `
            UPDATE customer_enquiry
            SET is_read = true,
                read_at = CURRENT_TIMESTAMP
            ${whereCondition}
        `;
        console.log("🚀 ~ file: customers.model.ts ~ line 337 ~ Customers ~ setAllEnquiriesAsRead ~ setAllEnquiriesAsReadQuery", setAllEnquiriesAsReadQuery);       
        const [rows, fields] = await mysql.query(setAllEnquiriesAsReadQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }

    public static async getCustomerDetailsbyVeh(data: any) {
        console.log("data",data);
        console.log("data id",data.vehicle_id);
        const getCustomerDetailsbyVehQuery = `select c.*,c.id as cust_id,c.status as cust_status,v.*,v.id as veh_id from customers c 
        
        left join vehicles v on v.customer_id = c.id where v.registration_no = "${data.vehicle_id}"`;
        console.log("quwey",getCustomerDetailsbyVehQuery);
        // left join buildings b on b.id = c.building_id 
        // left join location l on l.id = b.location_id
        // left join work_schedule w_s on w_s.vehicle_id = v.id
        // left join worker_building w_b on w_s.worker_building_id = w_b.id
        // left join workers w on w_b.worker_id = w.id 
         
        // where c.status=1 order by c.status desc;`;
        const [rows, fields] = await mysql.query(getCustomerDetailsbyVehQuery);
        const result = JSON.parse(JSON.stringify(rows));
        console.log("result",result);
        return result;
    }


    public static async getWorkDetails(data: any) {
        let date = moment(data.startDate).format('YYYY-MM-DD')
        let today = moment().format('YYYY-MM-DD')
        const getCustomerQuery = `SELECT * FROM carwash_db.schedule_logs s_l 
        left join work_schedule w_s on w_s.id=s_l.schedule_id where w_s.vehicle_id=${data.vehicle_id} and s_l.date >= '${date}' and s_l.date <= '${today}' ORDER BY s_l.date DESC, s_l.status DESC;`;
        const [rows, fields] = await mysql.query(getCustomerQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async deleteCustomer(id: any, user_name: any) {
        const getNotEndedSchedulesQuery = `select v.id as vehicle_id,ws.end_date,c.* from customers c
        left join vehicles v on v.customer_id=c.id
        left join work_schedule ws on ws.vehicle_id=v.id
        where c.id=${id} and ws.end_date is null;`;
        const [rows, fields] = await mysql.query(getNotEndedSchedulesQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result.length > 0) {
            for (let vehicleData of result) {
                const vehicle_id = vehicleData.vehicle_id;
                const updateVehicleSchedule = `update work_schedule set end_date=CURDATE() where vehicle_id=${vehicle_id}`;
                const [rows1, fields1] = await mysql.query(updateVehicleSchedule);
                const result1 = JSON.parse(JSON.stringify(rows1));
            }
        }
        const deleteCustomerQuery = `update customers set status=0,updated_by='${user_name}',updated_at=CURDATE() where id=${id}`;
        const [rows2, fields2] = await mysql.query(deleteCustomerQuery);
        const result2 = JSON.parse(JSON.stringify(rows2));
        let status;
        return status = {
            success: true,
            message: 'Customer deleted successfully.'
        };
    }
    public static async activateCustomer(id: any, user_name: any, activateDate: any) {
        const getPreviousDatesQuery = `select v.id as vehicle_id,v.registration_no as vehilce_no,c.* from vehicles v
        left join customers c on v.customer_id=c.id
        where c.id=${id} `;
        const [rows, fields] = await mysql.query(getPreviousDatesQuery);
        const result = JSON.parse(JSON.stringify(rows));
        if (result.length > 0) {
            const start_date = moment(result[0].created_at).format("YYYY-MM-DD HH:mm:ss");
            const end_date = moment(result[0].updated_at).format("YYYY-MM-DD HH:mm:ss");
            const cust_id = result[0].id;
            const insertCustHistoryQuery = `insert into customers_history(start_date,end_date,customer_id) values('${start_date}','${end_date}',${cust_id})`;
            const [rows1, fields1] = await mysql.query(insertCustHistoryQuery);
            const result1 = JSON.parse(JSON.stringify(rows1));

            if (result1) {
                const activateCustomerQuery = `update customers set status=1,created_at=CURDATE(),created_by='${user_name}' where id=${id}`;
                const [rows2, fields2] = await mysql.query(activateCustomerQuery);
                const result2 = JSON.parse(JSON.stringify(rows2));
                if (result2) {
                    for (let vehicleData of result) {
                        const vehicle_id = vehicleData.vehicle_id;
                        const scheduleIdQuery = `SELECT max(ws.id) as schedule_id from work_schedule ws 
                        INNER JOIN vehicles v on v.id=ws.vehicle_id
                        where v.id=${vehicle_id}`;
                        const [rows3, fields3] = await mysql.query(scheduleIdQuery);
                        const result3 = JSON.parse(JSON.stringify(rows3));
                        let scheduleId = result3[0].schedule_id;
                        const updateVehicleSchedule = `update work_schedule set end_date=null,start_date='${activateDate}' where vehicle_id=${vehicle_id} and id=${scheduleId}`;
                        const [rows1, fields1] = await mysql.query(updateVehicleSchedule);
                        const result1 = JSON.parse(JSON.stringify(rows1));
                    }
                }
                let status;
                return status = {
                    success: true,
                    message: 'Customer activated successfully.'
                }
            }
        }
    }
    public static async selfCustomerRegistration(data: any) {

        const addSelfCustomer = `insert into customers (first_name,last_name,building_id,mobile,flat_no,status,created_at,created_by) values ('${data.custDetails.firstName}','${data.custDetails.lastName}',1,'${data.custDetails.mobile}','${data.custDetails.flat_no}',1,CURDATE(),'${data.user_name}');`;
        const [rows, fields] = await mysql.query(addSelfCustomer);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async selfCustomerSchedule(cust: any, vehicleSchedule: any, customer_id: number, data: any) {

        let vehicles = vehicleSchedule;

        for (let i in vehicles) {
            let vehicle = vehicles[i];
            let schedule = vehicle.schedule;
            let sundayEle = schedule.pop();
            schedule.unshift(sundayEle);

            const checkVehicleQuery = `select * from vehicles v
                      inner join work_schedule ws on ws.vehicle_id=v.id
                      where registration_no='${vehicle.number}' and ws.end_date is null`;
            const [rows4, fields4] = await mysql.query(checkVehicleQuery);
            const result4 = JSON.parse(JSON.stringify(rows4));

            if (result4.length == 0 && !vehicle.hasOwnProperty('vehicle_id')) {

                const addvehicleQuery = `insert into vehicles (registration_no,customer_id,parking_no,building_id) value ('${vehicle.number}','${customer_id}','${vehicle.parking}',1);`;
                const [rows, fields] = await mysql.query(addvehicleQuery);
                const result = JSON.parse(JSON.stringify(rows));

                const vehicle_id = result.insertId;


                // const getWorker_id = `select worker_id from worker_building where building_id=${cust.building_id}`;
                // const [rows1, fields1] = await mysql.query(getWorker_id);
                // const worker_id_details = JSON.parse(JSON.stringify(rows1));
                // const worker_id = worker_id_details[0].worker_id;
                let presentDateFormated = moment(vehicle.startDate).format("DD-MM-YY")
                console.log('schedule startdate', presentDateFormated);

                // const checkScheduleQuery = `select * from work_schedule where vehicle_id in (select id from vehicles where registration_no='${vehicle.number}') and end_date is null `;
                // const [rows3, fields3] = await mysql.query(checkScheduleQuery);
                // const result3 = JSON.parse(JSON.stringify(rows3));

                // if (result3.length == 0) {
                const addScheduleQuery = `insert into work_schedule (vehicle_id,sunday,monday,tuesday,wednesday,thursday,friday,saturday,created_by,start_date,subscription_amount) value (${vehicle_id},${schedule[0]},${schedule[1]},${schedule[2]},${schedule[3]},${schedule[4]},${schedule[5]},${schedule[6]},'${data.user_name}','${vehicle.startDate}',${vehicle.subscription_amount});`;
                const [rows2, fields2] = await mysql.query(addScheduleQuery);
                const result1 = JSON.parse(JSON.stringify(rows2));

                if (result1) {
                    const schedule_id = result1.insertId;
                    let newDate = new Date();
                    let presentDate = new Date();
                    let presentDateFormated = moment(vehicle.startDate).format("YYYY-MM-DD")
                    //let lastDay = new Date(vehicle.startDate.getFullYear(), vehicle.startDate.getMonth() + 1, 0);
                    //let lastDay = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
                    //let lastDateFormated = moment(lastDay.toISOString()).format("YYYY-MM-DD")
                    let lastDateFormated = moment(vehicle.startDate).endOf('month').format("YYYY-MM-DD");
                    let i: number;
                    // let newDate;
                    let curDay = new Date(vehicle.startDate).getDay();
                    for (i = 0; i < schedule.length; i++) {
                        if (schedule[i] == 1) {
                            // newDate = new Date();
                            newDate = new Date(vehicle.startDate);
                            newDate.setDate(newDate.getDate() + i - curDay);
                            let newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")

                            while (new Date(newDateFormat) <= new Date(lastDateFormated)) {

                                if (new Date(newDateFormat) >= new Date(presentDateFormated)) {
                                    let curDate = newDate.toLocaleDateString();
                                    const addScheduleLogsQuery = `insert into schedule_logs (date,schedule_id,status,created_by,updated_by) value (STR_TO_DATE('${curDate}', '%m/%d/%Y'),${schedule_id},'pending','${data.created_by}','${data.updated_by}');`;
                                    const [rows, fields] = await mysql.query(addScheduleLogsQuery);
                                    const result = JSON.parse(JSON.stringify(rows));
                                }
                                newDate.setDate(newDate.getDate() + 7);
                                newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")
                            }
                        }
                    }

                }
            }

        }
        console.log('schedule and schedule logs are entered')
        const status = { success: true, message: 'Data is in Table' };
        return { status };
    }
    public static async getCustomerWashHistory(customerId: number, filter: WashFilter, paginate: Paginate) {
        const originalQuery = `
            FROM schedule_logs sl
            LEFT JOIN work_schedule ws ON ws.id=sl.schedule_id 
            JOIN vehicles veh ON veh.id = ws.vehicle_id
            JOIN customers cust ON cust.id = veh.customer_id
            WHERE cust.id = ${customerId}
            ${filter.car? `AND veh.id = '${filter.car}'`: ''}
            ${filter.startDate? `AND DATE(sl.date) >= '${filter.startDate}'`: ''}
            AND DATE(sl.date) <= ${filter.endDate? `'${filter.endDate}'`: ' now()'}
            ORDER BY sl.date DESC, sl.status DESC
        `;

        const pageQuery = `
            SELECT * 
            ${originalQuery}
            LIMIT ${paginate.endIndex}
            OFFSET ${paginate.startIndex};
        `;
        const countQuery = `
            select count(*) as total
            ${originalQuery}            
        `;
        const [rows, fields] = await mysql.query(pageQuery);
        const result = JSON.parse(JSON.stringify(rows));
        const [countRows, countFields] = await mysql.query(countQuery);
        const countResult = JSON.parse(JSON.stringify(countRows));
        return {
            ...countResult[0],
            ...paginate,
            data: result
        };
    }



}




