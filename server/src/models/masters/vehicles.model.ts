import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';
// import { try } from 'bluebird';
import { realpathSync } from 'fs';

export class Vehicles {
    public static async addVehicle(cust: any, vehicleSchedule: any, customer_id: number, data: any) {
        let vehicles = vehicleSchedule;

        for (let i in vehicles) {
            let vehicle = vehicles[i];
            let schedule = vehicle.schedule;
            let sundayEle = schedule.pop();
            schedule.unshift(sundayEle);

            const checkVehicleQuery = `select * from vehicles v
                  inner join work_schedule ws on ws.vehicle_id=v.id
                  where registration_no='${vehicle.number}' and ws.end_date is null`;
            const [rows4, fields4] = await mysql.query(checkVehicleQuery);
            const result4 = JSON.parse(JSON.stringify(rows4));

            if (result4.length == 0 && !vehicle.hasOwnProperty('vehicle_id')) {

                const addvehicleQuery = `insert into vehicles (registration_no,customer_id,parking_no,building_id) value ('${vehicle.number}','${customer_id}','${vehicle.parking}','${cust.building_id}');`;
                const [rows, fields] = await mysql.query(addvehicleQuery);
                const result = JSON.parse(JSON.stringify(rows));

                const vehicle_id = result.insertId;


                // const getWorker_id = `select worker_id from worker_building where building_id=${cust.building_id}`;
                // const [rows1, fields1] = await mysql.query(getWorker_id);
                // const worker_id_details = JSON.parse(JSON.stringify(rows1));
                // const worker_id = worker_id_details[0].worker_id;
                let presentDateFormated = moment(vehicle.startDate).format("DD-MM-YY")
                console.log('schedule startdate', presentDateFormated);

                // const checkScheduleQuery = `select * from work_schedule where vehicle_id in (select id from vehicles where registration_no='${vehicle.number}') and end_date is null `;
                // const [rows3, fields3] = await mysql.query(checkScheduleQuery);
                // const result3 = JSON.parse(JSON.stringify(rows3));

                // if (result3.length == 0) {
                const addScheduleQuery = `insert into work_schedule (worker_building_id,vehicle_id,sunday,monday,tuesday,wednesday,thursday,friday,saturday,created_by,start_date,subscription_amount) value (${vehicle.worker_building_id},${vehicle_id},${schedule[0]},${schedule[1]},${schedule[2]},${schedule[3]},${schedule[4]},${schedule[5]},${schedule[6]},'${data.user_name}','${vehicle.startDate}',${vehicle.subscription_amount});`;
                const [rows2, fields2] = await mysql.query(addScheduleQuery);
                const result1 = JSON.parse(JSON.stringify(rows2));

                if (result1) {
                    const schedule_id = result1.insertId;
                    let newDate = new Date();
                    let presentDate = new Date();
                    let presentDateFormated = moment(vehicle.startDate).format("YYYY-MM-DD")
                    //let lastDay = new Date(vehicle.startDate.getFullYear(), vehicle.startDate.getMonth() + 1, 0);
                    //let lastDay = new Date(new Date().setFullYear(new Date().getFullYear() + 1));
                    //let lastDateFormated = moment(lastDay.toISOString()).format("YYYY-MM-DD")
                    let lastDateFormated = moment(vehicle.startDate).endOf('month').format("YYYY-MM-DD");
                    let i: number;
                    // let newDate;
                    let curDay = new Date(vehicle.startDate).getDay();
                    for (i = 0; i < schedule.length; i++) {
                        if (schedule[i] == 1) {
                            // newDate = new Date();
                            newDate = new Date(vehicle.startDate);
                            newDate.setDate(newDate.getDate() + i - curDay);
                            let newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")

                            while (new Date(newDateFormat) <= new Date(lastDateFormated)) {

                                if (new Date(newDateFormat) >= new Date(presentDateFormated)) {
                                    let curDate = newDate.toLocaleDateString();
                                    const addScheduleLogsQuery = `insert into schedule_logs (date,schedule_id,status,created_by,updated_by) value (STR_TO_DATE('${curDate}', '%m/%d/%Y'),${schedule_id},'pending','${data.created_by}','${data.updated_by}');`;
                                    const [rows, fields] = await mysql.query(addScheduleLogsQuery);
                                    const result = JSON.parse(JSON.stringify(rows));
                                }
                                newDate.setDate(newDate.getDate() + 7);
                                newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")
                            }
                        }
                    }

                }
            }

        }
        console.log('schedule and schedule logs are entered')
        const status = { success: true, message: 'Data is in Table' };
        return status;

    }

    public static async checkVehicle(data: any) {
        var whereClause = '';
        for (let i in data) {
            let vehicle = data[i];
            whereClause = whereClause + '"' + vehicle.number + '"' + ','
        }
        whereClause = whereClause + '"' + '_' + '"';
        const ValidateCustQuery = `select * from vehicles v
          left join work_schedule ws on ws.vehicle_id=v.id and ws.end_date=null
          where registration_no in (${whereClause});`;
        const [rows, fields] = await mysql.query(ValidateCustQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
    public static async editVehicle(data: any, cust_id: number, cust: any) {
        for (let i in data) {
            let vehicle = data[i];
            let schedule = vehicle.schedule;
            let sundayEle = schedule.pop();
            const transaction = await mysql.getConnection();
            transaction.beginTransaction();
            schedule.unshift(sundayEle);
            try {
                // const getIdQuery = `select id from vehicles where registration_no='${vehicle.number}';`
                // const [rows1, fields1] = await mysql.query(getIdQuery);
                // var result1 = JSON.parse(JSON.stringify(rows1));


                // const getId_w_s_Query = `select id from work_schedule where vehicle_id=${vehicle.vehicle_id};`
                // const [rows2, fields2] = await mysql.query(getId_w_s_Query);
                // var result2 = JSON.parse(JSON.stringify(rows2));


                const check_schedule_Query = `select * from work_schedule where id=${vehicle.work_schedule_id};`
                const [scheduleRows, scheduleRowsfields] = await transaction.query(check_schedule_Query);
                var scheduleResults = JSON.parse(JSON.stringify(scheduleRows));

                if (scheduleResults[0].sunday == schedule[0] &&
                    scheduleResults[0].monday == schedule[1] &&
                    scheduleResults[0].tuesday == schedule[2] &&
                    scheduleResults[0].wednesday == schedule[3] &&
                    scheduleResults[0].thursday == schedule[4] &&
                    scheduleResults[0].friday == schedule[5] &&
                    scheduleResults[0].saturday == schedule[6]) {

                    //console.log('schedules are not changed')

                    try {
                        // let date= moment(vehicle.startDate).format("DD/MM/YY")
                        //console.log('schedule satrtDate', vehicle.startDate);
                        const editScheduleVehQuery = `update work_schedule set start_date = '${vehicle.startDate}',subscription_amount = ${vehicle.subscription_amount},worker_building_id=${vehicle.worker_building_id} where id=${vehicle.work_schedule_id};`
                        const [editScheduleRow, fields3] = await transaction.query(editScheduleVehQuery);
                        const result3 = JSON.parse(JSON.stringify(editScheduleRow));
                        if (moment(vehicle.startDate).isBefore(scheduleResults[0].start_date)) {
                            let i: number;
                            let curDay = new Date(vehicle.startDate).getDay();
                            let newDate;
                            let lastDateFormated = moment(scheduleResults[0].start_date).format("YYYY-MM-DD");
                            const recordsToBeInserted = [];
                            for (i = 0; i < schedule.length; i++) {
                                if (schedule[i] == 1) {
                                    newDate = new Date(vehicle.startDate);
                                    newDate.setDate(newDate.getDate() + i - curDay);
                                    let newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")
                                    while (new Date(newDateFormat) < new Date(lastDateFormated)) {
                                        if (moment(newDate).isSameOrAfter(vehicle.startDate)) {
                                            let curDate = moment(newDate).format("YYYY-MM-DD");
                                            const record = [curDate, vehicle.work_schedule_id, 'pending'];

                                            const checkScheduleLogExistsQuery = `select * from schedule_logs where schedule_id=${vehicle.work_schedule_id} and date='${curDate}'`;
                                            const [rows, fields] = await transaction.query(checkScheduleLogExistsQuery);
                                            const result = JSON.parse(JSON.stringify(rows));
                                            if (result.length == 0) {
                                                recordsToBeInserted.push(record);
                                            }


                                        }
                                        newDate.setDate(newDate.getDate() + 7);
                                        newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")

                                    }
                                }
                            }
                            try {
                                const addScheduleLogsQuery = `insert into schedule_logs (date,schedule_id,status) values ?;`;
                                const [rows, fields] = await transaction.query(addScheduleLogsQuery, [recordsToBeInserted]);
                                const result = JSON.parse(JSON.stringify(rows));
                                await transaction.commit();
                                await transaction.release();
                            } catch (err) {
                                throw err;
                            }


                        }
                        await transaction.commit();
                        await transaction.release()
                    } catch (err) {

                        throw err;
                    }

                } else {
                    let result5;
                    try {
                        const editScheduleVehQuery = `update work_schedule set end_date = CURDATE() where id=${vehicle.work_schedule_id};`
                        const [rows3, fields3] = await transaction.query(editScheduleVehQuery);
                        const result3 = JSON.parse(JSON.stringify(rows3));



                        try {
                            const deleteLogsVehQuery = `delete from schedule_logs where schedule_id=${scheduleResults[0].id} and status='pending' and date > CURDATE();`
                            const [rows4, fields4] = await transaction.query(deleteLogsVehQuery);
                            const result4 = JSON.parse(JSON.stringify(rows4));
                            try {
                                console.log('schedule startdate', vehicle.startDate);
                                // the new schedule will begin from next week
                                vehicle.startDate = moment(vehicle.startDate).add(1, 'day').format('YYYY-MM-DD');
                                const addScheduleQuery = `insert into work_schedule (worker_building_id,vehicle_id,sunday,monday,tuesday,wednesday,thursday,friday,saturday,created_by,updated_by,start_date,subscription_amount) value (${vehicle.worker_building_id},${vehicle.vehicle_id},${schedule[0]},${schedule[1]},${schedule[2]},${schedule[3]},${schedule[4]},${schedule[5]},${schedule[6]},'${data.created_by}','${data.updated_by}','${vehicle.startDate}',${vehicle.subscription_amount});`;
                                const [rows2, fields2] = await transaction.query(addScheduleQuery);
                                result5 = JSON.parse(JSON.stringify(rows2));

                                if (vehicle.vehicle_id != undefined) {
                                    console.log('logs are deleted');
                                    const schedule_id = result5.insertId;
                                    let newDate = new Date();
                                    let presentDate = new Date();
                                    let presentDateFormated = moment(presentDate.toISOString()).format("YYYY-MM-DD");
                                    let lastDay = new Date(newDate.getFullYear(), newDate.getMonth() + 1, 0);
                                    // let lastDay = new Date(new Date().setMonth(new Date().getMonth() + 1));
                                    console.log('-----lastDay------' + lastDay);
                                    let lastDateFormated = moment(lastDay.toISOString()).format("YYYY-MM-DD")
                                    let i: number;
                                    let curDay = newDate.getDay();
                                    const recordsToBeInserted = [];
                                    for (i = 0; i < schedule.length; i++) {
                                        if (schedule[i] == 1) {
                                            newDate = new Date();
                                            newDate.setDate(newDate.getDate() + i - curDay);
                                            let newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")

                                            while (new Date(newDateFormat) <= new Date(lastDateFormated)) {

                                                if (new Date(newDateFormat) >= new Date(presentDateFormated)) {
                                                    let curDate = moment(newDate).format("YYYY-MM-DD");
                                                    const record = [curDate, schedule_id, 'pending'];
                                                    recordsToBeInserted.push(record);

                                                }
                                                newDate.setDate(newDate.getDate() + 7);
                                                newDateFormat = moment(newDate.toISOString()).format("YYYY-MM-DD")
                                            }
                                        }
                                    }
                                    try {
                                        const addScheduleLogsQuery = `insert into schedule_logs (date,schedule_id,status) values ?;`;
                                        const [rows, fields] = await transaction.query(addScheduleLogsQuery, [recordsToBeInserted]);
                                        const result = JSON.parse(JSON.stringify(rows));
                                        await transaction.commit();
                                        await transaction.release();
                                    } catch (err) {
                                        throw err;
                                    }
                                }
                            } catch (err) {
                                throw err;
                            }
                        } catch (err) {
                            throw err;
                        }
                    } catch (err) {
                        throw err;
                    }
                }
            } catch (err) {
                await transaction.rollback();
                await transaction.release();
                throw err;
            }



            try {
                const editVehQuery = `update vehicles set parking_no='${vehicle.parking}', building_id=${cust.building_id}, customer_id=${cust_id} where registration_no='${vehicle.number}';`
                const [rows5, fields] = await mysql.query(editVehQuery);
                const result5 = JSON.parse(JSON.stringify(rows5));
            } catch (err) {
                throw err;
            }

        }

        return { status: true, message: 'vehicle details and schedule details are updated.' }


    }


    public static async removeVehicle(data: any, cust_id: number, cust: any) {
        for (let i in data) {
            let vehicle = data[i];
            // let schedule = vehicle.schedule;
            // let sundayEle = schedule.pop();
            // schedule.unshift(sundayEle);
            try {
                // const getIdQuery = `select id from vehicles where registration_no='${vehicle.number}';`
                // const [rows1, fields1] = await mysql.query(getIdQuery);
                // var result1 = JSON.parse(JSON.stringify(rows1));


                const getId_w_s_Query = `select id from work_schedule where vehicle_id=${vehicle.vehicle_id};`
                const [rows2, fields2] = await mysql.query(getId_w_s_Query);
                var result2 = JSON.parse(JSON.stringify(rows2));



            } catch (err) {
                return err;
            }

            try {
                const editScheduleVehQuery = `update work_schedule set end_date = CURDATE() where vehicle_id=${vehicle.vehicle_id};`
                const [rows3, fields3] = await mysql.query(editScheduleVehQuery);
                const result3 = JSON.parse(JSON.stringify(rows3));
            } catch (err) {
                return err;
            }


            try {
                const deleteLogsVehQuery = `delete from schedule_logs where schedule_id=${result2[0].id} and status='pending';`
                const [rows4, fields4] = await mysql.query(deleteLogsVehQuery);
                const result4 = JSON.parse(JSON.stringify(rows4));

                if (result4) {
                    console.log('logs are deleted for removed vehicles')
                }

            } catch (err) {
                return err;
            }


        }

        return { status: true, message: 'vehicle details and schedule details are updated.' }


    }

    public static async editSchedule(data: any, cust_id: number) {
        for (let i in data) {
            let vehicle = data[i];
            const editVehQuery = `update vehicles set parikng_no=${vehicle.parking}, building_id=${vehicle.building_id}, customer_id=${cust_id} where registration_no='${vehicle.number}';`
            const [rows, fields] = await mysql.query(editVehQuery);
            const result = JSON.parse(JSON.stringify(rows));
            return result;
        }
    }

    public static async getCarwashRecords(data: any) {

        const washRecordsQuery = `select sl.date as work_date, count(sl.schedule_id) as vehicles_cleaned
            from schedule_logs sl inner join work_schedule ws on ws.id = sl.schedule_id where sl.status='completed' and sl.date between '${data.fromDate}' AND '${data.toDate}'
            group by sl.date;`
        const [rows, fields] = await mysql.query(washRecordsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }

    public static async getPCarwashRecords(data: any) {

        const pwashRecordsQuery = `select sl.date as work_date, count(sl.schedule_id) as vehicles_cleaned
            from schedule_logs sl inner join work_schedule ws on ws.id = sl.schedule_id 
            inner join vehicles v on v.id=ws.vehicle_id inner join customers c on v.customer_id=c.id 
            where sl.status='pending' 
            and c.status != 0
            and sl.date between '${data.fromDate}' AND '${data.toDate}'
            group by sl.date;`
        const [rows, fields] = await mysql.query(pwashRecordsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }

    public static async getCarDetailsOfScheduleDate(data: any) {

        const carsDetailsQuery = `select sl.date,sl.schedule_id,c.first_name,c.last_name,v.registration_no as vehicle,w.name as worker_name,b.name as building_name from customers c
        inner join buildings b on b.id=c.building_id 
        inner join worker_building wb on wb.building_id = b.id
        inner join work_schedule ws on ws.worker_building_id=wb.id
        inner join schedule_logs sl on sl.schedule_id=ws.id
        inner join workers w on w.id=wb.worker_id
        inner join vehicles v on v.id=ws.vehicle_id and v.customer_id=c.id and v.building_id=b.id
        where sl.status='completed' and sl.date= '${data.schedule_date}';`
        const [rows, fields] = await mysql.query(carsDetailsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }
    public static async getPCarDetailsOfScheduleDate(data: any) {

        const pcarsDetailsQuery = `select sl.date,sl.schedule_id,c.first_name,c.last_name,v.registration_no as vehicle,w.name as worker_name,b.name as building_name from customers c
        inner join buildings b on b.id=c.building_id 
        inner join worker_building wb on wb.building_id = b.id
        inner join work_schedule ws on ws.worker_building_id=wb.id
        inner join schedule_logs sl on sl.schedule_id=ws.id
        inner join workers w on w.id=wb.worker_id
        inner join vehicles v on v.id=ws.vehicle_id and v.customer_id=c.id and v.building_id=b.id
        where sl.status='pending' and c.status != 0 and sl.date= '${data.schedule_date}';`
        const [rows, fields] = await mysql.query(pcarsDetailsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }
    public static async getScheduleById(id: any) {

        const getScheduleQuery = `select * from work_schedule where id=${id};`
        const [rows, fields] = await mysql.query(getScheduleQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;

    }
}
