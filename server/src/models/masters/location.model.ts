import mysql from '@app/config/db';
import { log } from '@app/common';
const jwt = require('jsonwebtoken');
import moment from 'moment';

export class Location {
    public static async getAllLocations() {
        const getLocationsQuery = `select * from location where status=1`;
        const [rows, fields] = await mysql.query(getLocationsQuery);
        const result = JSON.parse(JSON.stringify(rows));
        return result;
    }
}
