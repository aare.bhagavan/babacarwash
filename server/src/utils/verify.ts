import { verify, decode } from "jsonwebtoken";
const jwt = require('jsonwebtoken');
import { Request, Response, NextFunction } from "express";
import  config from '../config';
import { Users } from "@models/masters/user.model";
import { Token } from "@common/types";
import { AuthError } from "@common/error.response";
import { Login } from "@models/auth/login.model";

const getSecret = async (req: Request, res: Response) => {
  const token = req['headers'] && (req['headers']['X-ACCESS-TOKEN'] || req['headers']['authorization']) ? (req['headers']['X-ACCESS-TOKEN'] || req['headers']['authorization']) : null;
  if(token) {
    const decoded = jwt.decode(token.toString()) as Token;
    if(decoded && decoded.userId && typeof decoded.userId === 'number') {
      try {
        const isTokenExpired = await Login.isTokenExpired(decoded.userId, token.toString()) //udentModel.findOne({_id:decoded['_id'],lastLoggedIn: decoded['time']});
        const userDetails = await Users.isUserExistsByID(decoded.userId) //udentModel.findOne({_id:decoded['_id'],lastLoggedIn: decoded['time']});
        if(!isTokenExpired && userDetails) {
          return Promise.resolve(decoded.userId);
        }else {
          return Promise.reject(null);
        }
      } catch (error) {
        console.log(error);
        return Promise.reject(error);
      }
    }
  } else {
    return Promise.reject("Token not found");
  }
}
export const verifyToken = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const secret = await getSecret(req,res);
    // req['tokenId'] = secret;
    next();
  } catch (error) {
    console.log(error);
    res.status(AuthError.ERROR_CODE).json(AuthError.TOKEN_ERROR);    
  }
}
