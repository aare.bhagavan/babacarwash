export interface Token {
    iat: number;
    userId: number,
    exp: Number, 
    time: any
    resultPhone: string,
    username: string,
    role: string
}