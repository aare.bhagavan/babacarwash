import { RESPONSE } from "./response";

export const ERROR_MESSAGES = {
    AUTH_TOKEN_NOT_FOUND : "Something went Wrong..!",
    AUTH_CREDS_WRONG : "Please enter correct credentials..!",
    USER_UNREGISTERED : "Please enter Registered Mobile or Email..!",

    GOOGLE_AUTH_ERROR : "Something went Wrong..!",
    SOMETHING_WENT_WRONG: "Something went Wrong..!",
    CUSTOMER_NOT_FOUND: "Customer not found.",

    CUSTOMER_ID_INVALID: "Customer Id invalid.",
}


export class ErrorResponse extends RESPONSE {
    constructor(message: string, show: boolean = true) {
        super();
        this.status = false;
        this.message = message;
        this.show = show;
        this.result = null;
    }
    public static ERROR_CODE = 901;
    public static GOOGLE_ERROR = new ErrorResponse(ERROR_MESSAGES.GOOGLE_AUTH_ERROR);
    public static STANDARD_ERROR = new ErrorResponse(ERROR_MESSAGES.SOMETHING_WENT_WRONG);
    public static CUSTOMER_NOT_FOUND = new ErrorResponse(ERROR_MESSAGES.CUSTOMER_NOT_FOUND);
}

export class InputError extends RESPONSE {
    constructor(message: string) {
        super();
        this.status = false;
        this.message = "Input Error: " + message;
        this.result = null;
    }
    public static ERROR_CODE = 400;
    public static STANDARD_ERROR = new ErrorResponse(ERROR_MESSAGES.GOOGLE_AUTH_ERROR);
    public static CUSTOMER_ID_INVALID = new ErrorResponse(ERROR_MESSAGES.CUSTOMER_ID_INVALID);
    public static USER_UNREGISTERED = new ErrorResponse(ERROR_MESSAGES.USER_UNREGISTERED);
}
export class AuthError extends RESPONSE {
    constructor(message: string) {
        super();
        this.status = false;
        this.message = message;
        this.result = null;
    }
    public static ERROR_CODE = 401;
    public static TOKEN_ERROR = new ErrorResponse(ERROR_MESSAGES.AUTH_TOKEN_NOT_FOUND);
    public static CREDS_ERROR = new ErrorResponse(ERROR_MESSAGES.AUTH_CREDS_WRONG);
}