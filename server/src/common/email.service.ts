import * as nodemailer from 'nodemailer';
import { OAuth2Client} from 'googleapis-common';
import Mail from 'nodemailer/lib/mailer';

class EmailServiceClass {
    // private _transporter: nodemailer.Transporter; 
    private smtpTransport!: nodemailer.Transporter;
    constructor() {
        this.initializeTransporter();
    }

    private async initializeTransporter() {
        const oauth2Client = new OAuth2Client(
            "889765550081-oq7u1lc6vtvqtagstsrp75nglaajlsvp.apps.googleusercontent.com", // ClientID
            "xcP1XoUVfMHcC2DS6hGJXwME", // Client Secret
            "https://developers.google.com/oauthplayground" // Redirect URL
        );

        // // 4/wQEOLCe0k6OBlMeIxMJl6ciyyaSH0qbXY68nO-Ig-PhROcmjLgmQ_fnjoWUq_LhOLkgnhdym_th0d-QYrO2QjbI
    
        oauth2Client.setCredentials({
            refresh_token: "1//04rCuw5dOOIzqCgYIARAAGAQSNwF-L9IrXxX6dNUTSY--ETlZmyaAUsHctHjyLG8y_l-Rcx4PMng0qdnBHazDlXIUePKbPVvCE7E"
        });
        const tokens = await oauth2Client.refreshAccessToken()
        const accessToken = tokens.credentials.access_token;
    
        if (this.smtpTransport == null) {
            this.smtpTransport = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    type: "OAuth2",
                    user: "babacarwashing@gmail.com",
                    clientId: "889765550081-oq7u1lc6vtvqtagstsrp75nglaajlsvp.apps.googleusercontent.com",
                    clientSecret: "xcP1XoUVfMHcC2DS6hGJXwME",
                    refreshToken: "1//04Ld0Zh1cObuHCgYIARAAGAQSNwF-L9IreUG7SjSZ_9a2jlpWyXGEi2U6J7EMz20VLvWVh7APppE3s3TV2EgDvE0CpCqZmPhKtQk",
                    accessToken: accessToken
                }
            } as nodemailer.TransportOptions);
        }
    }
    private async sendEmail(mailOptions: Mail.Options) {
        this.smtpTransport.sendMail(mailOptions, (error, response) => {
            error ? console.log(error) : console.log(response);
            // smtpTransport.close();
        });
    }

    public async sendOTPMail(customerEmail: string, customerName: string, otp: string) {
        const mailOptions = {
            from: "babacarwashing@gmail.com",
            to: customerEmail,
            subject: `Baba Carwash OTP`,
            generateTextFromHTML: true,
            html: `
            <div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
                <div style="margin:50px auto;width:70%;padding:20px 0">
                <div style="border-bottom:1px solid #eee">
                    <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">BABA Carwash</a>
                </div>
                <p style="font-size:1.1em">Hi,</p>
                <p>Thank you for choosing Your Baba Carwash. Use the following OTP to complete your Password setup. OTP is valid for 5 minutes</p>
                <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${otp}</h2>
                <p style="font-size:0.9em;">Regards,<br />Baba Carwash</p>
                <hr style="border:none;border-top:1px solid #eee" />
                <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
                    <p>Your Brand Inc</p>
                    <p>1600 Amphitheatre Parkway</p>
                    <p>California</p>
                </div>
                </div>
            </div>
            `
        };
        this.sendEmail(mailOptions);
    }
}

export const EmailService = new EmailServiceClass();

// import * as nodemailer from 'nodemailer'; 
// import { OAuth2Client } from 'googleapis-common';
 
//     export class GMailService { 
//       private _transporter: nodemailer.Transporter; 
//       constructor() { 
//         this._transporter = nodemailer.createTransport( 
//           `smtps://<username>%40gmail.com:<password>@smtp.gmail.com` 
//         ); 
//       } 
//       sendMail(to: string, subject: string, content: string) { 
//         let options = { 
//           from: 'from_test@gmail.com', 
//           to: to, 
//           subject: subject, 
//           text: content 
//         } 
 
//         this._transporter.sendMail(  
//           options, (error, info) => { 
//             if (error) { 
//               return console.log(`error: ${error}`); 
//             } 
//             console.log(`Message Sent ${info.response}`); 
//           }); 
//       } 
//     } 