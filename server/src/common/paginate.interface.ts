export interface Paginate {
    startIndex: number;
    endIndex: number;
    pageSize: number;
}