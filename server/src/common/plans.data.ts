export const SubscriptionPlans = [
    {
        type: "monthly",
        subscriptionType: "Monthly Subscription",
        plantype: [
            {
                name: "basic",
                price: {
                    sedan: 60,
                    fourByFour: 80
                },
                frequency: 5,
                weeklyFrequency: 1,
                weeklyFrequencyText: "Once per week",
                id: 1
            },
            {
                name: "silver",
                price: {
                    sedan: 80,
                    fourByFour: 100
                },
                frequency: 10,
                weeklyFrequency: 2,
                weeklyFrequencyText: "Twice per week",
                id: 2
            },
            {
                name: "gold",
                price: {
                    sedan: 100,
                    fourByFour: 120
                },
                frequency: 14,
                weeklyFrequency: 1,
                weeklyFrequencyText: "Thrice per week",
                id: 3
            },
            {
                name: "platinum",
                price: {
                    sedan: 150,
                    fourByFour: 170
                },
                frequency: 26,
                weeklyFrequency: 1,
                weeklyFrequencyText: "Six times per week",
                id: 4
            },
        ]
    },
    {
        type: "onetime",
        subscriptionType: "One Time Wash (Mobile Service Only)",
        plantype: [
            {
                name: "basic",
                price: {
                    sedan: 25,
                    fourByFour: 35
                },
                frequency: 0,
                weeklyFrequency: 0,
                weeklyFrequencyText: "One time only",
                id: 1
            },
        ]
    }
]
