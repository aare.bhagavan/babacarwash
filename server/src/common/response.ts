export class RESPONSE {
    public status: boolean = true;
    public message: string = "Success";
    public result: any;
    public show: boolean = true;
    constructor(data: any = null) {
        this.result = data;        
    }
}  