import express, { RequestHandler } from "express";
import { json, urlencoded } from 'body-parser';
import cors from 'cors';
import { AuthController } from "./modules/auth";
// import { MasterController } from "./modules/masters/master.controller";
// import { WorkerController } from "./modules/worker/worker.controller";
import { UserController } from "./modules/user/user.controller";

// import {logger} from './common';
import swaggerJsdoc from 'swagger-jsdoc';
import * as swaggerUi from 'swagger-ui-express';
import { Options } from "swagger-jsdoc";
import * as swaggerdocument from "./swagger.json"
// import * as swaggerJsdoc from "swagger-jsdoc";
import { IncomingForm } from 'formidable';
import * as path from 'path';
import multer from 'multer';
import { MasterController } from "./modules/masters/master.controller";
import { PaymentController } from "./modules/payment/payment.controller";
import { CustomerDashboardController } from "./modules/customer/dashboard.controller";
import { CustomerProfileController } from "./modules/customer/profile.controller";
import { verifyToken } from "./utils/verify";
var fileUpload = require('express-fileupload');
// var xlstojson = require('xls-to-json-lc');
// var xlsxtojson = require('xlsx-to-json-lc');


export default class App {

  public app: express.Application = express();
  public port: number;
  private authController = new AuthController();
  private masterController = new MasterController();
  // private workerController = new WorkerController();
  private userController = new UserController();
  private paymentController = new PaymentController();
  private customerDashboardController = new CustomerDashboardController();
  private customerProfileController = new CustomerProfileController();

  // private swaggerJsdoc = swaggerJsdoc;
  private swaggerUi = swaggerUi;
  client: any;
  swaggerOptions?: Options;
  constructor(port: number) {
    this.port = port;
    this.config();

  }


  private config = () => {
    this.app.use(cors());
    this.bodyParserConfig();
    this.initializeRoutes();
    this.app.use(express.static('public'));
    // this.app.use('/*', (req, res, next) => { // This Method should always be last. All other routes must define above this.
    //   res.sendFile("/root/carwash/public/index.html");
    // });
    // this.swaggerOptions = {
    //   swaggerDefinition:{
    //     info:{
    //       title:"GHMC API",
    //       description:"ghms api information",
    //       version:'1.0'
    //     }
    //   },
    //   apis:["./modules/auth/*.ts","app.ts","app.js","index.ts","index.js","./modules/auth/*.js"]
    // };
    // const swaggerdocs = swaggerJsdoc(this.swaggerOptions);
    this.app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerdocument))
    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      next();
    });
    this.app.use(fileUpload());
  }

  store = multer.diskStorage({
    destination: (req, file) => { './uploads' },
    filename: (req, file) => { Date.now() + '.' + file.originalname }
  });

  upload = multer({ storage: this.store })

  private bodyParserConfig = () => {
    this.app.use(json({ limit: '50mb' }));
    this.app.use(urlencoded({ limit: '50mb', extended: true }));
  }


  private initializeRoutes = () => {

    this.app.use('/auth', this.authController.router);
    this.app.use('/master', this.masterController.router);
    // this.app.use('/worker', this.workerController.router);
    this.app.use('/user', this.userController.router);
    this.app.use('/designation', this.userController.router);
    this.app.use('/payment', this.paymentController.router);
    this.app.use('/customer/dashboard/', verifyToken, this.customerDashboardController.router);
    this.app.use('/customer/profile/', verifyToken, this.customerProfileController.router);
    
  }

  public listen = () => {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }


}
