const env: string = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
const port = process.env.PORT ? process.env.PORT : 3000;
const original = Object.freeze({
  PORT: port,
  MYSQL: {
    HOST: "localhost",
    PORT: "3306",
    DB: "carwash_db",
    USER: "carwash_user",
    PASSWORD: "carwash123#"
  },
  REDIS: {
    HOST: "redis",
    PORT: "6379",
    PASSWORD: "redispass"
  },
  JWT_SECRET: 'sdtcb',
  // GOOGLE: {
  //   CLIENT_ID: '165514553949-l3n5tqkmo1lg7sosig81169blhiohsbv.apps.googleusercontent.com',
  //   CLIENT_SECRET: '4COw4h3DalwuaNruWbtMQHQx',
  //   REDIRECT_URI: 'https://api.lrnio.com/auth/google/callback',
  //   SCOPE: ['https://www.googleapis.com/auth/userinfo.profile','https://www.googleapis.com/auth/gmail.readonly']
  // },
  LOG_LOCATION: 'logs/' + env + '/app.log',
});

const development = {
  ...original,
  PORT: 3000,
  MYSQL: {
    ...original.MYSQL,
    HOST: "mysql-container",
  }
};
const gitpod = {
  ...original,
  PORT: 3000,
  MYSQL: {
    ...original.MYSQL,
    HOST: "139.59.90.96",
    PORT: "33060",
  }
};

const test = {
  ...original,
  PORT: 3001,
  MYSQL: {
    ...original.MYSQL,
    HOST: "mysql-container"
  },
  REDIS: {
    ...original.REDIS,
    HOST: "localhost"
  }
}

const production = {
  ...original,
  MYSQL: {
    ...original.MYSQL,
    HOST: "localhost"
  },
  REDIS: {
    ...original.REDIS,
    HOST: "localhost"
  }
}

const config: { [key: string]: any } = {
  "development": development,
  "test": test,
  "gitpod": gitpod,
  "production": production
}

export default config[env];
