// import { Router } from 'express';
// import { Request, Response, NextFunction } from "express";
// import { RESPONSE, ErrorResponse, ERROR_MESSAGES } from '@app/common';
// import { log } from '@app/common';
// import { Worker } from '@models/worker/worker.model';
// import { UpdateWorker } from 'src/models/worker/updateworker.model';


// export class WorkerController {
//     public router: Router = Router();

//     constructor() {
//         this.initializeRoutes();
//     }

//     private initializeRoutes(): void {
//         //routes
//         /**
//           * @swagger
//           * /employees:
//           *  get:
//           *   description:'apijjj'
//           *    responses:
//           *     '200':
//           *      'description':'success'
//          */

//         this.router.post('/insert-worker', this.insertWorker);

//         this.router.post('/update-worker', this.updateWorker);
//         this.router.get('/getAllWorkers', this.getWorkers);
//         this.router.post('/edit-worker', this.editWorker);
//         this.router.post('/deleteWorker', this.deleteWorker);
//         this.router.get('/getAllDesignations', this.getDesignations);
//         this.router.get('/getDepZoneCircle', this.getDepZoneCircle);

//     }
//     //insert worker
//     public insertWorker = async (req: Request, res: Response) => {
//         try {
//             const data = req.body;
//             // const esi_no = req.body.esi_no;
//             // const pf_no = req.body.pf_uan_no;
//             // const department_id = req.body.department_id;
//             // const zone_id = req.body.zone_id;
//             // const circle_id = req.body.circle_id;
//             const insertResponse = await Worker.insertWorker(data);
//             res.status(200).json(insertResponse);
//         }
//         catch (error) {
//             log.error(error);
//             res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//         }
//     }

//     public updateWorker = async (req: Request, res: Response) => {
//         try {
//             const isWorkerExist = await UpdateWorker.isWorkerExist(req.body.pf_no);
//             if (isWorkerExist) {
//                 const updateWorkerResponse = await UpdateWorker.updateWorker(req.body.designation_id, req.body.pf_no);
//                 res.status(200).json(updateWorkerResponse);
//             } else {
//                 res.status(200).json({
//                     status: false,
//                     message: "Worker Not exists, please insert worker"
//                 });
//             }
//         }
//         catch (error) {
//             log.error(error);
//             res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//         }
//     }
//         //Get Workers
//         public getWorkers = async (req: Request, res: Response) => {
//             try {
//                 const workersList = await Worker.getWorkers();
//                 res.status(200).json(workersList);
//             }
//             catch (error) {
//                 log.error(error);
//                 res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//             }
//         }

//         public getDepZoneCircle = async (req: Request, res: Response) => {
//             try {
//                 const DepZoneCircle = await Worker.getDepZoneCircle();
//                 res.status(200).json(DepZoneCircle);
//             }
//             catch (error) {
//                 log.error(error);
//                 res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//             }
//         }


//           //insert worker
//     public editWorker = async (req: Request, res: Response) => {
//         try {
//             const data = req.body.workerData;
//             const id = req.body.id;
           
//             // const name = req.body.workerData.name;
//             // const esi_no = req.body.workerData.esi_no;
//             // const pf_no = req.body.workerData.pf_no;
//             // // const department_id = req.body.workerData.department;
//             // // const zone_id = req.body.workerData.zone;
//             // // const circle_id = req.body.workerData.circle;
//             // const designation_id=req.body.workerData.designation
//             // const dzc_id =req.body.workerData.dzc_id;
//             const insertResponse = await Worker.editWorker(data,id);
//             res.status(200).json(insertResponse);
//         }
//         catch (error) {
//             log.error(error);
//             res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//         }
//     }
//         //Delete Workers
//         public deleteWorker = async (req: Request, res: Response) => {
//             try {
//                 const id=req.body.id;
//                 const deleteWorkerResponse = await Worker.deleteWorker(id);
//                 res.status(200).json(deleteWorkerResponse);
//             }
//             catch (error) {
//                 log.error(error);
//                 res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//             }
//         }
//     public getDesignations = async (req: Request, res: Response) => {
//         try {
//             const designationList = await Worker.getDesignations();
//             res.status(200).json(designationList);
//         }
//         catch (error) {
//             log.error(error);
//             res.status(500).json(ErrorResponse.GOOGLE_ERROR);
//         }
//     }
// }




