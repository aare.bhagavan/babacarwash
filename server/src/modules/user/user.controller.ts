import { Router } from 'express';
import { Request, Response, NextFunction } from "express";
import { RESPONSE, ErrorResponse, ERROR_MESSAGES } from '@app/common';
import { log } from '@app/common';
import { Employee } from '@models/employee/employee.model';
import { Login } from 'src/models/auth/login.model';



export class UserController {
    public router: Router = Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        // this.router.post('/insertUser', this.insertUser);
        this.router.get('/getContactDetails', this.getContactDetails);
 
    }
    public getContactDetails = async (req: Request, res: Response) => {
        try {
            res.status(200).json(new RESPONSE({
                mobile: "+91 9505878984",
                email: "babacarwash@gmail.com",
                address: "address"
            }));
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
}




