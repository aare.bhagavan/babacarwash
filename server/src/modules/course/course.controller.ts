// import { Request, Response, NextFunction } from "express";
// import CourseModel from '../../models/course/course.model';
// import { compare, hash } from 'bcryptjs';
// import { sign, verify } from "jsonwebtoken";
// import config from '../../config';
// import instructorModel from '../../models/course/instructor.model';
// import courseProject from '../../models/course/course-project.model';
// import { Types } from 'mongoose';
// import categoryModel from "../../models/course/category.model";
// import feedbackModel from "../../models/course/feedback.model";
// import forumsModel from "../../models/course/forums.model";
// import v4 from 'uuid';

// export default class CourseController {
//   public createCourse = async (req: Request, res: Response) => {
//     try {
//       const course = await CourseModel.create(req.body);
//       if (course) {
//         res.status(201).json('created');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getAllCourses = async (req: Request, res: Response) => {
//     try {
//       const allCourses = await CourseModel.aggregate([
//         {
//           $project: {
//             title: 1,
//             mainDescription: 1,
//             imageURL: 1,
//             pricing: "$pricing.instructorLedPrice",
//             rating: 1
//           }
//         }
//       ]);
//       if (allCourses) {
//         res.status(200).json(allCourses);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getCourseById = async (req: Request, res: Response) => {
//     try {
//       const course = await CourseModel.aggregate(
//         [
//           {
//             $match: {
//               _id: Types.ObjectId(req.params.courseId)
//             },
//           },
//           {
//             $unwind: {
//               path: "$projects",
//               "preserveNullAndEmptyArrays": true
//             }
//           },
//           {
//             $lookup: {
//               from: "projects",
//               localField: "projects",
//               foreignField: "_id",
//               as: "projectDetails"
//             }
//           },
//           {
//             $unwind: {
//               path: "$projectDetails",
//               "preserveNullAndEmptyArrays": true
//             }
//           },
//           {
//             $unwind: {
//               path: "$instructors",
//               "preserveNullAndEmptyArrays": true
//             }
//           },
//           {
//             $lookup: {
//               from: "instructors",
//               localField: "instructors",
//               foreignField: "_id",
//               as: "instructorDetails"
//             }
//           },
//           {
//             $unwind: {
//               path: "$instructorDetails",
//               "preserveNullAndEmptyArrays": true
//             }
//           },
//           {
//             $group: {
//               _id: '$_id',
//               projectDetails: { $addToSet: '$projectDetails' },
//               projects: { $addToSet: '$projects' },
//               instructors: { $addToSet: '$instructors' },
//               instructorDetails: { $addToSet: '$instructorDetails' },
//               title: { $first: '$title' },
//               mainDescription: { $first: '$mainDescription' },
//               description1: { $first: '$description1' },
//               description2: { $first: '$description2' },
//               estimatedTime: { $first: '$estimatedTime' },
//               prerequisites: { $first: '$prerequisites' },
//               imageURL: { $first: '$imageURL' },
//               curriculum: { $first: '$curriculum' },
//               pricingDetails: { $first: '$pricing' },
//               rating: { $first: '$rating' },
//               resources: { $first: '$resources' }
//             }
//           },
//           {
//             $project: {
//               projects: 0,
//               instructors: 0,
//             }
//           }
//         ]);
//       if (course && course.length) {
//         res.status(200).json(course[0]);
//       } else {
//         res.status(404).json('No course found');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getAllCourseIds = async (req: Request, res: Response) => {
//     try {
//       const allCourses = await CourseModel.find({}, { _id: 1, title: 1, category: 1 });
//       if (allCourses) {
//         res.status(200).json(allCourses);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createInstructor = async (req: Request, res: Response) => {
//     try {
//       const course = await instructorModel.create(req.body);
//       if (course) {
//         res.status(201).json('created');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getAllInstructors = async (req: Request, res: Response) => {
//     try {
//       const allInstructors = await instructorModel.aggregate([
//         {
//           $unwind: {
//             path: "$technologies",
//             "preserveNullAndEmptyArrays": true
//           }
//         },
//         {
//           $lookup: {
//             from: "courses",
//             localField: "technologies",
//             foreignField: "_id",
//             as: "technologyDetails"
//           }
//         },
//         {
//           $unwind: {
//             path: "$technologyDetails",
//             "preserveNullAndEmptyArrays": true
//           }
//         },
//         {
//           $group: {
//             _id: '$_id',
//             technologies: { $addToSet: '$technologyDetails.title' },
//             name: { $first: '$name' },
//             experience: { $first: '$experience' },
//             description: { $first: '$description' },
//             rating: { $first: '$rating' }
//           }
//         },
//         {
//           $project: {
//             technologies: { $setDifference: ["$technologies", [null]] },
//             name: 1,
//             experience: 1,
//             description: 1
//           }
//         }

//       ]);
//       if (allInstructors) {
//         res.status(200).json(allInstructors);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getInstructorById = async (req: Request, res: Response) => {
//     try {
//       const course = await instructorModel.findById(req.params.id);
//       if (course) {
//         res.status(200).json(course);
//       } else {
//         res.status(404).json('No course found');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public updateInstructorById = async (req: Request, res: Response) => {
//     try {
//       const course = await instructorModel.findByIdAndUpdate(req.body.id, { $set: req.body });
//       if (course) {
//         res.status(200).json(course);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createProject = async (req: Request, res: Response) => {
//     try {
//       const course = await courseProject.create(req.body);
//       if (course) {
//         res.status(201).json('created');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getAllProjects = async (req: Request, res: Response) => {
//     try {
//       const allCourses = await courseProject.find();
//       if (allCourses) {
//         res.status(200).json(allCourses);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public deleteInstructor = async (req: Request, res: Response) => {
//     try {
//       const deleteInstructor = await instructorModel.findByIdAndRemove(req.body.id);
//       if (deleteInstructor) {
//         res.status(200).json('done');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createCategory = async (req: Request, res: Response) => {
//     try {
//       const category = await categoryModel.create(req.body);
//       if (category) {
//         res.status(201).json('created');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getAllCategories = async (req: Request, res: Response) => {
//     try {
//       const allCategories = await categoryModel.find();
//       if (allCategories) {
//         res.status(200).json(allCategories);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createFeedback = async (req: Request, res: Response) => {
//     try {
//       const feedback = await feedbackModel.create({ ...req.body, studentId: req['tokenId'] });
//       if (feedback) {
//         const allFeedbacks: any = await feedbackModel.find({ courseId: req.body.courseId });
//         if (allFeedbacks) {
//           console.log(allFeedbacks);
//           const rating = allFeedbacks.reduce((a: any, b: any) => a + b['rating'], 0) / allFeedbacks.length;
//           console.log(rating);
//           const course = await CourseModel.findByIdAndUpdate(req.body.courseId, { $set: { rating } });
//           if (course) {
//             res.status(201).json('created');
//           }
//         }
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public getFeedbackByCourse = async (req: Request, res: Response) => {
//     try {
//       const feedback = await feedbackModel.findOne({ courseId: req.body.courseId, studentId: Types.ObjectId(req['tokenId']) });
//       if (feedback) {
//         res.status(200).json(feedback);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createForum = async (req: Request, res: Response) => {
//     try {
//       const forum = await forumsModel.create({ ...req.body, studentId: req['tokenId'] });
//       if (forum) {
//         res.status(200).json(forum);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }



//   public getForumByCourse = async (req: Request, res: Response) => {
//     try {
//       const forum = await forumsModel.find({ courseId: req.body.courseId });
//       if (forum) {
//         res.status(200).json(forum);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public addMessagesToForum = async (req: Request, res: Response) => {
//     try {
//       const forum = await forumsModel.findByIdAndUpdate(req.body.topic, { $push: { messages: { id: v4(), message: req.body.message, name: req.body.name, studentId: Types.ObjectId(req['tokenId']), date: new Date() } } }, { new: true });
//       if (forum) {
//         res.status(200).json(forum);
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }

//   public createResource = async (req: Request, res: Response) => {
//     try {
//       const resource = await CourseModel.findByIdAndUpdate( req.body['courseId'],{$push:{resources:{label: req.body['label'], link: req.body['link']}}});
//       if (resource) {
//         res.status(200).json('done');
//       }
//     } catch (error) {
//       console.log(error);
//       res.status(500).json(error);
//     }
//   }


// }