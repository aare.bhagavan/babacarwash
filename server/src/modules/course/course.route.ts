// import { Router } from 'express';
// import CourseController from '../controllers/course/course.controller';
// import { verifyToken } from '../utils/verify';

// class CourseRouter {
//   public router: Router = Router();
//   private courseController: CourseController = new CourseController();

//   constructor() {
//     this.initializeRoutes();
//   }

//   private initializeRoutes(): void {
//     this.router.post('/create', this.courseController.createCourse);
//     this.router.get('/getCourseById/:courseId', this.courseController.getCourseById);
//     this.router.get('/getAllCourses', this.courseController.getAllCourses);
//     this.router.get('/getAllCourseIds', this.courseController.getAllCourseIds);
//     this.router.post('/createInstructor', this.courseController.createInstructor);
//     this.router.post('/updateInstructor', this.courseController.updateInstructorById);
//     this.router.get('/getInstructorById/:id', this.courseController.getInstructorById);
//     this.router.get('/getAllInstructors', this.courseController.getAllInstructors);
//     this.router.post('/createProject', this.courseController.createProject);
//     this.router.get('/getAllProjects', this.courseController.getAllProjects);
//     this.router.post('/deleteInstructor', this.courseController.deleteInstructor);
//     this.router.post('/createCategory', this.courseController.createCategory);
//     this.router.get('/getAllCategories', this.courseController.getAllCategories);
//     this.router.post('/createFeedback', verifyToken, this.courseController.createFeedback);
//     this.router.post('/getFeedbackByCourse',verifyToken, this.courseController.getFeedbackByCourse);
//     this.router.post('/createForum', verifyToken, this.courseController.createForum);
//     this.router.post('/getForumByCourse',verifyToken, this.courseController.getForumByCourse);
//     this.router.post('/addMessagesToForum',verifyToken, this.courseController.addMessagesToForum);
//     this.router.post('/createResourceByCourseId', verifyToken, this.courseController.createResource);
//   }
// }

// export default CourseRouter;