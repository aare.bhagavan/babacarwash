import { Router, response } from 'express';
import { Request, Response, NextFunction } from "express";
import { RESPONSE, ErrorResponse, ERROR_MESSAGES, InputError } from '@app/common';
import { log } from '@app/common';
import { Location } from '@models/masters/location.model';
import { Buildings } from '@models/masters/buildings.model';
import { Customers } from '@models/masters/customers.model';
import { Vehicles } from '@models/masters/vehicles.model';
import { Workers } from '@models/masters/workers.model';
import { Users } from '@models/masters/user.model';
import moment from 'moment';
import { nextTick } from 'process';
import { Paginate } from '@common/paginate.interface';
import { WashFilter } from 'src/common/wash.filter';
import { CustomerPaymentHistoryRequest, CustomerWashesHistoryRequest } from './dashboard.request';
import { Payments } from 'src/models/masters/payments.model';

export class CustomerProfileController {
    public router: Router = Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        //routes
        /**
          * @swagger
          * /employees:
          *  get:
          *   description:'apijjj'
          *    responses:
          *     '200':
          *      'description':'success'
         */

        this.router.post('/getProfile', this.validateCustomer, this.getCustomerProfile);
        this.router.post('/getCars', this.validateCustomer, this.getCustomerCars);
    }
    public validateCustomer = async (req: Request, res: Response, next: NextFunction) => {
        const data = req.body;
        console.log(data);
        if(!data.customerId) {
            res.status(InputError.ERROR_CODE).json(InputError.CUSTOMER_ID_INVALID);
            return;
        }
        try {
            const existingCustomer = await Customers.checkCustomerWithId(data.customerId);
            if(existingCustomer.length == 0) {
                res.status(ErrorResponse.ERROR_CODE).json(ErrorResponse.CUSTOMER_NOT_FOUND);
                return;
            }
            next();
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
    public getCustomerProfile = async (req: Request, res: Response) => {
        const data = req.body;
        try {
            const existingCustomer = await Customers.getCustomerDetails(data.customerId);
            console.log(existingCustomer);
            res.status(200).json(new RESPONSE(existingCustomer));
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
    public getCustomerCars = async (req: Request, res: Response) => {
        const data = req.body;
        try {
            const customercars = await Customers.getCustomerCars(data.customerId);
            console.log(customercars);
            res.status(200).json(new RESPONSE(customercars));
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }

}




