import { Paginate } from "@common/paginate.interface";
import { WashFilter } from "@common/wash.filter";

export class CustomerWashesHistoryRequest {
    public paginate: Paginate;
    public filter: WashFilter;
    public customerId: number;
    constructor(data: CustomerWashesHistoryRequest) {
        this.paginate = {
            endIndex:  data.paginate.endIndex? data.paginate.endIndex: 20,
            pageSize:  data.paginate.pageSize? data.paginate.pageSize: 20,
            startIndex: data.paginate.startIndex? data.paginate.startIndex: 0
        }
        this.filter = {
            startDate: data.filter.startDate,
            endDate: data.filter.endDate,
            car: data.filter.car
        }
        this.customerId = data.customerId;
    }

}

export class CustomerPaymentHistoryRequest {
    public paginate: Paginate;
    public filter: WashFilter;
    public customerId: number;
    constructor(data: CustomerPaymentHistoryRequest) {
        this.paginate = {
            endIndex:  data.paginate.endIndex? data.paginate.endIndex: 20,
            pageSize:  data.paginate.pageSize? data.paginate.pageSize: 20,
            startIndex: data.paginate.startIndex? data.paginate.startIndex: 0
        }
        this.filter = {
            startDate: data.filter.startDate,
            endDate: data.filter.endDate,
            car: data.filter.car
        }
        this.customerId = data.customerId;
    }
}