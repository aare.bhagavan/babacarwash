import { Router, response } from 'express';
import { Request, Response, NextFunction } from "express";
import { RESPONSE, ErrorResponse, InputError } from '@app/common';
import { log } from '@app/common';
import { Customers } from '@models/masters/customers.model';
import { CustomerPaymentHistoryRequest, CustomerWashesHistoryRequest } from './dashboard.request';
import { Payments } from '@models/masters/payments.model';
import { verifyToken } from 'src/utils/verify';

export class CustomerDashboardController {
    public router: Router = Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        //routes
        /**
          * @swagger
          * /employees:
          *  get:
          *   description:'apijjj'
          *    responses:
          *     '200':
          *      'description':'success'
         */

        this.router.post('/getWashHistory', this.validateCustomer, this.getCustomerWashes);
        this.router.post('/getPaymentHistory', this.validateCustomer, this.getCustomerPaymentHistory);
    }
    public validateCustomer = async (req: Request, res: Response, next: NextFunction) => {
        const data = req.body;
        console.log(data);
        if(!data.customerId) {
            res.status(InputError.ERROR_CODE).json(InputError.CUSTOMER_ID_INVALID);
            return;
        }
        try {
            const existingCustomer = await Customers.checkCustomerWithId(data.customerId);
            if(existingCustomer.length == 0) {
                res.status(ErrorResponse.ERROR_CODE).json(ErrorResponse.CUSTOMER_NOT_FOUND);
                return;
            }
            next();
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
    public getCustomerWashes = async (req: Request, res: Response) => {
        const data: CustomerWashesHistoryRequest = req.body;
        try {
            console.log(data);
            const requestData = new CustomerWashesHistoryRequest(data); 
            const washRecords = await Customers.getCustomerWashHistory(requestData.customerId, requestData.filter, requestData.paginate);
            console.log(washRecords);
            res.status(200).json(new RESPONSE(washRecords));
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
    public getCustomerPaymentHistory = async (req: Request, res: Response) => {
        const data: CustomerPaymentHistoryRequest = req.body;
        try {
            console.log(data);
            const requestData = new CustomerPaymentHistoryRequest(data); 
            const payments = await Payments.getCustomerPaymentsHistory(requestData.customerId, requestData.filter, requestData.paginate);
            console.log(payments);
            res.status(200).json(new RESPONSE(payments));
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.STANDARD_ERROR);
        }
    }
}




