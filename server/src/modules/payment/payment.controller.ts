import { Router } from 'express';
import { Request, Response } from "express";
import { ErrorResponse, RESPONSE } from '@app/common';
import { log } from '@app/common';
import { Payments } from '@models/masters/payments.model';
import moment from 'moment';
import { Workers } from '@models/masters/workers.model';
import { SubscriptionPlans } from '@common/plans.data';

export class PaymentController {
    public router: Router = Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes(): void {
        this.router.get('/getALLPaymentDetails', this.getALLPaymentDetails);
        this.router.post('/getuserpayment', this.getuserpaymentdetails)
        this.router.post('/getUserPaymentDetails', this.getUserPaymentDetails);
        this.router.post('/adminFeedBack', this.adminFeedBack);
        this.router.post('/payAmount', this.payAmount);
        this.router.post('/renewSchedules', this.renewSchedules);
        this.router.post('/getbuildingpaymentsdetails',this.getbuildingpaymentsdetails);
        this.router.post('/getWorkerPaymentReports',this.getWorkerPaymentReports);
        this.router.post('/getSubscriptionPlans',this.getSubscriptionPlans);
    }
    public getSubscriptionPlans = async (req: Request, res: Response) => {
        res.status(200).json(new RESPONSE(SubscriptionPlans as any));
    }

    public renewSchedules = async (req: Request, res: Response) => {
        try {
            var lastDateOfTheMonth = moment().endOf('month');
            let startDay = moment([moment(lastDateOfTheMonth).get('year'), moment(lastDateOfTheMonth).get('month'),'01']);
            const scheduleExtend = await Workers.ExtendScheduleDates(req.body, startDay);
            res.status(200).json(scheduleExtend);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }
    }

    public getALLPaymentDetails = async (req: Request, res: Response) => {
        try {
            const allPaymentDetails = await Payments.getALLPaymentDetails();
            res.status(200).json(allPaymentDetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }
    }
    public getUserPaymentDetails = async (req: Request, res: Response) => {
        console.log(req);
        try {
            const userPaymentDetails = await Payments.getUserPaymentDetails(req.body);
            res.status(200).json(userPaymentDetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }
    }
    public adminFeedBack = async (req: Request, res: Response) => {
        console.log(req);
        try {
            const userPaymentDetails = await Payments.adminFeedBack(req.body);

            if (req.body.type == 'charged') {
                var lastDateOfTheMonth = moment(req.body.data.date).endOf('month');
                let startDay = moment([moment(lastDateOfTheMonth).get('year'), moment(lastDateOfTheMonth).get('month'), moment(lastDateOfTheMonth).get('date')]).add(1, 'day');
                let dateFrom = moment(lastDateOfTheMonth).subtract(8, 'd').format('YYYY-MM-DD');
                let scheduleDate = moment(req.body.data.date).format('YYYY-MM-DD');
                if (moment(scheduleDate).isSameOrAfter(dateFrom) || moment(scheduleDate).isSame(lastDateOfTheMonth)) {
                    const scheduleExtend = await Workers.ExtendScheduleDates(req.body.data, startDay);
                    if (!scheduleExtend) {
                        console.log('schedule logs are not extended')
                    }
                }
            }
            //  else if (req.body.type = 'paid') {
            //     var lastDateOfTheMonth = moment().endOf('month');
            //     let startDay = moment([moment(lastDateOfTheMonth).get('year'), moment(lastDateOfTheMonth).get('month'), moment(lastDateOfTheMonth).get('date')]).add(1, 'day');
            //     let dateFrom = moment(lastDateOfTheMonth).subtract(8, 'd').format('YYYY-MM-DD');
            //     let scheduleDate = moment().format('YYYY-MM-DD');
            //     if (moment(scheduleDate).isSameOrAfter(dateFrom) || moment(scheduleDate).isSame(lastDateOfTheMonth)) {
            //         const scheduleExtend = await Workers.ExtendScheduleDates(req.body.data, startDay);
            //         if (!scheduleExtend) {
            //             console.log('schedule logs are not extended')
            //         }
            //     }
            // }

            res.status(200).json(userPaymentDetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }
    }
    public payAmount = async (req: Request, res: Response) => {
        console.log(req);
        try {
            const userPaymentDetails = await Payments.payAmount(req.body);
            res.status(200).json(userPaymentDetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }
    }

    public getuserpaymentdetails = async (req: Request, res: Response) => {
        console.log(req.body)
        let id = req.body.id;
        console.log

        try {
            const userpaymentdetails = await Payments.getuserpaymentdetails(id);
            res.status(200).json(userpaymentdetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }

    }
    public getbuildingpaymentsdetails= async (req: Request, res: Response)=>{
        console.log(req.body)
        let id = req.body.id;
        console.log
        try {
            const buildingpaymentdetails = await Payments.getbuildingpaymentdetails(req.body);
            res.status(200).json(buildingpaymentdetails);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }

    }
    public getWorkerPaymentReports= async (req: Request, res: Response)=>{
        console.log(req.body)
        let id = req.body.id;
        console.log
        try {
            const workerPaymentReports = await Payments.getWorkerPaymentReports(req.body);
            res.status(200).json(workerPaymentReports);
        }
        catch (error) {
            log.error(error);
            res.status(500).json(ErrorResponse.GOOGLE_ERROR);
        }

    }
}

