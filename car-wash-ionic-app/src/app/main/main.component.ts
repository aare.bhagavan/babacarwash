import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ROUTE } from '../routes';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {

  constructor( private router: Router,  private storage: Storage,   public navCtrl: NavController,) { }
  public appPages = [
    {
      title: 'Schedule',
      url: '/jobs',
      icon: 'home'
    },
    {
      title: 'Payments',
      url: '/payments',
      icon: 'card'
    },
    {
      title: 'one-time-wash',
      url: '/one-time-wash-registration',
      icon: 'card'
    },
    {
      title: 'Logout',
      url: '/login',
      icon: 'log-out'
    }
  ];
  menu: any;
  jobsstatus:any;
  paymentstatus:any;
  profilestatus:any;

  ngOnInit() {
    // console.log(this.paymentstatus)
    console.log("main function")
    this.jobsstatus=localStorage.getItem('statusjob')
    this.paymentstatus=localStorage.getItem('statuspayments')
    this.profilestatus=localStorage.getItem('statusprofile')

    // console.log( this.jobsstatus,  this.paymentstatus, this.profilestatus)
  }

    jobs(){
    console.log("jobs")
    // this.router.navigate(['/jobs']);
    this.navCtrl.navigateForward([ROUTE.JOBS]).then(() => {
      window.location.reload();
    });;
  //   this.router.navigateByUrl('/jobs', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['jobs']);
  // });
    this.jobsstatus=true
    // localStorage.setItem('statusjob',this.jobsstatus)
    this.paymentstatus=false
    this.profilestatus=false
    localStorage.setItem('statuspayments', this.paymentstatus)
    localStorage.setItem('statusprofile',this.profilestatus)
    localStorage.setItem('statusjob',this.jobsstatus)
    this.jobsstatus=localStorage.getItem('statusjob')
    this.paymentstatus=localStorage.getItem('statuspayments')
    this.profilestatus=localStorage.getItem('statusprofile')
  
     

  }
   payments(){ 
    console.log("payments")
    // this.router.navigate(['/payments']);
    this.navCtrl.navigateForward([ROUTE.PAYMENT])  .then(() => {
      window.location.reload();
    });;
  //   this.router.navigateByUrl('/payments', { skipLocationChange: true }).then(() => {
  //     this.router.navigate(['payments']);
  // });
    console.log("payments")
    this.paymentstatus=true
    console.log(this.paymentstatus)
    this.profilestatus=false
    this.jobsstatus=false
    localStorage.setItem('statuspayments', this.paymentstatus)
    localStorage.setItem('statusprofile',this.profilestatus)
    localStorage.setItem('statusjob',this.jobsstatus)
    this.jobsstatus=localStorage.getItem('statusjob')
    this.paymentstatus=localStorage.getItem('statuspayments')
    this.profilestatus=localStorage.getItem('statusprofile')
   

  
 



  }
  oneTimeWash(){
    // this.router.navigate(['/one-time-wash-registration']) 

    this.router.navigate(['/one-time-wash-registration'])  .then(() => {
      window.location.reload();
    });;
    this.profilestatus=true
    this.jobsstatus=false
    this.paymentstatus=false
    localStorage.setItem('statuspayments', this.paymentstatus)
    localStorage.setItem('statusprofile',this.profilestatus)
    localStorage.setItem('statusjob',this.jobsstatus)
    this.jobsstatus=localStorage.getItem('statusjob')
    this.paymentstatus=localStorage.getItem('statuspayments')
    this.profilestatus=localStorage.getItem('statusprofile')
    


  }
  logout(){
    localStorage.clear()
    this.router.navigate(['/login']);
    
  }

}
