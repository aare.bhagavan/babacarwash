import { Component,OnInit } from '@angular/core';
import { NavController, MenuController, ToastController, AlertController, LoadingController } from '@ionic/angular';
import { Platform} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Router } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    {
      title: 'Schedule',
      url: '/jobs',
      icon: 'home'
    },
    {
      title: 'Payments',
      url: '/payments',
      icon: 'card'
    },
    {
      title: 'Logout',
      url: '/login',
      icon: 'log-out'
    }
  ];
  menu: any;
  jobsstatus:boolean=true;
  paymentstatus:any;
  profilestatus:any;
  login_key:any;
  
  
  
  pages: { title: string; component: any; }[];
  isUserLoggedin:boolean;
  constructor(
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private statusBar: StatusBar,
    private storage: Storage
  ) {
    this.isUserLoggedin = JSON.parse(localStorage.getItem("isUserLogged"));
    console.log(this.isUserLoggedin);
    localStorage.removeItem("isUserLogged");
    this.initializeApp();
    this.verifytoken();
    // document.addEventListener('deviceready', function(e){
    //   window.addEventListener('native.keyboardshow', function () {
    //   cordova.plugins.Keyboard.disableScroll(true);
    //   });
    //   });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  async verifytoken(){
    console.log("inside function")
  
     

     this.login_key=localStorage.getItem('login_key')

     console.log(this.login_key)

     if(this.login_key=="true"){
       console.log("inside if ")
      
       

     }
     else{
      this.router.navigate(['/login'],{state: {updateInfos: true}});
     }

    }
  jobs(){
    console.log("inside")
    this.jobsstatus=true
    this.paymentstatus=false
    this.profilestatus=false

  }
  payments(){
    this.paymentstatus=true
    this.profilestatus=false
    this.jobsstatus=false



  }
  profile(){
    this.profilestatus=true
    this.jobsstatus=false
    this.paymentstatus=false
    


  }
}
