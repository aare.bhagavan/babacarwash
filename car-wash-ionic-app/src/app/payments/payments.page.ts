import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { EvAlertController } from '../alert.controller';
import { PaymentService } from '../api/payment.service';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import * as _ from "underscore";

import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
  AbstractControl
} from "@angular/forms";
import { SlicePipe } from '@angular/common';


@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage implements OnInit {
  myControl = new FormControl();
  // authForm: FormGroup;
  jobsarray: any;
  show_btn: boolean = false;
  building:any;
  color:any;
  location:any;
  status:any;
  today:any;
  user_id:any;
  allJobs:any;
  due_amount:any;
  isDialogPresent:boolean=true;
  constructor(public modalController: ModalController,
     private router1: Router,    public paymentService: PaymentService, private router: ActivatedRoute,
     public alertController: EvAlertController, private storage: Storage, public navCtrl: NavController,
    ) { 
      this. getUser_id();
      

     
    }
  // jobs: any = [
  //   { 'date': '10 August 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'location': 'JublieHills,Hyderabad', 'building': 'Building3010,Block-C', 'parking': '123', 'status': 'Pending', 'toggle': false, 'jobstatus': "true", 'payment': '30', 'washes': '20', 'misses': '1' },
  //   { 'date': '12 August 2020', 'car': "AURD", 'customer': "Shaik Abdul", 'carnumber': "DU-55-44", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Pending', 'toggle': false, 'jobstatus': "true", 'payment': '54', 'washes': '91', 'misses': '5' },
  //   { 'date': '22 July 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-43", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Paid', 'toggle': true, 'jobstatus': "true", 'payment': '90', 'washes': '22', 'misses': '3' },
  //   { 'date': '20 July 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-41", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '0919', 'status': 'Pending', 'toggle': false, 'jobstatus': "true", 'payment': '75', 'washes': '20', 'misses': '5' },
  //   { 'date': '25 July 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-00-99", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '3559', 'status': 'Paid', 'toggle': true, 'jobstatus': "true", 'payment': '80', 'washes': '25', 'misses': '3' },
  //   { 'date': '29 July 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-19-40", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '1234', 'status': 'Pending', 'toggle': false, 'jobstatus': "true", 'payment': '40', 'washes': '20', 'misses': '8' }

  // ];


  jobs: any = [
    {
      'building':'Building-1',
      'location':'JublieHills',
      'vehicles':[
        {'day': 'Today', 'date': '06/Aug/2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24",'parking': '123', 'status': 'Pending', 'jobstatus': "true",'address':'302','amount':'15' },
        {'day': 'Today', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20",'parking': '903', 'status': 'Collect', 'jobstatus': "true",'address':'304','amount':'10' },
        {'day': 'Yesterday', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99",'parking': '765', 'status': 'Collect', 'jobstatus': "true",'address':'300','amount':'100'}
      ]
    },
    {
      'building':'Building-2',
      'location':'BanjaraHills',
      'vehicles':[
        {'day': 'Today', 'date': '06/Aug/2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24",'parking': '123', 'status': 'Pending', 'jobstatus': "true",'address':'Flat-3,1st Floor','amount':'35' },
        {'day': 'Today', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20",'parking': '903', 'status': 'Collect', 'jobstatus': "true",'address':'Flat-2,2nd Floor','amount':'95' },
        {'day': 'Yesterday', 'date': 'Today', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99",'parking': '765', 'status': 'Collect', 'jobstatus': "true",'address':'Flat-1,1st Floor','amount':'86' }
      ]
    },
    {
      'building':'Building-3',
      'location':'Hi-Tech City',
      'vehicles':[
        {'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24",'parking': '123', 'status': 'Assigned', 'jobstatus': "true",'address':'309','amount':'25' },
        {'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20",'parking': '903', 'status': 'Completed', 'jobstatus': "true",'address':'322','amount':'65' },
        {'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99",'parking': '765', 'status': 'Pending', 'jobstatus': "true",'address':'300','amount':'65' }
      ]
    },
    {
      'building':'Building-4',
      'location':'Panjagutta',
      'vehicles':[
        {'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24",'parking': '123', 'status': 'Assigned', 'jobstatus': "true",'address':'212','amount':'15' },
        {'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20",'parking': '903', 'status': 'Completed', 'jobstatus': "true",'address':'200','amount':'30' },
        {'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99",'parking': '765', 'status': 'Pending', 'jobstatus': "true" ,'address':'208','amount':'55'}
      ]
    }



];



count: number = 0;
filter(value) {
  this.location=this.jobs
  this.status=true
  console.log("inside function")
  // const value=ev.target.value;
  if (value != null || value != "") {
    this.location = this.jobs.filter(device => (
      (device.building.toLowerCase().includes(value.toLowerCase()) || device.location.toLowerCase().includes(value.toLowerCase()
      ) )
    )
    )
  }
  else {
    this.location = this.jobs


  }
}
ngOnInit() {
  this.today=false
  this.building=false
  this.jobsarray = this.jobs
  this.count = this.jobs.length;
  this.myControl.valueChanges.subscribe(value => {
    this.filter(value)
  })
  this.getUser_id()
 

  
 

}

async getUser_id(){
    await this.storage.get('user_id').then(async (user_id) => {

      console.log(user_id+"user id")
     this.user_id = user_id;
     (await this.paymentService.getuserpayment(this.user_id)).subscribe((res:any)=>{
      console.log(res)
      console.log(res[0].vehicles);
  
      
  
     
    
      
  
      let uniqueArray = _.map(_.groupBy(res, (obj: any) => {
        return obj.building_id;
        }), (grouped) => {
        return grouped[0];
        })
  
        uniqueArray.forEach(building=> {
          building['vehicles'] = []
          let vehiclesData = res.filter(eachvehicle=>eachvehicle.building_id == building.building_id);
           
          vehiclesData.forEach((vehicle, index) => {

            this.due_amount=vehicle.amount_charged-vehicle.amount_paid
            if(this.due_amount==0){
            
              vehicle.payment_status="completed";
            }
            else{
            
              vehicle.payment_status=" not-completed";
            }
          let obj = {
          carnumber:vehicle.registration_no,
          parking_no:vehicle.parking_no,
          status:vehicle.payment_status,
          date:vehicle.date,
          payment_amount:vehicle.payment_amount,
          amount_paid:vehicle.amount_paid,
          parkingno:vehicle.parking_no,
          

          amount_charged:vehicle.amount_charged,
          payment_date:vehicle.payment_date,
          flat_no:vehicle.flat_no,
          vehicle_id:vehicle.vehicle_id,
          due_amount:vehicle.amount_charged-vehicle.amount_paid,
          sl_id:vehicle.s_id,
          user_name:vehicle.name,
          sldate:new Date(vehicle.date).toLocaleDateString(),
          
          
        
           }
          building.vehicles.push(obj)
          
           });
           
           });
           console.log(uniqueArray);
           this.allJobs=uniqueArray;
           
           
    })

})
console.log( this.user_id )
}





// async getUser_id(){
//   await this.storage.get('user_id').then(async (user_id) => {
//    this.user_id = user_id;

//    (await this.paymentService.getuserpayment(this.user_id)).subscribe((res:any)=>{
//     console.log(res)
//     // console.log(res[0].vehicles);
   
    // console.log(res[0].vehicles);

    

   
  
    

//     let uniqueArray = _.map(_.groupBy(res, (obj: any) => {
//       return obj.building_id;
//       }), (grouped) => {
//       return grouped[0];
//       })

//       uniqueArray.forEach(building=> {
//         building['vehicles'] = []
//         let vehiclesData = res.filter(eachvehicle=>eachvehicle.building_id == building.building_id);
         
//         vehiclesData.forEach((vehicle, index) => {
//         let obj = {
//         carnumber:vehicle.registration_no,
//         parking_no:vehicle.parking_no,
//         status:vehicle.status,
//         date:vehicle.date,
//         payment_amount:vehicle.payment_amount,
//         payment_date:vehicle.payment_date,
//         flat_no:vehicle.flat_no
//          }
//         building.vehicles.push(obj)
        
//          });
         
//          });
//          console.log(uniqueArray);
//          this.allJobs=uniqueArray;
//          console.log(this.allJobs[0].vehicles[0])
         
//   })
//  });

//  console.log(this.user_id+"id is")
// }







Today(){
  this.building=false
  this.today=!this.today
  this.color="red"
  
}

Building(){
  this.building=!this.building
  this.today=false

}
loadData(event) {
  if (event.status == "Completed") {
    this.jobs.forEach(element => {
          
      if (element.carnumber == event.carnumber) {
        element.status = "Pending";
      }
    });
  } else {
    this.jobs.forEach(element => {
      if (element.carnumber == event.carnumber) {
        element.status = "Completed";
      }
    });
  }
}
show_button() {
  console.log("show button")
  this.show_btn = true;
  console.log(this.show_btn)

}
async presentModal(selected:any) {
  
  console.log(selected.carnumber)
  console.log(selected.sl_id+"is")
  console.log(selected.user_name+"is")

  let data = {
    'schedule_id': selected.sl_id,
    
    'user_name':selected.user_name,
    'payment_registered_by':selected.user_name,
    'user_id':this.user_id,
    'added_by' :selected.user_name,
    'updated_by':selected.user_name,
    'status':"Complted",
    'note':"",
  }

  console.log(data)

  this.isDialogPresent=false;
  const res= await this.alertController.PaymentCollectDialog(data);
  
  console.log(res);
  if(res==true){
   
  
    this.isDialogPresent=true;
    this.getUser_id()
   
  
  }
}


  // async presentModal() {
  //   const modal = await this.modalController.create({
  //     component: ModalPage,
  //     cssClass: 'my-custom-class'
  //   });
  //   return await modal.present();
  // }


}
