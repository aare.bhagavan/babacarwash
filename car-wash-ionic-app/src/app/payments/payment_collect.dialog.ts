import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import { PaymentService } from '../api/payment.service';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Router} from '@angular/router';
// import { AuthService } from 'src/app/api/auth.service';
// import { EvAlertController } from '../alert.controller';
// import { DeclareFunctionStmt } from '@angular/compiler';
// import { ROUTE } from '../../routes';

// import { StorageProvider } from '../../api/storage.provider';

@Component({
  selector: 'app-payment-collect-dialog',
  template: `
  <app-payment-collect-dialog class='declarationdialog' >


  <div class="alert_dialog">
  <ion-grid class="gridcss">
    <ion-row justify-content-center>
      <p class="contentcolor" id="declare_head">Enter the amount collected from the Customer</p>
    </ion-row>
    <ion-row justify-content-center class="input_row">

    <input type="text" class="form-control" class="inputstyle ml-5" (input)="onSearchChange($event.target.value)">

    </ion-row>

    <ion-row justify-content-center style="justify-content:center">
    
 
     <a><button type="button" (click)="closeAlert()" class="btn mr-3">Cancel</button></a> 

      <a><button type="submit"  (click)="collectpayment()"  class="btn ml-4 yesBtn">Collect</button></a>



    </ion-row>
  </ion-grid>
</div>




 
 
      </app-payment-collect-dialog >

    
 
   
  `,
   styleUrls: ['../alert-dialog/alert-dialog.component.scss'],
})
export class PaymentCollectComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  @Input() data: any;
  amount:any;

  amount1: string = '';
  Ischecked: boolean = false; IsUserexists: boolean = false;
  IsTermsChecked: boolean = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController, private router: Router,
    // public storageProvider: StorageProvider,
    // private authservice: AuthService,
    //  public evAlertController: EvAlertController,
    // public alertController: EvAlertController
    private PaymentService: PaymentService,
  ) {
    
    this.backButtonEvent();
  }
 ngOnInit() {
  }
   handleAmount(event) {
    this.amount=event.target.value
    console.log(this.amount)

    
  }
  onSearchChange(searchValue: string): void {  
   
    this.amount=searchValue;
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });;
          return;
        }
      } catch (error) {
      }

    });
  }
  async closeAlert() {
    try {
      const element = await this.modalController.getTop();
      if (element) {
        element.dismiss().then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }
  // onSubmit() {
  //   return this.amount;
  // }

 

    async collectpayment(){
      
    console.log(this.amount)
    console.log(this.amount1)
    console.log(this.data);

    this.data["value"]=this.amount
    
    console.log(this.data);

    (await this.PaymentService.userPaymentDetails(this.data)).subscribe((response: any) => {
      console.log(response);

      if(response==true){
        Swal.fire({
          icon: 'success',
          title: `د.إ ${this.amount} is Paid`,
        })
        this.closeAlert();
        

       
      }
      // this.closeAlert(response);



    })

  }
}
