import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';

import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { RESPONSE } from './../api/response';
import { AuthService } from './../api/auth.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
}) 
export class TabsPage {
  clickedtab=1; pushes: any = [];selectedTabId = 1;unreadCount = 0;

  constructor(private platform: Platform, 
              private router: Router,
              private storage: Storage,
              // private navCtrl: NavController,
              private route: ActivatedRoute,public authService: AuthService) {
    this.backButtonEvent();
    this.platform.ready()
      .then(async () => {
      });
  }


  tabClicked(tabId: number) {
    this.selectedTabId = tabId;
  }
  

  logout() {
    localStorage.clear();
    this.storage.clear();
    this.router.navigate(['/login']);
  
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(()=>{
      console.log ('exit');
      navigator['app'].exitApp();
    })
  }


}
