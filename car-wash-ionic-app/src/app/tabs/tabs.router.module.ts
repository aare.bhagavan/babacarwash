import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'jobs',
        children: [
          {
            path: '',
            loadChildren: '../jobs/jobs.module#JobsPageModule'
          }
        ]
      },
      {
        path: 'payments',
        children: [
          {
            path: '',
            loadChildren: '../payments/payments.module#PaymentsPageModule'
          }
        ]
      },
      {
        path: 'onetime',
        children: [
          {
            path: '',
            loadChildren: '../one-time-wash-registration/one-time-wash-registration.module#OneTimeWashRegistrationPageModule'
          }
        ]
      },
      {
        path: 'logout',
        pathMatch:'full',
        redirectTo: '../login'
      },
      {
        path: 'tabs',
        redirectTo: '/tabs/jobs',
        pathMatch: 'full'
      }

    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
