import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
// import { ROUTE } from '../../routes';
// import { AuthService } from '../../api/auth.service';
// import { EvAlertController } from '../alert.controller';
// import { RESPONSE } from '../../api/response';
// import { StorageProvider } from 'src/app/api/storage.provider';

@Component({
    selector: '',
    template: `
    <app-alert-dialog [header]="header" [icon]="icon">
        <ion-grid>
            <ion-row justify-content-center>
                <h3 id="alert-message" class="contentcolor" [innerHTML]="message"></h3> 
            </ion-row>
            <ion-row justify-content-center>
                <ion-button (click)="closeAlert()"> Cancel </ion-button>
                <ion-button (click)="logout()"> Okay </ion-button>
            </ion-row>
        </ion-grid>
    </app-alert-dialog>
  `,
    styleUrls: ['./alert-dialog.component.scss'],
})
export class PaymentDialogComponent implements OnInit {

    @Input() header: any;
    @Input() message: any;
    @Input() icon: any;
    @Input() data: any;

    otpvalue: number;
    constructor(
        public modalController: ModalController,
        public navCtrl: NavController,
        // public authService: AuthService,
        // public alertController: EvAlertController,
        // public storageProvider: StorageProvider
    ) {
    }
    ngOnInit() {
    }
    async closeAlert() {
        try {
            const element = await this.modalController.getTop();
            if (element) {
                element.dismiss().then(() => {
                });
                return;
            }
        } catch (error) {
        }
    }
    // async logout() {
    //     this.storageProvider.getalltags().then(result => {
    //         let keys = result.filter(key => key != 'data');
    //         keys = keys.filter(key => key != 'Android Version');
    //         keys = keys.filter(key => key != 'IOS Version');
    //         console.log("LogoutDialogComponent -> logout -> keys", keys)
    //         this.storageProvider.clear(keys);
    //     });
    //     this.navCtrl.navigateBack(ROUTE.LOGIN);
    //     this.closeAlert();
    // }
}
