import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, MasterRequest, BusinessRequest, AuthRequest, AreaRequest, SymptomsRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Injectable({
    providedIn: 'root'
})
export class MastersService extends NetworkService {

    constructor(
        protected httpClient: HttpClient,
        public spinner: NgxSpinnerService
    ) {
        super(httpClient,
            spinner
        );
    }
    async getLocationsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_LOCATIONS);
        return this.request(request);
    }
    async getUsersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_USERS);
        return this.request(request);
    }
    async getAllCustomer() {
        const request = new REQUEST(MasterRequest.GET_ALL_CUSTOMERS);
        return this.request(request);
    }
    async getBuildingsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_BUILDINGS);
        return this.request(request);
    }
    async addNewBuilding(data: any) {
        const request = new REQUEST(MasterRequest.ADD_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async addCustomerDetails(data: any, weekDays: any, vehicles: any, month: number, year: number,scheduleType:string) {
        vehicles.forEach(vehicle => {
            vehicle.startDate=moment(vehicle.startDate).format("DD-MM-YYYY")
        });
        data = {
            custDetails: data,
            weekDays: weekDays,
            vehicles: vehicles,
            month: month,
            year: year,
            scheduleType:scheduleType
        }
        const request = new REQUEST(MasterRequest.ADD_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editBuilding(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async getWorkersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_WORKERS);
        return this.request(request);
    }
    // async getEditBuildings() {
    //     const request = new REQUEST(MasterRequest.GET_EDIT_BUILDINGS);
    //     return this.request(request);
    // }
    async addNewWorker(workerdata: any,user_name,workerBuildingData,date) {
        const data = {
            'workerData':workerdata,
            'user_name':user_name,
            'building_ids':workerBuildingData,
            'start_date':date,
        }
        const request = new REQUEST(MasterRequest.ADD_NEW_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editWorker(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteBuilding(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteWorker(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }

    async deleteUser(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_USER, data, REQUEST.POST);
        return (await this.request(request))
    }

    async getAllJobs(user_id: number) {
        const data = {
            user_id: user_id
        }
        const request = new REQUEST(MasterRequest.GET_ALL_JOBS, data, REQUEST.POST);
        return (await this.request(request))
    }
    async updateWorkStatus(details:any) {
        let data = {
            schedule_id: details.schedule_id,
            date:details.date.toDateString()
        }
        const request = new REQUEST(MasterRequest.UPDATE_WORK_STATUS,data, REQUEST.POST);
        return this.request(request);
    }
}



    // async getAllBusinessList() {
    //     const request = new REQUEST(BusinessRequest.GET_ALL_BUSINESS_LIST);
    //     return this.request(request);
    // }

//     async getDepartmentList(role: string, org_id: any) {
//         let roleData = {
//             "role": role,
//             "org_id": org_id,

//         };
//         const request = new REQUEST(MasterRequest.GET_ALL_DEPARTMENTS, roleData, REQUEST.POST);
//         return this.request(request);
//     }

//     async getDesignationName(designation_id: number) {
//         const data = {
//             designation_id: designation_id
//         }
//         const request = new REQUEST(MasterRequest.GET_DESIGNATION_NAME_BY_DEPARTMENT_ID, data, REQUEST.POST);
//         return this.request(request);
//     }

//     async getDesignationBasedOnDepartmentID(department_id: number) {
//         const data = {
//             department_id: department_id
//         };
//         const request = new REQUEST(MasterRequest.GET_DESIGNATION_BY_DEPARTMENT_ID, data, REQUEST.POST);
//         return this.request(request);
//     }
//     //Run PayRoll
//     async payRollRun(dept_id:number,month:number,year:number,dzc_Ids:any) {
//         const data = {
//            "department_id":dept_id,
//            "month":month,
//            "year":year,
//            "dzc_Ids":dzc_Ids           
//         };
//         const request = new REQUEST(MasterRequest.PAYROLL_RUN, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//       //Cancel PayRoll
//       async cancelPayRoll(id:number) {
//         const data = {
//           "id":id           
//         };
//         const request = new REQUEST(MasterRequest.CANCEL_PAYROLL, data, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async getOrganizationsList() {
//         const request = new REQUEST(MasterRequest.GET_ALL_ORGANIZATIONS);
//         return this.request(request);
//     }
//     // async getWagesList() {
//     //     const request = new REQUEST(MasterRequest.GET_ALL_WAGES);
//     //     return this.request(request);
//     // }
//     async getPayRollRunLogList() {
//         const request = new REQUEST(MasterRequest.GET_PAYROLL_RUN_LOG);
//         return this.request(request);
//     }
//     // async getPayRollRunMaxYear() {
//     //     const request = new REQUEST(MasterRequest.GET_PAYROLL_RUN_MAX_YEAR);
//     //     return this.request(request);
//     // }

//     async getUsersList(role: string, org_id: any) {
//         let roleData = {
//             "role": role,
//             "org_id": org_id,

//         };
//         const request = new REQUEST(MasterRequest.GET_USERSLIST,roleData, REQUEST.POST);
//         return this.request(request);
//     }
//     async getGenerateStatements() {
//         const request = new REQUEST(MasterRequest.GET_GENERATED_STATEMENTS);
//         return this.request(request);
//     }
//     async getRolesList() {
//         const request = new REQUEST(MasterRequest.GET_ALL_ROLES);
//         return this.request(request);
//     }

//     async getZoneList() {
//         // let roleData = {
//         //     "role": role,
//         //     "org_id": org_id,

//         // };
//         const request = new REQUEST(MasterRequest.GET_ALL_ZONES);
//         return this.request(request);
//     }
//     async getDesignationsList() {

//         const request = new REQUEST(MasterRequest.GET_ALL_DESIGNATIONS);
//         return this.request(request);
//     }

//     async getCircleList() {
//         // const roleData = {
//         //     "role": role,
//         //     "org_id": org_id,

//         // };
//         const request = new REQUEST(MasterRequest.GET_ALL_CIRCLES);
//         return this.request(request);
//     }


//     async getAllDZCList(org_id:number,role:any) {
//         const getdata={
//             "org_id":org_id,
//             "role":role,
//         };
//         const request = new REQUEST(MasterRequest.GET_ALL_DZC_LIST,getdata,REQUEST.POST);
//         return this.request(request);
//     }

//     async getCirclebyId(id: number) {
//         const data = {
//             'id': id
//         }
//         const request = new REQUEST(MasterRequest.GET_CIRCLE, data, REQUEST.POST);
//         return this.request(request);
//     }

//     async editZone(zone: string, id: number) {
//         const data = {
//             "name": zone,
//             "id": id,

//         };
//         const request = new REQUEST(MasterRequest.EDIT_ZONE, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async getGroupList(depId: number) {
//         const data = {
//             "department_id": depId
//         };
//         const request = new REQUEST(MasterRequest.GET_GROUPLIST_BY_DEP_ID, data, REQUEST.POST);
//         return this.request(request);
//     }
//     async addZone(data: any) {
//         const newZone = {
//             "name": data.zone,
//             // 'org_id': data.organization
//         };
//         const request = new REQUEST(MasterRequest.ADD_ZONE, newZone, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async validateAddPayroll(dept_id:number,month:number,year:number) {
//         const data = {
//            "department_id":dept_id,
//            "month":month,
//            "year":year,           
//         };
//         const request = new REQUEST(MasterRequest.VALIDATE_ADD_PAYROLL, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async addNewOrganization(fields:any) {
//         const data = {

//            org_name:fields.org_name
//         }
//         const request = new REQUEST(MasterRequest.ADD_NEW_ORGANIZATION, data, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async updateWorker(designation_id: number, pf_no: any) {
//         const data = {
//             "designation_id": designation_id,
//             'pf_no': pf_no
//         };
//         const request = new REQUEST(MasterRequest.UPDATEWORKER_DESIGNATION, data, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async editCircle(circledata: any, id: number) {
//         const data = {
//             "name": circledata.name,
//             'org_id': circledata.organization,
//             'description': circledata.description,
//             "id": id,

//         }
//         const request = new REQUEST(MasterRequest.EDIT_CIRCLE, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async addNewCircle(data: any) {
//         const request = new REQUEST(MasterRequest.ADD_CIRCLE, data, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async addNewDZC(fields: any) {
//         const data = {
//             department_id: fields.department_id,
//             zone_id: fields.zone_id,
//             circle_id: fields.circle_id
//         }
//         const request = new REQUEST(MasterRequest.ADD_NEW_DZC, data, REQUEST.POST);
//         return (await this.request(request))
//     }


//     async editDZC(fields: any, id: number) {
//         const data = {
//             department_id: fields.department_id,
//             zone_id: fields.zone_id,
//             circle_id: fields.circle_id,
//             dzc_id: id
//         }
//         const request = new REQUEST(MasterRequest.EDIT_DZC, data, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async addNewUser(fields: any) {
//         const data = {

//             email:fields.email,
//             name:fields.name,
//             phone:fields.phone,
//             password:fields.password,
//             role_id:fields.role_id,
//             org_id:fields.organization_id
//         }
//         const request = new REQUEST(MasterRequest.ADD_NEW_USER, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async editOrganization(fields: any) {
//         const data = {
//            org_id:fields.org_id,
//             org_name:fields.org_name

//         }
//         const request = new REQUEST(MasterRequest.EDIT_ORGANIZATION ,data, REQUEST.POST);
//         return (await this.request(request))
//     }


//     async deleteCircle(id: number) {
//         let data = {
//             'id': id
//         }
//         const request = new REQUEST(MasterRequest.DELETE_CIRCLE, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async deleteOrganization(id:number) {
//         const data = {
//             id:id

//          }
//         const request = new REQUEST(MasterRequest.DELETE_ORGANIZATION, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async deleteUser(id:number) {
//         const data = {
//             id:id

//          }
//         const request = new REQUEST(MasterRequest.DELETE_USER, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async deleteDesignation(id:number) {
//         const data = {
//             id:id

//          }
//         const request = new REQUEST(MasterRequest.DELETE_DESIGNATION, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async deleteZone(id: number) {
//         let data = {
//             'id': id
//         }
//         const request = new REQUEST(MasterRequest.DELETE_ZONE, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async deleteDepartment(id: number) {
//         let data = {
//             'id': id
//         }
//         const request = new REQUEST(MasterRequest.DELETE_DEPARTMENT, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async addDesignation(data: any) {
//         const newDesignation = {
//             "name": data.designation,
//             'department_id': data.department,
//             'salary': data.salary
//         };
//         const request = new REQUEST(MasterRequest.ADD_DESIGNATION, newDesignation, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async editDepartment(data: any) {
//         const departmentData = {
//             "name": data.name,
//             "id": data.id,

//         };
//         const request = new REQUEST(MasterRequest.EDIT_DEPARTMENT, departmentData, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async editUser(data: any) {
//         const userData = {
//             "id": data.id,
//             "user_name": data.name,
//             "email":data.email,
//             "phone":data.phone,
//             "password":data.password,
//             "role_id":data.role_id,
//             "org_id":data.org_id
//         };
//         const request = new REQUEST(MasterRequest.EDIT_USER, userData, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async editDesignation(data: any) {
//         const designationData = {
//             "id": data.id,
//             "name":data.name,
//             "department_id":data.department_id,
//             "salary":data.salary

//         };
//         const request = new REQUEST(MasterRequest.EDIT_DESIGNATION, designationData, REQUEST.POST);
//         return (await this.request(request))
//     }

//     async addDepartment(data: any) {
//         const request = new REQUEST(MasterRequest.ADD_DEPARTMENT, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async downloadPFandESI(id: number) {
//         let data = {
//             'pay_roll_run_id':id
//         }
//         const request = new REQUEST(MasterRequest.DOWNLOAD_PF_ESI, data, REQUEST.POST);
//         return (await this.request(request))
//     }
//     async getDocumentsProcessList() {
//         const request = new REQUEST(MasterRequest.GET_ALL_DOCUMENTS_PROCESS);
//         return this.request(request);
//     }
//     async deleteDocument(id: number) {
//         let data = {
//             'id': id
//         }
//         const request = new REQUEST(MasterRequest.DELETE_DOCUMENT, data, REQUEST.POST);
//         return (await this.request(request))
//     }

// }
