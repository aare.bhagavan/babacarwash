export class RESPONSE {

    static readonly SUCCESS = true;
    static readonly ERROR = 'Error';

    // constructor() {}
    public s: boolean; // status
    public m: string; // message
    public message: string; // message
    public d: any; // data
    public role: string; // Role
    public status: boolean;
    public Data: any;
    public StatusMessage: string;
    public _isScalar: boolean;
    public business_id: string;
    public id: string;
    public address: any;
    public ok : boolean;
    public numOfRecords:number;
  success: boolean;

    public isSuccess(): boolean {
        return this.s === RESPONSE.SUCCESS;
    }
    public getData(): any {
        return this.d;
    }
}
