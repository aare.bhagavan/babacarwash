import { Injectable } from '@angular/core';
import { REQUEST, PaymentRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({
    providedIn: 'root'
})
export class PaymentService extends NetworkService {

    constructor(protected httpClient: HttpClient, public spinner: NgxSpinnerService,) {
        super(httpClient, spinner);
    }

    async getAllPaymentDetails() {
        const request = new REQUEST(PaymentRequest.GET_ALL_PAYMENT_DETAILS);
        return this.request(request);
    }

    async userPaymentDetails(data: any) {
        console.log(data);
        var user_name=data.user_name;
        var value=data.value;
        
       
        
        let token = localStorage.getItem("token");
        const request = new REQUEST(PaymentRequest.GET_USER_PAYMENT_DETAILS,{data,token,user_name,value}, REQUEST.POST);
        return this.request(request);
    }

    async getuserpayment(data: number) {
        let datajson={"id":data}
        const request = new REQUEST(PaymentRequest.GET_USER_PAYMENT,datajson, REQUEST.POST);
        return this.request(request);
    }

}