import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OneTimeWashRegistrationPage } from './one-time-wash-registration.page';

describe('OneTimeWashRegistrationPage', () => {
  let component: OneTimeWashRegistrationPage;
  let fixture: ComponentFixture<OneTimeWashRegistrationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneTimeWashRegistrationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OneTimeWashRegistrationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
