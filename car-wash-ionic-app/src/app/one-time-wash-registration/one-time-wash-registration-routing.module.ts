import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OneTimeWashRegistrationPage } from './one-time-wash-registration.page';

const routes: Routes = [
  {
    path: '',
    component: OneTimeWashRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OneTimeWashRegistrationPageRoutingModule {}
