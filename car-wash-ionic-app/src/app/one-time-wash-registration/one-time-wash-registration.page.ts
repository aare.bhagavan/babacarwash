import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import Swal from 'sweetalert2';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-one-time-wash-registration',
  templateUrl: './one-time-wash-registration.page.html',
  styleUrls: ['./one-time-wash-registration.page.scss'],
})
export class OneTimeWashRegistrationPage implements OnInit {
  registrationForm: FormGroup;
  minDate = moment(new Date()).format("YYYY-MM-DD");
  user_name;

  constructor(
    public formBuilder: FormBuilder,
    public router: ActivatedRoute,
    public authService: AuthService,
  ) {

    this.user_name = localStorage.getItem('user_name');
  }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      // first_name: ['', Validators.required],
      // last_name: ['', Validators.required],
      // mobile: ['', Validators.required],
      parking_no:['',Validators.required],
      vehicle_no: ['', Validators.required],
     
      amount: ['', Validators.required],
    });
  }
  async register() {
    (await this.authService.oneTimeRegistration(this.registrationForm.value, this.user_name)).subscribe((res:any) => {
      console.log('response....', res)
      if (res == true) {
        Swal.fire({ icon: 'success', title: 'Registration Successful !', });
        this.registrationForm.reset();
      }
    });
    
  }

}
