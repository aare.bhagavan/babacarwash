import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OneTimeWashRegistrationPageRoutingModule } from './one-time-wash-registration-routing.module';

import { OneTimeWashRegistrationPage } from './one-time-wash-registration.page';
import { MainModule } from '../main/main.module';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    MainModule,
    OneTimeWashRegistrationPageRoutingModule
  ],
  declarations: [OneTimeWashRegistrationPage],
  providers:[
    FormBuilder,
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class OneTimeWashRegistrationPageModule {}
