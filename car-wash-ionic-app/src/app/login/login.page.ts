import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthService } from '../api/auth.service';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'app-login',

  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],

})
export class LoginPage implements OnInit {
  isUserLogged: boolean = false;
  showtags: boolean = false;
  loginForm: FormGroup;
  user_id: number;
  jobsstatus:any=true;
  paymentstatus:any=false
  profilestatus:any=false;

  constructor(
    private router: Router,
    public menu: MenuController,
    public authService: AuthService,
    private _formBuilder: FormBuilder,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      number: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  
  }

  async login() {
    console.log(this.loginForm);
   
    (await this.authService.login(this.loginForm.controls['number'].value.toString(), this.loginForm.controls['password'].value)).subscribe((res: any) => {
      console.log(res);
      if (res.success == true) {
        this.user_id = res.user_id;
        this.storage.set('user_id', this.user_id);
        this.storage.set('user_name',res.name);
        localStorage.setItem('token', res.token);
        localStorage.setItem('user_name', res.name);
        localStorage.setItem('login_key',"true");
      
        // localStorage.setItem('user_id',res.user_id);

        // const navigationExtras: NavigationExtras = {
        //   queryParams: {
        //    user_id:this.user_id
        //   }
        // };

        //this.router.navigate(['/jobs'],navigationExtras);
        this.router.navigate(['/tabs/jobs']);
        localStorage.setItem('statuspayments', this.paymentstatus)
        localStorage.setItem('statusprofile',this.profilestatus)
        localStorage.setItem('statusjob',this.jobsstatus)
      } else {
        console.log('Invalid Creds')
        if(res.message=="Invalid Credentials")
        {
          Swal.fire({
            icon: 'error',
            width: 250,
            padding: '4px',
            html:'<h3>Invalid Credentials</h3>',
          
          
         
          })
        }
        else{
          Swal.fire({
            icon: 'error',
            width: 250,
            padding: '4px',
            html:'<h3>Something Went Wrong</h3>',
           })
          
        }
     
      }

    });
  }
  public focusInput (event): void {
    let total = 0;
    let container = null;

    const _rec = (obj) => {

        total += obj.offsetTop;
        const par = obj.offsetParent;
        if (par && par.localName !== 'ion-content') {
            _rec(par);
        } else {
            container = par;
        }
    };
    _rec(event.target);
    setTimeout(() => {
      container.scrollToPoint(0, total - 50, 400);
    }, 500);
  }
}

