import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { ModalComponent } from './modal/modal.component';
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Platform, IonRouterOutlet, AlertController } from '@ionic/angular';
import { ModalController ,ActionSheetController,PopoverController,NavController} from '@ionic/angular';
import { Router ,NavigationExtras} from '@angular/router';
import { CleaningStatusComponent } from './jobs/cleaning_status.dialog';
import { PaymentCollectComponent } from './payments/payment_collect.dialog';


@Injectable({
    providedIn: 'root'
  })
export class EvAlertController {

    
    // change these images accordingly
    // private readonly WARNING_IMG = 'assets/imgs/alert_warning.svg';
    // private readonly SUCCESS_IMG = 'assets/imgs/alert_success.svg';
    // private readonly ERROR_IMG = 'assets/imgs/alert_error.svg';
    private readonly WARNING_IMG = 'assets/imgs/confused.svg';
    private readonly SUCCESS_IMG = 'assets/imgs/happy.svg';
    private readonly ERROR_IMG = 'assets/imgs/sad.svg';

    public dataReturned;
    private modal;

    constructor(   public navCtrl: NavController, private platform: Platform, public modalController: ModalController,public toastController: ToastController) {
        this.backButtonEvent();
    }

    public async showError(header: string, message: string) {
        return this.presentModalError("", message, this.ERROR_IMG);
    }
    public async showSuccess(header: string, message: string) {
        return this.presentModalSuccess("", message, this.SUCCESS_IMG);
    }
    public async showWarning(header: string, message: string) {
        return this.presentModalWarning("", message, this.WARNING_IMG);
    }
    public async showSuccessBack(header: string, message: string,page:string,comingfrom:string,id:string) {
        return this.presentModalWarningBack("", message, this.SUCCESS_IMG,page,comingfrom,id);
    }

    // public async dismissAlert(){
    //     return await this.modal.present();
    // }

    
  backButtonEvent(){
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
            element.dismiss();
            return;
        }
    } catch (error) {
    }

    });
  }

    public async showModal(id: string) {
         return this.presentModalNew(id);
    }
    async presentModalError(header: string, message: string, icon: string) {
        this.modal = await this.modalController.create({
            component: AlertDialogComponent,
            componentProps: {
                header, message,
                icon: this.ERROR_IMG
            },
            showBackdrop: true,
            cssClass: ['auto-height', 'bottom']
        });
        return await this.modal.present();
    }

    async presentModalSuccess(header: string, message: string, icon: string) {
        this.modal = await this.modalController.create({
            component: AlertDialogComponent,
            componentProps: {
                header, message,
                icon: this.SUCCESS_IMG
            },
            showBackdrop: true,
            cssClass: ['auto-height', 'bottom']
        });
        return await this.modal.present();
    }
    async presentModalWarning(header: string, message: string, icon: string) {
        this.modal = await this.modalController.create({
            component: AlertDialogComponent,
            componentProps: {
                header, message,
                icon: this.WARNING_IMG
            },
            showBackdrop: true,
            cssClass: ['auto-height', 'bottom'],
        });
        return await this.modal.present();
    }

    async presentModalWarningBack(header: string, message: string, icon: string,page:string,comingfrom:string,id:string) {
        this.modal = await this.modalController.create({
            component: AlertDialogComponent,
            componentProps: {
                header, message,
                icon: this.SUCCESS_IMG
            },
            showBackdrop: true,
            cssClass: ['auto-height', 'bottom']
        });
        this.modal.onDidDismiss().then(() => {
            console.log("comingfrom",comingfrom);
                if(comingfrom == 'packages'){
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                          ServiceID: id,
                          comingfrom:comingfrom
                        }
                      };
                      this.navCtrl.navigateForward(['/home-details'], navigationExtras);
                }else if(comingfrom == 'event'){
                    let navigationExtras: NavigationExtras = {
                        queryParams: {
                            eventId: id,
                          comingfrom:comingfrom
                        }
                      };
                      this.navCtrl.navigateForward(['/event-details'], navigationExtras);
                }
          });
        // this.modal.onDidDismiss().then(() => {
        //       let navigationExtras: NavigationExtras = {
        //         queryParams: {
        //           ServiceID: '135',
        //           comingfrom:'packages'
        //         }
        //       };
        //       this.navCtrl.navigateForward(['/home-details'], navigationExtras);
        //     }
        //   });
        return await this.modal.present();
    }

    async presentModalNew(id: string ) {
        
        const modal = await this.modalController.create({
            component: ModalComponent,
            componentProps: {
                id
            },
            showBackdrop: true,
            // duration: 8000,
            // backdropDismiss :false,
            cssClass: ['auto-height', 'bottom']
        });

        // modal.onDidDismiss().then((dataReturned) => {
        //     if (dataReturned !== null) {
        //       this.dataReturned = dataReturned.data;
        //       console.log('Modal Sent Data :'+ JSON.stringify(dataReturned));
        //     }
        //   });
          return await modal.present();
        //  return await this.dataReturned;
    }
    public async showConfirmationDialog(data) {
        this.modal = await this.modalController.create({
            component: CleaningStatusComponent,
            componentProps: {
                header: "StatusConfirmation",
                message: "",
                data: data,
                // icon: this.SUCCESS_IMG,
                Ischecked: true
            },
            showBackdrop: true,
            backdropDismiss: false,
            cssClass: ['auto-height', 'bottom']
        });
        await this.modal.present();
        const outputData = await this.modal.onDidDismiss();
        return outputData;
    }
    public async PaymentCollectDialog(data) {
        this.modal = await this.modalController.create({
            component: PaymentCollectComponent,
            componentProps: {
                header: "StatusConfirmation",
                message: "",
                 data: data,
                // icon: this.SUCCESS_IMG,
                Ischecked: true
            },
            showBackdrop: true,
            backdropDismiss: false,
            cssClass: ['auto-height', 'bottom']
        });
        await this.modal.present();
        const outputData = await this.modal.onDidDismiss();
        return true;
    }


}
