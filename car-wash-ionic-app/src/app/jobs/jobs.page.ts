import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray,
  AbstractControl
} from "@angular/forms";
import { element } from 'protractor';
import { ModalController } from '@ionic/angular';
import { CleaningStatusComponent } from './cleaning_status.dialog';
import { EvAlertController } from '../alert.controller';
import Swal from 'sweetalert2';
import { MastersService } from '../api/masters.service';
import { NavigationExtras, ActivatedRoute } from '@angular/router';
import { build$ } from 'protractor/built/element';
import * as _ from "underscore";
import { Storage } from '@ionic/storage';
import * as moment from 'moment';



@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.page.html',
  styleUrls: ['./jobs.page.scss'],
})
export class JobsPage implements OnInit {
  myControl = new FormControl();
  authForm: FormGroup;
  jobsarray: any;
  show_btn: boolean = false;
  today: any;
  building: any;
  color: any;
  location: any;
  status: any;
  user_id;
  allJobs: any = [];
  allBuildings: any = [];
  buildingData: any;
  isDialogPresent: boolean = true;
  buildingsArray: any = [];
  searchValue:string='';

  constructor(public fb: FormBuilder,
    public modalController: ModalController,
    public alertController: EvAlertController,
    public mastersService: MastersService,
    private router: ActivatedRoute,
    private storage: Storage

  ) {
    // this.router.queryParams.subscribe(params => {
    //   this.user_id = params.user_id
    // });
    //  this.getUser_id();

    this.getAllJobs();
  }


  date: Date = new Date();
  tomorrow: Date = new Date(this.date.getTime() + (1000 * 60 * 60 * 24));
  // jobs: any = [
  //   { 'day': 'TODAY', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'location': 'JublieHills', 'building': 'Building3010,Block-C', 'parking': '123', 'status': 'Pending', 'toggle': false, 'jobstatus': "true" },
  //   { 'day': 'TODAY', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "susmitha", 'carnumber': "DU-12-99", 'location': 'Secundrabad', 'building': 'Building007,A-Block', 'parking': '0923', 'status': 'Pending', 'toggle': false, 'jobstatus': "true" },
  //   { 'day': 'YESTERDAY', 'date': '02 Aug 2020', 'car': "AURD", 'customer': "Shaik Abdul", 'carnumber': "DU-55-44", 'location': 'BanjaraHills', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Completed', 'toggle': true, 'jobstatus': "true" },
  //   // { 'day': '', 'date': '01 Aug 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-43", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '0909', 'status': 'Completed', 'toggle': false, 'jobstatus': "true" },
  //   // { 'day': '', 'date': '01 Aug 2020', 'car': "BENZ", 'customer': "Mirza Baig", 'carnumber': "DU-55-43", 'location': 'BanjaraHills,Hyderabad', 'building': 'Building200,A-towers', 'parking': '09839', 'status': 'Completed', 'toggle': false, 'jobstatus': "true" },


  // ];
  jobs: any = [
    {
      'building': 'Building-1',
      'location': 'JublieHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-2',
      'location': 'BanjaraHills',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-3',
      'location': 'Hi-Tech City',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    },
    {
      'building': 'Building-4',
      'location': 'Panjagutta',
      'vehicles': [
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "HM-12-24", 'parking': '123', 'status': 'Assigned', 'jobstatus': "true" },
        { 'day': 'Today', 'date': '06 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "DU-12-20", 'parking': '903', 'status': 'Completed', 'jobstatus': "true" },
        { 'day': 'Yesterday', 'date': '05 Aug 2020', 'car': "BMW", 'customer': "Rizwan", 'carnumber': "Du-10-99", 'parking': '765', 'status': 'Pending', 'jobstatus': "true" }
      ]
    }



  ];



  count: number = 0;
  filter(value) {
    this.location = this.jobs
    this.status = true
    console.log("inside function")
    // const value=ev.target.value;
    if (value != null || value != "") {
      this.location = this.jobs.filter(device => (
        (device.building.toLowerCase().includes(value.toLowerCase()) || device.location.toLowerCase().includes(value.toLowerCase()
        ))
      )
      )
    }
    else {
      this.location = this.jobs


    }
  }
  ngOnInit() {
   
    this.today = false
    this.building = false
    this.jobsarray = this.jobs
    this.count = this.jobs.length;
    this.myControl.valueChanges.subscribe(value => {
      this.filter(value)
    })

  }
  // async getUser_id(){
  //    await this.storage.get('user_id').then((user_id) => {
  //     this.user_id = user_id;
  //   });
  // }
  Today() {
    this.building = false
    this.today = !this.today
    this.color = "red"

  }

  Building() {
    this.building = !this.building
    this.today = false
    this.buildingsArray = this.allJobs;
    this.buildingsArray.filter(building=>building.building_id==this.searchValue);
    console.log(this.buildingsArray)

  }
  loadData(event) {
    if (event.status == "Completed") {
      this.jobs.forEach(element => {

        if (element.carnumber == event.carnumber) {
          element.status = "Pending";
        }
      });
    } else {
      this.jobs.forEach(element => {
        if (element.carnumber == event.carnumber) {
          element.status = "Completed";
        }
      });
    }
  }
  show_button() {
    console.log("show button")
    this.show_btn = true;
    console.log(this.show_btn)

  }
  async presentModal(schedule_id,schedule_date) {
    console.log("inside")
    this.isDialogPresent = false;
    let data = {
      'schedule_id': schedule_id,
      'date': moment(schedule_date, 'DD-MM-YYYY').toDate()
      // moment(schedule_date).format('YYYY-MM-DD')
    }
    const res = await this.alertController.showConfirmationDialog(data);
    console.log(res);
    if (res) {
      this.isDialogPresent = true;
      this.getAllJobs();
    }

  }
  async getAllJobs() {
    await this.storage.get('user_id').then(async (user_id) => {
      this.user_id = user_id;
      console.log(this.user_id);
      (await this.mastersService.getAllJobs(this.user_id)).subscribe((response: any) => {
        console.log("all jobs", response)
        //this.allJobs=response;
        let uniqueArray = _.map(_.groupBy(response, (obj: any) => {
          return obj.building_id;
        }), (grouped) => {
          return grouped[0];
        })

        //console.log(uniqueArray);



        uniqueArray.forEach(building => {
          building['vehicles'] = []
          let vehiclesData = response.filter(eachvehicle => eachvehicle.building_id == building.building_id);

          vehiclesData.forEach((vehicle, index) => {
            if ((new Date(vehicle.date).toLocaleDateString() == this.date.toLocaleDateString()) && (vehicle.status != 'completed')) {
              vehicle.status = 'Assigned';
            }

            let obj = {
              carnumber: vehicle.registration_no,
              parking_no: vehicle.parking_no,
              status: vehicle.status,
              date: new Date(vehicle.date).toLocaleDateString(),
              schedule_id: vehicle.schedule_id
            }
            building.vehicles.push(obj)

          });

        });
        console.log(uniqueArray);
        this.allJobs = uniqueArray;


      });



    });

  }
}
