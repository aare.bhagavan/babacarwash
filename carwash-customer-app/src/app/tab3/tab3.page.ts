
import { Component, ViewChild, Inject, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FormControl, Validators, FormGroup, FormBuilder, AbstractControl } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, IonSlides } from '@ionic/angular';
import { RouterModule, Routes, Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  ProfileForm: FormGroup;
  maskedPhone = "******";
  data: any;
  otp: any;
  showResend: boolean;
  timer: number;data1:any;phone:any;reqform:any;

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private fb: FormBuilder,
    private platform: Platform,
    public loadingCtrl: LoadingController,
    public authService: AuthService, private router: ActivatedRoute) {
      
      this.router.queryParams.subscribe(params => {
        console.log("submit otp params", params);
        this.phone = params.phone;
        this.otp = params.otp;
        this.reqform = params.reqform;
        this.maskedPhone = this.phone.substr(0, 1) + "******" + this.phone.substr(6, 4);
      });

    }

    ngOnInit() {
      this.ProfileForm = this.fb.group({
        'otp': ['', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(6), Validators.maxLength(6)]]
      });
      this.startTimer();
     }

     async forgotpassword() {
      this.navCtrl.navigateBack('/tabs/tab2');
    }

    async submitOtp() {
      console.log("otp entered",this.ProfileForm.value.otp,"otp db",this.otp);
      (await this.authService.verifyOtp(this.phone,this.ProfileForm.value.otp,this.reqform)).subscribe(async (res: any) => {
        console.log("res",res);
        // this.otp = res.data.otp
        if (res.status == true) {
          const navigationExtras: NavigationExtras = {
            queryParams: {
              phone: this.phone,
              otp: this.ProfileForm.value.otp,
              reqform: this.reqform
            }
          };
          if(this.reqform == 'signupform'){
            this.navCtrl.navigateForward(['/signup'],navigationExtras);
          }else{
            this.navCtrl.navigateForward(['/resetpassword'],navigationExtras);
          }
        } else {
          const alert = await this.alertCtrl.create({
            header: 'Message',
            message: res.message,
          });
          await alert.present();
          setTimeout(() => alert.dismiss(), 1000);
        }
      });
    }
  
    
  
    startTimer() {
      this.timer = 10;
      this.showResend = false;
      const intervalId = setInterval(() => {
        this.timer--;
        if (this.timer <= 0) {
          clearInterval(intervalId);
          this.enableResend();
        }
      }, 1000);
    }
  
    enableResend() {
      console.log("enabing resend");
      this.showResend = true;
    }
    // resendotp:boolean =false;
    async resendOTP() {
      // this.resendotp = true;
      if(this.reqform == 'signupform'){
        (await this.authService.verifyUser(this.phone,this.reqform,true)).subscribe(async (res: any) => {
          console.log("res",res);
          this.otp = res.data.otp
          if (res.status == true) {
            const alert = await this.toastCtrl.create({
              message: this.otp + ' - OTP sent to Registered Mobile Number',
              duration: 5000
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 1000);
          } else {
            const alert = await this.alertCtrl.create({
              header: 'Message',
              message: res.message,
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 1000);
          }
        });
      }
      else{
        (await this.authService.generateOtp(this.phone,this.reqform)).subscribe(async (res: any) => {
          console.log("res",res);
          this.otp = res.otp
          if (res.status == true) {
            const alert = await this.toastCtrl.create({
              message: this.otp + ' - OTP sent to Registered Mobile Number',
              duration: 5000
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 1000);
          } else {
            const alert = await this.alertCtrl.create({
              header: 'Message',
              message: res.message,
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 1000);
          }
        });
      }
      
  
      //timer will restart after pressing "resend"
      this.showResend = true;
      this.startTimer();
    }

}
