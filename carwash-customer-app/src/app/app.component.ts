import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from './storage.service';
import { MastersService } from './api/masters.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public loginKey: string;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private spinner: NgxSpinnerService,
    private storageService: StorageService,
    private mastersService: MastersService

  ) {
    this.initializeApp();
    this.verifytoken();
    this.getContactDetails();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.spinner.show();
 
    setTimeout(() => {
      /** spinner ends after 5 seconds */
      this.spinner.hide();
    },3000)
  }


  async getContactDetails() {
    (await this.mastersService.getContactDetails()).subscribe((response: any) => {
      console.log("🚀 ~ file: enquired-customers.component.ts ~ line 150 ~ EnquiredCustomersComponent ~ response", response)
    });
  }
  async verifytoken() {
    console.log("inside function")
    this.loginKey =  this.storageService.isLoggedIn();
    console.log(this.loginKey)
    if (this.loginKey == "true") {
      this.router.navigate(['/tabs'], {});
    }
    else {
      this.router.navigate(['/'], {});
      // this.router.navigate(['/login'], { state: { updateInfos: true } });
    }
  }
}
