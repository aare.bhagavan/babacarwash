
import { Component, ViewChild, Inject, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { FormControl, Validators, FormGroup, FormBuilder, FormGroupDirective, NgForm, AbstractControl } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, IonSlides } from '@ionic/angular';
import { RouterModule, Routes, Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { AuthService } from '../api/auth.service';
import Swal from 'sweetalert2';

// import { ErrorStateMatcher } from '@angular/material/core';

// export class MyErrorStateMatcher implements ErrorStateMatcher {
//   isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
//     const invalidCtrl = !!(control && control.invalid && control.parent.dirty);
//     const invalidParent = !!(control && control.parent && control.parent.invalid && control.parent.dirty);

//     return (invalidCtrl || invalidParent);
//   }
// }
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.page.html',
  styleUrls: ['./resetpassword.page.scss'],
})
export class ResetpasswordPage implements OnInit {

  ProfileForm: FormGroup;
  passwordVisible = false;
  confirmPasswordVisible = false;
  passwordtext = 'password';
  confirmPasswordtext = 'password';
  // matcher = new MyErrorStateMatcher();
  dataToSubmit: any;
  phone: any;
  otp: any;
  form: String = '';

  constructor(public navCtrl: NavController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private fb: FormBuilder,
    public loadingCtrl: LoadingController,
    private platform: Platform,
    public authService: AuthService,public route: Router,
    private router: ActivatedRoute) { 

      this.router.queryParams.subscribe(params => {
        console.log("reset pwd params", params);
        this.phone = params.phone;
        this.otp = params.otp
        this.form = params.reqform;
      });

    }

  ngOnInit() {
    this.ProfileForm = this.fb.group({
      'password': ['', [Validators.required]],
      'password2': ['', Validators.required],
    }, { validator: this.checkPasswords });
  }

  async submitOtp() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        phone: this.phone,
        otp: this.otp
      }
    };
    this.navCtrl.navigateBack(['/submitotp'], navigationExtras);
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.controls.password.value;
    let confirmPass = group.controls.password2.value;

    return pass === confirmPass ? null : { notSame: true }
  }

  async submitPassword() {
    if(this.ProfileForm.value.password == this.ProfileForm.value.password2){
      this.dataToSubmit = { 'password': this.ProfileForm.value.password, 'phone': this.phone };
    (await this.authService.generateResetPwd(this.dataToSubmit)).subscribe(async (res: any) => {
      console.log("res", res);
      if (res.status == true) {
        Swal.fire({
          icon: 'success',
          title: "Password Updated Succesfully..",
          timer: 3000
        });
        this.route.navigate(['/']);
      } 
      else {
        Swal.fire({
          icon: 'error',
          title: 'Error',
          text: res.message,
        });
      }
    });
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Password and confirm password should be same',
      });
    }
  }

  async passwordVisibility(name, value, type) {
    if (name == 'password') {
      this.passwordVisible = value;
      this.passwordtext = type;
    } else if (name == 'confirm') {
      this.confirmPasswordVisible = value;
      this.confirmPasswordtext = type;
    }
  }

}
