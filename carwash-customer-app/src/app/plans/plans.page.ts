import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { PaymentService } from '../api/payment.service';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.page.html',
  styleUrls: ['./plans.page.scss'],
})
export class PlansPage implements OnInit {

  readonly TYPE_SEDAN = "sedan";
  readonly TYPE_4X4 = "4x4";
  selectedChip: string;
  selectedPlan :any = {
    selectedVehicleType: {},
    selectedSubscriptionType: ''
  }
  queryParamData;

  selectedPlanName : string;
  plans=[];
  // plans = [
  //   monthly: {
  //     {
  //       name: "basic",
  //       price: {
  //         sedan: 60,
  //         fourByFour: 60
  //       },
  //       frequency: 5,
  //       weeklyFrequency: 1,
  //       weeklyFrequencyText: "once",
  //       id:"opwm"
  //     },
  //     {
  //       name: "basic",
  //       price: {
  //         sedan: 100,
  //         fourByFour: 120
  //       },
  //       frequency: 10,
  //       weeklyFrequency: 2,
  //       weeklyFrequencyText: "twice",
  //       id:"tpwm"
  //     },
  //     {
  //       name: "basic",
  //       price: {
  //         sedan: 60,
  //         fourByFour: 60
  //       },
  //       frequency: 5,
  //       weeklyFrequency: 1,
  //       weeklyFrequencyText: "thrice",
  //       id:"thpwm"
  //     },
  //   }
    
  // ]
  // plans = [
  //   {
  //     subscriptionType: "Monthly Subscription",
  //     plantype:[
  //       {
  //         name: "basic",
  //         price: {
  //           sedan: 60,
  //           fourByFour: 80
  //         },
  //         frequency: 5,
  //         weeklyFrequency: 1,
  //         weeklyFrequencyText: "Once per week",
  //         id:"opwm"
  //       },
  //       {
  //         name: "silver",
  //         price: {
  //           sedan: 100,
  //           fourByFour: 120
  //         },
  //         frequency: 10,
  //         weeklyFrequency: 2,
  //         weeklyFrequencyText: "Twice per week",
  //         id:"tpwm"
  //       },
  //       {
  //         name: "gold",
  //         price: {
  //           sedan: 120,
  //           fourByFour: 150
  //         },
  //         frequency: 14,
  //         weeklyFrequency: 1,
  //         weeklyFrequencyText: "Thrice per week",
  //         id:"thpwm"
  //       },
  //       {
  //         name: "platinum",
  //         price: {
  //           sedan: 150,
  //           fourByFour: 180
  //         },
  //         frequency: 26,
  //         weeklyFrequency: 1,
  //         weeklyFrequencyText: "Thrice per week",
  //         id:"qupwm"
  //       },
  //     ]
  //   },
  //   {
  //     subscriptionType: "One Time Wash (Mobile Service Only)",
  //     plantype :[
  //       {
  //         name: "basic",
  //         price: {
  //           sedan: 30,
  //           fourByFour: 60
  //         },
  //         frequency: 0,
  //         weeklyFrequency: 0,
  //         weeklyFrequencyText: "One time only",
  //         id:"opws"
  //       },
  //     ]
  //   }
  // ]


  
  color: any;
  constructor(
    private router: Router,
    private paymentService: PaymentService,
    private _ngZone: NgZone,
    private activatedRoute: ActivatedRoute
  ) {
    this.selectedChip = this.TYPE_SEDAN;
    this.activatedRoute.queryParams.subscribe(params => {
        console.log("paras",params);
        this.queryParamData = params;
      });
    console.log("arr",this.plans);
    console.log("selectedChip",this.selectedChip);
  }

  onChipSelected(selectedChip) {
    this.selectedChip = selectedChip;
    console.log("--selectedChip",this.selectedChip);
  }

  ngOnInit(){

       this.getPlans(); 
  }

  async getPlans() {
    (await this.paymentService.getSubscriptionPlans()).subscribe((res: any) => {
      this._ngZone.run(() => {
        this.plans = res.result;
        console.log("🚀 ~ file: plans.page.ts ~ line 150 ~ PlansPage ~ this._ngZone.run ~ this.plans", this.plans)
      });
    });
  }

  planSelected(selectedPlan, subscriptionType){
    selectedPlan.selectedVehicleType =  this.selectedChip;
    selectedPlan.selectedSubscriptionType =  subscriptionType;
    this.selectedPlan = selectedPlan;
    
    // console.log("plan",this.selectedPlan);
    // if(this.selectedPlan ==  'opwm'){
    //   this.selectedPlanName = 'Once per week'
    // }
    // if(this.selectedPlan ==  'tpwm'){
    //   this.selectedPlanName = 'Twice per week'
    // }
    // if(this.selectedPlan ==  'thpwm'){
    //   this.selectedPlanName = 'Thrice per week'
    // }
    // if(this.selectedPlan ==  'otws'){
    //   if(this.selectedChip == 'sedan'){
    //     this.selectedPlanName = 'One time wash - Sedan car'
    //   }else{
    //     this.selectedPlanName = 'One time wash - 4*4 car'
    //   }      
    // }
  }

  enquiry(){
    const navigationExtras: NavigationExtras = {
      queryParams: {
        selectedPlan: JSON.stringify(this.selectedPlan),
        selectedChip: this.selectedChip,
        ...this.queryParamData
      }
    };

    this.router.navigate(['/customer-details-entry'], navigationExtras);
  }
}
