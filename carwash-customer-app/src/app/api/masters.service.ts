import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, MasterRequest, BusinessRequest, AuthRequest, UserRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { StorageService } from '../storage.service';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class MastersService extends NetworkService {

    constructor(
        protected httpClient: HttpClient,
        public spinner: NgxSpinnerService,
        public router: Router,
        public storageService: StorageService
    ) {
        super(httpClient,
            spinner,
            router
        );
    }
    async getLocationsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_LOCATIONS);
        return this.request(request);
    }
    async getUsersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_USERS);
        return this.request(request);
    }
    async getAllCustomer() {
        const request = new REQUEST(MasterRequest.GET_ALL_CUSTOMERS);
        return this.request(request);
    }
    async getBuildingsList() {
        const request = new REQUEST(MasterRequest.GET_ALL_BUILDINGS);
        return this.request(request);
    }
    async addNewBuilding(data: any) {
        const request = new REQUEST(MasterRequest.ADD_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async addCustomerDetails(data: any, weekDays: any, vehicles: any, month: number, year: number,scheduleType:string) {
        vehicles.forEach(vehicle => {
            vehicle.startDate=moment(vehicle.startDate).format("DD-MM-YYYY")
        });
        data = {
            custDetails: data,
            weekDays: weekDays,
            vehicles: vehicles,
            month: month,
            year: year,
            scheduleType:scheduleType
        }
        const request = new REQUEST(MasterRequest.ADD_CUSTOMER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editBuilding(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async getWorkersList() {
        const request = new REQUEST(MasterRequest.GET_ALL_WORKERS);
        return this.request(request);
    }
    // async getEditBuildings() {
    //     const request = new REQUEST(MasterRequest.GET_EDIT_BUILDINGS);
    //     return this.request(request);
    // }
    async addNewWorker(workerdata: any,user_name,workerBuildingData,date) {
        const data = {
            'workerData':workerdata,
            'user_name':user_name,
            'building_ids':workerBuildingData,
            'start_date':date,
        }
        const request = new REQUEST(MasterRequest.ADD_NEW_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async editWorker(data: any) {
        const request = new REQUEST(MasterRequest.EDIT_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteBuilding(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_BUILDING, data, REQUEST.POST);
        return (await this.request(request))
    }
    async deleteWorker(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_WORKER, data, REQUEST.POST);
        return (await this.request(request))
    }

    async deleteUser(id: number,user_name:any) {
        const data = {
            id: id,
            user_name:user_name,
        }
        const request = new REQUEST(MasterRequest.DELETE_USER, data, REQUEST.POST);
        return (await this.request(request))
    }

    async getAllJobs(user_id: number) {
        const data = {
            user_id: user_id
        }
        const request = new REQUEST(MasterRequest.GET_ALL_JOBS, data, REQUEST.POST);
        return (await this.request(request))
    }
    async updateWorkStatus(details:any) {
        let data = {
            schedule_id: details.schedule_id,
            date:details.date.toDateString()
        }
        const request = new REQUEST(MasterRequest.UPDATE_WORK_STATUS,data, REQUEST.POST);
        return this.request(request);
    }
    async getContactDetails() {
        let data = this.storageService.getContactDetails();
        if(data) return of(data);
        const request = new REQUEST(UserRequest.GET_CONTACT_DETAILS,{}, REQUEST.GET);
        return (await this.request(request)).pipe(
            tap(async (data: any) => {
                if (data.status) {
                    this.storageService.setContactDetails(data.result);
                }
            })
        );
    }
    
}