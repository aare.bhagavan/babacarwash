import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, MasterRequest, DashboardRequest, ProfileRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { CustomerCars } from './request.model';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ProfileService extends NetworkService {

    constructor(
        protected httpClient: HttpClient,
        public spinner: NgxSpinnerService,
        public router: Router
    ) {
        super(httpClient, spinner, router);
    }
    async getCustomerCars(customerId) {
        const requestData: CustomerCars = {customerId};
        const request = new REQUEST(ProfileRequest.GET_CARS, requestData, REQUEST.POST, false);
        return this.request(request);
    }

}

