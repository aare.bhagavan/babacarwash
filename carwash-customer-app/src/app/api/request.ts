// import { BusinessService } from 'app/api/business.service';
import { environment } from '../../environments/environment';
export class REQUEST {
    constructor(
        public endPoint: string,
        public data: any = {},
        public method: string = REQUEST.GET,
        public isLoadingRequired = true,
        public authReq: boolean = false) { }

    static readonly GET = 'get';
    static readonly POST = 'post';
    static readonly DELETE = 'delete';
    static readonly TOKEN_KEY = 'auth-token';
    static readonly SERVER_KEY = 'serverToken';


    static readonly DOMAIN = environment.domain;
    // static readonly mobile = REQUEST.mobile;
    // static readonly SOCKET = environment.socket;
}
export class AuthRequest extends REQUEST {
    // static readonly LOGIN = `${REQUEST.DOMAIN}/login`; // need to change from swagger
    static readonly SEND_OTP = `${REQUEST.DOMAIN}/otp/business/`;
    // static readonly VERIFY_OTP = `${REQUEST.DOMAIN}/otp/business/validate`;



    // static readonly LOGIN = `${REQUEST.DOMAIN}/business/user/login`; // need to change from swagger
    // static readonly REGISTRATION = `${REQUEST.DOMAIN}/business/user`;

    // static readonly LOGIN = `${REQUEST.DOMAIN}/business/user/login`; // need to change from swagger
    static readonly LOGIN = `${REQUEST.DOMAIN}/auth/login`;
    static readonly LOGOUT = `${REQUEST.DOMAIN}/auth/logout`;
    static readonly REGISTRATION = `${REQUEST.DOMAIN}/business/user/?role=user`;
    static readonly ONE_TIME_REGISTRATION = `${REQUEST.DOMAIN}/auth/oneTimeResgistration`;
    static readonly CUSTOMER_APP_REGISTRATION = `${REQUEST.DOMAIN}/auth/customerAppRegistration`;
    static readonly GENERATE_OTP = `${REQUEST.DOMAIN}/auth/generate-otp`;
    static readonly VERIFY_USER = `${REQUEST.DOMAIN}/auth/verify-user`;
    static readonly VERIFY_OTP = `${REQUEST.DOMAIN}/auth/verify-otp`;
    static readonly GENERATE_RESET_PWD = `${REQUEST.DOMAIN}/auth/reset-password`;

    static readonly USER_REGISTRATION = (role: string): string => {
        return `${REQUEST.DOMAIN}/business/user/register?role=${role}`;
    };
}

export class BusinessRequest extends REQUEST {

    static getMemberEditUrl = (id: number, business_id: number): string => {
        return `${REQUEST.DOMAIN}/business/member/${id}/${business_id}/edit`;
    };

    static getBusinessMember = (business_id: any): string => {
        return `${REQUEST.DOMAIN}/business/member/${business_id}`;
    };

    static addBusinessMember = (business_id: any): string => {
        return `${REQUEST.DOMAIN}/business/member/${business_id}`;
    };

    static getSubBusinessDetails = (superBusiness_id: any): string => {
        return `${REQUEST.DOMAIN}/business/super/${superBusiness_id}`;
    };
    static EditUeser = (id: any): string => { return `${REQUEST.DOMAIN}/business/user/${id}/edit`; }

    static readonly GET_SUPER_ADMIN_LIST = `${REQUEST.DOMAIN}/business/super/list`;
    static readonly GET_ADMIN_USER_LIST = `${REQUEST.DOMAIN}/business/`;

    static readonly ADD_BUSINESS_MEMBER = `${REQUEST.DOMAIN}/business/member/`;
    // static readonly EDIT_BUSINESS_MEMBER = `${REQUEST.DOMAIN}/business/member/` + id + `/edit`;
    static readonly GET_BUSINESS_MEMBER = `${REQUEST.DOMAIN}/business/member/`;
    static readonly VERIFY_MEMBER = `${REQUEST.DOMAIN}/business/member/verify/`;
    static readonly ADD_NEW_AREA = `${REQUEST.DOMAIN}/business/member/addArea`;
    static EDIT_AREA = (id: number): string => {
        return `${REQUEST.DOMAIN}/business/member/area/edit/${id}`;
    };
    static GET_MEMBER_TYPE = (id: number): string => {
        return `${REQUEST.DOMAIN}/business/checkbusinesstype/${id}`;
    };

    static AREA_SYNC = `${REQUEST.DOMAIN}/address/sync`;

    static readonly GET_BUSINESS_USERS = `${REQUEST.DOMAIN}/business/user/`;


    static readonly GET_ALL_BUSINESS_LIST = `${REQUEST.DOMAIN}/business/`;
    static readonly ADD_NEW_BUSINESS_LIST = `${REQUEST.DOMAIN}/business/`;
    static editBusiness = (id: any): string => {
        return `${REQUEST.DOMAIN}/business/${id}/edit`;
    };


    static getSuperBusiness = (superBusinessId: any): string => {
        return `${REQUEST.DOMAIN}/business/user/${superBusinessId}`;
    };

    static getListAllSuperBusiness = (superBusinessId: number): string => {
        return `${REQUEST.DOMAIN}/business/super/${superBusinessId}`;
    };

    static readonly ADD_NEW_SUPER_BUSINESS = `${REQUEST.DOMAIN}/business/super`;

    static readonly ADD_A_SUPER_BUSINESS = `${REQUEST.DOMAIN}/business/super/add`;
    static readonly LIST_ALL_SUPER_BUSINESS = `${REQUEST.DOMAIN}/business/super/list`;
    static editSuperBusiness = (id: number): string => {
        return `${REQUEST.DOMAIN}/business/super/edit/${id}`;
    };
}


export class MasterRequest extends REQUEST {
    static readonly GET_ALL_LOCATIONS = `${REQUEST.DOMAIN}/master/getAllLocations`;
    static readonly GET_ALL_CUSTOMERS = `${REQUEST.DOMAIN}/master/getAllCustomers`;
    static readonly GET_ALL_USERS = `${REQUEST.DOMAIN}/master/getUsers`;

    static readonly GET_ALL_BUILDINGS = `${REQUEST.DOMAIN}/master/getAllBuildings`;
    static readonly ADD_BUILDING = `${REQUEST.DOMAIN}/master/addBuilding`;
    static readonly ADD_CUSTOMER = `${REQUEST.DOMAIN}/master/addCustomer`;
    static readonly EDIT_BUILDING = `${REQUEST.DOMAIN}/master/editBuilding`;
    static readonly GET_ALL_WORKERS = `${REQUEST.DOMAIN}/master/getAllWorkers`;
    // static readonly GET_EDIT_BUILDINGS = `${REQUEST.DOMAIN}/master/getEditBuildings`;
    static readonly ADD_NEW_WORKER = `${REQUEST.DOMAIN}/master/addNewWorker`;
    static readonly EDIT_WORKER = `${REQUEST.DOMAIN}/master/editWorker`;
    static readonly DELETE_BUILDING = `${REQUEST.DOMAIN}/master/deleteBuilding`;
    static readonly DELETE_WORKER = `${REQUEST.DOMAIN}/master/deleteWorker`;
    static readonly DELETE_USER = `${REQUEST.DOMAIN}/master/deleteUser`;
    static readonly GET_ALL_JOBS = `${REQUEST.DOMAIN}/master/getScheduleListByUserId`;
    static readonly UPDATE_WORK_STATUS = `${REQUEST.DOMAIN}/master/updateWorkStatus`;



    static readonly GET_ALL_DEPARTMENTS = `${REQUEST.DOMAIN}/master/getAllDepartments`;
    static readonly GET_ALL_ZONES = `${REQUEST.DOMAIN}/master/getAllZones`;
    static readonly GET_ALL_CIRCLES = `${REQUEST.DOMAIN}/master/getAllCircles`;
    static readonly GET_GROUPLIST_BY_DEP_ID = `${REQUEST.DOMAIN}/master/getGroupByDepId`;
    static readonly GET_ALL_DZC_LIST = `${REQUEST.DOMAIN}/master/getDZC`;
    static readonly EDIT_ZONE = `${REQUEST.DOMAIN}/master/editZone`;
    static readonly ADD_ZONE = `${REQUEST.DOMAIN}/master/addZone`;
    static readonly VALIDATE_ADD_PAYROLL = `${REQUEST.DOMAIN}/master/validateAddPayroll`;
    static readonly GET_ALL_DESIGNATIONS = `${REQUEST.DOMAIN}/master/getAllDesignations`;
    static readonly PAYROLL_RUN = `${REQUEST.DOMAIN}/master/payRollRun`;
    static readonly CANCEL_PAYROLL = `${REQUEST.DOMAIN}/master/cancelPayRoll`;
    static readonly ADD_ORGANIZATION = `${REQUEST.DOMAIN}/master/addOrganization`;
    static readonly GET_USERSLIST = `${REQUEST.DOMAIN}/master/getUsersList`;
    static readonly ADD_DESIGNATION = `${REQUEST.DOMAIN}/master/addDesignation`;
    static readonly UPDATEWORKER_DESIGNATION = `${REQUEST.DOMAIN}/worker/update-worker`;
    // static readonly GET_ALL_WAGES = `${REQUEST.DOMAIN}/master/getAllwages`;
    static readonly GET_PAYROLL_RUN_LOG = `${REQUEST.DOMAIN}/master/getAllPayRollRunLog`;
    static readonly GET_GENERATED_STATEMENTS = `${REQUEST.DOMAIN}/master/getAllGeneratedStatements`;
    static readonly GET_PAYROLL_RUN_MAX_YEAR = `${REQUEST.DOMAIN}/master/getAllPayRollRunMaxYear`;
    static readonly GET_ALL_ORGANIZATIONS = `${REQUEST.DOMAIN}/master/getAllOrganizations`;
    static readonly GET_ALL_ROLES = `${REQUEST.DOMAIN}/master/getAllRoles`;
    static readonly GET_CIRCLE = `${REQUEST.DOMAIN}/master/getCirclebyId`;
    static readonly EDIT_CIRCLE = `${REQUEST.DOMAIN}/master/editCircle`;
    static readonly GET_DESIGNATION_BY_DEPARTMENT_ID = `${REQUEST.DOMAIN}/master/getDesignationByDepartmentID`;
    static readonly GET_DESIGNATION_NAME_BY_DEPARTMENT_ID = `${REQUEST.DOMAIN}/master/getDesignationNameByDepartmentID`;
    static readonly ADD_CIRCLE = `${REQUEST.DOMAIN}/master/addCircle`;
    static readonly ADD_NEW_DZC = `${REQUEST.DOMAIN}/master/addDZC`;
    static readonly ADD_NEW_USER = `${REQUEST.DOMAIN}/master/addUser`;
    static readonly ADD_NEW_ORGANIZATION = `${REQUEST.DOMAIN}/master/addOrganization`;
    static readonly EDIT_DZC = `${REQUEST.DOMAIN}/master/updateDZC`;
    static readonly EDIT_ORGANIZATION = `${REQUEST.DOMAIN}/master/updateOrganization`;
    static readonly DELETE_CIRCLE = `${REQUEST.DOMAIN}/master/deleteCircle`;
    static readonly DELETE_ORGANIZATION = `${REQUEST.DOMAIN}/master/deleteOrganization`;
   // static readonly DELETE_USER = `${REQUEST.DOMAIN}/master/deleteUser`;
    static readonly DELETE_ZONE = `${REQUEST.DOMAIN}/master/deleteZone`;
    static readonly DELETE_DEPARTMENT = `${REQUEST.DOMAIN}/master/deleteDepartment`;
    static readonly EDIT_DEPARTMENT = `${REQUEST.DOMAIN}/master/editDepartment`;
    static readonly EDIT_USER = `${REQUEST.DOMAIN}/master/editUser`;
    static readonly ADD_DEPARTMENT = `${REQUEST.DOMAIN}/master/addDepartment`;
    static readonly DOWNLOAD_PF_ESI = `${REQUEST.DOMAIN}/master/downloadPFandESI`;
    static readonly GET_ALL_DOCUMENTS_PROCESS = `${REQUEST.DOMAIN}/master/getDocumentsProcessList`;
    static readonly DELETE_DOCUMENT = `${REQUEST.DOMAIN}/master/deleteDocument`;
    static readonly EDIT_DESIGNATION = `${REQUEST.DOMAIN}/master/editDesignation`;
    static readonly DELETE_DESIGNATION = `${REQUEST.DOMAIN}/master/deleteDesignation`;
}

export class WorkerRequest extends REQUEST {
    static readonly INSERT_WORKER = `${REQUEST.DOMAIN}/worker/insert-worker`;
    static readonly EDIT_WORKER = `${REQUEST.DOMAIN}/worker/edit-worker`;
    static readonly UPDATE_WORKER = `${REQUEST.DOMAIN}/worker/update-worker`;
    static readonly GET_ALL_WORKERS = `${REQUEST.DOMAIN}/worker/getAllWorkers`;
    static readonly GET_ALL_DEPZONECIRCLE = `${REQUEST.DOMAIN}/worker/getDepZoneCircle`;
    static readonly DELETE_WORKER = `${REQUEST.DOMAIN}/worker/deleteWorker`;
    static readonly GET_ALL_DESIGNATIONS = `${REQUEST.DOMAIN}/worker/getAllDesignations`;
    static readonly ATTENDENCE_UPLOAD = `${REQUEST.DOMAIN}/auth/importexcel`;
}

export class UserRequest extends REQUEST {
    static readonly GET_CONTACT_DETAILS = `${REQUEST.DOMAIN}/user/getContactDetails`;
}

export class PaymentRequest extends REQUEST {
    static readonly GET_ALL_PAYMENT_DETAILS = `${REQUEST.DOMAIN}/payment/getALLPaymentDetails`;
    static readonly GET_USER_PAYMENT_DETAILS = `${REQUEST.DOMAIN}/payment/payAmount`;
    static readonly GET_USER_PAYMENT = `${REQUEST.DOMAIN}/payment/getuserpayment`;
    static readonly GET_SUBSCRIPTION_PLANS = `${REQUEST.DOMAIN}/payment/getSubscriptionPlans`;
}

export class DashboardRequest extends REQUEST {
    static readonly GET_WASHES = `${REQUEST.DOMAIN}/customer/dashboard/getWashHistory`;
    static readonly GET_PAYMENTS = `${REQUEST.DOMAIN}/customer/dashboard/getPaymentHistory`;
    static readonly GET_CUSTOMER_DETAILS = `${REQUEST.DOMAIN}/customer/profile/getProfile`;
}
export class ProfileRequest extends REQUEST {
    static readonly GET_CUSTOMER_DETAILS = `${REQUEST.DOMAIN}/customer/profile/getProfile`;
    static readonly GET_CARS = `${REQUEST.DOMAIN}/customer/profile/getCars`;
    static readonly GET_DATE_CARS = `${REQUEST.DOMAIN}/customer/profile/getDateCars`;
}





