export class Paginate {
    static readonly PAGE_SIZE: number = 20;
    static readonly START_INDEX: number = 0;    
    public startIndex: number = Paginate.START_INDEX;
    public endIndex: number = Paginate.PAGE_SIZE;
    public reset() {
        this.startIndex = Paginate.START_INDEX;
        this.endIndex = Paginate.PAGE_SIZE;    
    }
}

export class WashFilter {
    public car: string;
    public startDate;
    public endDate;
    public date: string;
}

export class WashHistory {
    public customerId;
    public paginate: Paginate;
    public filter: WashFilter;
}
export class PaymentsHistory {
    public customerId;
    public paginate: Paginate;
    public filter: WashFilter;
}
export class CustomerDetails {
    public customerId;
}
export class CustomerCars {
    public customerId;
}