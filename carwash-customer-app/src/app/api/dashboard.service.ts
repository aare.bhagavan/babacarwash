import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, MasterRequest, DashboardRequest } from './request';
import { NetworkService } from './network.service';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Paginate, PaymentsHistory, WashFilter, WashHistory,CustomerDetails } from './request.model';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class DashboardService extends NetworkService {

    constructor(
        protected httpClient: HttpClient,
        public spinner: NgxSpinnerService,
        public router: Router
    ) {
        super(httpClient,
            spinner,
            router
        );
    }
    async getCustomerWashes(customerId, paginate: Paginate, filter: WashFilter) {
        const requestData: WashHistory = {customerId, paginate, filter};
        const request = new REQUEST(DashboardRequest.GET_WASHES, requestData, REQUEST.POST);
        return this.request(request);
    }
    async getCustomerPayments(customerId, paginate: Paginate, filter: WashFilter) {
        const requestData: PaymentsHistory = {customerId, paginate, filter};
        const request = new REQUEST(DashboardRequest.GET_PAYMENTS, requestData, REQUEST.POST);
        return this.request(request);
    }
    async getCustomerDetails(customerId) {
        const requestData: CustomerDetails = {customerId};
        const request = new REQUEST(DashboardRequest.GET_CUSTOMER_DETAILS, requestData, REQUEST.POST);
        return this.request(request);
    }
}

