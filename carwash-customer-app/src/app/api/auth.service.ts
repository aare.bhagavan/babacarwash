import { Injectable } from '@angular/core';
// import { SocketEvents, HomeMasterEvent, SheduleEvents, SocketMasterService } from './socket.service';
import { REQUEST, BusinessRequest, AuthRequest } from './request';
import { NetworkService } from './network.service';
// import { Observable, Observer, Subject, from } from 'rxjs';
// import { RESPONSE } from './response';
// import { Time } from '@angular/common';
// import { tap, map, delayWhen } from 'rxjs/operators';
// import { StorageProvider } from './storage.provider';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';
import { StorageService } from '../storage.service';
// import { EvAlertController } from '../components/alert.controller';
// import { LoadingController } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends NetworkService {
  // public onScheduled: Subject<any> = new Subject<any>();

  constructor(
    // protected storageProvider: StorageProvider,
    protected httpClient: HttpClient,
    public spinner: NgxSpinnerService,
    public router: Router
    // protected alertController: EvAlertController,
    // protected socketMaster: SocketMasterService,
    // protected loadingCtrl: LoadingController
  ) {
    super(httpClient,
      spinner,
      router
    );
  }

  async login(mobileno: string, password: string , logintype : string) {
    const data = { mobileno, password , logintype };
    const request = new REQUEST(AuthRequest.LOGIN, data, REQUEST.POST);
    return (await this.request(request)).pipe(
      tap(async (data: any) => {
        if (data.status) {
          localStorage.setItem(StorageService.KEY_USER_ID, data.customer_id);
          localStorage.setItem('token', data.token);
          localStorage.setItem('user_name', data.name);
          localStorage.setItem('login_key', "true");

        }
      })
    );
  }
  async logout() {
    const data = { 
      token : localStorage.getItem("token"),
      userId: localStorage.getItem("user_id")
    }
    const request = new REQUEST(AuthRequest.LOGOUT, data, REQUEST.POST);
    return (await this.request(request)).pipe(
      tap(async (data) => {
        if (data.status) {
          localStorage.clear();
        }
      })
    );
  }
  async generateOtp(phone: string,reqform:string) 
    {
        const request = new REQUEST(AuthRequest.GENERATE_OTP, {phone,reqform}, REQUEST.POST);
        return (await this.request(request));
    }

    async verifyUser(phone: string,reqform:string,resendotp) 
    {
        const request = new REQUEST(AuthRequest.VERIFY_USER, {phone,reqform,resendotp}, REQUEST.POST);
        return (await this.request(request));
    }
    async verifyOtp(phone: string,otp:string,reqform:string) 
    {
        const request = new REQUEST(AuthRequest.VERIFY_OTP, {phone,otp,reqform}, REQUEST.POST);
        return (await this.request(request));
    }
    async generateResetPwd(data) 
    {
        const request = new REQUEST(AuthRequest.GENERATE_RESET_PWD, data, REQUEST.POST);
        return (await this.request(request));
    }

  async customerAppRegistration(data) {
    console.log("ui data",data);
    const request = new REQUEST(AuthRequest.CUSTOMER_APP_REGISTRATION, data, REQUEST.POST);
    return (await this.request(request))
  }

  async register(phone: string, society_id: string, name: string, password: string, email: string, role: string) {
    const data = {
      "phone": phone,
      "password": password,
      "user_name": name,
      "email": email,
      "business_id": parseInt(society_id),
    };
    const request = new REQUEST(AuthRequest.REGISTRATION, data, REQUEST.POST);
    return (await this.request(request))
  }

  async oneTimeRegistration(registerData: any, user_name: any) {
    let data = {
      registerData,
      user_name : user_name,
      token : localStorage.getItem("token"),
    }
    const request = new REQUEST(AuthRequest.ONE_TIME_REGISTRATION, data, REQUEST.POST);
    return (await this.request(request))
  }

  async userRegister(phone: string, society_id: string, name: string, password: string, email: string, role: string) {
    let data;
    if (role == 'super_admin') {
      data = {
        "phone": phone,
        "password": password,
        "user_name": name,
        "email": email,
        "super_business_id": parseInt(society_id),
      }
    } else {
      data = {
        "phone": phone,
        "password": password,
        "user_name": name,
        "email": email,
        "business_id": parseInt(society_id),
      };
    }
    const request = new REQUEST(AuthRequest.USER_REGISTRATION(role), data, REQUEST.POST);
    return (await this.request(request))
  }


}
































