import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/storage.service';

@Component({
    selector: 'app-help-dialog',
    template: `
    <div class='declarationdialog'  >
      <div class="alert_dialog" >
        <ion-grid class="gridcss">
            <ion-row class="dialog-header" justify-content-center style="padding-left:10px;">
                <h4 class="contentcolor" id="declare_head" >Not able to Login?</h4>
                <span>Please contact our Customer Care..</span>
            </ion-row>
            <ion-row>
            <ion-col>
              <ion-row class="input_row">
                <a href="https://wa.me/{{this.whatsappNumber}}">
                    <ion-item lines="none" style="padding: 16px">
                        <img src="/assets/images/whatsapp.png" alt="" style="width:48px" srcset="" >
                    </ion-item>
                </a>
              </ion-row>
              <ion-row class="input_row">
                <ion-item lines="none">
                  <ion-label>Whatsapp</ion-label>
                </ion-item>
              </ion-row>
            </ion-col>
            <ion-col >
              <ion-row class="input_row">
                <a href="tel:{{data.mobile}}">
                  <ion-item lines="none" style="padding: 16px">
                    <img src="/assets/images/call.png" alt="" style="width:48px" srcset="" >
                  </ion-item>
                </a>
              </ion-row>
              <ion-row class="input_row">
                <ion-item lines="none" >
                  <ion-label>Call us</ion-label>
                </ion-item>
              </ion-row>
            </ion-col>
          </ion-row>
        </ion-grid>
      </div>
    </div>
  `,
    styleUrls: ['../alert-dialog/alert-dialog.component.scss'],
  })


export class LoginHelpComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  @Input() data: any;
  amount: any;
  amountcal:any;
  amount_charged:any;
  lessamount:any;
  whatsappNumber;
  whatsappText = encodeURIComponent("Hello, I'm unable to login to your application.");
  amount1: string = '';
  Ischecked: boolean = false; IsUserexists: boolean = false;
  IsTermsChecked: boolean = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController, private router: Router,
    public storageProvider: StorageService,
    // private authservice: AuthService,
    //  public evAlertController: EvAlertController,
    // public alertController: EvAlertController
  ) {

    this.backButtonEvent();
  }
  ngOnInit() {
    this.whatsappNumber = this.data.mobile.slice(1).trim();
    this.amountcal=false;
  }
  async onetimewash() {
    await this.closeAlert();
    const data: NavigationExtras = {
      queryParams : {
        existing: true,
        customerId: this.storageProvider.getUserId()
      }
    }
    this.router.navigate(['/plans'], data);
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });;
          return;
        }
      } catch (error) {
      }

    });
  }
  async closeAlert() {
    try {
      const element = await this.modalController.getTop();
      if (element) {
        element.dismiss().then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }
}
