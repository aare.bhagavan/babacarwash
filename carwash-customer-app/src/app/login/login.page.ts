import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, MaxLengthValidator, Validators } from '@angular/forms';
import { Router, NavigationExtras } from '@angular/router';
import { AuthService } from '../api/auth.service';
// import { Storage } from '@ionic/storage';
import Swal from 'sweetalert2';
import { StorageService } from '../storage.service';
import { EvAlertController } from '../alert.controller';
import { MastersService } from '../api/masters.service';

const NEED_CUSTOMER_SUPPORT_ATTEMPTS = 2;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  user_id: number; submitted = false;
  loginAttempts: number = 0;

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    public authService: AuthService,
    private storage: StorageService,
    private mastersService: MastersService,
    private alertController: EvAlertController
  ) { }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      mobnumber: ['', [Validators.required, Validators.maxLength(10), Validators.minLength(10)]],
      password: ['', Validators.required]
    });

  }

  goToPlansPage() {
    this.router.navigate(['/plans'], {});
  }

  goToForgotPwdPage() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        reqform: 'forgotpwdform',
        form: this.loginForm.value.mobnumber
      }
    };
    this.router.navigate(['/forgot-password'], navigationExtras);
  }

  async login() {
    // this.submitted = true;

    console.log(this.loginForm);
    if (this.loginForm.valid) {
      // const mobileNumber = this.loginForm.value.mobnumber;
      // const password = this.loginForm.value.password;

      // (await this.authService.login(mobileNumber, password)).subscribe((res: any) => {
      (await this.authService.login(this.loginForm.controls['mobnumber'].value.toString(), this.loginForm.controls['password'].value, 'customer')).subscribe(async (res: any) => {

        console.log(res);
        if (res.success == true) {
          this.user_id = res.customer_id;
          // this.storage.set('user_id', this.user_id);
          // this.storage.set('user_name',res.name);
          this.storage.setToken(res.token);
          localStorage.setItem('token', res.token);
          localStorage.setItem('user_name', res.name);
          localStorage.setItem('login_key', "true");

          localStorage.setItem('user_id', ""+this.user_id);
          // localStorage.setItem('user_id', "261");

          const navigationExtras: NavigationExtras = {
            queryParams: {
              user_id: this.user_id
            }
          };

          this.router.navigate(['/tabs'], navigationExtras);
          // this.router.navigate(['/tabs/jobs']);
          // localStorage.setItem('statuspayments', this.paymentstatus)
          // localStorage.setItem('statusprofile',this.profilestatus)
          // localStorage.setItem('statusjob',this.jobsstatus)
        } else {
          console.log('Invalid Creds')
          console.log('Invalid Creds', this.loginAttempts);
          this.loginAttempts++;
          if (this.loginAttempts >= NEED_CUSTOMER_SUPPORT_ATTEMPTS) {
            const contactDetails = await (await this.mastersService.getContactDetails()).toPromise();
            const res = await this.alertController.LoginHelpDialog(contactDetails);
          } else {

            if (res.message == "Invalid Credentials") {
              Swal.fire({
                icon: 'error',
                width: 250,
                padding: '4px',
                html: '<h3>Invalid Credentials</h3>',
              })
            }
            else if (res.message = "Not registered as customer") {
              Swal.fire({
                icon: 'error',
                width: 250,
                padding: '4px',
                html: '<h3>Not registered as customer</h3>',
              })

            } else {
              Swal.fire({
                icon: 'error',
                width: 250,
                padding: '4px',
                html: '<h3>Something Went Wrong</h3>',
              })

            }
          }
        }

      });
    }
  }
  public focusInput(event): void {
    let total = 0;
    let container = null;

    const _rec = (obj) => {

      total += obj.offsetTop;
      const par = obj.offsetParent;
      if (par && par.localName !== 'ion-content') {
        _rec(par);
      } else {
        container = par;
      }
    };
    _rec(event.target);
    setTimeout(() => {
      container.scrollToPoint(0, total - 50, 400);
    }, 500);
  }

}
