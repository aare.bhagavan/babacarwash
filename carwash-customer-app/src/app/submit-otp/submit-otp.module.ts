import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SubmitOtpPageRoutingModule } from './submit-otp-routing.module';

import { SubmitOtpPage } from './submit-otp.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ComponentsModule,
    SubmitOtpPageRoutingModule
  ],
  declarations: [SubmitOtpPage]
})
export class SubmitOtpPageModule {}
