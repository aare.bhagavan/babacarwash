import { Component,OnInit} from '@angular/core';
import { FormBuilder, FormGroup, MaxLengthValidator, Validators } from '@angular/forms';
import { Builder } from 'selenium-webdriver';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  readonly TYPE_SEDAN = "sedan";
  readonly TYPE_4X4 = "4x4";
  selectedChip: string;
  selectedPlan: string = 's'; 
  selectedPlanName : string;
  enquiryForm: FormGroup;
  
  color: any;
  constructor(private _formBuilder: FormBuilder,private router: Router,) {
    this.selectedChip = this.TYPE_SEDAN;
    this.enquiryForm = new FormGroup({

    });

  }

  onChipSelected(selectedChip) {
    this.selectedChip = selectedChip;
  }

  ngOnInit(){

  }

  planSelected(selectedPlan){
    this.selectedPlan = selectedPlan;
    console.log("plan",this.selectedPlan);
    if(this.selectedPlan ==  'opwm'){
      this.selectedPlanName = 'Once per week'
    }
    if(this.selectedPlan ==  'tpwm'){
      this.selectedPlanName = 'Twice per week'
    }
    if(this.selectedPlan ==  'thpwm'){
      this.selectedPlanName = 'Thrice per week'
    }
    if(this.selectedPlan ==  'otws'){
      this.selectedPlanName = 'One time wash - Sedan car'
    }
    if(this.selectedPlan ==  'otw4'){
      this.selectedPlanName = 'One time wash - 4*4 car'
    }
  }

  enquiry(){
    const navigationExtras: NavigationExtras = {
      queryParams: {
        selectedPlan: this.selectedPlanName
      }
    };

    this.router.navigate(['/customer-details-entry'], navigationExtras);
  }

}
