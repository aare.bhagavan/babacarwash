import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { IonInfiniteScroll } from '@ionic/angular';
import { DashboardService } from '../../api/dashboard.service';
import { Paginate, WashFilter } from '../../api/request.model';
import { StorageService } from 'src/app/storage.service';
const DATE_FORMAT = "YYYY-MM-DD";
@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public paymentsDue;
  public paymentsDone;
  carfilter:boolean = false;
  datefilter:boolean = false;
  filter = new WashFilter();
  paginate = new Paginate();
  building = false;
  today = false;
  totalRecords = 0;
  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.paymentsDue = [];
    this.paymentsDone = [];
    this.getPayments();
  }


  async getPayments() {
    const customerId = localStorage.getItem(StorageService.KEY_USER_ID);
    return (await this.dashboardService.getCustomerPayments(customerId, this.paginate, this.filter)).toPromise()
      .then((res: any) => {
         if(res.status == true) {
          console.log(res);
          // this.paymentsDue.push(res.result.filter((wash) => moment().diff(wash.payment_date) == 0));
          // this.paymentsDone.push(res.result.filter((wash) => moment().diff(wash.payment_date) > 0));
          const today = moment().startOf('day');
          const payments = res.result.data;
          this.totalRecords = res.result.total;
          this.paymentsDue = [...this.paymentsDue, ...(payments.filter((payment) => today.diff(moment(payment.payment_date, DATE_FORMAT).startOf('day')) == 0))];
          this.paymentsDone = [...this.paymentsDone, ...(payments.filter((payment) => today.diff(moment(payment.payment_date, DATE_FORMAT).startOf('day')) > 0))];
          console.log("payments",res);
          const count = res.result.data.length;
          if(count < Paginate.PAGE_SIZE) {
            this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
          }
        }
      })
  }
  async loadData($event) {
    console.log("infinite scroll");
    this.paginate.startIndex += this.paginate.endIndex + 1;
    this.paginate.endIndex += Paginate.PAGE_SIZE;
    await this.getPayments();
    $event.target.complete();
  }

  resetData() {
    this.paymentsDue = [];
    this.paymentsDone = [];
    this.paginate.reset();    
    this.infiniteScroll.disabled = false;
  }
  filterCar($event) {
    this.filter.car = $event.id;
    this.resetData();
    this.getPayments();
  }
  onCarFilterCleared() {
    this.filter.car = undefined;
    this.resetData();
    this.getPayments();
  }
  filterDate($event) {
    this.filter.startDate = $event.startDate;
    this.filter.endDate = $event.endDate;
    this.resetData();
    this.getPayments();
  }
  onDateFilterCleared() {
    this.filter.startDate = undefined;
    this.filter.endDate = undefined;
    this.resetData();
    this.getPayments();    
  }
  carfilterShow(){
    if(this.carfilter == true){
      this.carfilter = false;
      this.datefilter = false;
    }else{
      this.carfilter = true;
      this.datefilter = false;
    }  
  }
  datefilterShow(){
    console.log("this.datefilter",this.datefilter);
    if(this.datefilter == true){
      this.carfilter = false;
      this.datefilter = false;
    }else{
      this.carfilter = false;
      this.datefilter = true;
    }  
  }
}
