import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WashesPageRoutingModule } from './washes-routing.module';

import { WashesPage } from './washes.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { CarFilterComponent } from 'src/app/components/car-filter/car-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WashesPageRoutingModule,
    ComponentsModule
  ],
  declarations: [WashesPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WashesPageModule {}
