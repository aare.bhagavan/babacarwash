import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { IonInfiniteScroll } from '@ionic/angular';
import { DashboardService } from '../../api/dashboard.service';
import { Paginate, WashFilter } from '../../api/request.model';
import { StorageService } from '../../storage.service';
const DATE_FORMAT = "YYYY-MM-DD";
@Component({
  selector: 'app-washes',
  templateUrl: './washes.page.html',
  styleUrls: ['./washes.page.scss'],
})
export class WashesPage implements OnInit {

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  public todayWashes = [];
  public pastWashes = [];
  carfilter:boolean = false;
  datefilter:boolean = false;
  filter = new WashFilter();
  paginate = new Paginate();
  building = false;
  today = false;
  totalRecords = 0;
  constructor(
    private dashboardService: DashboardService,
    private storageService: StorageService
  ) { }

  ngOnInit() {
    this.getCustomers();
  }
  async getCustomers() {
    const customerId = this.storageService.getUserId();
    return (await this.dashboardService.getCustomerWashes(customerId, this.paginate, this.filter)).toPromise()
      .then((res: any) => {
         if(res.status == true) {
          console.log(res);
          const today = moment().startOf('day');
          const washes = res.result.data;
          this.totalRecords = res.result.total;
          this.todayWashes = [...this.todayWashes, ...washes.filter((wash) => today.diff(moment(wash.date, DATE_FORMAT).startOf('day')) == 0)];
          this.pastWashes = [ ...this.pastWashes, ...washes.filter((wash) => today.diff(moment(wash.date, DATE_FORMAT).startOf('day')) > 0)];
          console.log(this.pastWashes);
          const count = res.result.data.length;
          if(count < Paginate.PAGE_SIZE && this.infiniteScroll) {
            this.infiniteScroll.disabled = !this.infiniteScroll?.disabled;
          }
        }
      });
  }
  
  async loadData($event) {
    console.log("infinite scroll");
    this.paginate.startIndex += this.paginate.endIndex + 1;
    this.paginate.endIndex += Paginate.PAGE_SIZE;
    await this.getCustomers();
    $event.target.complete();  
  }
  resetData() {
    this.todayWashes = [];
    this.pastWashes = [];
    this.paginate.reset();
    this.infiniteScroll.disabled = false;
  }
  filterCar($event) {
    this.filter.car = $event.id;
    this.resetData();
    this.getCustomers();
  }
  onCarFilterCleared() {
    this.filter.car = undefined;
    this.resetData();
    this.getCustomers();    
  }
  filterDate($event) {
    this.filter.startDate = $event.startDate;
    this.filter.endDate = $event.endDate;
    this.resetData();
    this.getCustomers();
  }
  onDateFilterCleared() {
    this.filter.startDate = undefined;
    this.filter.endDate = undefined;
    this.resetData();
    this.getCustomers();    
  }
  carfilterShow(){
    
    if(this.carfilter == true){
      this.carfilter = false;
      this.datefilter = false;
    }else{
      this.carfilter = true;
      this.datefilter = false;
    }  
  }
  datefilterShow(){
    if(this.datefilter == true){
      this.carfilter = false;
      this.datefilter = false;
    }else{
      this.carfilter = false;
      this.datefilter = true;
    }  
  }
}
