import { Component, OnInit, Input } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { StorageService } from 'src/app/storage.service';

@Component({
    selector: 'app-help-dialog',
    template: `
    <div class='declarationdialog'  >
      <div class="alert_dialog" >
        <ion-grid class="gridcss">
          <ion-row justify-content-center>
            <p class="contentcolor" id="declare_head" style="margin-left:10px;">Select an option below</p>
          </ion-row>
          <ion-row>
            <ion-col (click)="onetimewash()">
              <ion-row class="input_row">
                <ion-item lines="none">
                  <img src="/assets/images/new_car.svg" alt="" style="width:75px" srcset="" >
                </ion-item>
              </ion-row>
              <ion-row class="input_row">
                <ion-item lines="none">
                  <ion-label>New Car Wash</ion-label>
                </ion-item>
              </ion-row>
            </ion-col>
            <ion-col >
              <ion-row class="input_row">
                <a href="tel:{{this.data.contactDetails.mobile}}">
                  <ion-item lines="none" >
                    <img src="/assets/images/contact.png" alt="" style="width:75px" srcset="" >
                  </ion-item>
                </a>
              </ion-row>
              <ion-row class="input_row">
                <ion-item lines="none" >
                  <ion-label>Help</ion-label>
                </ion-item>
              </ion-row>
            </ion-col>
          </ion-row>
        </ion-grid>
      </div>
    </div>
  `,
    styleUrls: ['../../alert-dialog/alert-dialog.component.scss'],
  })


export class ProfileHelpComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  @Input() data: any;
  amount: any;
  amountcal:any;
  amount_charged:any;
  lessamount:any;

  amount1: string = '';
  Ischecked: boolean = false; IsUserexists: boolean = false;
  IsTermsChecked: boolean = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController, private router: Router,
    public storageProvider: StorageService,
    // private authservice: AuthService,
    //  public evAlertController: EvAlertController,
    // public alertController: EvAlertController
  ) {

    this.backButtonEvent();
  }
  ngOnInit() {
    this.amountcal=false
  }
  async onetimewash() {
    await this.closeAlert();
    const data: NavigationExtras = {
      queryParams : {
        existing: true,
        customerId: this.storageProvider.getUserId()
      }
    }
    this.router.navigate(['/plans'], data);
  }
  backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });;
          return;
        }
      } catch (error) {
      }

    });
  }
  async closeAlert() {
    try {
      const element = await this.modalController.getTop();
      if (element) {
        element.dismiss().then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }
}
