import { Component, OnInit } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { Subject } from 'rxjs';
import { DashboardService } from '../../api/dashboard.service';
import { EvAlertController } from '../../alert.controller';
import { AuthService } from 'src/app/api/auth.service';
import { NavigationExtras, Router } from '@angular/router';
import { StorageService } from 'src/app/storage.service';
import { MastersService } from 'src/app/api/masters.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public profileDetails = [];
  isDialogPresent: boolean = true;
  name: string;
  mobile: string;
  email: string;
  subcount: number;
  contactDetails;
  weekdays = [
    "sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"
  ];
  constructor(public actionSheetCtrl: ActionSheetController, 
    private dashboardService: DashboardService, 
    public authService: AuthService,
    public alertController: EvAlertController,
    public mastersService: MastersService,
    public router: Router,
    public storageService: StorageService
    ) { }

  ngOnInit() {
    this.getCustomerDetails();
    this.getContactDetails();
  }

  async getContactDetails() {
    (await this.mastersService.getContactDetails()).subscribe((response: any) => {
      this.contactDetails = response;
    });
  }
  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }
  async getCustomerDetails() {
    const customerId = localStorage.getItem(StorageService.KEY_USER_ID);
    (await this.dashboardService.getCustomerDetails(customerId))
      .subscribe((res: any) => {
        if (res.status) {
          console.log(res);
          this.profileDetails = res.result;
          const lastname = this.profileDetails[0].last_name? this.profileDetails[0].last_name: ''; 
          this.name = this.profileDetails[0].first_name + " " + lastname;
          this.mobile = this.profileDetails[0].mobile;
          this.email = this.profileDetails[0].email;
          this.subcount = this.profileDetails.filter(sub => sub.start_date).length;

          this.profileDetails.forEach(sub => {
            let subscribedDays = 0; 
            let washDay = []; 
            let daycount = 0;
            Object.keys(sub).filter((subKey) => {
              if (this.weekdays.includes(subKey)) {
                sub[subKey] == 1 ? subscribedDays++ : "";

                if (subscribedDays <= 3) {
                  sub[subKey] == 1 ? washDay.push(subKey.substring(0, 3)): "";
                }
              }

            });
            sub.washDay = washDay.join(",");
            if (subscribedDays == 1) {
              sub.noOfDays = "Once"
            } else if (subscribedDays == 2) {
              sub.noOfDays = "Twice"
            } else if (subscribedDays == 3) {
              sub.noOfDays = "Thrice"
            } else if (subscribedDays == 4) {
              sub.noOfDays = "Four times"
            } else if (subscribedDays == 5) {
              sub.noOfDays = "Five times"
            } else if (subscribedDays == 6) {
              sub.noOfDays = "Six times"
            } else if (subscribedDays == 7) {
              sub.noOfDays = "Seven times"
            }
            sub.subscribedDays = subscribedDays
          });
          console.log("profi", this.profileDetails)
        }
      })
  }
  async logout() {
    (await this.authService.logout()).subscribe((res) => {
      if(res.status) {
        this.router.navigate(['/']);
      }
    })

  }

  updatePassword() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        reqform: "update-password",
        mobile: this.mobile,
      }
    };
    this.router.navigate(['/forgot-password'], navigationExtras);
  }

  async help(selected: any) {

    // localStorage.setItem("charged_amount",selected.due_amount);

    let data = {
      'customer_id': this.storageService.getUserId(),
      contactDetails: this.contactDetails
    }

    console.log(data)

    this.isDialogPresent = false;
    const res = await this.alertController.ProfileHelpDialog(data);

    console.log(res);
    if (res == true) {
      this.isDialogPresent = true;
      // this.getUser_id();
    }
  }


}


