import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProfileService } from '../../api/profile.service';
import { StorageService } from '../../storage.service';
import { DashboardService } from '../../api/dashboard.service';

@Component({
  selector: 'bc-car-filter',
  templateUrl: './car-filter.component.html',
  styleUrls: ['./car-filter.component.scss'],
})
export class CarFilterComponent implements OnInit {
  searchControl = new FormControl();
  searchvalue: string;
  originalResults = [];
  filteredList = [];
  selectedCar = null;
  
  @Output() onCarSelected = new EventEmitter();
  @Output() onFilterCleared = new EventEmitter();

  constructor(
    private profileService: ProfileService,
    private storageService: StorageService
    ) { 
    this.getCarListing();
  }

  async ngOnInit() {
    this.searchControl.valueChanges.subscribe((value) => this.filterResults(value));
  }

  async getCarListing() {
    const customerId = this.storageService.getUserId();
    (await this.profileService.getCustomerCars(customerId))
    .subscribe((res: any) => {
       if(res.status) {
        this.originalResults = res.result;
        this.filteredList = this.originalResults;
        console.log("filter list",this.filteredList);
      }
    });
  }
  async filterResults(value: string) {
    console.log("fiter",value)
    this.filteredList = this.originalResults.filter(item => item.registration_no.toLowerCase().includes(value.toLowerCase()));
    console.log("filter results list",this.filteredList);

  } 
  onCarItemClicked(car) {
    if(this.selectedCar == car.id) {
      return;
    }
    this.selectedCar = car.id;
    this.onCarSelected.emit(car);
  }
  onClearButtonClicked($event) {
    this.selectedCar = null;
    $event.stopPropagation();
    this.onFilterCleared.emit();
  }

}
