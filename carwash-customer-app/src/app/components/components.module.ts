import { NgModule } from '@angular/core';
import { CommonModule,DatePipe } from '@angular/common';
import { CarFilterComponent } from './car-filter/car-filter.component';
import { DateFilterComponent } from './date-filter/date-filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { APIServiceModule } from '../api/api.module';
import { BackButtonComponent } from './back.button';



@NgModule({
  declarations: [
    CarFilterComponent,
    DateFilterComponent,
    BackButtonComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    APIServiceModule
  ],
  exports: [
    CarFilterComponent, DateFilterComponent, BackButtonComponent
  ],
  providers:[DatePipe]
})
export class ComponentsModule { }
