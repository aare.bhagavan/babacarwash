import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ProfileService } from 'src/app/api/profile.service';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

export const DATE_FORMAT = "YYYY-MM-DD";

@Component({
  selector: 'bc-date-filter',
  templateUrl: './date-filter.component.html',
  styleUrls: ['./date-filter.component.scss'],
})
export class DateFilterComponent implements OnInit {

  // searchControl = new FormControl();
  // searchvalue: string;
  originalResults = [];
  filteredList = [];
  selectedDate = null;
  readonly CHIP_FROM_DATE = "fromdate";
  readonly CHIP_TO_DATE = "todate";
  readonly TODAY = "today";
  readonly THIS_WEEK = "this_week";
  readonly THIS_MONTH = "this_month";
  readonly LAST_WEEK = "last_week";
  readonly LAST_MONTH = "last_month";
  readonly FROM_DATE = "from_date";
  readonly TO_DATE = "to_date";
  customFromDate;
  customToDate;
  selectedChip: string;
  color: any;  
  todayDate: Date = new Date();
  finalTodayDate:string;


  @Output() onDateSelected = new EventEmitter();
  @Output() onFilterCleared = new EventEmitter();

  constructor(private profileService: ProfileService,private datePipe: DatePipe) { 
    // this.selectedChip = this.TODAY;
    console.log("selectedChip",this.selectedChip);
  }
  onChipSelected(selectedChip) {
    this.selectedChip = selectedChip;
    console.log("--selectedChip",this.selectedChip);
    const dateRange = this.getDateRangeFromChip(selectedChip);
    this.onDateSelected.emit(dateRange);
  }

  onCustomDateSelected($event, selectedChip) {
    this.selectedChip = null;
    let date = $event?.detail?.value;
    if(date && date != "") {
      date = moment(date).format(DATE_FORMAT);
    } else {
      date = undefined;
    }
    
    if(selectedChip == this.CHIP_FROM_DATE)  {
      this.customFromDate = date;
    } else {
      this.customToDate = date;
    }
    
    if(this.customFromDate && this.customToDate) {
      this.onDateSelected.emit({
        startDate: this.customFromDate,
        endDate: this.customToDate
      });
    }
  }

  async ngOnInit() {
    console.log("date filter compo");
    // this.searchControl.valueChanges.subscribe((value) => this.filterResults(value));
  }

  getDateRangeFromChip(selectedChip) {
    const dateRange = {
      startDate: moment(),
      endDate: moment()
    };
    const MOMENT_WEEK = "week";
    const MOMENT_MONTH = "month";
    switch(selectedChip) {
      case this.THIS_WEEK: {
        dateRange.startDate = moment().clone().startOf(MOMENT_WEEK);
        break;
      }
      case this.LAST_WEEK: {
        const lastWeek = moment().subtract(1, 'weeks');
        dateRange.startDate = lastWeek.clone().startOf(MOMENT_WEEK);
        dateRange.endDate = lastWeek.clone().endOf(MOMENT_WEEK);
        break;
      }
      case this.THIS_MONTH: {
        dateRange.startDate = moment().clone().startOf(MOMENT_MONTH);
        dateRange.endDate = moment().clone().endOf(MOMENT_MONTH);
        break;
      }
      case this.LAST_MONTH: {
        const lastMonth = moment().subtract(1, 'months');
        dateRange.startDate = lastMonth.clone().startOf(MOMENT_MONTH);
        dateRange.endDate = lastMonth.clone().endOf(MOMENT_MONTH);
        break;
      }
    }
    return {
      startDate: moment(dateRange.startDate).format(DATE_FORMAT),
      endDate: moment(dateRange.endDate).format(DATE_FORMAT)
    }    
  }

  onDateItemClicked(car) {
    if(this.selectedDate == car.vehicle_id) {
      return;
    }
    this.selectedDate = car.vehicle_id;
    this.onDateSelected.emit(car);
  }
  onClearButtonClicked($event) {
    this.selectedDate = null;
    this.customFromDate = null;
    this.customToDate = null;
    $event.stopPropagation();
    this.onFilterCleared.emit();
  }

}
