import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
    selector: 'button-back',
    template: `
        <ion-icon class="back" name="arrow-back" (click)="goBack()"></ion-icon>
    `,
    styles: [`
        .back {
            padding: 16px;
            color: white;
            font-size: 22px;    
        }
    `]
  })


export class BackButtonComponent {

  constructor(
    private platform: Platform, 
    public navCtrl: NavController, 
    private router: Router
  ) {

  }
  goBack() {
    this.navCtrl.back();
  }
}
