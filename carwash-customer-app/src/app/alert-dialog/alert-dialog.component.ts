import { EvAlertController } from './../alert.controller';
import { Component, OnInit, Input } from '@angular/core';
// import { EvAlertController } from '../alert.controller';
import { Platform, IonRouterOutlet, AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-alert-dialog',
  templateUrl: './alert-dialog.component.html',
  styleUrls: ['./alert-dialog.component.scss'],
})
export class AlertDialogComponent implements OnInit {

  @Input() header: any;
  @Input() message: any;
  @Input() icon: any;
  buttonvalue: string = '';
  otpvalue: string;
  otpno = false;
  constructor(
    private platform: Platform, public modalController: ModalController, public navCtrl: NavController
  ) {
    this.backButtonEvent();
  }


  ngOnInit() {
  }

  async closeAlert() {
    try {
      const element = await this.modalController.getTop();
      if (element) {
        element.dismiss().then(() => {
        });
        return;
      }
    } catch (error) {
    }
  }

  async backButtonEvent() {
    this.platform.backButton.subscribe(async () => {
      try {
        const element = await this.modalController.getTop();
        if (element) {
          element.dismiss().then(() => {
          });
          return;
        }
      } catch (error) {
      }
    });
  }
}
