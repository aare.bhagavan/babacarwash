import { Component } from '@angular/core';
import { Router, NavigationExtras ,ActivatedRoute} from '@angular/router';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, IonSlides } from '@ionic/angular';
import { AuthService } from '../api/auth.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  ProfileForm: FormGroup;
  reqform: string = 'forgotpwdform';
  mainheading: string = '';
  subheading: string = '';
  mobile:any;
  imgstatus:any=false;
  img1status:any=false;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    private fb: FormBuilder,
    public authService: AuthService,
    private router: ActivatedRoute,
    public route: Router
    ) {

      this.router.queryParams.subscribe(params => {
        console.log("forgot pwd params", params);
        this.mobile=params.form
        console.log(this.mobile);
        if (params.reqform != null) {
          this.reqform = params.reqform;
          if(this.reqform == 'forgotpwdform'){
            this.imgstatus="true";
            this.mainheading = 'Forgot Password';
            this.subheading = 'Enter your registered Mobile Number, You will get an OTP on your registered mobile number';
          }else{
            this.img1status="true";
            this.mainheading = 'Sign-Up';
            this.subheading = 'Enter your Mobile Number to be registered, You will get an OTP on your registered mobile number';
          }
  
          console.log("mainheading",this.mainheading,"subheading",this.subheading);
        }
      });

    }

    ngOnInit() {
      this.ProfileForm = this.fb.group({
        'phonenumber': ['', [Validators.required]]
      });
    }

    login(){
      // this.route.navigate(['/../login'], {});
      this.navCtrl.navigateBack('/login');
    }

    async submitPhoneNumber() {

      if (this.reqform == 'forgotpwdform') {
  
        (await this.authService.generateOtp(this.ProfileForm.value.phonenumber,this.reqform)).subscribe(async (res: any) => {
          console.log("res", res, "reqpage", this.reqform);
          if (res.status == true) {
            const alert = await this.toastCtrl.create({
              message: res.otp + ' - OTP sent to Registered Mobile Number',
              duration: 5000
            });
            await alert.present();
            const navigationExtras: NavigationExtras = {
              queryParams: {
                otp: res.otp,
                phone: this.ProfileForm.value.phonenumber,
                reqform: this.reqform
              }
            };
  
            setTimeout(() => alert.dismiss(), 5000);
            this.route.navigate(['/tabs/tab3'], navigationExtras);
  
          } else {
            const alert = await this.alertCtrl.create({
              header: 'Message',
              message: res.message,
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 5000);
          }
        });
      } else if (this.reqform == 'signupform') {
        (await this.authService.verifyUser(this.ProfileForm.value.phonenumber, this.reqform, false)).subscribe(async (res: any) => {
          console.log("res", res, "reqpage", this.reqform);
          if (res.status == true) {
            const alert = await this.toastCtrl.create({
              message: res.data.otp + ' - OTP sent to Registered Mobile Number',
            });
            await alert.present();
            const navigationExtras: NavigationExtras = {
              queryParams: {
                otp: res.data.otp,
                phone: this.ProfileForm.value.phonenumber,
                reqform: this.reqform
              }
            };
  
            setTimeout(() => alert.dismiss(), 5000);
            this.navCtrl.navigateForward(['/submitotp'], navigationExtras);
  
          } else {
            const alert = await this.alertCtrl.create({
              header: 'Message',
              message: res.message,
            });
            await alert.present();
            setTimeout(() => alert.dismiss(), 5000);
            this.navCtrl.navigateBack('/login');
          }
        });
      }
  
    }
  
    ionViewWillLeave() {
      this.ProfileForm.reset();
    }

}
