import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerDetailsEntryPageRoutingModule } from './customer-details-entry-routing.module';

import { CustomerDetailsEntryPage } from './customer-details-entry.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    CustomerDetailsEntryPageRoutingModule,ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [CustomerDetailsEntryPage]
})
export class CustomerDetailsEntryPageModule {}
