import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, MaxLengthValidator, Validators } from '@angular/forms';
import { Router, NavigationExtras ,ActivatedRoute} from '@angular/router';
import { AuthService } from '../api/auth.service';
import Swal from 'sweetalert2';
import { StorageService } from '../storage.service';
import { DashboardService } from '../api/dashboard.service';

const ENQUIRY_TYPES = {
  NEW_ACCOUNT: 'NEW_ACCOUNT',
  NEW_CAR_ENQUIRY: 'NEW_CAR_ENQUIRY',
  COMPLAINTS: 'COMPLAINTS'
};
@Component({
  selector: 'app-customer-details-entry',
  templateUrl: './customer-details-entry.page.html',
  styleUrls: ['./customer-details-entry.page.scss'],
})
export class CustomerDetailsEntryPage implements OnInit {

  customerDetailsForm: FormGroup;submitted = false;planselected:string;
  existingCustomer = false;
  customerId;

  constructor(private _formBuilder: FormBuilder,
    private router: Router,  private router1: ActivatedRoute, 
    public authService: AuthService,
    private dashboardService: DashboardService,
    private storage: StorageService) { 
      this.router1.queryParams.subscribe(params => {
        console.log("paras",params);
          this.planselected = params.selectedPlan
          console.log(this.planselected);  
          if(params.existing) {
            this.customerId = params.customerId;
            this.getCustomerData(params.customerId);
          }
      });
    }

  ngOnInit() {
    this.customerDetailsForm = this._formBuilder.group({
      mobnumber: ['', [Validators.required]],
      email: ['', [Validators.required,]],
      premises: [''],
      address: [''],
      name: ['', Validators.required],
      comments: ['']
    });
  }


  async getCustomerData(customerId) {
    (await this.dashboardService.getCustomerDetails(customerId))
      .subscribe((res: any) => {
        if (res.status) {
          const profile = res.result[0];
          const controls = this.customerDetailsForm.controls;
          if(profile) {
            this.existingCustomer = true;
            controls.mobnumber.setValue(profile.mobile);
            controls.email.setValue(profile.email);
            controls.address.setValue(profile.flat_no);
            controls.name.setValue(profile.first_name);  
          }
        }
      })
      
  }
  async send() {
    this.submitted = true;
    
    console.log(this.customerDetailsForm);
    if (this.customerDetailsForm.valid) {
      const enquiryType = this.existingCustomer? ENQUIRY_TYPES.NEW_CAR_ENQUIRY: ENQUIRY_TYPES.NEW_ACCOUNT;
      const controls = this.customerDetailsForm.controls;
      // const mobileNumber = this.loginForm.value.mobnumber;
      // const password = this.loginForm.value.password;
      const requestData = {
        "plan": this.planselected,
        "phone": controls['mobnumber'].value.toString(),
        "email": controls['email'].value.toString(),
        "premises": controls['premises'].value.toString(),
        "address": controls['address'].value,
        "name": controls['name'].value,
        "comments": controls['comments'].value,
        "enquiryType": enquiryType,
        "customerId": this.customerId
      };
      (await this.authService.customerAppRegistration(requestData)).subscribe((res: any) => {
          console.log(res);
          if (res.affectedRows == 1) {
            Swal.fire({
              icon: 'success',
              width: 250,
              padding: '4px',
              html: '<h3>Details submitted succesfully, our customer care will contact you shortly.</h3>',
            });

            const navigationExtras: NavigationExtras = {
              queryParams: {
              }
            };

            const redirectTo = this.existingCustomer? "/tabs/profile": "/";
            this.router.navigate([redirectTo], navigationExtras);


          } else {
              Swal.fire({
                icon: 'error',  
                width: 250,
                padding: '4px',
                html: '<h3>Something Went Wrong</h3>',
              })
          }

        });
    }
  }
  public focusInput(event): void {
    let total = 0;
    let container = null;

    const _rec = (obj) => {

      total += obj.offsetTop;
      const par = obj.offsetParent;
      if (par && par.localName !== 'ion-content') {
        _rec(par);
      } else {
        container = par;
      }
    };
    _rec(event.target);
    setTimeout(() => {
      container.scrollToPoint(0, total - 50, 400);
    }, 500);
  }

}
