import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerDetailsEntryPage } from './customer-details-entry.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerDetailsEntryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerDetailsEntryPageRoutingModule {}
