import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class StorageService {
    static readonly KEY_TOKEN = "token";
    static readonly KEY_LOGGED_IN = "logged_in";
    static readonly KEY_USER_ID = "user_id";
    static readonly KEY_CONTACT_DETAILS = "contactDetails";
    private storage = localStorage;

    getToken() {
        return this.storage.getItem(StorageService.KEY_TOKEN);
    }
    isLoggedIn() {
        return this.storage.getItem(StorageService.KEY_LOGGED_IN);
    }
    setToken(token) {
        this.storage.setItem(StorageService.KEY_TOKEN, token);
        this.storage.setItem(StorageService.KEY_LOGGED_IN, "true");
    }
    getUserId() {
        return this.storage.getItem(StorageService.KEY_USER_ID);
    }
    setUserId(userId) {
        this.storage.setItem(StorageService.KEY_USER_ID, userId);
    }
    getContactDetails() {
        const data = this.storage.getItem(StorageService.KEY_CONTACT_DETAILS);
        try {
            return JSON.parse(data);
        }
        catch(e) {
            return null;
        }
    }
    setContactDetails(data) {
        this.storage.setItem(StorageService.KEY_CONTACT_DETAILS, JSON.stringify(data));
    }
    removeUserData() {
        this.storage.setItem(StorageService.KEY_USER_ID, undefined);
        this.storage.setItem(StorageService.KEY_LOGGED_IN, undefined);
        this.storage.setItem(StorageService.KEY_TOKEN, undefined);
    }
}