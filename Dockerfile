FROM 16-alpine3.11
ENV APP_PATH=`pwd`
ENV SERVER_PATH="$APP_PATH/server"
ENV CLIENT_APP_PATH="$APP_PATH/admin"

RUN npm install --prefix $SERVER_PATH && npm run build:prod --prefix $SERVER_PATH
RUN cp $SERVER_PATH/package.json $SERVER_PATH/dist/
RUN rsync -avz --exclude 'dbdump' --exclude 'client' --exclude 'node_modules' --exclude 'build' --exclude 'logs' -r $SERVER_PATH/dist/* /app
RUN npm install --prefix /app

RUN && npm install --prefix $CLIENT_APP_PATH && npm run build:prod --prefix $CLIENT_APP_PATH
RUN rsync -avz -r $CLIENT_APP_PATH/dist/* -e "ssh -i $PRIVATE_KEY" /app/public
RUN cd /app && npm run pm2:start:prod

EXPOSE 3000
