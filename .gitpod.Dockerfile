FROM gitpod/workspace-mysql
USER gitpod

# Export environment variables
ENV DATABASE_USER=root
ENV DATABASE_HOST=0.0.0.0
ENV DATABASE_PASSWORD=password
