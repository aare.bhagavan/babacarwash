env=$1
echo $env
APP_PATH=`pwd`
HOST="root@139.59.90.96" #Dev
PRIVATE_KEY="$APP_PATH/carwash.pem"
SERVER_PATH="$APP_PATH/server"
CLIENT_APP_PATH="$APP_PATH/admin"


now=$(date +'%Y%m%d%H%M%S')
ssh -i $PRIVATE_KEY $HOST "mysqldump -u carwash_user -p carwash_db > ~/backups/carwash_db_$now.sql"
echo 'dump completed.. Downloading now..'
scp -i $PRIVATE_KEY $HOST:~/backups/carwash_db_$now.sql ./dbdump
echo 'Downloaded to ./dbdump directory'



# mysql -u carwash_user -p carwash_db < ./dbdump/carwash_db_$now.sql
