APP_PATH=`pwd`
docker rm --force gitlab-runner
echo "$APP_PATH/gitlab-ci/"
docker run -d --name gitlab-runner \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v $APP_PATH/:/app \
    --privileged=true \
    --restart=always \
    gitlab/gitlab-runner:latest
docker exec -t gitlab-runner apt update
docker exec -t gitlab-runner apt -y install apt-utils docker.io docker

docker exec -t gitlab-runner gitlab-runner register \
        --non-interactive \
        --url 'https://gitlab.com/' \
        --registration-token 'n_JHBuMTYAk1HWT4j7V8' \
        --executor 'docker' \
        --docker-image docker:stable \
        --description 'docker-runner' \
        --tag-list 'docker,node' \
        --run-untagged='true' \
        --locked='false' 

docker exec -t gitlab-runner docker login -u username -p password registry.gitlab.com
