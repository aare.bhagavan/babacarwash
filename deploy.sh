env=$1
echo $env
APP_PATH=`pwd`
# HOST="root@139.59.90.96" #Dev
# PRIVATE_KEY="$APP_PATH/carwash.pem"

HOST="mittu_thefire@34.123.206.250" #Google
PRIVATE_KEY="$APP_PATH/gcloud-cw.pem" # Google

SERVER_PATH="$APP_PATH/server"
CLIENT_APP_PATH="$APP_PATH/admin"


##Backend deployment
cd $SERVER_PATH && npm install && npm run build:prod
cp $SERVER_PATH/package.json $SERVER_PATH/dist/
rsync -avz --exclude 'dbdump' --exclude 'client' --exclude 'node_modules' --exclude 'build' --exclude 'logs' -r $SERVER_PATH/dist/* -e "ssh -i $PRIVATE_KEY" $HOST:~/carwash
ssh -i $PRIVATE_KEY $HOST 'cd ~/carwash && npm install'

##client deployment
# cd $CLIENT_APP_PATH && npm install && npm run build:prod
# rsync -avz -r $CLIENT_APP_PATH/dist/* -e "ssh -i $PRIVATE_KEY" $HOST:~/carwash/public




ssh -i $PRIVATE_KEY $HOST 'pm2 delete all'
ssh -i $PRIVATE_KEY $HOST 'cd ~/carwash && pm2 start "npm run start:prod" --name carwash'


